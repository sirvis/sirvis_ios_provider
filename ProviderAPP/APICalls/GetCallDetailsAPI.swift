//
//  GetCallDetailsAPI.swift
//  LSP
//
//  Created by Vengababu Maparthi on 06/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire
import CocoaLumberjack


class CallDetailsAPI {
    let disposebag = DisposeBag()
    let Notification_response = PublishSubject<APIResponseModel>()
    
    
    /// Method to call get configuration details Service API
    func getTheCallDetails(){
        
        let strURL = API.BASE_URL + "provider/callTypeSetting"
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.alertVC(errMSG: error.localizedDescription)
                
                
            }).disposed(by: disposebag)
        
        
    }
    
    /// Method to call get configuration details Service API
    func updatetheCallData(params:[String:Any]){
        
        let strURL = API.BASE_URL + "provider/callTypeSetting"
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:params,
                         encoding:JSONEncoding.default,
                         headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.alertVC(errMSG: error.localizedDescription)
                
                
            }).disposed(by: disposebag)
        
    }
    
    /// Method to parse Service API Response
    ///
    
    
    
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
        DDLogVerbose(" Response: \(responseDict)");
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: statusCode)!
        let apiCall = APILibrary()
        Helper.hidePI()
        switch responseCodes {
        case .BadRequest:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .InternalServerError:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .TokenExpired:
            
            let defaults = UserDefaults.standard
            if let sessionToken =  responseDict["data"]   as? String  {
                defaults.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                defaults.synchronize()
                apiCall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{
                        
                    }
                })
            }
            break
            
        case .UserLoggedOut:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
            
        case .profileReject:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
        case .adminNotAccepted:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.Notification_response.onNext(responseModel)
            break
        }
    }
}
