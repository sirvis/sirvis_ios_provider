//
//  BookingFlowApiExt.swift
//  LSP
//
//  Created by Rajan Singh on 05/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire
import CocoaLumberjack

extension BookingFlowApiModel {
    
    func networkRequestApiCall(serviceName: String, requestType: Alamofire.HTTPMethod, parameters: [String : Any]?,headerParam:HTTPHeaders?, responseType: RequestType) {
        
        let mainUrl = API.BASE_URL + serviceName
        
        RxAlamofire
            .requestJSON(requestType, mainUrl ,parameters:parameters, encoding: JSONEncoding.default, headers: headerParam)
            .debug()
            .subscribe(onNext: { (r, json) in
                if  var dict  = json as? [String:Any]{
                    var statuscode:Int = r.statusCode
                    if dict["code"] != nil{ statuscode = (dict["code"] as? Int)!}
                    dict["resposeType"] = responseType
                    GenericUtilityMethods.hidePI()
                    self.ParseAPIResponse(response: dict, statusCode: statuscode, methodName: requestType.rawValue, responseType: responseType, completion: { (response) in
                        self.subject_response.onNext(response)
                    }, failure: { (error) in
                        self.showErrorMessage(error: error)
                    })
                    
                }
            }, onError: {  (error) in
                self.subject_response.onError(error)
               // GenericUtilityMethods.showAlert(alert: ALERT.message, message: error.localizedDescription)
            })
            .disposed(by: disposeBag)
        
        
    }
    
    /// error message on failure of the api call
    ///
    /// - Parameter error: error details
    func showErrorMessage(error:Error) {
        
        GenericUtilityMethods.hidePI()
        DDLogDebug("Error:\(error.localizedDescription)")
        GenericUtilityMethods.showAlert(alert: "ALERT", message: error.localizedDescription)
    }
    
    
    /// Parse data To Response which contains all the data, status Code , RequestType
    ///
    /// - Parameters:
    ///   - response: data in [String : Any] Format
    ///   - statusCode: status Code
    ///   - methodName: Api method name
    ///   - responseType: type of request
    ///   - completion: On sucessfull call Of the Api
    ///   - failure: returns an error when the api fails to get called
    func ParseAPIResponse(response:[String : Any],
                          statusCode:Int,
                          methodName:String,
                          responseType: RequestType,
                          completion: @escaping(APIResponseModel1) -> Void,
                          failure:@escaping (Error) -> Void) {
        
        DDLogDebug("\n\n\(methodName) Response : \(response.description)\n\n")
        
        if statusCode > 0 {
            
            switch statusCode {
                
            case HTTPSCommonErrorCodes.BadRequest.rawValue:
                
               break
            case HTTPSCommonErrorCodes.InternalServerError.rawValue:
              break
                
            default:
                
                let responseModel:APIResponseModel1!
                
                responseModel = APIResponseModel1.init(statusCode: statusCode, dataResponse: response, responseType: responseType)
                
                completion(responseModel)
                
            }
            
        }
        else {
            var error:NSError = NSError(domain: "", code: -60, userInfo: [:])
            let message = "ALERT"
            let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey: NSLocalizedString(message, comment: message),
                                                NSLocalizedFailureReasonErrorKey: NSLocalizedString(message, comment: message)]
            error = NSError(domain: "", code: -60, userInfo: userInfo as! [String : Any])
            
            failure(error)
            
        }
        
    }
    
    /// common errors
    ///
    /// - Parameter response: response
    /// - Returns: Error
    func getCommonErrorDetails(response:[String : Any]) -> Error {
        
        var error:NSError = NSError(domain: "", code: -60, userInfo: [:])
        
        if let message = response["message"] as? String {
            
            let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey       : NSLocalizedString(message, comment: message),
                                                NSLocalizedFailureReasonErrorKey: NSLocalizedString(message, comment: message)]
            error = NSError(domain: "", code: -60, userInfo: userInfo as! [String : Any])
            
        }
        return error
    }
    
}
