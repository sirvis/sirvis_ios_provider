//
//  BookingFlowApiModel.swift
//  LSP
//
//  Created by Rajan Singh on 05/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire
import CocoaLumberjack


class BookingFlowApiModel: NSObject {
    
   
    let disposeBag = DisposeBag()
    let subject_response = PublishSubject<APIResponseModel1>()
    
   
 
    /// send Request To get invoice Details
    ///
    /// - Parameter bookingId: booking id
    func sendRequestForInvoiceDetail(bookingId: String) {
        let params: [String : Any] = [:]
        
        Helper.showPI(message: "Looding....")
        
        var header:HTTPHeaders!
        
        header = RXNetworkHelper.getAOTHHeader()
        
        let serviceName = "provider/booking/" + "\(bookingId)"
        
        networkRequestApiCall(serviceName: serviceName,
                              requestType: .get,
                              parameters: nil,
                              headerParam: header,
                              responseType: RequestType.invoice)
    }
    
    
  
    

    

}
