//
//  ScheduleBookingModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 10/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation




class ScheduleBookModel: NSObject {
    
    var bookingsData:CompletedBookings!
    
    /// parse the schedule booking data for to show schedule data
    ///
    /// - Parameter acceptData: schedule booking details having the
    /// - Returns: returns the schedule model data
    func parsingTheServiceResponse(acceptData:[String: Any]) -> ScheduleBookModel{
        let bookingData = ScheduleBookModel()
        bookingData.bookingsData = CompletedBookings.init(acceptData: acceptData)
        return bookingData
    }
}

