//
//  ScheduleDataModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 01/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
class ScheduleDataMod : NSObject{
    var repeatDays: [String] = ["Everyday","WeekDays","WeekEnd","SelectDays"]
    var everyday: [String] = ["Sunday","Monday","Tuesday","Wednessday","Thursday","Friday","Saturday"]
    var weekDays: [String] = ["Monday","Tuesday","Wednessday","Thursday","Friday"]
    var weekEnd: [String] = ["Sunday","Saturday"]
    var duration:[String] = ["This Month","2 Months","3 Months","4 Months","CUSTOM","Start date",
                             "End date"]
    
    var callDuration:[[String:String]] = [["key":"10 Mins","val":"10"],["key":"20 Mins","val":"20"],["key":"30 Mins","val":"30"],["key":"45 Mins","val":"45"],["key":"1 Hr","val":"60"],["key":"1 Hr 15 Mins","val":"75"],["key":"1 Hr 30 Mins","val":"90"]]
}
