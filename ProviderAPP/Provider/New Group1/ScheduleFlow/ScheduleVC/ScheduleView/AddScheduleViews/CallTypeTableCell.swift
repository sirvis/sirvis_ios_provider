
//
//  CallTypeTableCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 05/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class CallTypeTableCell: UITableViewCell {
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var keyValue: UILabel!
    @IBOutlet weak var arrowImg: UIImageView!
    
    @IBOutlet weak var value: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
