//
//  ScheduleTableViewCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 07/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {

    @IBOutlet weak var amORPmLabel: UILabel!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var eventNameAndCustomerName: UILabel!
    @IBOutlet weak var scheduledTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
