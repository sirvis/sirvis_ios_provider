//
//  SelectTimeVC.swift
//  LSP
//
//  Created by Vengababu Maparthi on 05/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import RxSwift

class SelectTimeVC: UIViewController {
    @IBOutlet weak var selecTimeTableView: UITableView!
    var scheduleData = ScheduleDataMod()
    var indexSelected = -1
    let successIndex = PublishSubject<(Int,Bool)>()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backToVc(_ sender: Any) {
        self.successIndex.onNext((indexSelected,false))
        self.navigationController?.popViewController(animated: true)
    }
}

extension SelectTimeVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.successIndex.onNext((indexPath.row,true))
        tableView.deselectRow(at: indexPath, animated: true)
           self.navigationController?.popViewController(animated: true)
    }
}

extension SelectTimeVC:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return scheduleData.callDuration.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeekDaysTableCell", for: indexPath) as! WeekDaysTableCell
        if indexSelected == indexPath.row{
            cell.selectButton.isSelected = true
        }else{
            cell.selectButton.isSelected = false
        }
        cell.updateTheCell(dict:scheduleData.callDuration[indexPath.row])
        return cell
    }
}
