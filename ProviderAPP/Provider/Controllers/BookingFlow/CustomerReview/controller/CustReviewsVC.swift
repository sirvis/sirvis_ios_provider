//
//  CustReviewsVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 05/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import FloatRatingView
import Kingfisher

class CustReviewsVC: UIViewController {

    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var custRating: FloatRatingView!
    @IBOutlet weak var custName: UILabel!
    @IBOutlet weak var custImage: UIImageView!
    @IBOutlet weak var custReviewTableView: UITableView!
    var custReviewData = CustReviewModel()
    var value = 0
    var refreshControl = UIRefreshControl()
    var customerID = ""
    var custPic = ""
    var cusName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshTableView()
        
        custReviewTableView.estimatedRowHeight = 10
        custReviewTableView.rowHeight = UITableView.automaticDimension
        
        self.getTheReview(indeVal: "0")
        custName.text = cusName
        let imageURL = custPic
        custImage.kf.setImage(with: URL(string: imageURL),
                                 placeholder:UIImage.init(named: "signup_profile_default_image"),
                                 options: [.transition(ImageTransition.fade(1))],
                                 progressBlock: { receivedSize, totalSize in
        },
                                 completionHandler: { image, error, cacheType, imageURL in
        })
        
        
        // Do any additional setup after loading the view.
    }
    
    func refreshTableView() {
        refreshControl = UIRefreshControl()
        refreshControl.triggerVerticalOffset = 100.0
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        self.refreshControl.beginRefreshing()
        self.custReviewTableView.bottomRefreshControl = refreshControl
    }
    
    @objc func refresh() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.value += 1
            self.getTheReview(indeVal: String(describing:self.value))
        }
    }

    func getTheReview(indeVal:String) {
        
        custReviewData.getTheReviewByIndexWise(custData: indeVal + "/" + customerID) { (succeeded, reviewData) in
            self.custReviewData = reviewData
            self.custReviewTableView.reloadData()
            self.custReviewTableView.bottomRefreshControl?.endRefreshing()
        }
    }
    
    @IBAction func backToVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///This function calculates the height of content Label  ListingCell row's and returns the height of the label.
    func heightForView(text: String, width: CGFloat) -> CGFloat {
        
        let label: UILabel = UILabel(frame: CGRect(x: 0,
                                                   y: 0,
                                                   width: width,
                                                   height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }

}

extension CustReviewsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        if custReviewData.reviewInnerData.count == 0{
            emptyLabel.isHidden = false
            emptyImage.isHidden = false
        }else{
            emptyLabel.isHidden = true
            emptyImage.isHidden = true
        }
        return custReviewData.reviewInnerData.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let about = tableView.dequeueReusableCell(withIdentifier: "custAboutMe", for: indexPath) as! aboutCustTableCell
            about.aboutLabel.text = self.custReviewData.aboutMe
            return about
        }else{
            let review = tableView.dequeueReusableCell(withIdentifier: "custReviews", for: indexPath) as! ReviewCell
            review.updateCustReviewData(reviewData:self.custReviewData.reviewInnerData[indexPath.row-1])
            return review
        }
        
    }
}

extension CustReviewsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            var width: CGFloat = 0.0
            var height: CGFloat = 0.0
            let cell = tableView.dequeueReusableCell(withIdentifier: "custAboutMe") as! aboutCustTableCell
            width = cell.contentView.frame.width
            width = width - 40.0
            if self.custReviewData.aboutMe.length > 0{
                height =  heightForView(text: self.custReviewData.aboutMe , width: width)
                height = height + 20.0
            }else{
                height = 0
            }
            return height
        }else{
            var width: CGFloat = 0.0
            var height: CGFloat = 0.0
            let cell = tableView.dequeueReusableCell(withIdentifier: "custReviews") as! ReviewCell
            width = cell.contentView.frame.width
            width = width - 40.0
            height =  heightForView(text: self.custReviewData.reviewInnerData[indexPath.row-1].reviewText , width: width)
            height = height + 50.0
            return height
        }
    }
}

