//
//  QuestionInnerCell.swift
//  LSP
//
//  Created by Rahul Sharma on 6/18/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class QuestionInnerCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var answerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var identifier: String {
        return String(describing: self)
    }


}
