//
//  QuestionOuterCell.swift
//  LSP
//
//  Created by Rahul Sharma on 6/18/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class QuestionOuterCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var questionsTable: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        questionsTable.delegate = self
        questionsTable.dataSource = self
        
        questionsTable.estimatedRowHeight = 80
        questionsTable.rowHeight = UITableView.automaticDimension
        questionsTable.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionInnerCell") as! QuestionInnerCell
        cell.questionLabel.text = "What do you want"
        cell.answerLabel.text = "cleaning service"
        return cell
    }
    

}
