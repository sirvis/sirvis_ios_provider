//
//  UploadImageCollectionCell.swift
//  LSP
//
//  Created by Rahul Sharma on 2/19/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class UploadImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var uploadImageView: UIImageView!
}
