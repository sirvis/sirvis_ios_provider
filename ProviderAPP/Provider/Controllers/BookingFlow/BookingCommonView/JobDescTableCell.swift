//
//  JobDescTableCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 10/04/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
//jobDesc
class JobDescTableCell: UITableViewCell {

    @IBOutlet weak var jobDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
