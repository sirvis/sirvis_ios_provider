//
//  NotesCell.swift
//  LSP
//
//  Created by Rahul Sharma on 2/6/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class NotesCell: UITableViewCell {

    @IBOutlet weak var notesTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //notesTextView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//extension NotesCell: UITextViewDelegate {
//    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
//
//        return true
//    }
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        textView.text = ""
//    }
//    func textViewDidEndEditing(_ textView: UITextView) {
//
//    }
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if (text == "\n") {
//            textView.resignFirstResponder()
//            return false
//        }
//        return true
//    }
//}
