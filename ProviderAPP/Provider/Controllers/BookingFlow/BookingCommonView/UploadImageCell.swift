//
//  UploadImageCell.swift
//  LSP
//
//  Created by Rahul Sharma on 2/6/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

protocol UploadImageDelegate {
    func didTapUploadImage()
}

class UploadImageCell: UITableViewCell {
    @IBOutlet weak var uploadedimageView: UIImageView!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var delegate: UploadImageDelegate?
    var imageUrls =  [String]()
    var pickedImages =  [UIImage]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
    

}

extension UploadImageCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pickedImages.count < 5 ? (pickedImages.count + 1) : pickedImages.count
      
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if pickedImages.count < 5 {
            if indexPath.item == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadImageCollectionCell", for: indexPath) as! UploadImageCollectionCell
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemPhotoCollectionCell", for: indexPath) as! ItemPhotoCollectionCell
                if pickedImages.count != 0 {
                    cell.pickedImageView.image = pickedImages[indexPath.row - 1]
                }
                
                return cell
            }
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemPhotoCollectionCell", for: indexPath) as! ItemPhotoCollectionCell
            if pickedImages.count != 0 {
                cell.pickedImageView.image = pickedImages[indexPath.row]
            }
            return cell
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.item == 0 else {
            return
        }
        
        guard pickedImages.count < 5 else {
            return
        }
        
        delegate?.didTapUploadImage()
    }
    
}
