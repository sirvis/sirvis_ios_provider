//
//  JobPhotosTableCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 10/04/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
//jobPhotos
class JobPhotosTableCell: UITableViewCell {

    @IBOutlet weak var jobPhotosCollectionView: UICollectionView!
    var photoUrls = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //setupCell()
    }

    
    func setupCell() {
        jobPhotosCollectionView.delegate = self
        jobPhotosCollectionView.dataSource = self
        jobPhotosCollectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension JobPhotosTableCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoUrls.count
    }
  
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JobImageCell", for: indexPath) as?  JobImageCell
        let imageURL = URL(string: photoUrls[indexPath.item])
        cell!.jobImageview.kf.setImage(with: imageURL!,
                                      placeholder:UIImage.init(named: "history_gallery_icon"),
                                      options: [.transition(ImageTransition.fade(1))],
                                      progressBlock: { receivedSize, totalSize in
        },
                                      completionHandler: { image, error, cacheType, imageURL in
        })
            return cell!
    }
    
    
}

