//
//  HeaderTableCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 08/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class HeaderTableCell: UITableViewCell {

    @IBOutlet weak var headerOfCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
