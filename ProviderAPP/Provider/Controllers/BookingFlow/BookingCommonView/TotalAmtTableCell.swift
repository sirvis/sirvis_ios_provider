//
//  TotalAmtTableCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 08/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class TotalAmtTableCell: UITableViewCell {
    @IBOutlet weak var totalAmt: UILabel!
    @IBOutlet weak var timeOfAppointment: UILabel!
    @IBOutlet weak var dateOfAppointment: UILabel!
    
    @IBOutlet weak var repeatDaysLabel: UILabel!
    @IBOutlet weak var daysLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateTheFieldsFromAcceptReject(req:Request!) {
        let startdateArray = Helper.getTheDateFromTimeStamp(timeStamp:req.actuallGigTimeStart).components(separatedBy: "|")
        self.dateOfAppointment.text = "Date: " + startdateArray[0]
        self.timeOfAppointment.text = "Time: " + startdateArray[1]
        self.totalAmt.text =  Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",req.totalAmt))
    }

    func updateTheFieldsFromOnBooking(req:Accepted!) {
        let startdateArray = Helper.getTheDateFromTimeStamp(timeStamp:req.actuallGigTimeStart).components(separatedBy: "|")
        self.dateOfAppointment.text = "Date: " + startdateArray[0]
        self.timeOfAppointment.text = "Time: " + startdateArray[1]
        self.totalAmt.text =  Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",req.totalAmt))
    }

}
