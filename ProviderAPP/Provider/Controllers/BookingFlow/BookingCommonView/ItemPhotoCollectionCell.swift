//
//  ItemPhotoCollectionCell.swift
//  LSP
//
//  Created by Rahul Sharma on 2/4/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class ItemPhotoCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var pickedImageView: UIImageView!
    
}
