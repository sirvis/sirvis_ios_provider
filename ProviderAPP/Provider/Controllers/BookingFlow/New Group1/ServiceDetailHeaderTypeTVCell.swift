//
//  ServiceDetailHeaderTypeTVCell.swift
//  LSP
//
//  Created by Rajan Singh on 01/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ServiceDetailHeaderTypeTVCell: UITableViewCell {


    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sellimageView: UIImageView!
    @IBOutlet weak var bottomLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    


}
