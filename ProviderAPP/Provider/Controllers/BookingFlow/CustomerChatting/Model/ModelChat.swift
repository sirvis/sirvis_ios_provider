//
//  ModelChat.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 19/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

class Message: NSObject {
    var owner: MessageOwner
    var type: MessageType
    var content: Any
    var timestamp: Int64 = 0
    var isRead: Bool
    
    private var toID: String?
    private var fromID: String?
    
    //MARK: Inits
    init(type: MessageType, content: Any, owner: MessageOwner, timestamp: Int64, isRead: Bool) {
        self.type = type
        self.content = content
        self.owner = owner
        self.timestamp = timestamp
        self.isRead = isRead
    }
}
