//
//  OnBookingTableViewExtension.swift
//  LSP
//
//  Created by Rahul Sharma on 12/18/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import Kingfisher

extension OnBookingViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension OnBookingViewController:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if status == BookingStatus.onTheWayToPickup.rawValue {
            return 9 //7
        }else{
            if self.updateViewButton.isSelected{
                return 9 //7
            }else{
                return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 2
        case 2:
            if activeBid {
                if bidBookingDict?.bookingType == .Repeat {
                    return 2
                }
                else {
                    return 3
                }
                
            }
            else {
                
                return 3
                
            }
            
        case 3:
            if activeBid ||  bookingDict?.bookingModel == .Bidding{
                return 0
            }
            else {
                if bookingDict?.serviceType == .Fixed{
                  return  2 + (bookingDict?.paymentServices.count)!
                } else {
                    return  2 + (bookingDict?.paymentServices.count)! + 1 // for hourly price data
                }
                
            }
        case 4:
            if activeBid ||  bookingDict?.bookingModel == .Bidding{
                return 0
            }
            return (bookingDict?.paymentServices.count)!
            
        case 5:
            
//            if activeBid ||  bookingDict?.bookingModel == .Bidding{
//                return 0
//            }
            return 1
            
        case 6:
            if activeBid {
                if (bidBookingDict?.questionAnswers.count)! > 0 {
                    return 1 + (bidBookingDict?.questionAnswers.count)!
                }
                else {
                    return 0
                }
            }
            else {
                if (bookingDict?.questionAnswers.count)! > 0 {
                    return 1 + (bookingDict?.questionAnswers.count)!
                }
                else {
                    return 0
                }
            }
            
       
            
        case 7:
            return 3
        case 8:
            if activeBid{
                if bidBookingDict?.jobDesc == "" {
                    return 0
                }
                else {
                    return 3
                }
            }
            else {
                if bookingDict?.jobDesc == "" {
                    return 0
                }
                else {
                    return 3
                }
            }
            
        default:
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderTableCell
        let mediumView = tableView.dequeueReusableCell(withIdentifier: "mediumView") as! MediumViewTableCell
        
        if activeBid {
            switch indexPath.section {
            case 0:
                
                if status == BookingStatus.onTheWayToPickup.rawValue {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddress") as! AddressBookingTableCell
                    cell.selectionStyle = .none
                    cell.custAddress.text = bidBookingDict?.addLine1
                    return cell
                }else{
                    if self.updateViewButton.isSelected{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddress") as! AddressBookingTableCell
                        cell.selectionStyle = .none
                        cell.custAddress.text = bidBookingDict?.addLine1
                        return cell
                    }else{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "custData") as! CustProfileBookingTableCell
                        cell.custName.text =  bidBookingDict?.firstName
                        cell.custAddress.text = bidBookingDict?.addLine1
                        cell.selectionStyle = .none
                        return cell
                        
                    }
                }
            case 1:
                
                switch indexPath.row{
                case 0:
                    return mediumView
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddressProfile") as! AddressProfileBookingTableCell
                    cell.selectionStyle = .none
                    cell.custName.text = bidBookingDict?.firstName
                    cell.categoryName.text =  bidBookingDict?.categoryName //"Category name"
                    cell.bookingID.text = "Job ID: ".localized +  String(describing:bidBookingDict!.bookingId)
                    return cell
                }
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "totalAmt") as! TotalAmtTableCell
                if bidBookingDict?.bookingType == .Repeat { // multiple shift
                    switch indexPath.row{
                    case 0:
                        return mediumView
                        
                    default:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "ShiftDetailCell") as! ShiftDetailCell
                        cell.setupActiveBid(data: bidBookingDict!)
                        return cell
                    }
                }
                else { // not multilple shift
                    switch indexPath.row{
                    case 0:
                        return mediumView
                        
                    case 1 :
                        header.headerOfCell.text = "Amount".localized
                        return header
                    default:
                        let dateArray = Helper.getTheDateFromTimeStamp(timeStamp:bidBookingDict!.actuallGigTimeStart).components(separatedBy: "|")
                        
                        cell.dateOfAppointment.text = "Date: ".localized + dateArray[0]
                        cell.timeOfAppointment.text = "Time: ".localized + dateArray[1]
                        let total = bidBookingDict?.totalAmt
                        cell.totalAmt.text =  Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))
                        return cell
                    }
                }
                
            case 6:
                switch indexPath.row{
                case 0:
                    return mediumView
                default:
                    
                    if bidBookingDict?.questionAnswers[indexPath.row - 1].type == 10  && bidBookingDict?.questionAnswers[indexPath.row - 1].answer != "" {
                         let urlStringArr = bidBookingDict?.questionAnswers[indexPath.row - 1].answer.components(separatedBy: ",")
                      
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "jobPhotos") as! JobPhotosTableCell
                        cell.photoUrls = urlStringArr!
                        cell.setupCell()
                        
                        return cell
                        
                    }
                    else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionInnerCell") as! QuestionInnerCell
                        cell.questionLabel.text = "Q:".localized + (bidBookingDict?.questionAnswers[indexPath.row - 1].question)!
                        cell.answerLabel.text = bidBookingDict?.questionAnswers[indexPath.row - 1].answer
                        return cell
                    }
                    
                    
                }
                
           
                
            case 7:
                switch indexPath.row{
                case 0:
                    return mediumView
                case 1:
                    header.headerOfCell.text =  "Payment Method".localized
                    return header
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "paymentType") as! PaymentMethodTableCell
                    
                    if (bidBookingDict?.paidByWallet)! {
                        cell.paymentType.text = "Wallet".localized + " + " + (bidBookingDict?.paymentMethod)!
                    }else{
                        cell.paymentType.text = bidBookingDict?.paymentMethod
                    }
                    
                    if bookingDict?.paymentMethod == "Cash" {
                        cell.paymentTypeImage.image = #imageLiteral(resourceName: "cash")
                    } else {
                        cell.paymentTypeImage.image = #imageLiteral(resourceName: "card")
                    }
                    return cell
                }
                
            case 8:
                switch indexPath.row{
                case 0:
                    return mediumView
                case 1:
                    header.headerOfCell.text =  "Job Description".localized
                    return header
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "jobDesc") as! JobDescTableCell
                    cell.jobDescription.text = bidBookingDict!.jobDesc
//                    if bidBookingDict?.jobDesc == "" {
//                        cell.jobDescription.text = "This booking does not have a description"
//                    }
//                    else {
//                        cell.jobDescription.text = bidBookingDict!.jobDesc
//                    }
                    return cell
                }
            default:
                switch indexPath.row{
                case 0:
                    return mediumView
                case 1:
                    header.headerOfCell.text =  "Job Photos".localized
                    return header
                default:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "jobPhotos") as! JobPhotosTableCell
                    return cell
                }
            }
        } else {  // non bidding - Accepted to Booking details
            switch indexPath.section {
            case 0:
                
                if status == 6 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddress") as! AddressBookingTableCell
                    cell.selectionStyle = .none
                    cell.custAddress.text = bookingDict?.addLine1
                    return cell
                }else{
                    if self.updateViewButton.isSelected{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddress") as! AddressBookingTableCell
                        cell.selectionStyle = .none
                        cell.custAddress.text = bookingDict?.addLine1
                        return cell
                    }else{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "custData") as! CustProfileBookingTableCell
                        cell.custName.text =  bookingDict?.firstName
                        cell.custAddress.text = bookingDict?.addLine1
                        cell.selectionStyle = .none
                        return cell
                        
                    }
                }
            case 1:
                
                switch indexPath.row{
                case 0:
                    return mediumView
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddressProfile") as! AddressProfileBookingTableCell
                    cell.selectionStyle = .none
                    cell.updateTheFieldswithDataFromOnBooking(req:bookingDict)
                    return cell
                }
                
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "totalAmt") as! TotalAmtTableCell
                switch indexPath.row{
                case 0:
                    return mediumView
                case 1 :
                    header.headerOfCell.text = "Amount".localized
                    return header
                default:
                    cell.updateTheFieldsFromOnBooking(req:bookingDict)
                    return cell
                }
            case 3:
                
              
                    let cell = tableView.dequeueReusableCell(withIdentifier: "bookingEvents") as! BookingEventsTableCell
                    cell.selectionStyle = .none
                    
                    switch indexPath.row{
                    case 0:
                        return mediumView
                    case 1:
                        header.headerOfCell.text =  "Selected Services".localized
                        return header
                        
                    default:
                        if bookingDict?.serviceType == .Fixed{
                            cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)! + " * " + String(describing:bookingDict!.services[indexPath.row - 2].quantity)
                            cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.services[indexPath.row - 2].servicePrice))
                        }else{
                            
                            cell.key.text = Helper.timeInHourMin((bookingDict?.totalJobTime)!)
                            cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.totalHourlyFee))
                        }
                        return cell
                    }
                
                
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "bookingEvents") as! BookingEventsTableCell
                cell.key.text = bookingDict!.paymentServices[indexPath.row].key
                cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.paymentServices[indexPath.row].value))
                return cell
                
            case 5:
                let totalCell = tableView.dequeueReusableCell(withIdentifier: "finalAmt") as! FinalAmtTableCell
                
                let total = bookingDict?.totalAmt
                
                totalCell.totalAmt.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))
                
                return totalCell
                
            case 6:
                switch indexPath.row{
                case 0:
                    return mediumView
                default:
                    
                    if bookingDict?.questionAnswers[indexPath.row - 1].type == 10  && bookingDict?.questionAnswers[indexPath.row - 1].answer != "" {
                        


                        let cell = tableView.dequeueReusableCell(withIdentifier: "jobPhotos") as! JobPhotosTableCell
                         let urlStringArr = bookingDict?.questionAnswers[indexPath.row - 1].answer.components(separatedBy: ",")
                        
                        cell.photoUrls = urlStringArr!
                        cell.setupCell()
                        return cell
                        
                    }
                    else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionInnerCell") as! QuestionInnerCell
                        cell.questionLabel.text = "Q:" + (bookingDict?.questionAnswers[indexPath.row - 1].question)!
                        cell.answerLabel.text = bookingDict?.questionAnswers[indexPath.row - 1].answer
                        return cell
                    }
                    
                }
           
                
            case 7:
                switch indexPath.row{
                case 0:
                    return mediumView
                case 1:
                    header.headerOfCell.text =  "Payment Method".localized
                    return header
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "paymentType") as! PaymentMethodTableCell
                    
                    if (bookingDict?.paidByWallet)! {
                        cell.paymentType.text = "Wallet".localized + " + " + (bookingDict?.paymentMethod)!
                    }else{
                        cell.paymentType.text = bookingDict?.paymentMethod
                    }
                    
                    if bookingDict?.paymentMethod == "Cash" {
                        cell.paymentTypeImage.image = #imageLiteral(resourceName: "cash")
                    } else {
                        cell.paymentTypeImage.image = #imageLiteral(resourceName: "card")
                    }
                    return cell
                }
                
            case 8:
                switch indexPath.row{
                case 0:
                    return mediumView
                case 1:
                    header.headerOfCell.text =  "Job Description".localized
                    return header
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "jobDesc") as! JobDescTableCell
                    
                    cell.jobDescription.text = bookingDict!.jobDesc
                    return cell
                }
            default:
                switch indexPath.row{
                case 0:
                    return mediumView
                case 1:
                    header.headerOfCell.text =  "Job Photos".localized
                    return header
                default:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "jobPhotos") as! JobPhotosTableCell
                    return cell
                }
            }
        }
    }
}
