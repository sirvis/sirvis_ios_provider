//
//  OnBookingViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 15/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import JaneSliderControl
import Kingfisher
import MapKit

//6: When the driver is on the way to pickup location
//7: when the driver arrived to pickup location
//8: when the driver started the trip after loading
//9: when the driver reached the drop location
//16: when the vehicle is unloaded



class OnBookingViewController: UIViewController,UIScrollViewDelegate {
    
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    var animateView = UIView()
    var constantView = UIView()
    let locationManager = CLLocationManager()
    var location        = CLLocation()
    
    @IBOutlet var googleMaps: UIButton!
    @IBOutlet var getCurrentLocation: UIButton!
    @IBOutlet var wazeMaps: UIButton!
    @IBOutlet weak var updateViewButton: UIButton!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet weak var leftSlider: SliderControl!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var cancelBookingButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var bookingTableView: UITableView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var updateViewContraint: NSLayoutConstraint!
    @IBOutlet weak var navigationButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var sliderTopView: UIView!
    @IBOutlet weak var myQuoteView: UIView!
    @IBOutlet weak var quotedPriceLabel: UILabel!
    
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var chatBtn: UIButton!
    
    
    let source       = GMSMarker()
    let destination  = GMSMarker()
    var movingMarker = GMSMarker()
    
    var bookingMod = BookingModel()
    var commonBookingDict: Any?
    var bookingDict:Accepted?
    var bidBookingDict: ActiveBid?
    var status:Int        = 0
    var activeBid = false
    
    var latitude:Double  = 0.00
    var longitude:Double = 0.00
    var travelledDistance:Double = 0.00
    var didFindMyLocation = false
    var isPathPlotted     = false
    var tableHeaderViewHeight = 240 as CGFloat
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    var isFromChatScreen = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView = (self.bookingTableView.tableHeaderView as! GMSMapView)
        self.bookingTableView.tableHeaderView = nil
        self.bookingTableView.addSubview(mapView)
        self.bookingTableView.contentInset = UIEdgeInsets(top: tableHeaderViewHeight,left: 0, bottom: 0, right: 0)
        self.bookingTableView.contentOffset = CGPoint(x:0,y:-tableHeaderViewHeight)
        bookingTableView.estimatedRowHeight = 10
        bookingTableView.rowHeight = UITableView.automaticDimension
        
        self.mapView.frame = CGRect(x:0, y: self.bookingTableView.contentOffset.y, width:self.bookingTableView.bounds.size.width, height: -self.bookingTableView.contentOffset.y)
        setupUI()
        
        self.bookingTableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        do {
            // Set the map style by passing the URL of the local file. Make sure style.json is present in your project
            if let styleURL = Bundle.main.url(forResource: "Style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find style.json")
            }
        } catch {
            print("The style definition could not be loaded: \(error)")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if self.timer != nil{
            self.timer.invalidate()
        }
        // movingMarker.iconView = nil
    }
    
    func  setupUI() {
        if activeBid {
            sliderTopView.isHidden = true
            cancelBookingButton.isHidden = true
            myQuoteView.isHidden = false
            quotedPriceLabel.text = Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",(bidBookingDict?.quotedAmount)!))
            chatBtn.isHidden = true
            callBtn.isHidden = true
        }
        else {
            sliderTopView.isHidden = false
            cancelBookingButton.isHidden = false
            myQuoteView.isHidden = true
            if isFromChatScreen {
                chatBtn.isHidden = true
                callBtn.isHidden = true
            }
            else {
                chatBtn.isHidden = false
                callBtn.isHidden = false
            }
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.bookingTableView.contentOffset.y < -tableHeaderViewHeight {
            self.mapView.frame = CGRect(x:0, y: self.bookingTableView.contentOffset.y, width:self.bookingTableView.bounds.size.width, height: -self.bookingTableView.contentOffset.y)
        }
    }
    
    func updateTheOnbookingUI()  {
        initiateMap()
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(true)
        self.mapView.bringSubviewToFront(self.topView)
        self.mapView.bringSubviewToFront(self.navigationView)
        self.mapView.bringSubviewToFront(self.getCurrentLocation)
        leftSlider.sliderText = ""
        titleLabel.text = "Accepted".localized
        if  activeBid {
            status = (bidBookingDict?.statusCode)!
        }
        else {
            status = (bookingDict?.statusCode)!
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude) ,longitude: CLLocationDegrees(longitude) , zoom: 16)
        self.mapView.animate(to: camera)
        
        if status == BookingStatus.bookingAccepted.rawValue || status == BookingStatus.pendingBid.rawValue {
            
            self.updateTableMapVieHeightInitially(status:false)
        }else{
            self.updateTableMapVieHeightInitially(status:true)
        }
        updateCustomerInfoStatus()
        self.bookingTableView.reloadData()
        self.mapView.bringSubviewToFront(self.backButton)
        self.mapView.bringSubviewToFront(self.cancelBookingButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateTheOnbookingUI()
        setupGestureRecognizer()
        NotificationCenter.default.addObserver(self, selector: #selector(handleBookingAccept), name: Notification.Name("gotNewBooking"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(bookingCancelHandling(_:)), name: Notification.Name("cancelledBooking"), object: nil)
    }
    
    
    @objc  func handleBookingAccept() {
        
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    
    ///********notifies when the booking has been cancelled*******//
    @objc func bookingCancelHandling(_ notification: NSNotification) {
        if String(describing:notification.userInfo!["bookingId"]!)  == String(describing:bookingDict!.bookingId) {
            _ = navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "cancelledBooking"), object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "gotNewBooking"), object: nil);
    }
    
    fileprivate func sliderName(_ slider:SliderControl) -> String {
        switch (slider) {
        case self.leftSlider: return "Middle Left Slider"
        default: return "Unknown Slider"
        }
    }
    
    
    @IBAction func callBtnTapped(_ sender: Any) {
        
        let dropPhone = "tel://" + (bookingDict?.mobileNum)!
        if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
            
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
        
    }
    
    @IBAction func chatBtnTapped(_ sender: Any) {
        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat") as? ChatVC {
            
            if activeBid {
                controller.customerID = (bidBookingDict?.customerId)!
                controller.bookingID = String(describing:bidBookingDict!.bookingId)
                controller.custImage = (bidBookingDict?.profilePic)!
                controller.custName =  (bidBookingDict?.firstName)!
                controller.jobDesc = (bidBookingDict?.categoryName)!
                controller.amt = (bidBookingDict?.quotedAmount)!
                controller.isFromBookingFlow = true
                controller.bookingModel = (bidBookingDict?.bookingModel)!
            }
            else {
                controller.customerID = (bookingDict?.customerId)!
                controller.bookingID = String(describing:bookingDict!.bookingId)
                controller.custImage = (bookingDict?.profilePic)!
                controller.custName =  (bookingDict?.firstName)!
                controller.jobDesc = (bookingDict?.categoryName)!
                controller.amt = (bookingDict?.totalAmt)!
                controller.bookingModel = (bookingDict?.bookingModel)!
                controller.isFromBookingFlow = true
            }
            
            if let navigator = navigationController {
                TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                          subType: CATransitionSubtype.fromTop.rawValue,
                                                                          for: (self.navigationController?.view)!,
                                                                          timeDuration: 0.3)
                navigator.pushViewController(controller, animated: false)
            }
        }
    }
    
    @IBAction func updateTableView(_ sender: Any) {
        if status == 7{
            if self.updateViewButton.isSelected{
                self.updateTableMapVieHeight(status:true)
                
            }else{
                
                self.updateTableMapVieHeight(status:false)
            }
            bookingTableView.reloadData()
        }
    }
    
    func updateTheHeaderViewAnimation() {
        DispatchQueue.main.async {
            self.bookingTableView.contentInset = UIEdgeInsets(top: self.tableHeaderViewHeight,left: 0, bottom: 0, right: 0)
            self.bookingTableView.contentOffset = CGPoint(x:0,y:-self.tableHeaderViewHeight)
            self.mapView.frame = CGRect(x:0, y: self.bookingTableView.contentOffset.y, width:self.bookingTableView.bounds.size.width, height: -self.bookingTableView.contentOffset.y)
        }
    }
    
    func updateTableMapVieHeightInitially(status:Bool){
        if status{
            self.updateViewButton.isSelected = false
            var newFrame: CGRect = self.mapView.frame
            newFrame.size.height = UIScreen.main.bounds.size.height - 220
            self.tableHeaderViewHeight  = UIScreen.main.bounds.size.height - 220
            self.mapView.frame = newFrame
            self.navigationButtonConstraint.constant = 0
            self.view.layoutIfNeeded()
        }else{
            self.updateViewButton.isSelected = true
            self.navigationButtonConstraint.constant = -65
            var newFrame: CGRect = self.mapView.frame
            newFrame.size.height = 240
            self.tableHeaderViewHeight  = 240
            self.mapView.frame = newFrame
            self.view.layoutIfNeeded()
        }
        self.bookingTableView.reloadData()
        updateTheHeaderViewAnimation()
    }
    
    func updateTableMapVieHeight(status:Bool){
        if status{
            UIView.animate(withDuration: 0.3,
                           delay: 0.1,
                           options: .curveEaseOut,
                           animations: {() -> Void in
                            self.updateViewButton.isSelected = false
                            var newFrame: CGRect = self.mapView.frame
                            newFrame.size.height = UIScreen.main.bounds.size.height - 220
                            self.tableHeaderViewHeight  = UIScreen.main.bounds.size.height - 220
                            self.mapView.frame = newFrame
                            self.navigationButtonConstraint.constant = 0
                            self.view.layoutIfNeeded()
                            
            }, completion: {(_ finished: Bool) -> Void in
                print("Completed")
            })
        }else{
            UIView.animate(withDuration: 0.3,
                           delay: 0.1,
                           options: .curveEaseOut,
                           animations: {() -> Void in
                            self.updateViewButton.isSelected = true
                            self.navigationButtonConstraint.constant = -65
                            var newFrame: CGRect = self.mapView.frame
                            newFrame.size.height = 240
                            self.tableHeaderViewHeight  = 240
                            self.mapView.frame = newFrame
                            self.view.layoutIfNeeded()
                            
            }, completion: {(_ finished: Bool) -> Void in
                print("Completed")
            })
        }
        updateTheHeaderViewAnimation()
    }
    
    @IBAction func backToHomeVC(_ sender: Any) {
        
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func callToCustomer(_ sender: Any) {
       
        
    }
    
    
    @IBAction func cancelBooking(_ sender: Any) {
        
        let cancelView = CancelView.instance
        cancelView.bookingID = (bookingDict?.bookingId)!
        cancelView.bookingType = (bookingDict?.bookingType)!
        if (bookingDict?.bookingType)! == .Schedule{
            cancelView.startTime = (bookingDict?.bookingRequestedFor)!
            cancelView.entTime = (bookingDict?.bookingRequestEnds)!
        }
        cancelView.getTheCancelData()
        cancelView.delegate = self
        cancelView.show()
        
        
    }
    
    //MARK: - slider to current position
    
    
    @IBAction func getCurrentLocation(_ sender: Any) {
        getCurrentLocationPostion()
    }
    
    @IBAction func wayToGoogleMaps(_ sender: Any) {
        if activeBid {
            MapNavigation.navgigateTogoogleMaps(latit: (bidBookingDict?.latitude)! , logit: (bidBookingDict?.longitude)!)
        }
        else {
            MapNavigation.navgigateTogoogleMaps(latit: (bookingDict?.latitude)! , logit: (bookingDict?.longitude)!)
        }
        
    }
    
    
    @IBAction func applemapsBtnTapped(_ sender: Any) {
        var coordinate = CLLocationCoordinate2D()
        if activeBid {
             coordinate = CLLocationCoordinate2DMake((bidBookingDict?.latitude)!,(bidBookingDict?.longitude)!)
        } else {
              coordinate = CLLocationCoordinate2DMake((bookingDict?.latitude)!,(bookingDict?.longitude)!)
        }
        
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        //mapItem.name = "Target location"
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
    //*****way pick r drop loc using waze maps*****//
    @IBAction func wayToWazeMaps(_ sender: Any) {
        viewWaze(location : location)
    }
    
    //MARK: - opens the waze maps for navigation
    func viewWaze(location : CLLocation) {
        if activeBid {
            MapNavigation.navgigateToWazeMaps(latit: (bidBookingDict?.latitude)!, logit: (bidBookingDict?.longitude)!)
        }
        else {
            MapNavigation.navgigateToWazeMaps(latit: (bookingDict?.latitude)!, logit: (bookingDict?.longitude)!)
        }
    }
    
    @IBAction func sliderChanged(_ sender: SliderControl) {
        print("Changed")
    }
    
    ///MARK:- Slider text data
    @IBAction func sliderFinished(_ sender: SliderControl) {
        self.updateBookingStatus(status:status)
    }
    
    func getCurrentLocationPostion(){
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude) ,longitude: CLLocationDegrees(longitude) , zoom: 16)
        self.mapView.animate(to: camera)
        
    }
    
    //MARK: - update the driver status images and address according the
    func updateCustomerInfoStatus(){
        resetSlider()
        let sectionType : BookingStatus = BookingStatus(rawValue : status)!
        switch sectionType {
        case .bookingAccepted:
            updateViewContraint.constant = 0
            leftSlider.sliderText = onBookingVC.onTheWay
            titleLabel.text = "Accepted"
            status = BookingStatus.onTheWayToPickup.rawValue
            break
            
        case .onTheWayToPickup:
            updateViewContraint.constant = 21
            titleLabel.text = onBookingVC.onTheWay
            leftSlider.sliderText = onBookingVC.arrived
            status = BookingStatus.arrivedAtPickup.rawValue
            self.updateTableMapVieHeight(status:true)
            bookingTableView.reloadData()
            
            break
            
        case .arrivedAtPickup:
            self.performSegue(withIdentifier: "toTimerVC", sender: nil)
            
            break
        default:
            break
        }
    }
    
    
    @IBAction func slideCanceled(_ sender: SliderControl) {
        print("Cancelled")
        
    }
    
    
    @IBAction func messageAction(_ sender: Any) {
        
        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat") as? ChatVC {
            
            
            if activeBid {
                controller.customerID = (bidBookingDict?.customerId)!
                controller.bookingID = String(describing:bidBookingDict!.bookingId)
                controller.custImage = (bidBookingDict?.profilePic)!
                controller.custName =  (bidBookingDict?.firstName)!
                controller.jobDesc = (bidBookingDict?.categoryName)!
                controller.amt = (bidBookingDict?.quotedAmount)!
            }
            else {
                controller.customerID = (bookingDict?.customerId)!
                controller.bookingID = String(describing:bookingDict!.bookingId)
                controller.custImage = (bookingDict?.profilePic)!
                controller.custName =  (bookingDict?.firstName)!
                controller.jobDesc = (bookingDict?.categoryName)!
                controller.amt = (bookingDict?.totalAmt)!
            }
            
            if let navigator = navigationController {
                TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                          subType: CATransitionSubtype.fromTop.rawValue,
                                                                          for: (self.navigationController?.view)!,
                                                                          timeDuration: 0.3)
                navigator.pushViewController(controller, animated: false)
            }
        }
    }
    
    
    
    //MARK: - update driver status to server
    func updateBookingStatus(status:Int) {
        Helper.showPI(message:loading.load)
        let ud = UserDefaults.standard
        
        
        var distance = 0.0
        if status == BookingStatus.arrivedAtPickup.rawValue{
            let distanceData = DistanceCalculation().getTheSavedDistance(distanceDocID:String(describing:self.bookingDict!.bookingId) + "_Dist")
            if !distanceData.isEmpty{
                distance = Double((distanceData["dist"] as?  NSNumber)!)
            }
        }
        
        if (ud .value(forKey: "currentLat") != nil) {
            
            let lati = Helper.unwrappOptional(str : ud.object(forKey: "currentLat") as? String)
                   let longi = Helper.unwrappOptional(str : ud.object(forKey: "currentLog") as? String)
            
            let params : [String : Any] = ["status": status,
                                           "bookingId":bookingDict!.bookingId as Any,
                                           "latitude" :lati,
                                           "longitude":longi,
                                           "distance":distance]
            
            bookingMod.updateBookingStatus(dict: params ,completionHandler: { (succeeded, travelfee) in
                if succeeded{
                    self.bookingDict?.statusCode = status
                    if status == BookingStatus.arrivedAtPickup.rawValue{
                        let deleteProfileDoc = Couchbase.sharedInstance
                        deleteProfileDoc.deleteDocument(withDocID:String(describing:self.bookingDict!.bookingId) + "_Dist")
                    }
                    self.updateCustomerInfoStatus()
                }else{
                    self.resetSlider()
                }
            })
        }else{
            let params : [String : Any] =  ["status": status,
                                            "bookingId":bookingDict!.bookingId as Any,
                                            "latitude" :latitude,
                                            "longitude":longitude,
                                            "distance":distance]
            
            bookingMod.updateBookingStatus(dict: params, completionHandler: { (succeeded, travelfee) in
                if succeeded{
                    
                    if status == BookingStatus.arrivedAtPickup.rawValue{
                        let deleteProfileDoc = Couchbase.sharedInstance
                        deleteProfileDoc.deleteDocument(withDocID:String(describing:self.bookingDict!.bookingId) + "_Dist")
                    }
                    self.bookingDict?.statusCode = status
                    self.updateCustomerInfoStatus()
                }else{
                    self.resetSlider()
                }
            })
        }
    }
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        switch segue.identifier! as String {
            
        case "toTimerVC":
            if let nextScene =  segue.destination as? TimerViewController
            {
                nextScene.bookingDict = bookingDict
            }
            break
            
        case "onthewayCustReview":
            let nav = segue.destination as! UINavigationController
            if let review: CustReviewsVC = nav.viewControllers.first as! CustReviewsVC?
            {
                review.customerID = (bookingDict?.customerId)!
            }
            break
        case "toChatVC":
            let nav = segue.destination as! UINavigationController
            if let chat: ChatVC = nav.viewControllers.first as! ChatVC?
            {
                if activeBid{
                    chat.customerID = (bidBookingDict?.customerId)!
                    chat.bookingID = String(describing:bidBookingDict!.bookingId)
                    chat.custImage = (bidBookingDict?.profilePic)!
                    chat.custName =  (bidBookingDict?.firstName)!
                    chat.jobDesc = (bidBookingDict?.categoryName)!
                    chat.amt = (bidBookingDict?.quotedAmount)!
                    chat.bookingModel = (bidBookingDict?.bookingModel)!
                    
                    
                }
                else {
                    chat.customerID = (bookingDict?.customerId)!
                    chat.bookingID = String(describing:bookingDict!.bookingId)
                    chat.custImage = (bookingDict?.profilePic)!
                    chat.custName =  (bookingDict?.firstName)!
                    chat.jobDesc = (bookingDict?.categoryName)!
                    chat.amt = (bookingDict?.totalAmt)!
                    chat.bookingModel = (bookingDict?.bookingModel)!
                }
                
            }
            break
        default:
            break
        }
    }
}




extension OnBookingViewController:cancelDeleteBooking{
    func cancelReason(){
        
        self.bookingDict?.statusCode = BookingStatus.bookingCancel.rawValue
        _ = navigationController?.popToRootViewController(animated: true)
    }
}


extension OnBookingViewController: UINavigationControllerDelegate {
    
    internal func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}
