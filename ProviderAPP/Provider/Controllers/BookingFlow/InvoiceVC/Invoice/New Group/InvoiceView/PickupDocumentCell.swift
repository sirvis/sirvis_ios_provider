//
//  PickupDocumentCell.swift
//  LSP
//
//  Created by Rahul Sharma on 2/8/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class PickupDocumentCell: UITableViewCell {
    @IBOutlet weak var pickupNote: UILabel!
    
    @IBOutlet weak var pickupImageView: UIImageView!
    @IBOutlet weak var pickupImageCollection: UICollectionView!
    var imageUrls =  [String]()
    var pickedImages =  [UIImage]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pickupImageCollection.delegate = self
        pickupImageCollection.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension PickupDocumentCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageUrls.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemPhotoCollectionCell", for: indexPath) as! ItemPhotoCollectionCell
        if imageUrls.count != 0 {
            //cell.pickedImageView.image = pickedImages[indexPath.row]
            cell.pickedImageView.kf.setImage(with: URL(string: imageUrls[indexPath.row]),
                                          placeholder:UIImage.init(named: "upload_image_one"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: { image, error, cacheType, imageURL in
            })

        }
        return cell
        
}

}
