//
//  PhotoTableViewCell.swift
//  LSP
//
//  Created by Rajan Singh on 15/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

protocol PhotoTableViewCellDelegate {
    func cellClicked(index: Int)
    func cellClickForInvoice(isPickup: Bool, index: Int)
}

extension PhotoTableViewCellDelegate {
    
    func cellClicked(index: Int) {
        
    }
    
    func cellClickForInvoice(isPickup: Bool, index: Int) {
        
    }
}

class ProviderProphotoTableViewCell: UITableViewCell {

    @IBOutlet weak var jobPhotoView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var jobNotesLabel: UILabel!
    @IBOutlet weak var jobPhotoLabel: UILabel!
    var delegate: PhotoTableViewCellDelegate?
    var invoiceVM: InvoiceViewModel? = nil
    var isPickup = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setAllData(invoiceVm: InvoiceViewModel, isPickup: Bool) {
        self.invoiceVM = invoiceVm
        self.isPickup = isPickup
    }

}

extension ProviderProphotoTableViewCell: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            delegate!.cellClickForInvoice(isPickup: self.isPickup, index: indexPath.row)
        
    }
    
}

extension ProviderProphotoTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         if invoiceVM!.invoiceM != nil {
            if isPickup {
                return invoiceVM!.getCountForPickupImages()
            }else {
                return invoiceVM!.getCountForDropImages()
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "providerJobPhotoCollCell", for: indexPath) as! ProviderProJobPhotoCVCell
            var urlM: URL?
           
                if isPickup {
                    self.jobNotesLabel.text = invoiceVM?.getPickupNotes()
                    urlM = URL(string:  invoiceVM!.getPickupImage(index: indexPath.row))
                    
                }else {
                    self.jobNotesLabel.text = invoiceVM?.getDropNotes()
                    urlM = URL(string:  invoiceVM!.getDropImage(index: indexPath.row))
                }
            
            cell.jobPhotoCellImage.kf.setImage(with: urlM,
                                               placeholder: #imageLiteral(resourceName: "check _off"),
                                               options: [.transition(.fade(1))])
        
            return cell
        }
    
}







