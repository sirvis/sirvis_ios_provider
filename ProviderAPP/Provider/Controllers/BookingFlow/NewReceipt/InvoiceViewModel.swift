//
//  InvoiceViewModel.swift
//  LSP
//
//  Created by Rajan Singh on 09/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AXPhotoViewer

class InvoiceViewModel: NSObject {
    let disposeBag = DisposeBag()
    let invoice_Data = PublishSubject<InvoiceModel>()
    var invoiceM: InvoiceModel? = nil
    let acessClass = AccessTokenReFresh.sharedInstance()
    var apiTag: Int!
    var bookingId = ""
    
    
    func getCountForDropImages()-> Int {
        return invoiceM!.invoiceData_Dict[0].dropImages.count
    }
    
    func getCountForPickupImages()-> Int {
         return invoiceM!.invoiceData_Dict[0].pickUpImages.count
    }
    
    func getDropImage(index: Int)-> String {
        return invoiceM!.invoiceData_Dict[0].dropImages[index]
    }
    
    func getPickupImage(index: Int)-> String {
        return invoiceM!.invoiceData_Dict[0].pickUpImages[index]
    }
    func getPickupNotes()-> String {
        return invoiceM!.invoiceData_Dict[0].pickupNotes
    }
    func getDropNotes()-> String {
        return invoiceM!.invoiceData_Dict[0].dropNotes
    }
    
    func getAuxDropPhotoImages()-> [AXPhoto] {
        var photos = [AXPhoto]()
        for index in 0...(invoiceM!.invoiceData_Dict[0].dropImages.count - 1) {
            let photoExt =  AXPhoto(
                attributedTitle: NSAttributedString(string: "Job Photos"),
                attributedDescription: NSAttributedString(string: ""),
                url: URL(string: invoiceM!.invoiceData_Dict[0].dropImages[index]))
            photos.append(photoExt)
        }
        return photos
    }
    
    func getAuxPickPhotoImages()-> [AXPhoto] {
        var photos = [AXPhoto]()
        for index in 0...(invoiceM!.invoiceData_Dict[0].pickUpImages.count - 1) {
            let photoExt =  AXPhoto(
                attributedTitle: NSAttributedString(string: "Job Photos"),
                attributedDescription: NSAttributedString(string: ""),
                url: URL(string: invoiceM!.invoiceData_Dict[0].pickUpImages[index]))
            photos.append(photoExt)
        }
        return photos
    }
    
    
    func setModelData(invoiceM: InvoiceModel) {
        self.invoiceM = InvoiceModel()
    }
    
    func getInvoiceModel() -> InvoiceModel {
        return invoiceM!
    }
    
    func getProviderName( index: Int)-> String {
        return invoiceM!.invoiceData_Dict[index].fullName
    }
    
    func getCallType( index: Int)-> Int {
        return invoiceM!.invoiceData_Dict[index].callType
    }
    
    func getBookingModel(index: Int)-> Int {
         return invoiceM!.invoiceData_Dict[index].bookingModel
    }
    
    func getBookingStatus( index: Int)-> Int {
        return invoiceM!.invoiceData_Dict[index].status
    }
    
    func getStatusMessage( index: Int) -> String {
        return invoiceM!.invoiceData_Dict[index].statusMsg
    }
    
    func getSignUrl( index: Int) -> String {
        return invoiceM!.invoiceData_Dict[index].signUrl
    }
    
    func getStatusType( index: Int) -> Int {
        return invoiceM!.invoiceData_Dict[index].status
    }
    
    func getBookingID( index: Int)-> String {
        return invoiceM!.invoiceData_Dict[index].bookingId
    }
    
    func getBookingAdress( index: Int)-> String {
        return invoiceM!.invoiceData_Dict[index].address
    }
    
    func getBookingTime( index: Int)-> String {
        return invoiceM!.invoiceData_Dict[index].bookingTime
    }
    
    func getProviderImage( index: Int)-> String {
        return invoiceM!.invoiceData_Dict[index].profilePic
    }
    
    func getBookingTotallPrice( index: Int)-> String {
        if invoiceM!.invoiceData_Dict[index].acountingData[0].currencyAbr {
            return (invoiceM!.invoiceData_Dict[index].acountingData[0].currencySymbol + String(format: " %.2f" ,invoiceM!.invoiceData_Dict[index].acountingData[0].totall) )
        }else {
            return (String(format: " %.2f" ,invoiceM!.invoiceData_Dict[index].acountingData[0].totall) + invoiceM!.invoiceData_Dict[index].acountingData[0].currencySymbol)
        }
    }
    
    func getCountOfBooking(bookingStatus: Int) -> Int {
        return invoiceM!.invoiceData_Dict.count
    }
    
    func getCategoryName( index: Int) -> String {
        return invoiceM!.invoiceData_Dict[index].category
    }
    
    func getNumberOfAddedService( index: Int)-> Int {
        
        return invoiceM!.invoiceData_Dict[index].gigDatas.count
    }
    
    func getServicePrice( index: Int, serviceIndex: Int) -> Float {
        return invoiceM!.invoiceData_Dict[index].gigDatas[serviceIndex].price
    }
    
    func getServiceName( index: Int, serviceIndex: Int) -> String {
        return invoiceM!.invoiceData_Dict[index].gigDatas[serviceIndex].name
    }
    
    func getServiceUnit( index: Int, serviceIndex: Int) -> Int {
        return invoiceM!.invoiceData_Dict[index].gigDatas[serviceIndex].unit
    }
    
    func getPaymentType( index: Int) -> Int {
        return invoiceM!.invoiceData_Dict[index].acountingData[0].paymentMethod
    }
    
    func getCardLastFourDigit( index: Int) -> String {
        return invoiceM!.invoiceData_Dict[index].acountingData[0].cardNo
    }
    
    func getDiscount()-> String {
        if invoiceM!.invoiceData_Dict[0].acountingData[0].discount > 0 {
            if invoiceM!.invoiceData_Dict[0].acountingData[0].currencyAbr{
                return ( invoiceM!.invoiceData_Dict[0].acountingData[0].currencySymbol + String(format: " %.2f" ,invoiceM!.invoiceData_Dict[0].acountingData[0].discount) )
            }else {
                return (String(format: " %.2f" ,invoiceM!.invoiceData_Dict[0].acountingData[0].discount) + invoiceM!.invoiceData_Dict[0].acountingData[0].currencySymbol)
            }
        }else {
            return ""
        }
    }
    
    
    
    func getVisitFee( index: Int) -> String {
        if invoiceM!.invoiceData_Dict[index].acountingData[0].visitFee > 0 {
            if invoiceM!.invoiceData_Dict[index].acountingData[0].currencyAbr {
                return ( invoiceM!.invoiceData_Dict[index].acountingData[0].currencySymbol + String(format: " %.2f" ,invoiceM!.invoiceData_Dict[index].acountingData[0].visitFee) )
            }else {
                return (String(format: " %.2f" ,invoiceM!.invoiceData_Dict[index].acountingData[0].visitFee) + invoiceM!.invoiceData_Dict[index].acountingData[0].currencySymbol)
            }
        }else {
            return ""
        }
    }
    
    func getBidAmount( index: Int) -> String {
        if invoiceM!.invoiceData_Dict[index].acountingData[0].bidAmount > 0 {
            if invoiceM!.invoiceData_Dict[index].acountingData[0].currencyAbr {
                return ( invoiceM!.invoiceData_Dict[index].acountingData[0].currencySymbol + String(format: " %.2f" ,invoiceM!.invoiceData_Dict[index].acountingData[0].bidAmount) )
            }else {
                return (String(format: " %.2f" ,invoiceM!.invoiceData_Dict[index].acountingData[0].bidAmount) + invoiceM!.invoiceData_Dict[index].acountingData[0].currencySymbol)
            }
        }else {
            return ""
        }
    }
    
    
    func getCancellationFees()-> String {
        if invoiceM!.invoiceData_Dict[0].acountingData[0].cancellationFee > 0 {
            if invoiceM!.invoiceData_Dict[0].acountingData[0].currencyAbr{
                return ( invoiceM!.invoiceData_Dict[0].acountingData[0].currencySymbol + String(format: " %.2f" ,invoiceM!.invoiceData_Dict[0].acountingData[0].cancellationFee) )
            }else {
                return (String(format: " %.2f" ,invoiceM!.invoiceData_Dict[0].acountingData[0].cancellationFee) + invoiceM!.invoiceData_Dict[0].acountingData[0].currencySymbol)
            }
        }else {
            return ""
        }
    }
    
    func getTravelFee( index: Int) -> String {
        if invoiceM!.invoiceData_Dict[index].acountingData[0].travelFee > 0 {
            if invoiceM!.invoiceData_Dict[index].acountingData[0].currencyAbr{
                return ( invoiceM!.invoiceData_Dict[index].acountingData[0].currencySymbol + String(format: " %.2f" ,invoiceM!.invoiceData_Dict[index].acountingData[0].travelFee) )
            }else {
                return (String(format: " %.2f" ,invoiceM!.invoiceData_Dict[index].acountingData[0].travelFee) + invoiceM!.invoiceData_Dict[index].acountingData[0].currencySymbol)
            }
        }else {
            return ""
        }
    }
    
    func getAllAdditionSerCount()-> Int {
        return invoiceM!.invoiceData_Dict[0].aditionalS.count
    }
    
    func getAdditionSerName(index: Int)-> String {
        if invoiceM!.invoiceData_Dict[0].aditionalS[index].name.count > 0 {
            return invoiceM!.invoiceData_Dict[0].aditionalS[index].name
        }
        return ""
    }
    
    func getAdditionSerAmount(index: Int)-> String {
        if invoiceM!.invoiceData_Dict[0].acountingData[0].currencyAbr {
            return ( invoiceM!.invoiceData_Dict[0].acountingData[0].currencySymbol + String(format: " %.2f" ,invoiceM!.invoiceData_Dict[0].aditionalS[index].price) )
        }else {
            return (String(format: "%.2f " ,invoiceM!.invoiceData_Dict[0].aditionalS[index].price) +  invoiceM!.invoiceData_Dict[0].acountingData[0].currencySymbol)
        }
    }
    
    func getBookingStatus()-> Int {
        return invoiceM!.invoiceData_Dict[0].status
    }
    func getCallType1()-> Int {
        return invoiceM!.invoiceData_Dict[0].callType
    }
    func getPaidByWallet()-> Bool{
            return invoiceM!.invoiceData_Dict[0].acountingData[0].paidByWallet
    }
    
    func getCurrencySymbol()-> String {
        return invoiceM!.invoiceData_Dict[0].acountingData[0].currencySymbol
    }
    
    func getCurrencyAbr()-> Bool {
        return invoiceM!.invoiceData_Dict[0].acountingData[0].currencyAbr
    }
    func getAddress()-> String {
        return invoiceM!.invoiceData_Dict[0].address
    }
    func bookingTme()-> String {
        return invoiceM!.invoiceData_Dict[0].bookingTime
    }
    
    func getRemaningAmount()-> String {
        if invoiceM!.invoiceData_Dict[0].acountingData[0].remainingAmount > 0 {
            if invoiceM!.invoiceData_Dict[0].acountingData[0].currencyAbr{
                return ( invoiceM!.invoiceData_Dict[0].acountingData[0].currencySymbol + String(format: " %.2f" ,invoiceM!.invoiceData_Dict[0].acountingData[0].remainingAmount) )
            }else {
                return (String(format: " %.2f" ,invoiceM!.invoiceData_Dict[0].acountingData[0].remainingAmount) + invoiceM!.invoiceData_Dict[0].acountingData[0].currencySymbol)
            }
        }else {
            return ""
        }
    }
    
    func getCaptureAmount()-> String {
        if invoiceM!.invoiceData_Dict[0].acountingData[0].captureAmount > 0 {
            if invoiceM!.invoiceData_Dict[0].acountingData[0].currencyAbr{
                return ( invoiceM!.invoiceData_Dict[0].acountingData[0].currencySymbol + String(format: " %.2f" ,invoiceM!.invoiceData_Dict[0].acountingData[0].captureAmount) )
            }else {
                return (String(format: " %.2f" ,invoiceM!.invoiceData_Dict[0].acountingData[0].captureAmount) + invoiceM!.invoiceData_Dict[0].acountingData[0].currencySymbol)
            }
        }else {
            return ""
        }
    }
    
}


extension InvoiceViewModel {
    
    
    func sendRequestToFetchInvoiceDetails(bookingId: String) {
        self.bookingId = bookingId
        acessClass.acessDelegate = self
        let bookingFlowapiModal = BookingFlowApiModel()
        bookingFlowapiModal.sendRequestForInvoiceDetail(bookingId: bookingId)
        
        bookingFlowapiModal.subject_response.subscribe(onNext: { (response) in
            print("\(response)")
            
            self.gotResponseFromServer(response: response, requestType: response.responseType!)
        }).disposed(by: disposeBag)
    }
    
    
}


extension InvoiceViewModel {
    func gotResponseFromServer(response: APIResponseModel1, requestType: RequestType) {
        apiTag = requestType.rawValue
        if (response.data[SERVICE_RESPONSE.Error] != nil ) {
            GenericUtilityMethods.showAlert(alert: "Error", message: response.data[SERVICE_RESPONSE.Error] as! String)
            return
        }
        
        switch response.httpStatusCode {
            
        case errorCodes.UserNotFound.rawValue :
            if (response.data[SERVICE_RESPONSE.ErrorMessage] as? String) != nil {
                GenericUtilityMethods.showAlert(alert: "Message", message: response.data[SERVICE_RESPONSE.ErrorMessage] as! String)
            }
            
            break
            
        case errorCodes.error400.rawValue:
            
            switch requestType {
                
            case .invoice:
                self.invoiceM = InvoiceModel()
                invoiceM!.initializeData(dataDict: response.data, httpsErrorCode: response.httpStatusCode)
                invoice_Data.onNext(invoiceM!)
                
            default:
                break
            }
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType {
                
            case .invoice:
                invoiceM = InvoiceModel()
                invoiceM?.initializeData(dataDict: response.data, httpsErrorCode: response.httpStatusCode)
                invoice_Data.onNext(invoiceM!)
                
            default:
                break
            }
            
        case errorCodes.TokenExpired.rawValue :
            let userDefaults:UserDefaults = UserDefaults.standard
         
            userDefaults.synchronize()
            acessClass.getAcessToken()
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            break
            
        default:
            if (response.data[SERVICE_RESPONSE.ErrorMessage] as? String) != nil {
                GenericUtilityMethods.showAlert(alert: "Message", message: response.data[SERVICE_RESPONSE.ErrorMessage] as! String)
            }
            
        }
        //        self.main_response.onNext(response)
    }
}



extension InvoiceViewModel: AccessTokeDelegate {
    
    /// to recall the api
    @objc func recallApi() {
        switch apiTag {
        case RequestType.invoice.rawValue:
            sendRequestToFetchInvoiceDetails(bookingId: self.bookingId)
            
        default:
            break
        }
    }
}



