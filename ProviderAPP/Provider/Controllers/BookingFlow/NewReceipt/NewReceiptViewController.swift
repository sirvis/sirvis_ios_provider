//
//  NewReceiptViewController.swift
//  LSP
//
//  Created by Rahul Sharma on 29/06/20.
//  Copyright © 2020 3Embed. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AXPhotoViewer
import Kingfisher

struct cellIdentifier {
    static let mainCell                = "mainCell"
    static let serviceFeeCell          = "serviceFeeCell"
    static let seviceCollectionCell    = "SeviceCollectionCell"
    static let paymentMCell            = "paymentMCell"
    static let signatureCell           = "signatureCell"
    static let jobStatusDateTimeTVCell = "jobStatusDateTimeTVCell"
}

class NewReceiptViewController: UIViewController {

    @IBOutlet weak var mapPinImageView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var backViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var addressLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var mapPinHeight: NSLayoutConstraint!
    var invoiceVM = InvoiceViewModel()
    var bookingId = ""
    var apiCall = true
    var disposeBag = DisposeBag()
    let acessClass = AccessTokenReFresh()
    var apiTag: Int!
    var index = 0
    var totallCellWP = 0
    var photoIndex = 0
    var isPickUP = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        invoiceVM.sendRequestToFetchInvoiceDetails(bookingId: bookingId)
        if apiCall {
            subscribeToApiCalls()
            apiCall = false
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
      
    }
    override func viewDidAppear(_ animated: Bool) {
        acessClass.acessDelegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// it get the response from the Api call and process the required Operation
    func subscribeToApiCalls() {
        invoiceVM.invoice_Data.subscribe(onNext: {
            (reponse) in
            self.apiSucessResponse(apiName:reponse.requestType! , sucess: reponse.httpStatusCode == HTTPSResponseCodes.SuccessResponse.rawValue ? true : false , errorCode: reponse.httpStatusCode)
        }).disposed(by: disposeBag)
    }
    
    func callcullateHeight()-> CGFloat {
        var calcullateTheTVH: CGFloat = 0
        calcullateTheTVH = (CGFloat((60) + ((invoiceVM.getNumberOfAddedService(index: 0) + 1) * 43 ) + (( invoiceVM.getPaymentType(index: 0) == 2 ? 3 : 2) * 43 )))
        if (invoiceVM.getVisitFee(index: 0).count > 0 && invoiceVM.getBookingStatus() != 12)
            ||
            (invoiceVM.getTravelFee( index: 0).count > 0 && invoiceVM.getBookingStatus() != 12)
            ||
            (invoiceVM.getCancellationFees().count > 0 && invoiceVM.getBookingStatus() == 12)
            || (invoiceVM.getDiscount().count > 0 && invoiceVM.getBookingStatus() != 12)
            ||
            (invoiceVM.getAllAdditionSerCount() > 0 && invoiceVM.getBookingStatus() != 12 ) {
            calcullateTheTVH = calcullateTheTVH + 40
        }
        
        if invoiceVM.getVisitFee(index: 0).count == 0 || invoiceVM.getBookingStatus() == 12 {
            calcullateTheTVH = calcullateTheTVH + 0
        }else {
            calcullateTheTVH = calcullateTheTVH + 40
        }
        
        if invoiceVM.getTravelFee(index: 0).count == 0  || invoiceVM.getBookingStatus(index: 0) == 12 {
            calcullateTheTVH = calcullateTheTVH + 0
        }else {
            calcullateTheTVH = calcullateTheTVH + 40
        }
        if invoiceVM.getDiscount().count == 0  || invoiceVM.getBookingStatus() == 12 {
            calcullateTheTVH = calcullateTheTVH + 0
        }else {
            calcullateTheTVH = calcullateTheTVH + 40
        }
        
        if (invoiceVM.getCancellationFees().count > 0) && invoiceVM.getBookingStatus() == 12 {
            calcullateTheTVH = calcullateTheTVH + 40
        }else {
            calcullateTheTVH = calcullateTheTVH + 0
        }
        return (calcullateTheTVH + 350 )
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension NewReceiptViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 || section == 1 {
            return 0.16
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1 {
            return 0.16
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension NewReceiptViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var addSection = 0
        
        if invoiceVM.invoiceM != nil && (invoiceVM.getCountForDropImages() > 0 || invoiceVM.getCountForPickupImages() > 0 ){
            addSection = 1
        }
        return (4 + addSection)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if invoiceVM.invoiceM != nil {
            if section == 4 {
               var totallCell = 0
                if invoiceVM.getCountForDropImages() > 0 {
                    totallCell += 1
                }
                if invoiceVM.getCountForPickupImages() > 0 {
                    totallCell += 1
                }
                return totallCell
            }
            if section == 0 {
                if invoiceVM.getBookingModel(index: 0) == 3 {
                    return 2
                }
                return (invoiceVM.getNumberOfAddedService(index: 0) + 1)
            }else if section == 1 {
                return  (6 + invoiceVM.getAllAdditionSerCount() + 1)
            }else if section == 2 {
                if invoiceVM.getPaidByWallet(){
                    return 3
                }else {
                    return 2
                }
            }else {
                return 1
            }
        }
        return 0
    }
    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        var tottalIndex = 0
//        if invoiceVM.invoiceM != nil {
//            if invoiceVM.getTravelFee(index: index).count > 0 && invoiceVM.getVisitFee(index: index).count > 0{
//                tottalIndex += 2
//            }else if invoiceVM.getTravelFee(index: index).count > 0 || invoiceVM.getVisitFee(index: index).count > 0 {
//                tottalIndex += 1
//            }
//            totallCellWP = (invoiceVM.getNumberOfAddedService(index: index) + 1 + tottalIndex)
//            if invoiceVM.getPaymentType(index: index) == 1 {
//                tottalIndex = totallCellWP + 3
//            }else {
//                tottalIndex = totallCellWP + 2
//            }
//            return (tottalIndex + 1)
//        }
//        return 0
//
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 4 {
           if invoiceVM.getCountForPickupImages() > 0 && invoiceVM.getPickupNotes() != "" {
            let content = invoiceVM.getPickupNotes()
            let font = UIFont(name: "Hind-Regular", size:13.0 )
            let addressLabelHeight:CGFloat = GenericUtilityMethods.heightForView(text: content, font: font!, width: self.view.frame.size.width - 60)
            return (addressLabelHeight + 120)
            }
            if invoiceVM.getCountForDropImages() > 0 && invoiceVM.getDropNotes() != "" {
                let content = invoiceVM.getDropNotes()
                let font = UIFont(name: "Hind-Regular", size:13.0 )
                let addressLabelHeight:CGFloat = GenericUtilityMethods.heightForView(text: content, font: font!, width: self.view.frame.size.width - 60)
                return (addressLabelHeight + 120)
            }
            return 120
        }
            if indexPath.section == 1 {
                switch indexPath.row {
                case 0:
                    if (invoiceVM.getVisitFee(index: 0).count > 0 && invoiceVM.getBookingStatus() != 12)
                        ||
                        (invoiceVM.getTravelFee(index: 0).count > 0 && invoiceVM.getBookingStatus() != 12)
                        ||
                        (invoiceVM.getCancellationFees().count > 0 && invoiceVM.getBookingStatus() == 12)
                        || (invoiceVM.getDiscount().count > 0 && invoiceVM.getBookingStatus() != 12)
                        ||
                        (invoiceVM.getAllAdditionSerCount() > 0 && invoiceVM.getBookingStatus() != 12 ) {
                        return 40
                    }
                    return 0
                    
                case 1:
                    if invoiceVM.getVisitFee(index: 0).count == 0 || invoiceVM.getBookingStatus() == 12 {
                        return 0
                    }
                    return 40
                    
                case 2:
                    if invoiceVM.getTravelFee(index: 0).count == 0  || invoiceVM.getBookingStatus() == 12 {
                        return 0
                    }
                    return 40
                    
                case 4:
                    if invoiceVM.getDiscount().count == 0  || invoiceVM.getBookingStatus() == 12 {
                        return 0
                    }
                    return 40
                    
                    
                case 3:
                    if (invoiceVM.getCancellationFees().count > 0) && invoiceVM.getBookingStatus() == 12 {
                        return 40
                    }
                    return 0
                    
                default:
                    return 40
                }
            }
        
        if indexPath.section == 3 {
            return 160
        }
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.signatureCell, for: indexPath) as! InvoiceSignatureTVCell
            let url = invoiceVM.getSignUrl(index: index)
            if url.count > 0 {
                let url = URL(string: url)
//                cell.signatureImage.kf.setImage(with: url)
                cell.signatureImage.kf.setImage(with: url,
                                                   placeholder:UIImage.init(named: "loading"),
                                                   options: [.transition(ImageTransition.fade(1))],
                                                   progressBlock: { receivedSize, totalSize in
                },
                                                   completionHandler: { image, error, cacheType, imageURL in
                                                    
                })
            }
            return cell
        }
        
        if indexPath.row == 0 && indexPath.section != 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SerHeaderTypeTVCell", for: indexPath) as! ServiceDetailHeaderTypeTVCell
            cell.sellimageView.isHidden = true
            cell.bottomLineView.isHidden = true
            cell.titleLabel.text = ReciptHeaderString[indexPath.section]
            return cell
        }
        
            switch indexPath.section {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                cell.feeAmount.isHidden = false
                cell.bottomView.isHidden = false
                if invoiceVM.getBookingModel(index: 0) == 3 {
                    cell.feeTitle.text = paymentDis[8]
                    cell.feeAmount.text = invoiceVM.getBidAmount(index: 0)
                }else {
                    let serviceName = invoiceVM.getServiceName(index: 0, serviceIndex: (indexPath.row - 1))
                    if invoiceVM.getCallType(index: 0) == 1 {
                        cell.feeTitle.text = ServiceDetailsString[2]
                    }else {
                    cell.feeTitle.text = (serviceName.count > 0 ? serviceName : hourlyStringFile) + " x " + "\(invoiceVM.getServiceUnit(index: 0, serviceIndex: (indexPath.row - 1)))"
                    }
                    if invoiceVM.getCurrencyAbr() {
                        cell.feeAmount.text = invoiceVM.getCurrencySymbol() + " \(invoiceVM.getServicePrice(index: 0, serviceIndex: (indexPath.row - 1)))"
                    }else {
                        cell.feeAmount.text = "\(invoiceVM.getServicePrice(index: 0, serviceIndex: (indexPath.row - 1))) " + invoiceVM.getCurrencySymbol()
                    }
                }
                return cell
                
            case 1:
                switch indexPath.row {
                case 1:
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                    cell.feeAmount.isHidden = false
                    cell.bottomView.isHidden = false
                    cell.feeTitle.text = visitTraveString[0]
                    cell.feeAmount.text = invoiceVM.getVisitFee(index: 0)
                    return cell
                    
                case 2:
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                    cell.feeAmount.isHidden = false
                    cell.bottomView.isHidden = false
                    cell.feeTitle.text = visitTraveString[1]
                    cell.feeAmount.text = invoiceVM.getTravelFee(index: 0)
                    return cell
                    
                case 3:
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                    cell.feeAmount.isHidden = false
                    cell.bottomView.isHidden = false
                    cell.feeTitle.text = visitTraveString[2]
                    cell.feeAmount.text = invoiceVM.getCancellationFees()
                    return cell
                    
                case 4:
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                    cell.feeAmount.isHidden = false
                    cell.bottomView.isHidden = false
                    cell.feeTitle.text = visitTraveString[3]
                    cell.feeAmount.text = invoiceVM.getDiscount()
                    return cell
                case 5:
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                    cell.bottomView.isHidden = false
                    cell.feeTitle.text = "VAT"
                    cell.feeAmount.text = Utility.currencySymbol + " " + "\(totVat)"
                    
                    return cell
                case (6 + invoiceVM.getAllAdditionSerCount()):
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                    cell.bottomView.isHidden = false
                    cell.feeTitle.text = paymentDis[3]
                    cell.feeAmount.text = invoiceVM.getBookingTotallPrice(index: 0)
                    return cell
               
                    
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                    cell.bottomView.isHidden = false
                    cell.feeTitle.text = invoiceVM.getAdditionSerName(index: (indexPath.row - 6) )
                    cell.feeAmount.text = invoiceVM.getAdditionSerAmount(index: (indexPath.row - 6))
                    return cell
                }
                
            case 4:
                if indexPath.row == 0 {
                    if invoiceVM.getCountForPickupImages() > 0 {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "JobPhotoCell", for: indexPath) as! ProviderProphotoTableViewCell
                        cell.jobPhotoLabel.text = "Pickup Photos"
                        cell.jobNotesLabel.text = invoiceVM.getPickupNotes()
                        cell.setAllData(invoiceVm: invoiceVM, isPickup: true)
                        cell.collectionView.reloadData()
                        cell.delegate = self
                        return cell
                    }else if invoiceVM.getCountForDropImages() > 0 {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "JobPhotoCell", for: indexPath) as! ProviderProphotoTableViewCell
                        cell.jobPhotoLabel.text = "Drop Photos"
                        cell.jobNotesLabel.text = invoiceVM.getDropNotes()
                        cell.setAllData(invoiceVm: invoiceVM, isPickup: false)
                        cell.collectionView.reloadData()
                        cell.delegate = self
                        return cell
                    }else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                        cell.feeAmount.isHidden = false
                        return cell
                    }
                }else if invoiceVM.getCountForDropImages() > 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "JobPhotoCell", for: indexPath) as! ProviderProphotoTableViewCell
                    cell.jobPhotoLabel.text = "Drop Photos"
                    cell.setAllData(invoiceVm: invoiceVM, isPickup: false)
                    cell.collectionView.reloadData()
                    cell.delegate = self
                    return cell
                }else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                    cell.feeAmount.isHidden = false
                    return cell
                }
                
            default:
                switch indexPath.row {
                case 1:
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                    cell.bottomView.isHidden = false
                    var paymentType = 0
                    paymentType  = invoiceVM.getPaymentType(index: 0)
                    switch paymentType {
                    case 1:
                        cell.feeTitle.text = paymentTypeConfig[2]
                        cell.bottomView.isHidden = true
                        
                    case 2:
                        cell.feeTitle.text = cardPaymentStringFile
                        
                    default:
                        cell.feeTitle.text = paymentTypeConfig[1]
                        
                    }
                    cell.feeAmount.isHidden = false
                    if invoiceVM.getPaidByWallet() {
                        cell.feeAmount.text = invoiceVM.getRemaningAmount()
                    }else {
                        cell.feeAmount.text = invoiceVM.getBookingTotallPrice(index: 0)
                    }
                    return cell
                    
                case 2:
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                    cell.feeTitle.text = paymentTypeConfig[1]
                    cell.bottomView.isHidden = true
                    
                    cell.feeAmount.text =  invoiceVM.getCaptureAmount()
                    return cell
                    
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.serviceFeeCell, for: indexPath) as! ServiceDetailFeeTableViewCell
                    cell.feeAmount.isHidden = false
                    return cell
                    
                }
            }
    }
}

// MARK: - Access Token Delegate
extension NewReceiptViewController: AccessTokeDelegate {
    
    /// to recall the api
    @objc func recallApi() {
        switch apiTag {
        case RequestType.invoice.rawValue:
            invoiceVM.sendRequestToFetchInvoiceDetails(bookingId: bookingId)
        default:
            break
        }
    }
    
}

// MARK: - handeling common api errors
extension NewReceiptViewController {
    
    func apiSucessResponse(apiName: RequestType, sucess: Bool, errorCode: Int) {
        if sucess {
            switch apiName {
                
            case .invoice:
                backViewHeight.constant = callcullateHeight()
                tableViewHeight.constant = (callcullateHeight() - 145 )
                tableView.reloadData()
                self.addressLabel.text = invoiceVM.getAddress()
                //Giveing Date And Time on place Of Address when call type is TeleCall
                if invoiceVM.getCallType1() == 3{
                    self.mapPinImageView.isHidden = true
                    self.addressLabel.text = GenericUtilityMethods.converFormettedDateString(longDate: invoiceVM.bookingTme())
                }
               
            default:
                break
            }
        }
        if !sucess {
            switch errorCode {
                
            case errorCodes.TokenExpired.rawValue:
                apiTag = apiName.rawValue
                self.acessClass.navigationBar = navigationController!
                self.acessClass.getAcessToken()
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                apiTag = apiName.rawValue
                self.acessClass.navigationBar = navigationController!
                self.acessClass.logOutMethod()
                
            case errorCodes.error401.rawValue: break
                
                
            case errorCodes.UserNotFound.rawValue:
                break
                
            default:
                break
            }
        }
        
    }
    
}

extension NewReceiptViewController: PhotoTableViewCellDelegate {
    
    func cellClickForInvoice(isPickup: Bool, index: Int) {
        isPickUP = isPickup
        if isPickup {
            let dataSource = AXPhotosDataSource(photos:   invoiceVM.getAuxPickPhotoImages(), initialPhotoIndex: index)
            let photosViewController = AXPhotosViewController(dataSource: dataSource)
            photoIndex = index
            photosViewController.delegate = self
            self.present(photosViewController, animated: true)
        }else {
            let dataSource = AXPhotosDataSource(photos:   invoiceVM.getAuxDropPhotoImages(), initialPhotoIndex: index)
            let photosViewController = AXPhotosViewController(dataSource: dataSource)
            photoIndex = index
            photosViewController.delegate = self
            self.present(photosViewController, animated: true)
        }
    }
    
}

extension NewReceiptViewController: AXPhotosViewControllerDelegate {
    
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing,
                           viewControllerForLocation location: CGPoint) -> UIViewController? {
        var dataSource: AXPhotosDataSource?
        if isPickUP {
            dataSource = AXPhotosDataSource(photos:   invoiceVM.getAuxPickPhotoImages(), initialPhotoIndex: index)
        }else {
            dataSource = AXPhotosDataSource(photos:   invoiceVM.getAuxDropPhotoImages(), initialPhotoIndex: index)
        }
//        let dataSource = PhotosDataSource(photos:  providerVM.getAuxPhotoImages(), initialPhotoIndex: photoIndex)
        let previewingPhotosViewController = AXPreviewingPhotosViewController(dataSource: dataSource!)
        
        return previewingPhotosViewController
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing,
                           commit viewControllerToCommit: UIViewController) {
        
        if let previewingPhotosViewController = viewControllerToCommit as? AXPreviewingPhotosViewController {
            self.present(AXPhotosViewController(from: previewingPhotosViewController), animated: false)
        }
    }
}


