//
//  ServiceDetailFeeTableViewCell.swift
//  LSP
//
//  Created by Rajan Singh on 02/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

let fellCell = "FeeCell"

class ServiceDetailFeeTableViewCell: UITableViewCell {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var feeAmount: UILabel!
    @IBOutlet weak var feeTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
  
}


