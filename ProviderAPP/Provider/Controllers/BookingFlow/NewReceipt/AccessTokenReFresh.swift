//
//  AccessTokenReFresh.swift
//  LSP
//
//  Created by Rajan Singh on 31/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire


@objc protocol AccessTokeDelegate{
    @objc func recallApi()
}

class AccessTokenReFresh: NSObject {
    
    static var obj:AccessTokenReFresh? = nil
    
    var navigationBar: UINavigationController!
    var acessDelegate: AccessTokeDelegate? = nil
    var disposeBag = DisposeBag()
    
    class func sharedInstance() -> AccessTokenReFresh {
        
        if obj == nil {
            
            obj = AccessTokenReFresh()
        }
        
        return obj!
    }
    
    /// acess token api call
    func getAcessToken() {
        
       
        
        
        
    }
    
    
    
    func networkRequestApiCall(serviceName: String, requestType: Alamofire.HTTPMethod, parameters: [String : Any]?, headerParam:HTTPHeaders?, responseType: RequestType) {
        
        let mainUrl = API.BASE_URL + serviceName
        
        RxAlamofire
            .requestJSON(requestType, mainUrl ,parameters:parameters, headers: headerParam)
            .debug()
            .subscribe(onNext: { (r, json) in
                if  var dict  = json as? [String:Any]{
                    var statuscode:Int = r.statusCode
                    if dict["code"] != nil{ statuscode = (dict["code"] as? Int)!}
                    dict["resposeType"] = responseType
                    GenericUtilityMethods.hidePI()
                    self.ParseAPIResponse(response: dict, statusCode: statuscode, methodName: requestType.rawValue, responseType: responseType, completion: { (response) in
                        
                    }, failure: { (error) in
                        self.showErrorMessage(error: error)
                    })
                    
                }
            }, onError: {  (error) in
               
            })
            .disposed(by: disposeBag)
        
        
    }
    
    /// error message on failure of the api call
    ///
    /// - Parameter error: error details
    func showErrorMessage(error:Error) {
        
        GenericUtilityMethods.hidePI()
        Helper.alertVC(errMSG: error.localizedDescription)
       
    }
    
    
    /// Parse data To Response which contains all the data, status Code , RequestType
    ///
    /// - Parameters:
    ///   - response: data in [String : Any] Format
    ///   - statusCode: status Code
    ///   - methodName: Api method name
    ///   - responseType: type of request
    ///   - completion: On sucessfull call Of the Api
    ///   - failure: returns an error when the api fails to get called
    func ParseAPIResponse(response:[String : Any],
                          statusCode:Int,
                          methodName:String,
                          responseType: RequestType,
                          completion: @escaping(APIResponseModel) -> Void,
                          failure:@escaping (Error) -> Void) {
      
        
        if statusCode > 0 {
            
            switch statusCode {
                
            case HTTPSCommonErrorCodes.BadRequest.rawValue:
                break
                
            case HTTPSCommonErrorCodes.InternalServerError.rawValue:
                
               break
                
            default:
                
                let responseModel:APIResponseModel!
                
               
                
               
                
            }
            
        }
        else {
            var error:NSError = NSError(domain: "", code: -60, userInfo: [:])
            
        
            
        }
        
    }
    
    /// common errors
    ///
    /// - Parameter response: response
    /// - Returns: Error
    func getCommonErrorDetails(response:[String : Any]) -> Error {
        
        var error:NSError = NSError(domain: "", code: -60, userInfo: [:])
        
        if let message = response["message"] as? String {
            
            let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey       : NSLocalizedString(message, comment: message),
                                                NSLocalizedFailureReasonErrorKey: NSLocalizedString(message, comment: message)]
            error = NSError(domain: "", code: -60, userInfo: userInfo as! [String : Any])
            
        }
        return error
    }
    
    
    
    //MARK - WebService Response -
    func webServiceResponse(response:APIResponseModel, requestType:RequestType)
    {
        GenericUtilityMethods.hidePI()
        if (response.data[SERVICE_RESPONSE.Error] != nil) {
            
          
            return
        }
        
        switch requestType
        {
        case RequestType.refressAccessToken:
            
            switch response.httpStatusCode {
                
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                
                self.recalApi()
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                logOutMethod()
                break
                
            default:
                
                if let errorMessage = response.data[SERVICE_RESPONSE.ErrorMessage] as? String {
                    
                }
                
                
                break
            }
            
        default:
            
            break
        }
        
    }
    
    func recalApi(){
        
//        if (self.acessDelegate != nil) {
        
            self.acessDelegate?.recallApi()
//    }
    }
    
    
    func logOutMethod() {
        
       
        /*
         CouchDBDocument.sharedInstance().deleteDocument(database: CouchDBObject.sharedInstance().database, id: Utility.searchAddressDocId)
         
         CouchDBDocument.sharedInstance().deleteDocument(database: CouchDBObject.sharedInstance().database, id: Utility.manageAddressDocId)
         
         CouchDBDocument.sharedInstance().deleteDocument(database: CouchDBObject.sharedInstance().database, id: Utility.defaultCardDocId)
         
         */
      
    }
    
    
}

