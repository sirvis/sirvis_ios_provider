//
//  InvoicePaymentTVCell.swift
//  LSP
//
//  Created by Rajan Singh on 09/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class InvoicePaymentTVCell: UITableViewCell {

    @IBOutlet weak var feeAmountLabel: UILabel!
    @IBOutlet weak var feeImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
