//
//  InvoiceModel.swift
//  LSP
//
//  Created by Rajan Singh on 09/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class InvoiceModel: NSObject {
    
    struct accounting {
        var amount                      = Float()
        var cancellationFee             = Float()
        var discount                    = Float()
        var cardNo                      = String()
        var paymentMethod               = Int()
        var currencySymbol              = String()
        var currencyAbr                 = Bool()
        var paymentName                 = String()
        var totall                      = Float()
        var visitFee                    = Float()
        var travelFee                   = Float()
        var captureAmount               = Float()
        var additionSerFee              = Float()
        var paidByWallet                = Bool()
        var remainingAmount             = Float()
        var totalActualHo               = Int()
        var bidAmount                   = Float()
        
        init(amount: Float, cancellationFee: Float, discount: Float, cardNo: String, paymentMethod: Int, paymentName: String, totall: Float, visitFee: Float, travelFee: Float, captureAmount: Float, paidByWallet: Bool, remainingAmount: Float, totalActualHo: Int, currencySymbol: String, currencyAbr: Bool, additionSerFee: Float, bidAmount: Float) {
            self.amount = amount
            self.additionSerFee = additionSerFee
            self.currencyAbr = currencyAbr
            self.cancellationFee = cancellationFee
            self.discount = discount
            self.cardNo = cardNo
            self.paymentMethod = paymentMethod
            self.paymentName = paymentName
            self.totall = totall
            self.visitFee = visitFee
            self.travelFee = travelFee
            self.captureAmount = captureAmount
            self.paidByWallet = paidByWallet
            self.remainingAmount = remainingAmount
            self.totalActualHo = totalActualHo
            self.currencySymbol = currencySymbol
            self.bidAmount = bidAmount
        }
    }
    
    struct gigData {
        var name                            = String()
        var price                           = Float()
        var unit                            = Int()
        
        init(name: String, price: Float, unit: Int) {
            self.name = name
            self.price = price
            self.unit = unit
        }
    }
    
    struct additionalSer {
        var name                            = String()
        var price                           = Float()
        
        
        init(name: String, price: Float) {
            self.name = name
            self.price = price
        }
    }
    
    
    struct invoiceDetail {
        var address                         = String()
        var bookingTime                     = String()
        var fullName                        = String()
        var initialJonP                     = String()
        var finalJobP                       = String()
        var photoMessage                    = String()
        var profilePic                      = String()
        var callType                        = Int()
        var status                          = Int()
        var statusMsg                       = String()
        var bookingModel                    = Int()
        var category                        = String()
        var bookingId                       = String()
        var signUrl                         = String()
        var acountingData                   = [accounting]()
        var gigDatas                        = [gigData]()
        var aditionalS                      = [additionalSer]()
        var pickUpImages                    = [String]()
        var dropImages                      = [String]()
        var pickupNotes                     = String()
        var dropNotes                       = String()
        var totalVat                        = Double()
        
        
        init(pickupNotes:String,dropNotes:String,address: String, bookingTime: String, fullName: String, profilePic: String, status: Int, callType: Int,  category: String, bookingId: String, bookingModel: Int, initialJonP: String, finalJobP: String, photoMessage: String, signUrl: String, acountingData: [accounting], gigDatas: [gigData], statusMsg: String, aditionalS: [additionalSer], pickUpImages: [String],dropImages: [String] , vat : Double) {
            self.dropNotes = dropNotes
            self.pickupNotes = pickupNotes
            self.address = address
            self.callType = callType
            self.bookingTime = bookingTime
            self.fullName = fullName
            self.profilePic = profilePic
            self.status = status
            self.category = category
            self.bookingId = bookingId
            self.signUrl = signUrl
            self.bookingModel = bookingModel
            self.acountingData = acountingData
            self.gigDatas = gigDatas
            self.statusMsg = statusMsg
            self.aditionalS = aditionalS
            self.initialJonP = initialJonP
            self.finalJobP = finalJobP
            self.photoMessage = photoMessage
            self.pickUpImages = pickUpImages
            self.dropImages = dropImages
            self.totalVat = vat
        }
    }
    
    var invoiceData_Dict = [invoiceDetail]()
    var requestType: RequestType? = nil
    var httpStatusCode:Int = 0
    
    func initializeData(dataDict: [String:Any], httpsErrorCode: Int){
        print(dataDict)
        let mainDataDict = GenericUtility.dictionaryForObj(object: dataDict[SERVICE_RESPONSE.DataResponse])
        var gigAData = [gigData]()
        var account = [accounting]()
        var addition_Ser = [additionalSer]()
        let accountDict = GenericUtility.dictionaryForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.accounting]!)
        let accountDa = accounting.init(
            amount: GenericUtility.floatForObj(object: accountDict[SERVICE_RESPONSE.ProviderBookings.amount]!) ,
            cancellationFee: GenericUtility.floatForObj(object: accountDict[SERVICE_RESPONSE.ProviderBookings.cancellationFee]!) ,
            discount: GenericUtility.floatForObj(object: accountDict[SERVICE_RESPONSE.ProviderBookings.discount]!) ,
            cardNo: GenericUtility.strForObj(object: accountDict[SERVICE_RESPONSE.ProviderBookings.last4]!),
            paymentMethod: GenericUtility.intForObj(object: accountDict[SERVICE_RESPONSE.ProviderBookings.paymentMethod]!),
            paymentName: GenericUtility.strForObj(object: accountDict[SERVICE_RESPONSE.ProviderBookings.pgName]!),
            totall: Float(accountDict[SERVICE_RESPONSE.ProviderBookings.total]! as! NSNumber),
            visitFee: GenericUtility.floatForObj(object: accountDict[SERVICE_RESPONSE.ProviderBookings.visitFee]!),
            travelFee: Float(accountDict[SERVICE_RESPONSE.ProviderBookings.travelFee]! as! NSNumber),
            captureAmount: accountDict[SERVICE_RESPONSE.ProviderBookings.captureAmount] == nil ? 0.0 : (GenericUtility.floatForObj(object: accountDict[SERVICE_RESPONSE.ProviderBookings.captureAmount]!)),
            paidByWallet: accountDict[SERVICE_RESPONSE.ProviderBookings.paidByWallet] == nil ? false : (GenericUtility.intForObj(object: accountDict[SERVICE_RESPONSE.ProviderBookings.paidByWallet]!) == 0 ? false : true),
            remainingAmount: accountDict[SERVICE_RESPONSE.ProviderBookings.remainingAmount] == nil ? 0.0 : (GenericUtility.floatForObj(object: accountDict[SERVICE_RESPONSE.ProviderBookings.remainingAmount]!)),
            totalActualHo:  accountDict[SERVICE_RESPONSE.ProviderBookings.totalActualHo] != nil ? GenericUtility.intForObj(object: accountDict[SERVICE_RESPONSE.ProviderBookings.totalActualHo]!) : 0 ,
            currencySymbol: GenericUtility.strForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.currencySymbol]!),
            currencyAbr: GenericUtility.intForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.currencyAbbr]!) == 1 ? true : false,
            additionSerFee: accountDict[SERVICE_RESPONSE.ProviderBookings.additionalSFee] == nil ? 0.0 : (GenericUtility.floatForObj(object: accountDict[SERVICE_RESPONSE.ProviderBookings.additionalSFee]!)),
            bidAmount: accountDict[SERVICE_RESPONSE.ProviderBookings.bidAmount] != nil ?  Float(truncating: accountDict[SERVICE_RESPONSE.ProviderBookings.bidAmount]! as! NSNumber) : Float(0.0))
        account.append(accountDa)
        
        let cart = GenericUtility.arrayForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.cart]!)
      //  let gigArray = GenericUtility.arrayForObj(object: cart[SERVICE_RESPONSE.ProviderBookings.checkOutItem])
        if cart.count > 0 {
            for gigDict in cart {
                let gigDataDict = gigData.init(
                    name: GenericUtility.strForObj(object: gigDict[SERVICE_RESPONSE.ProviderBookings.serviceName]!),
                    price: GenericUtility.floatForObj(object: gigDict[SERVICE_RESPONSE.ProviderBookings.amount]!),
                    unit: GenericUtility.intForObj(object: gigDict[SERVICE_RESPONSE.ProviderBookings.GigQuantity]!) )
                gigAData.append(gigDataDict)
            }
        }
        
        let addMSerArray = GenericUtility.arrayForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.aitionalService]!)
        if addMSerArray.count > 0 {
            for addSDict in addMSerArray {
                let addSerData = additionalSer.init(
                    name: GenericUtility.strForObj(object: addSDict[SERVICE_RESPONSE.ProviderBookings.serviceName]!),
                    price: GenericUtility.floatForObj(object: addSDict[SERVICE_RESPONSE.ProviderBookings.Gigprice]!))
                addition_Ser.append(addSerData)
            }
        }
        let taxServices = GenericUtility.arrayForObj(object: accountDict["totalTaxApplied"] ?? "")
        if taxServices.count > 0 {
            for addSDict in taxServices {
                let addSerData = additionalSer.init(
                    name: GenericUtility.strForObj(object: addSDict["taxName"] ?? ""),
                    price: GenericUtility.floatForObj(object: addSDict["taxAmount"] ?? ""))
                addition_Ser.append(addSerData)
            }
        }
        let pickUpImages = GenericUtility.arrayForObj(object: mainDataDict["pickupImages"] ?? [""]) as! [String]
        let dropImages = GenericUtility.arrayForObj(object: mainDataDict["dropImages"] ?? [""]) as! [String]
        
        let providerDetail = GenericUtility.dictionaryForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.providerData]!)
        let invoiceData = invoiceDetail.init(
            pickupNotes: GenericUtility.strForObj(object: mainDataDict["pickupNotes"] ?? ""), dropNotes: GenericUtility.strForObj(object: mainDataDict["dropNotes"] ?? ""), address: GenericUtility.strForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.addLine1]!),
            bookingTime: TimeFormats.changeDateFormate(timeStamp: GenericUtility.strForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.bookingReqDT]!)),
            fullName: GenericUtility.strForObj(object: providerDetail[SERVICE_RESPONSE.ProviderBookings.firstName]!) + GenericUtility.strForObj(object: providerDetail[SERVICE_RESPONSE.ProviderBookings.lastName]!),
            profilePic: GenericUtility.strForObj(object: providerDetail[SERVICE_RESPONSE.ProviderBookings.profilePic]!),
            status: GenericUtility.intForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.status]!),
            callType: GenericUtility.intForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.callType]!),
            category: mainDataDict[SERVICE_RESPONSE.ProviderBookings.categoryId] != nil ? GenericUtility.strForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.categoryId]!) : "" ,
            bookingId: GenericUtility.strForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.bookingId]!),
            bookingModel: GenericUtility.intForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.bookingModel]!), initialJonP: "", finalJobP: "", photoMessage: "",
            signUrl: GenericUtility.strForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.signUrl]!),
            acountingData: account,
            gigDatas: gigAData,
            statusMsg: GenericUtility.strForObj(object: mainDataDict[SERVICE_RESPONSE.ProviderBookings.statusMsg]!),
            aditionalS: addition_Ser,
            pickUpImages: pickUpImages,
            dropImages: dropImages, vat: totVat
        )
        invoiceData_Dict.append(invoiceData)
        requestType = RequestType.invoice
        httpStatusCode = httpsErrorCode
        
    }
    
    
}
