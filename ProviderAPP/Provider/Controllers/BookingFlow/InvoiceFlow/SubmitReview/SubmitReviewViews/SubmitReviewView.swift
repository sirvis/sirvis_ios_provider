//
//  SubmitReviewView.swift
//  LSP
//
//  Created by Rahul Sharma on 10/25/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import FloatRatingView
import Kingfisher
import RxSwift
import RxKeyboard
import RxCocoa

protocol ReviewDetailsdelegate {
    func reviewDetailsTapped()
}

class SubmitReviewView: UIView {

    @IBOutlet weak var bookingDateLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var receiptbtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var rateExperienceLabel: UILabel!
    
     var delegate: ReviewDetailsdelegate?
    
    var isShown:Bool = false
    var bookingID = 0
    var reviewText = ""
    var  ratingBycust = 4.0
    var histModel = HistoryModel()
    var submitRevModel = SubmitReviewModel()
    var signatureURL = ""
    var bookingDict:Accepted?
    let disposeBag = DisposeBag()
    
    private static var share: SubmitReviewView? = nil
    static var instance: SubmitReviewView {
        
        if (share == nil) {
            share = Bundle(for: self).loadNibNamed("SubmitReviewView",
                                                   owner: nil,
                                                   options: nil)?.first as? SubmitReviewView
            
            
        }
        return share!
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ratingView.delegate = self
        ratingView.rating = 4.0
        profileImageView.layer.cornerRadius =  profileImageView.frame.height / 2
        profileImageView.layer.masksToBounds = true
        profileImageView.layer.borderWidth = 2.0
        profileImageView.layer.borderColor = COLOR.APP_COLOR.cgColor
        rateExperienceLabel.text = "Rate your experience with"
    }
    
    
    @IBAction func receiptBtnTapped(_ sender: Any) {
        let Vc:ReceiptPageView = ReceiptPageView.instance
        Vc.signUrl = signatureURL
        Vc.show(bookingData:bookingDict)
    
    }
    
    @IBAction func donrBtnTapped(_ sender: Any) {
        
        let dict : [String : Any] =  ["bookingId"      : bookingID as Any,
                                      "rating"         : ratingBycust,
                                      "review"    : reviewText]
        self.submitRevModel.submitReview(params:dict,completionHandler: { (succeeded) in
            
            if succeeded{
                self.reviewTextView.resignFirstResponder()
               self.hide()
            }else{
                
            }
        })
    }
    
    //Get booking details
    func getBookingData() {
        histModel.getBookingDataById(BookingId: String(bookingID)) { (success, bookingData, BidData) in
            if success {
                self.bookingDict = bookingData
                self.nameLabel.text = bookingData.firstName
                self.totalAmountLabel.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData.totalAmt))
                self.signatureURL = bookingData.signature
                let bookingDate = Helper.getTheScheduleViewDataFormat(timeStamp:bookingData.bookingRequestedFor)
                self.bookingDateLabel.text = "\(bookingDate)"
                
                let imageURLString = bookingData.profilePic
               
                    
                    
                    self.profileImageView.kf.setImage(with: URL(string: imageURLString),
                                               placeholder:UIImage.init(named: "signup_profile_default_image"),
                                               options: [.transition(ImageTransition.fade(1))],
                                               progressBlock: { receivedSize, totalSize in
                    },
                                               completionHandler: { image, error, cacheType, imageURL in
                    })
                    
                    
                
                
                
            }
        }
    }
    
    func show() {
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
    }
    
    func hide() {
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
            SubmitReviewView.share = nil
        }
    }
    
    
}

extension SubmitReviewView: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        reviewTextView = textView
        if textView.text == "Tell us more"{
            textView.text = nil
            textView.textColor = UIColor.black
            
        }
        let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.size.height)
        scrollView.setContentOffset(bottomOffset, animated: true)
       // self.moveViewUp(activeView: reviewTextView, keyboardHeight: 250)
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if textView.text.isEmpty {
            textView.text = "Tell us more"
            textView.textColor = UIColor.lightGray
        }else{
            if textView.text  != "Tell us more"{
                reviewText = textView.text
            }
        }
        self.scrollView.contentInset.bottom = 0
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    

}

//MARK: - Rating view delegate

extension SubmitReviewView: FloatRatingViewDelegate{
    /**
     Returns the rating value when touch events end
     */
    
    public func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        ratingBycust = Double(rating)
    }
}
