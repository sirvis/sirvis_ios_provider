//
//  TimerViewControllerExtension.swift
//  LSP
//
//  Created by Vengababu Maparthi on 06/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import Kingfisher


extension TimerViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension TimerViewController:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if  bookingDict?.callType == .Incall{
            return 5
        }
      else if bookingDict?.callType == .TeleCall {
            return 9
        }else{
            return 9
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if bookingDict?.callType == .Incall || bookingDict?.callType == .TeleCall{
            switch section {
            case 0:
                return 3
            case 1:
                return 3
            case 2:
                return 4
            case 3:
                if (bookingDict?.questionAnswers.count)! > 0 {
                    return 1 + (bookingDict?.questionAnswers.count)!
                }
                else {
                    return 0
                }
            default:
                return 3
            }
        }else{
            switch section {
            case 0:
                return 0
            case 1:
                return 2
            case 2:
                return 3
            case 3:
                if  bookingDict?.bookingModel == .Bidding{
                    return 0
                }
                else {
                return  2 + (self.bookingDict?.services.count)!
                }
                
            case 4:
                if  bookingDict?.bookingModel == .Bidding{
                    return 0
                }
                return bookingDict!.paymentServices.count
                
            case 6:
                 return 1 + (bookingDict?.questionAnswers.count)!
                
            case 5:
                if  bookingDict?.bookingModel == .Bidding{
                    return 0
                }
                return 1
            case 7:
                return 3
            case 8:
                
                if bookingDict?.jobDesc == "" {
                    return 0
                }
                else {
                    return 4
                }
                
            default:
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderTableCell
        let mediumView = tableView.dequeueReusableCell(withIdentifier: "mediumView") as! MediumViewTableCell
        
        if bookingDict?.callType == .Incall || bookingDict?.callType  == .TeleCall  {
            switch indexPath.section{
            case 0:
                switch indexPath.row{
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddress") as! AddressBookingTableCell
                    cell.selectionStyle = .none
                    cell.custAddress.text = bookingDict?.addLine1
                    return cell
                case 1 :
                    return mediumView
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "timerProfile") as! CustProfileBookingTableCell
                    cell.selectionStyle = .none
                    cell.custName.text = bookingDict?.firstName
                    cell.categoryName.text = bookingDict?.categoryName
                    return cell
                }
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "totalAmt") as! TotalAmtTableCell
                switch indexPath.row{
                case 0 :
                    return mediumView
                case 1:
                    header.headerOfCell.text = "TOTAL BILL AMOUNT"
                    return header
                default:
                    let dateArray = Helper.getTheDateFromTimeStamp(timeStamp:bookingDict!.actuallGigTimeStart).components(separatedBy: "|")
                    
                    cell.dateOfAppointment.text = "Date: " + dateArray[0]
                    cell.timeOfAppointment.text = "Time: " + dateArray[1]
                    
                    
                    let total = bookingDict?.totalAmt
                    cell.totalAmt.text =  Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))
                    return cell
                }
            case 2:
                
                switch indexPath.row{
                case 0 :
                    return mediumView
                case 3:
                    let totalCel = tableView.dequeueReusableCell(withIdentifier: "finalAmt") as! FinalAmtTableCell
                    let total = bookingDict?.totalAmt
                    totalCel.totalAmt.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))
                    return totalCel
                case 2:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "timerEvents") as! BookingEventsTableCell
                    cell.key.text = "Discount"
                    cell.value.text = (bookingDict?.currencySymbol)! + "\(bookingDict!.discount)"
                    return cell
                default:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "timerEvents") as! BookingEventsTableCell
                    cell.key.text = "Consultation Fee"
                    cell.value.text = (bookingDict?.currencySymbol)! + "\(bookingDict!.totalHourlyFee)"
                    return cell
                }
                
            case 3:
                switch indexPath.row{
                case 0:
                    return mediumView
                    
                default:
                    if bookingDict?.questionAnswers[indexPath.row - 1].type == 10  && bookingDict?.questionAnswers[indexPath.row - 1].answer != "" {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "jobPhotos") as! JobPhotosTableCell
                        let urlStringArr = bookingDict?.questionAnswers[indexPath.row - 1].answer.components(separatedBy: ",")
                        
                        cell.photoUrls = urlStringArr!
                        cell.setupCell()
                        return cell
                        
                    }
                    else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionInnerCell") as! QuestionInnerCell
                        cell.questionLabel.text = "Q:" + (bookingDict?.questionAnswers[indexPath.row - 1].question)!
                        cell.answerLabel.text = bookingDict?.questionAnswers[indexPath.row - 1].answer
                        return cell
                    }
                }
                
            default:
                
                switch indexPath.row{
                case 0:
                    return mediumView
                case 1:
                    header.headerOfCell.text =  "Payment Method"
                    return header
                    
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "paymentType") as! PaymentMethodTableCell
                    if (bookingDict?.paidByWallet)! {
                        cell.paymentType.text = "Wallet" + " + " + (bookingDict?.paymentMethod)!
                    }else{
                        cell.paymentType.text = bookingDict?.paymentMethod
                    }
                     if bookingDict?.paymentMethod == "Cash" {
                                       cell.paymentTypeImage.image = #imageLiteral(resourceName: "cash")
                                   } else {
                                       cell.paymentTypeImage.image = #imageLiteral(resourceName: "card")
                                   }
                    return cell
                }
            }
        }else{ //outcall, telecall
            
            switch indexPath.section {
            case 0:
                switch indexPath.row{
                case 0:
                    return mediumView
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddress") as! AddressBookingTableCell
                    cell.selectionStyle = .none
                    cell.custAddress.text = bookingDict?.addLine1
                    return cell
                }
            case 1:
                switch indexPath.row{
                case 0:
                    return mediumView
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "timerProfile") as! CustProfileBookingTableCell
                    cell.selectionStyle = .none
                    cell.custName.text = bookingDict?.firstName
                    cell.categoryName.text = bookingDict?.categoryName
                    
                    return cell
                }
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "totalAmt") as! TotalAmtTableCell
                switch indexPath.row{
                case 0 :
                    return mediumView
                case 1:
                    header.headerOfCell.text = "TOTAL BILL AMOUNT"
                    return header
                default:
                    let dateArray = Helper.getTheDateFromTimeStamp(timeStamp:bookingDict!.actuallGigTimeStart).components(separatedBy: "|")
                    
                    cell.dateOfAppointment.text = "Date: " + dateArray[0]
                    cell.timeOfAppointment.text = "Time: " + dateArray[1]
                    
                    
                    let total = bookingDict?.totalAmt
                    cell.totalAmt.text =  Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))
                    return cell
                }
                
            case 3:
                
               
               
                    let cell = tableView.dequeueReusableCell(withIdentifier: "timerEvents") as! BookingEventsTableCell
                    cell.selectionStyle = .none
                    
                    switch indexPath.row{
                    case 0:
                        return mediumView
                    case 1:
                        header.headerOfCell.text =  "PAYMENT BREAKDOWN"
                        return header
                        
                    default:
                        
                        if bookingDict?.serviceType == .Fixed{
                            cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)! + " * " + String(describing:bookingDict!.services[indexPath.row - 2].quantity)
                            cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.services[indexPath.row - 2].servicePrice))
                        }else{
                            
                            cell.key.text = Helper.timeInHourMin((bookingDict?.totalJobTime)!)
                            cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.totalHourlyFee))
                        }
                        return cell
                    }
                
                
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "timerEvents") as! BookingEventsTableCell
                cell.key.text = bookingDict!.paymentServices[indexPath.row].key
                cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.paymentServices[indexPath.row].value))
                return cell
                
                
            case 6:
                switch indexPath.row{
                case 0:
                    return mediumView
                    
                default:
                    if bookingDict?.questionAnswers[indexPath.row - 1].type == 10  && bookingDict?.questionAnswers[indexPath.row - 1].answer != "" {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "jobPhotos") as! JobPhotosTableCell
                        let urlStringArr = bookingDict?.questionAnswers[indexPath.row - 1].answer.components(separatedBy: ",")
                        
                        cell.photoUrls = urlStringArr!
                        cell.setupCell()
                        return cell
                        
                    }
                    else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionInnerCell") as! QuestionInnerCell
                        cell.questionLabel.text = "Q:" + (bookingDict?.questionAnswers[indexPath.row - 1].question)!
                        cell.answerLabel.text = bookingDict?.questionAnswers[indexPath.row - 1].answer
                        return cell
                    }
                }
            case 5:
                let totalCel = tableView.dequeueReusableCell(withIdentifier: "finalAmt") as! FinalAmtTableCell
                
                let total = bookingDict?.totalAmt
                
                totalCel.totalAmt.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))
                
                return totalCel
                
            case 7:
                switch indexPath.row{
                case 0:
                    return mediumView
                case 1:
                    header.headerOfCell.text =  "Payment Method"
                    return header
                    
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "paymentType") as! PaymentMethodTableCell
                    if (bookingDict?.paidByWallet)! {
                        cell.paymentType.text = "Wallet" + " + " + (bookingDict?.paymentMethod)!
                    }else{
                        cell.paymentType.text = bookingDict?.paymentMethod
                    }
                    if bookingDict?.paymentMethod == "Cash" {
                        cell.paymentTypeImage.image = #imageLiteral(resourceName: "cash")
                    } else {
                        cell.paymentTypeImage.image = #imageLiteral(resourceName: "card")
                    }
                    return cell
                }
                
            default:
                switch indexPath.row{
                case 0:
                    return mediumView
                case 3:
                    return mediumView
                case 1:
                    header.headerOfCell.text =  "Job Description"
                    return header
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "jobDesc") as! JobDescTableCell
                    if bookingDict?.jobDesc == "" {
                        cell.jobDescription.text = "This booking does not have a description"
                    }
                    else {
                        cell.jobDescription.text = bookingDict!.jobDesc
                    }
                    return cell
                }
            }
        }
    }
}
