//
//  UploadItemPhotoController.swift
//  LSP
//
//  Created by Rahul Sharma on 2/6/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

protocol didChooseImageAndNotesDelegate: class {
    func didChooseImageNotes(notes: String, imageURLs: [String])
    
    
}

class UploadItemPhotoController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    var imageurl = ""
    var notes = ""
    var pickedImage: UIImage?
    weak var delegate: didChooseImageAndNotesDelegate?
    var uploadImageModel: UploadImage?
    var imageUrls = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.estimatedRowHeight = 30.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.reloadData() 
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmBtnTapped(_ sender: Any) {
      
        
//        if imageUrls.count == 0 {
//            Helper.alertVC(errMSG: "Please upload at least one image")
//        } else {
            self.delegate?.didChooseImageNotes(notes: notes, imageURLs: imageUrls)
        self.navigationController?.popViewController(animated: true)
       // }
    }
    
    
    
    //open actionsheet
    
    @objc func openActionSheet() {
        let actionSheetController: UIAlertController = UIAlertController(title: signup.selectImage as String?,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: signup.cancel as String?,
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: signup.camera,
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: signup.gallery as String?,
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    // MARK: - choose photo from gallery
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - choose Camera
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
   
}

extension UploadItemPhotoController: UIImagePickerControllerDelegate {

  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    let pickedImage = info[.originalImage] as? UIImage
  
//    if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
        self.pickedImage = pickedImage!
        self.tableView.reloadData()
        Helper.showPI(message: "Loading".localized)
    UploadImageModel().updateTheImageToAamzon(image: pickedImage!, imagePath: AMAZONUPLOAD.WORKIMAGES +  Helper.currentTimeStamp  + ".png") { (success, imageURL) in
            
            DispatchQueue.main.async {
                self.imageUrls.append(imageURL)
                Helper.hidePI()
            }
            
            
            
            
        }
        picker.dismiss(animated: true, completion: nil)
//    }
    
}}

extension UploadItemPhotoController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
 
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryHeader") as! CategoryHeaderTableCell
        if section == 0 {
            cell.titleOfDoc.text = "Photos".localized
        } else {
            cell.titleOfDoc.text = "Notes".localized
        }
        
        return cell.contentView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UploadImageCell") as! UploadImageCell
            cell.delegate = self
            
           
            if pickedImage != nil {
                cell.pickedImages.append(pickedImage!)
                
            }
             cell.collectionView.reloadData()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotesCell") as! NotesCell
        
            return cell
        }
        
    }
    
}


extension UploadItemPhotoController: UITextViewDelegate {
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        return true
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        notes = textView.text
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

extension UploadItemPhotoController: UploadImageDelegate {
    func didTapUploadImage() {
        openActionSheet()
    }
}


