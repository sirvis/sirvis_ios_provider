//
//  TimerViewController.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 03/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import JaneSliderControl
import Kingfisher

class TimerViewController: UIViewController {
    @IBOutlet weak var startTimeButton: UIButton!
    @IBOutlet weak var sliderView: SliderControl!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var bookingIDLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var timerTableview: UITableView!
    @IBOutlet weak var videoCallBtn: UIButton!
    @IBOutlet weak var audioCallBtn: UIButton!
    @IBOutlet weak var chatBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    var bookingMod = BookingModel()
    var bookingDict:Accepted?
    var status:Int = 0
    var bookingTimer  = Timer()
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    var eventModel   = Events()
    var isInitialFlow = true
    var remainingTime:Int = 0
    var isImageUploaded = false
    var uploadedImageUrl = [String]()
    var pickupNotes = ""
    var dropImageUrl = [String]()
    var dropNotes = ""
    var isFromChatScreen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupControllerData()
       
         NotificationCenter.default.addObserver(self, selector: #selector(bookingCancelHandling(_:)), name: Notification.Name("cancelledBooking"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(bookingCancelHandling(_:)), name: Notification.Name("BookingExpired"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
      //   self.navigationController?.hideTransparentNavigationBar()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "cancelledBooking"), object: nil);
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "BookingExpired"), object: nil);
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        bookingTimer.invalidate()
    }
    
    //set up timer data
    func initateTheTimerControllerData()  {
        bookingIDLabel.text = "Job ID".localized + " \((bookingDict?.bookingId)!)"
        status = (bookingDict?.statusCode)!
        startTimeButton.layer.borderColor = COLOR.APP_COLOR.cgColor
        startTimeButton.layer.borderWidth = 2
        
        if status == 2 || status == 8 {
            startTimeButton.isHidden = false
            if bookingDict?.bookingTime?.status == 1 {
                startTimeButton.isSelected = true
                remainingTime = self.getCreationTimeInt(expiryTimeStamp:(bookingDict?.bookingTime?.startTimeStamp)!)  + (bookingDict?.bookingTime?.second)!
                if !bookingTimer.isValid  && remainingTime > 0{
                    bookingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
                }
            }
            else if bookingDict?.bookingTime?.status == 0 {
                startTimeButton.isSelected = false
                remainingTime = (bookingDict?.bookingTime?.second)!
            }
            timerLabel.text = self.timeFormate(Int(remainingTime))
        }else{
            startTimeButton.isHidden = true
        }
        self.updateCustomerInfoStatus()
    }
    
    //Call Booking details API
    func FetchBookingDetails() {
        eventModel.assignedBookingAPI { (eventData) in
            for eachAccepetData in eventData.acceptedData {
                if eachAccepetData.bookingId == self.bookingDict?.bookingId {
                    self.bookingDict = eachAccepetData
                    self.initateTheTimerControllerData()
                }
            }
            
        }
    }
    
    //
    func canStartTeleCall() -> Bool {
        let currentTime = Date()
        let bookingStartTimeInDate = NSDate(timeIntervalSince1970: TimeInterval(bookingDict?.actuallGigTimeStart ?? 0))
        let bookingEndtime = NSDate(timeIntervalSince1970: TimeInterval(bookingDict?.bookingRequestEnds ?? 0))
        
        let prediff = Int(bookingStartTimeInDate.timeIntervalSince1970 - currentTime.timeIntervalSince1970)  //booking start time and current time difference
        let postdiff = Int(bookingEndtime.timeIntervalSince1970 - currentTime.timeIntervalSince1970) ////booking end time and current time difference
        
        if prediff <= bookingDict?.callDuration ?? 0 ||  postdiff <= bookingDict?.callDuration ?? 0 {
            return true
            
        } else {
            return false
        }
        
    }
    
    
    //setup call buttons for telecall flow
    
    func setupButtonsTelecall() {
        if bookingDict?.statusCode == 9 {
            bottomView.isHidden =  true
            videoCallBtn.isHidden = true
            audioCallBtn.isHidden = true
            chatBtn.isHidden = true
            cancelBtn.isHidden = true
        } else {
            bottomView.isHidden =  false
            audioCallBtn.isHidden = false
            chatBtn.isHidden = false
            cancelBtn.isHidden = false
            videoCallBtn.isHidden = false
        }
    }
    
    // setup Controller UI and Data
    func setupControllerData() {
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(true)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        
        
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        
        if bookingDict?.callType == .TeleCall  {
            headerView.isHidden = true
            
            
            var frame = self.timerTableview.tableHeaderView!.frame;
            frame.size.height = 0;
            self.timerTableview.tableHeaderView?.frame  = frame
            
           setupButtonsTelecall()
            
        }else if bookingDict?.callType == .Incall{
             headerView.isHidden = true
            videoCallBtn.isHidden = true
            audioCallBtn.isHidden = false
            chatBtn.isHidden = false
            var frame = self.timerTableview.tableHeaderView!.frame;
            frame.size.height = 0;
            self.timerTableview.tableHeaderView?.frame  = frame
            cancelBtn.isHidden = false
        }else{
            headerView.isHidden = false
            videoCallBtn.isHidden = true
            audioCallBtn.isHidden = true
            chatBtn.isHidden = true
            cancelBtn.isHidden = true
        }
        FetchBookingDetails()
        setupGestureRecognizer()
    }
    
    
   
    
    
    ///********notifies when the booking has been cancelled*******//
    @objc  func bookingCancelHandling(_ notification: NSNotification) {
        if let notiDict = notification.userInfo as? [AnyHashable:Any] {
            if let bid = notiDict["bookingId"] as? Int {
                if String(describing:bid)  == String(describing:bookingDict!.bookingId) {
                    _ = navigationController?.popToRootViewController(animated: true)
                }
            }
        }
       
       
    }
    @IBAction func sliderChanged(_ sender: SliderControl) {
        print("Changed")
    }

    @IBAction func slideCancelled(_ sender: SliderControl) {
        print("Cancelled")
    }
    
    @IBAction func sliderFinished(_ sender: Any) {
        if bookingDict?.callType == .OutCall {
            if status == 8 {
                if isImageUploaded {
                    self.updateBookingStatus(status:status)
                    isImageUploaded = false
                } else {
                    askForImageUpload()
                }
            } else if status == 9{
                if isImageUploaded {
                    self.updateBookingStatus(status:status)
                    isImageUploaded = false
                } else {
                    askForImageUpload()
                }
            }else{
                self.updateBookingStatus(status:status)
            }
        } else {
             self.updateBookingStatus(status:status)
            if bookingDict?.callType == .TeleCall {
                self.navigationController?.popToRootViewController(animated: true)
                
            }
        }
        
    
        
    }
    
    //MARK: Cancel booking butn tapped
    
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        let cancelView = CancelView.instance
        cancelView.bookingID = (bookingDict?.bookingId)!
        cancelView.bookingType = (bookingDict?.bookingType)!
        if (bookingDict?.bookingType)! == .Schedule{
            cancelView.startTime = (bookingDict?.bookingRequestedFor)!
            cancelView.entTime = (bookingDict?.bookingRequestEnds)!
        }
        cancelView.getTheCancelData()
        cancelView.delegate = self
        cancelView.show()
        
    }
    
    @IBAction func startTimeAction(_ sender: Any) {
        if startTimeButton.isSelected {
            startTimeButton.isSelected = false
            self.updateTimeStatusToServer(status:0)
            self.bookingTimer.invalidate()
        }else{
            startTimeButton.isSelected = true
            if remainingTime == 0{
                remainingTime = (bookingDict?.bookingTime?.second)!
            }
            self.updateTimeStatusToServer(status:1)
        }
    }
    
    
    @IBAction func videoCallBtnTapped(_ sender: Any) {
        
        if canStartTeleCall() {
        
            
            let window = UIApplication.shared.keyWindow!
            window.endEditing(true)
            let audioView = VideoCallView(frame: CGRect(x:0, y:0, width: window.frame.width, height: window.frame.height))
            audioView.usersName =  (bookingDict?.firstName) ?? ""
            audioView.userNameLabel.text =  (bookingDict?.firstName) ?? ""
            audioView.userID = (bookingDict?.customerId)!
            if bookingDict?.bookingId != nil {
                audioView.bookingID = String(bookingDict!.bookingId)
            }
            //audioView.bookingID = String(describing: bookingDict!.bookingId)
            let imageURL = (bookingDict?.profilePic)!
            audioView.callID = "\(Int.random(in: 0...10000000000))"
            audioView.userImage.kf.setImage(with: URL(string: imageURL),
                                                placeholder:UIImage.init(named: "signup_profile_default_image"),
                                                options: [.transition(ImageTransition.fade(1))],
                                                progressBlock: { receivedSize, totalSize in
            },
                                                completionHandler: { image, error, cacheType, imageURL in
            })
            audioView.videoCallFromTo()
            window.addSubview(audioView)
            
            
            
        } else {
            Helper.alertVC(errMSG: "You cannot start Telecall before".localized + " \(bookingDict!.callDuration)" + " sec of scheduled booking time".localized)
        }
       
    }
    
    @IBAction func audioCallBtnTapped(_ sender: Any) {
        if bookingDict?.callType == .Incall {
           
            let dropPhone = "tel://" + (bookingDict?.mobileNum)!
            if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
                
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        } else { //Telecall
            
            if canStartTeleCall() {
                let window = UIApplication.shared.keyWindow!
                window.endEditing(true)
                let audioView = AudioCallView(frame: CGRect(x:0, y:0, width: window.frame.width, height: window.frame.height))
                audioView.userNameLbl.text =  (bookingDict?.firstName) ?? ""
                audioView.callerID = (bookingDict?.customerId)!
                
                if bookingDict?.bookingId != nil {
                    audioView.bookingID = String(bookingDict!.bookingId)
                }
            
                let imageURL = (bookingDict?.profilePic)!
                audioView.callId = "\(Int.random(in: 0...10000000000))"
                audioView.userImageView.kf.setImage(with: URL(string: imageURL),
                                                    placeholder:UIImage.init(named: "signup_profile_default_image"),
                                                    options: [.transition(ImageTransition.fade(1))],
                                                    progressBlock: { receivedSize, totalSize in
                },
                                                    completionHandler: { image, error, cacheType, imageURL in
                })
                audioView.startingTheCall()
                window.addSubview(audioView)
            } else {
                  Helper.alertVC(errMSG: "You can start Telecall before ".localized + "\(bookingDict!.callDuration) " + " sec of scheduled booking time".localized)
            }
            
            
         }
       
        
    }
    
    @IBAction func chatBtnTapped(_ sender: Any) {
        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat") as? ChatVC {
            
           
                controller.customerID = (bookingDict?.customerId)!
                controller.bookingID = String(describing:bookingDict!.bookingId)
                controller.custImage = (bookingDict?.profilePic)!
                controller.custName =  (bookingDict?.firstName)!
                controller.jobDesc = (bookingDict?.categoryName)!
                controller.amt = (bookingDict?.totalAmt)!
                controller.bookingModel = (bookingDict?.bookingModel)!
                controller.isFromBookingFlow = true
            
            
            if let navigator = navigationController {
                TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                          subType: CATransitionSubtype.fromTop.rawValue,
                                                                          for: (self.navigationController?.view)!,
                                                                          timeDuration: 0.3)
                navigator.pushViewController(controller, animated: false)
            }
        }
    }
    
    @IBAction func messageViewAction(_ sender: Any) {
        
        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat") as? ChatVC {
            controller.customerID = (bookingDict?.customerId)!
            controller.bookingID = String(describing:bookingDict!.bookingId)
            controller.custImage = (bookingDict?.profilePic)!
            controller.custName =  (bookingDict?.firstName)!
            controller.delegate = self
            if let navigator = navigationController {
                TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                          subType: CATransitionSubtype.fromTop.rawValue,
                                                                          for: (self.navigationController?.view)!,
                                                                          timeDuration: 0.3)
                navigator.pushViewController(controller, animated: false)
            }
        }
    }
    
    @IBAction func callingAction(_ sender: Any) {
        let dropPhone = "tel://" + (bookingDict?.mobileNum)!
        if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
            
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    
    func updateTimeStatusToServer(status:Int)  {
        startTimeButton.isHidden = false
        
        let val =  remainingTime
        let params : [String : Any] = [
            "bookingId":bookingDict!.bookingId as Any,
            "status":status,
            "second": val]
        
        bookingMod.updateBookingTimer(dict: params ,completionHandler: { (succeeded) in
            if succeeded{
                self.bookingDict?.bookingTime?.status = status
                if  self.startTimeButton.isSelected {
                    if !self.bookingTimer.isValid {
                        self.bookingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
                    }
                }
            }else{
                
            }
        })
    }
    
    
    //MARK: open popup for image upload
    
    func askForImageUpload() {
       if bookingDict?.callType == CallType.OutCall {
//            let alert = UIAlertController(title: "Message".localized, message: "Are you sure you want to continue  without uploading a photo of the job?".localized, preferredStyle: .alert)
//            let yesButton = UIAlertAction(title: "Continue".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                self.updateBookingStatus(status:self.status)
//            })
//            let noButton = UIAlertAction(title: "Add Photo".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
//                self.openUploadImageScreen()
//            })
//
//            alert.addAction(yesButton)
//            alert.addAction(noButton)
//            present(alert, animated: true)
        }
        else {
             self.updateBookingStatus(status:8)
        }
        
       
    }
    
    
    //MARK: open UploadItemPhotoController
    func openUploadImageScreen() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let itemVC = sb.instantiateViewController(withIdentifier: "UploadItemPhotoController") as? UploadItemPhotoController
        itemVC?.delegate = self
        self.navigationController?.pushViewController(itemVC!, animated: true)
    }
    
    @objc func timerTick() {
        remainingTime = remainingTime + 1
        timerLabel.text = self.timeFormate(Int(remainingTime))
    }
    
    /**
     *  Check for Time formate
     *
     *  @param remainingTime Remaining time
     *
     *  @return Return must 00:00/00
     */
    func timeFormate(_ remainingTime: Int) -> String {
        
        let Hours : Int = remainingTime / 3600
        let min: Int = Int(fmod(Double(remainingTime), 3600) / 60)
        let secs: Int = remainingTime % 60
        
        
        if remainingTime == 0 {
            bookingTimer.invalidate()
            return "00:00:00"
        }else{
            var hoursString = ""
            var minsString = ""
            var secsString = ""
            if Hours < 10{
                hoursString = "0" + String(describing:Hours)
            }else{
                hoursString = String(describing:Hours)
            }
            
            if min < 10{
                minsString = "0" + String(describing:min)
            }else{
                minsString = String(describing:min)
            }
            
            if secs < 10{
                secsString = "0" + String(describing:secs)
            }else{
                secsString = String(describing:secs)
            }
            
            return hoursString + ":" + minsString + ":" + secsString
        }
    }
    /**
     *  Get time interval between two times
     *
     *  @param dateFrom Datefrom
     *  @param dateTo   DateTo
     *
     *  @return Returns [01h:30m]
     */
    func getCreationTimeInt(expiryTimeStamp : Int) -> Int{
        let distanceTime = Date().timeIntervalSince1970
        return Int(distanceTime) - expiryTimeStamp
    }
    
    @IBAction func backToHome(_ sender: Any) {
     //   self.navigationController?.popToRootViewController(animated: true)
        if isFromChatScreen {
            self.navigationController?.popViewController(animated: true)
        } else {
             self.navigationController?.popToRootViewController(animated: true)
        }
       
    }
    
    //MARK: - slider to current position
    func resetSlider()
    {
        self.sliderView.reset()
    }
    
    //MARK: - update the driver status images and address according the
    func updateCustomerInfoStatus(){
        resetSlider()
        let sectionType : BookingStatus = BookingStatus(rawValue : status)!
        startTimeButton.isEnabled = false
        switch sectionType {
        case .bookingAccepted:
            
            if bookingDict?.callType == .Incall || bookingDict?.callType == .TeleCall{
                status = 9
                sliderView.sliderText = "APPOINTMENT COMPLETE".localized
            }else{
                status = 8
                sliderView.sliderText = "START".localized
            }
            break
            
        case .onTheWayToPickup:
            if bookingDict?.callType == .Incall || bookingDict?.callType == .TeleCall{
                status = 9
                sliderView.sliderText = "APPOINTMENT COMPLETE".localized
            }else{
                status = 8
                sliderView.sliderText = "START".localized
            }
            break
            
        case .arrivedAtPickup:
            if bookingDict?.callType == .Incall || bookingDict?.callType == .TeleCall{
                status = 9
                sliderView.sliderText = "APPOINTMENT COMPLETE".localized
            }else{
               
                status = 8
                sliderView.sliderText = "START".localized
            }
            break
            
        case .tripStarted:
            sliderView.sliderText = "JOB COMPLETED".localized
            
            status = 9
            startTimeButton.isHidden = false
            startTimeButton.isEnabled = true
            
            break
        case .raiseInvoice:
            if bookingDict?.callType == .TeleCall {
              //  self.navigationController?.popToRootViewController(animated: true)
                
            } else {
                self.navigationController?.isNavigationBarHidden = true
                self.performSegue(withIdentifier: "toInvoiceVC", sender: nil)
            }
            
            break
            
        default:
            break
        }
    }
    
    
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        switch segue.identifier! as String {
            
        case "toInvoiceVC":
            self.navigationController?.isNavigationBarHidden = true
            self.tabBarController?.tabBar.isHidden = true
            if let nextScene =  segue.destination as? InvoiceViewController
            {
                
                nextScene.bookingDict = bookingDict
                //nextScene.bid = String(describing: bookingDict?.bookingId)
            }
            break
        case "timerCustReview":
            let nav = segue.destination as! UINavigationController
            if let review: CustReviewsVC = nav.viewControllers.first as! CustReviewsVC?
            {
                review.customerID = (bookingDict?.customerId)!
            }
            break
        default:
            break
        }
    }
    
    //MARK: - update driver status to server
    func updateBookingStatus(status:Int) {
        Helper.showPI(message:loading.load)
        var distance = 0.0
        if status == 9 && bookingDict!.callType != .Incall{
            let distanceData = DistanceCalculation().getTheSavedDistance(distanceDocID:String(describing:self.bookingDict!.bookingId) + "_Dist")
            if !distanceData.isEmpty{
                distance = Double((distanceData["dist"] as?  NSNumber)!)
            }
        }
        
        let ud = UserDefaults.standard
        
        let lati = Helper.unwrappOptional(str : ud.object(forKey: "currentLat") as? String)
        let longi = Helper.unwrappOptional(str : ud.object(forKey: "currentLog") as? String)
        
        
        let params : [String : Any] = ["status": status,
                                       "bookingId":bookingDict!.bookingId as Any,
                                       "latitude" :lati,
                                       "longitude":longi,
                                       "distance":distance,
                                       "second": remainingTime,
                                       "pickupNotes": pickupNotes,
                                       "pickupImages": uploadedImageUrl,
                                       "dropNotes": dropNotes,
                                       "dropImages": dropImageUrl]
        
        bookingMod.updateBookingStatus(dict: params ,completionHandler: { (succeeded, travelFee) in
            if succeeded{
                self.bookingDict?.statusCode = status
                if status == 8{
                    self.startTimeButton.isSelected = true
                    self.bookingDict?.bookingTime?.status = 1
                    self.updateTimeStatusToServer(status:1)
                }else if status == 9{  // delete the document for distance in couchDB
                    let deleteProfileDoc = Couchbase.sharedInstance
                    deleteProfileDoc.deleteDocument(withDocID:String(describing:self.bookingDict!.bookingId) + "_Dist")
                }
                self.updateCustomerInfoStatus()
            }else{
                self.resetSlider()
            }
        })
    }
}

extension TimerViewController: ChatVCDelegate {
    func TappedBackToTimer() {
        isInitialFlow = false
        FetchBookingDetails()
    }
}

extension TimerViewController: UINavigationControllerDelegate{
    
    internal func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension TimerViewController:didChooseImageAndNotesDelegate {
    func didChooseImageNotes(notes: String, imageURLs: [String]) {
        isImageUploaded = true
        //cameraBtn.setBackgroundImage(UIImage(named: "camera with check"), for: .normal)
        if status == 8 {
            self.pickupNotes = notes
            self.uploadedImageUrl = imageURLs
        } else {
            self.dropImageUrl = imageURLs
            self.dropNotes = notes
        }
       
       
}

}

extension TimerViewController:cancelDeleteBooking{
    func cancelReason(){
        
        self.bookingDict?.statusCode = BookingStatus.bookingCancel.rawValue
        _ = navigationController?.popToRootViewController(animated: true)
    }
}
