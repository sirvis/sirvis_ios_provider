//
//  VehicleTypeModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 08/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxAlamofire
import RxCocoa
import RxSwift

class Cities {
    
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    
    
    
    /// cities api to get cities
    ///
    /// - Parameter completionHandler: return the cities array dict on completion
    func getCitiesAPI(completionHandler:@escaping (Bool,[CityModel]) -> ()) {
        
        let rxApiCall = CitiesAPI()
        rxApiCall.getCityAPI(method:API.METHOD.getCities)
        rxApiCall.City_Response
            .subscribe(onNext: {response in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .dataNotFound:
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .SuccessResponse:
                    Helper.hidePI()
                    completionHandler(true,CityModel().parseTheCityData(responseData: response.data["data"] as! [[String: Any]]))
                    break
                default:
                    Helper.hidePI()
                    completionHandler(false,[])
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}

