//
//  File.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 22/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

class CityModel  {
  // cities model with city name and city id
    var cityID = ""
    var cityName = ""
    
    func parseTheCityData(responseData: [[String: Any]]) -> [CityModel]{
        var citiesArray = [CityModel]()
        
        for items in responseData{
            let city = CityModel()
            
            if let id = items["id"] as? String{
                city.cityID = id
            }
            
            if let cityName = items["city"] as? String{
                city.cityName = cityName
            }
            citiesArray.append(city)
        }
        return citiesArray
    }
}
