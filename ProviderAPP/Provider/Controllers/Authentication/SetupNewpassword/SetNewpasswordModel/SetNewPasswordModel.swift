//
//  SetNewPasswordModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 17/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxAlamofire

class NewPasswordModel {
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    
    
    
    /// update the new password after verifying the phone number
    ///
    /// - Parameters:
    ///   - params: contains newpassword , proID, user type
    ///   - completionHandler: return boolean
    func updateNewPassWordAPi(params:[String:Any] ,
                              completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message:loading.update)
        let setNewpassword = SetNewPasswordAPI()
        setNewpassword.UpdateNewPassword(method: API.METHOD.UPDATEPASSWORD, parameters: params)
        setNewpassword.NewPassword_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .dataNotFound:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                case .SuccessResponse:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    completionHandler(true)
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    completionHandler(false)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}
