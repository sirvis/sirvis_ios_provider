
//
//  SigninTextFieldVC.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 17/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension SignInViewController:UITextFieldDelegate{
    
    
    func assignSavedData(){
        if (Utility.savedID.length > 2){
            view1.backgroundColor = COLOR.APP_COLOR
            view2.backgroundColor = COLOR.APP_COLOR
            emailTextField.text = Utility.savedID
            passwrodTextField.text = Utility.savedPassword
            signinModel.emailAddress.value =  emailTextField.text!
            signinModel.password.value =   passwrodTextField.text!
            rememberMeButton.isSelected = true
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        switch textField {
        case emailTextField:
            view1.backgroundColor = COLOR.APP_COLOR
            break
            
        case passwrodTextField:
            view2.backgroundColor = COLOR.APP_COLOR
            
            break
        default:
            
            break
        }
        activeTextField = textField
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {  //delegate method
        
        switch textField {
        case emailTextField:
            if !(emailTextField.text?.isEmpty)!{
                view1.backgroundColor = COLOR.APP_COLOR
            }else{
                view1.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
            }
            break
            
        case passwrodTextField:
            if !(passwrodTextField.text?.isEmpty)!{
                view2.backgroundColor = COLOR.APP_COLOR
            }else{
                view2.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
            }
        default:
            break
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        if textField == emailTextField{
            self.passwrodTextField.becomeFirstResponder()
            return false
        }
        else{
            signinMethod()
            dismisskeyBord()
        }
        return true
    }
}
