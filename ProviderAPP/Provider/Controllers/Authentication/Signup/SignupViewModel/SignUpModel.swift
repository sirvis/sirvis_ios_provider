//
//  SignUpModel.swift
//  RunnerLive
//
//  Created by Rahul Sharma on 29/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxAlamofire
import RxCocoa
import RxSwift

var mycityId = ""


class Signup:NSObject {
    
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    var firstName = Variable<String>(""){
        didSet {
            print("Assign email address")
        }
    }
    var latName = Variable<String>("")
    var emailAddress = Variable<String>("")
    var password = Variable<String>("")
    var phoneNumber = Variable<String>("")
    var countryCode = Variable<String>("")
    var latit = Variable<Double>(0.00)
    var logit = Variable<Double>(0.00)
    var proImage = Variable<String>("")
    var dateOfBirth = Variable<String>("")
    var artistCat = Variable<String>("")
    var cityID = Variable<String>("")
    var address = Variable<String>("")
    var documents = Variable<[String:Any]>([String:Any]())
    var taggedAs = Variable<String>("home")
    var referral = Variable<String>("")
    var subCatIds = Variable<String>("")
    var addressCity = Variable<String>("")
    var addressState = Variable<String>("")
    var addressCountry = Variable<String>("")
    var addressPinCode = Variable<String>("")
    var gender = Variable<Int>(1)
    var isVat = Variable<Int>(0)
    var vatNumber = Variable<String>("")
    
    
    
    
    
    /// signup API
    ///
    /// - Parameter completionHandler: return provider id on completion
    func signupApiCall(completionHandler:@escaping (Bool,String,Int,String) -> ()) {
        
        let signupParams = SignUPParams.init(firstNam: firstName.value, lastNam: latName.value, email: emailAddress.value, phone: phoneNumber.value, latit: 26.2041, logit: 28.0473, profilePic: proImage.value, dateOFBirth: "1980-01-01", pass: password.value, deviceID: Utility.deviceId, tokenPush: Utility.pushToken, categoryID: "0", city: "0", appVersion: Utility.appVersion, nameDevice: "Apple", modelDevice: Utility.modelName, typeDevice: "1",deviceVer: Utility.deviceVersion, countryCode: countryCode.value, artistAddress: "Johannesburg South Africa" , documentData:documents.value ,taggedAddress:"0",addState:"Johannesburg",addCity:"Johannesburg",addCountry: "South Africa",addPinCode:"2000",subCatId:"0",genderType:3, referral: referral.value, vatNumber: "", isVatApplicabel: 0)
        print(signupParams)
        Helper.showPI(message: loading.signup)
        let rxApiCall = SignUpAPI()
        rxApiCall.makeApiCallForSignup(method:API.METHOD.SIGNUP , params: signupParams)
        rxApiCall.signup_Response
            .subscribe(onNext: {response in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
 
                case .preconditionFailed1:
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                    
                case .preconditionFailed:
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                    
                case .SuccessResponse:
          

                    UserDefaults.standard.set(self.emailAddress.value, forKey: USER_INFO.USER_EMAIL)
                     UserDefaults.standard.set(self.emailAddress.value, forKey: USER_INFO.USER_EMAIL)
                    UserDefaults.standard.synchronize()
                    
                    if let dict = response.data["data"] as? [String:Any]{
                        if let providerID = dict["providerId"] as? String, let otp = dict["expireOtp"] as? Int{
                            completionHandler(true,providerID , otp, response.data["message"] as! String)
                        }
                    }
                
                    Helper.hidePI()
                    break
                    
                default:
                    Helper.hidePI()
                    completionHandler(false,"", 0,"")
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    /// getCategoryByCity API
    ///
    /// - Parameter completionHandler: return provider id on categoryArray
    func getTheCategoriesUsingCityID(method:String,completionHandler:@escaping (Bool,[Categories]) -> ()) {
        let rxApiCall = SignUpAPI()
        rxApiCall.getCategoriesAccordingToCity(method: method)
        rxApiCall.signup_Response
            .subscribe(onNext: {response in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .dataNotFound:
                    completionHandler(false,[])
                    break
                    
                case .SuccessResponse:
                    Helper.hidePI()
                    print(response.data["cityId"])
                    mycityId = response.data["cityId"] as! String
                    completionHandler(true,Categories.parsingTheServiceResponse(responseData: response.data["data"] as! [[String: Any]]))
                    break
                default:
                    Helper.hidePI()
                    completionHandler(false,[])
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    
    /// email validation
    ///
    /// - Parameters:
    ///   - params: email address
    ///   - completionHandler: return email available r not
    func emailValidate(completionHandler:@escaping (Bool) -> ()) {
        
        let emailValidate = ValidatePhoneEmail.init(email: emailAddress.value, countryCod: countryCode.value, phone: phoneNumber.value)
        let rxApiCall = SignUpAPI()
        rxApiCall.validateEmail(method: API.METHOD.EMAILVALIDATE,parameters:emailValidate)
        rxApiCall.signup_Response
            .subscribe(onNext: {response in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    completionHandler(true)
                    break
                case .preconditionFailed:
                    Helper.hidePI()
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                default:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    /// phone validation
    ///
    /// - Parameters:
    ///   - params: phone number
    ///   - completionHandler: return phone number is available r not
    func phoneValidate(completionHandler:@escaping (Bool) -> ()) {
        var phoneNum = ""
        if phoneNumber.value.count > 0 {
            if phoneNumber.value.prefix(1) == "0" {
                phoneNum = String(phoneNumber.value.dropFirst())
            } else {
                phoneNum = phoneNumber.value
            }
        } else {
            phoneNum = phoneNumber.value
        }
        
        
        
        let phoneValidate = ValidatePhoneEmail.init(email: emailAddress.value, countryCod: countryCode.value, phone: phoneNum)
        let rxApiCall = SignUpAPI()
        rxApiCall.validatePhone(method: API.METHOD.PHONEVALIDATE,parameters:phoneValidate)
        rxApiCall.signup_Response
            .subscribe(onNext: {response in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    completionHandler(true)
                    break
                case .preconditionFailed:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .invalidEnter:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                default:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    //// Referral API
    ///
    /// - Parameters:
    ///   - apiMethod: referral api
    ///   - paramDict: referral code
    ///   - completionHandler: return code available r not
    func validateReferral(apiMethod:String,completionHandler:@escaping (Bool) -> ()) {
        
        let params : [String : Any]
        
        params  = ["code":  referral.value,
                   "userType" : 2,
                   "lat":latit.value,
                   "long":logit.value]
        
        Helper.showPI(message: loading.referralVali)
        let rxApiCall = SignUpAPI()
        rxApiCall.validateReferralCode(method: apiMethod,parameters:params)
        rxApiCall.signup_Response
            .subscribe(onNext: {response in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    completionHandler(true)
                    break
                case .preconditionFailed:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .invalidEnter:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                default:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}
