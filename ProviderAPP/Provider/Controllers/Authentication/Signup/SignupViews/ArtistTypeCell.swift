//
//  ArtistTypeCell.swift
//  RunnerLive
//
//  Created by Rahul Sharma on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ArtistTypeCell: UICollectionViewCell {
    
    
//    @IBOutlet weak var artistTypeButton: UIButton!
    
    @IBOutlet weak var artistTypeLabel: UILabel!
    
    @IBOutlet weak var topView: UIView!
    
    func setLayout() -> Void {
        
        artistTypeLabel.layer.borderColor = COLOR.APP_COLOR.cgColor
        artistTypeLabel.layer.borderWidth = 1.0
    }
    

}

