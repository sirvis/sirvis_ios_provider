//
//  LocationManager.swift
//  Trustpals
//
//  Created by Vasant Hugar on 05/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import CoreLocation

@objc protocol NewLocationManagerDelegate {
    
    /// When Update Location
    @objc optional func didUpdateLocation(location: NewLocationManager)
    
    /// When Failed to Update Location
    @objc optional func didFailToUpdateLocation()
    
    /// Did Change Authorized
    ///
    /// - Parameter authorized: YES or NO
    @objc optional func didChangeAuthorization(authorized: Bool)
}

class NewLocationManager: NSObject {
    
    var latitute: Float = 13.3254453
    var longitude: Float = 77.2456456
    var name: String = ""
    var city: String = ""
    var state: String = ""
    var country: String = ""
    var zipcode: String = ""
    var street: String = ""
    var countryCode: String = ""
    
    var locationObj: CLLocationManager? = nil
    var delegate: NewLocationManagerDelegate? = nil
    
    private static var obj: NewLocationManager? = nil
    /// Create Shared Instance
    static var shared: NewLocationManager {
        if obj == nil {
            obj = NewLocationManager()
        }
        return obj!
    }
    
    override init() {
        super.init()
        locationObj = CLLocationManager()
        locationObj?.delegate = self
        locationObj?.requestWhenInUseAuthorization()
    }
    /// Start Location Update
    func start() {
        locationObj?.startUpdatingLocation()
    }
    /// Stop Location Update
    func stop() {
        locationObj?.stopUpdatingLocation()
    }
}

extension NewLocationManager: CLLocationManagerDelegate {
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch(status) {
            
        case .notDetermined:
            
            delegate?.didChangeAuthorization!(authorized: false)
            
            break
            
        case .restricted, .denied:
            
            print("No access")
            
//            let view = LocationEnableView.shared
//            view.show()
            
            delegate?.didChangeAuthorization!(authorized: false)
            
            break
            
        case .authorizedAlways, .authorizedWhenInUse:
            
            print("Access")
            
            locationObj?.startUpdatingLocation()
            
//            let view = LocationEnableView.shared
//            view.hide()
            
            delegate?.didChangeAuthorization!(authorized: true)
            
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.first != nil {
            
            stop()
            
            let geoCoder = CLGeocoder()
            let location = locations.first
            
            latitute = Float((location?.coordinate.latitude)!)
            longitude = Float((location?.coordinate.longitude)!)
            
            geoCoder.reverseGeocodeLocation(location!) {
                
                (placemarks, error) -> Void in
                
                if let arrayOfPlaces: [CLPlacemark] = placemarks as [CLPlacemark]? {
                    
                    // Place details
                    if let placemark: CLPlacemark = arrayOfPlaces.first {
                        
                        // Address dictionary
                        print("Address Dict :\(placemark.addressDictionary!)")
                        
                        // Location name
                        if let name = placemark.addressDictionary?["Name"] as? String {
                            self.name = name
                        }
                        
                        // Street address
                        if let street = placemark.addressDictionary?["Thoroughfare"] as? String {
                            self.street = street
                        }
                        
                        // City
                        if let city = placemark.addressDictionary?["City"] as? String {
                            self.city = city
                        }
                        
                        // Zip code
                        if let zip = placemark.addressDictionary?["ZIP"] as? String {
                            self.zipcode = zip
                        }
                        
                        // Country
                        if let country = placemark.addressDictionary?["Country"] as? String {
                            self.country = country
                        }
//                        if Google.myCOuntry.characters.count == 0 {
//                            Google.myCOuntry = self.country
//                        }
                        self.delegate?.didUpdateLocation!(location: self)
                    }
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("\nDid fail location : %@\n",error.localizedDescription)
        delegate?.didFailToUpdateLocation!()
    }
}
