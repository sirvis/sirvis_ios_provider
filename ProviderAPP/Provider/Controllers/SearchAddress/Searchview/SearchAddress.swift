//
//  SearchAddress.swift
//  DayRunner
//
//  Created by Vasant Hugar on 27/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class SearchAddress: NSObject {

    var address   = ""
    var name      = ""
    var latitude  = ""
    var longitude = ""
    var id        = ""
    
    override init() {
        super.init()
    }
}
