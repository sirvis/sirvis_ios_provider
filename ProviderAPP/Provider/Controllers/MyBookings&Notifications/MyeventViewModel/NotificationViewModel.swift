//
//  NotificationViewModel.swift
//  LSP
//
//  Created by Rahul Sharma on 6/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import RxAlamofire
import Alamofire
import RxSwift
import RxCocoa

class NotificationViewModel {
    func fetchNotifications(skip:Int, limit: Int){
        let notificationAPI = NotificationAPI()
        notificationAPI.getNotifications(skip: skip, limit: limit)
        
    }
    
}
