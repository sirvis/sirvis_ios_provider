//
//  NotificationViewController.swift
//  LSP
//
//  Created by Rahul Sharma on 6/11/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Foundation

class NotificationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let notificationModel = NotificationModel()
    var notificationData: NotificationModel?
    var refreshCount = 0
    let limitCount = 10
    var refreshControl = UIRefreshControl()
    var fromAdminPush = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchNotications(skip: refreshCount)
        refreshTableView()
        
        
        
    }
    //MARK: back Button Tapped
    @IBAction func backBtnTapped(_ sender: Any) {
       // self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Refresh tableview
    func refreshTableView() {
        refreshControl = UIRefreshControl()
        refreshControl.triggerVerticalOffset = 100.0
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        self.refreshControl.beginRefreshing()
        self.tableView.bottomRefreshControl = refreshControl
    }
    
    @objc func refresh() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.refreshCount += 1
             self.fetchNotications(skip: self.refreshCount)
        }
    }

    
    //MARK: call get Notifation API
    func fetchNotications(skip: Int){
        notificationModel.fetchNotifications(skip: skip  *  limitCount, limit: limitCount) { (success, data) in
            if success{
                self.notificationData = data
                self.tableView.reloadData()
                    self.tableView.bottomRefreshControl?.endRefreshing()
            }
        }
    }
}

//MARK: Tableview datasource delegate methods
extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if notificationData?.notifications.count != nil {
            return (notificationData?.notifications.count)!
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as? NotificationCell
        cell?.updateTheView(data: (notificationData?.notifications[indexPath.row])!)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
