//
//  MyEventsViewController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 04/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Firebase
//import AICustomViewControllerTransition

class MyEventsViewController: UIViewController {

    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var notificationCount: UILabel!
//    @IBOutlet weak var walletCount: UILabel!
    @IBOutlet weak var walletButton: UIButton!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var upcomingTable: UITableView!
    @IBOutlet weak var requestsTable: UITableView!
    
    @IBOutlet weak var upcomingBookingCount: UILabel!
    @IBOutlet weak var requestBookingCount: UILabel!
    @IBOutlet weak var profileReviewLabel: UILabel!
    @IBOutlet weak var requestsButton: UIButton!
    @IBOutlet weak var upcomingButton: UIButton!
    @IBOutlet weak var offlineOnlineButton: UIButton!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var noAcceptedView: UIView!
    @IBOutlet weak var noRequestView: UIView!
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    
    var fromNormalRequest:Bool = false
    var fromUpcomingRequest:Bool = false
    
    var distanceTime = Timer()
    var model   = Events() //Events model
    var bookingData:MyeventModel?
    var locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
    var isBidding = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileReviewLabel.isHidden = true
        let url:String = "/topics/" + Utility.fcmTopic
        Messaging.messaging().subscribe(toTopic:url)
        setupUI()
        hideBiddingView()
//        locationtracker.startLocationTracking()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {

        NotificationCenter.default.addObserver(self, selector: #selector(makeTheHomeServiceCall), name: Notification.Name("gotNewBooking"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeTheHomeServiceCall), name: Notification.Name("cancelledBooking"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeTheDriverOffline), name: Notification.Name("makeOfflineFromAdmin"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeTheHomeServiceCall), name: Notification.Name("accountAccepted"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeTheHomeServiceCall), name: Notification.Name("BookingExpired"), object: nil)
        
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(false)
         self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        
        /// updating model if the booking cancelled,ACCEPTED,DECLINED OR COMPLETED
        if UserDefaults.standard.value(forKey: "selectedIndex") != nil && self.bookingData != nil{
            let val = UserDefaults.standard.value(forKey: "selectedIndex") as! Int
            if (self.bookingData?.activeBidData.count)!  > 0 ||  (self.bookingData?.requestData.count)!  > 0{
                if (self.bookingData?.activeBidData.count)! > 0 {
                    if (self.bookingData?.activeBidData.indices.contains(val))!{
                        if self.bookingData!.activeBidData[val].statusCode == 10{
                            self.bookingData?.activeBidData.remove(at: val)
                        }
                    }
                }
                if (self.bookingData?.requestData.count)! > 0{
                    if (self.bookingData?.requestData.indices.contains(val))!{
                        if self.bookingData!.requestData[val].statusCode == 3 || self.bookingData!.requestData[val].statusCode == 4{
                            self.bookingData?.requestData.remove(at: val)
                        }
                    }
                }
                self.upcomingTable.reloadData()
                self.requestsTable.reloadData()
            }
        }
    }
    
   
    
    
    @IBAction func walletBtnTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "openWallet", sender: nil)
    }
    
    
   
    
    @IBAction func notificationViewAction(_ sender: Any) {
        //performSegue(withIdentifier: "NotificationVC", sender: nil)
       let  vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    @objc func makeTheDriverOffline() {
        self.offlineOnlineButton.isSelected = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        upcomingTable.addPullToRefresh { (finished) in
            if finished{
                 self.makeTheHomeServiceCall()
            }
        }
        
        requestsTable.addPullToRefresh { (finished) in
            if finished{
                  self.makeTheHomeServiceCall()
            }
        }
        
        self.makeTheHomeServiceCall()//Booking data
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
//            self.walletCount.isHidden = false
//            self.walletCount.text = String(Utility.walletAmt)
//        })
    }
    

    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "gotNewBooking"), object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "cancelledBooking"), object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "accountAccepted"), object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "makeOfflineFromAdmin"), object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "BookingExpired"), object: nil);

    }
    
    func setupUI(){
        
        self.requestsTable.rowHeight = UITableView.automaticDimension
        self.requestsTable.estimatedRowHeight = 128.0
        
        self.upcomingTable.rowHeight = UITableView.automaticDimension
        self.upcomingTable.estimatedRowHeight = 128.0
        

        
//        walletCount.layer.cornerRadius = 4
//        walletCount.layer.masksToBounds = true
//        walletCount.sizeToFit()

    }

   
    //hide active bid
    func hideBiddingView() {
        self.topViewHeightConstraint.constant = 0
        self.view.layoutIfNeeded()
        self.mainScrollView.isScrollEnabled = false
        self.upcomingBookingCount.isHidden = true
        self.requestBookingCount.isHidden = true
    }
    
    // makes the assigned trip service call and retrives the data
    @objc func makeTheHomeServiceCall(){
        
        model.Delegate = self
        model.assignedBookingAPI {  (responseData) in
           if  responseData.isBidCategory { // show bidding view
                self.topViewHeightConstraint.constant = 40
                self.view.layoutIfNeeded()
                self.mainScrollView.isScrollEnabled = true
            self.upcomingBookingCount.isHidden = false
            self.requestBookingCount.isHidden = false
            
           } else { //hide  bidding view
               self.hideBiddingView()
            }
            
            for pendingBookingID in responseData.pendingBookings {
                self.showReviewPopup(bid: pendingBookingID)
            }
            
            self.bookingData = responseData
            
            self.upcomingTable.reloadData()
            self.requestsTable.reloadData()
            self.notificationCount.text = String(responseData.notificationCount)
            if (self.bookingData?.activeBidData.count)! > 0{
                let count = (self.bookingData?.activeBidData.count)!
                self.upcomingBookingCount.text = String(describing:count)
            }else{
                self.upcomingBookingCount.text = "0"
            }
            
            if (self.bookingData?.requestData.count)!  > 0{
                let count = (self.bookingData?.requestData.count)!
                self.requestBookingCount.text = String(describing:count)
            }else{
                self.requestBookingCount.text = "0"
            }
        }
    }
    
    //Show pending review business popup
    func showReviewPopup(bid: Int) {
        let popup : SubmitReviewView = SubmitReviewView.instance
        popup.bookingID = bid
        popup.getBookingData()
        popup.show()
    }
    
    //MARK:- update artist status
    @IBAction func offlineOnlineAction(_ sender: Any) {
        if self.offlineOnlineButton.currentTitle != "Deactivated"{
            if offlineOnlineButton.isSelected {
                model.updateMasterStatus(status: "3")
                Helper.showPI(message:loading.goOnline)
            }else{
                model.updateMasterStatus(status: "4")
                Helper.showPI(message:loading.goOffline)
            }
        }
    }
    
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        UserDefaults.standard.set(sender, forKey: "selectedIndex")
        UserDefaults.standard.synchronize()
        
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(true)

        
        switch segue.identifier! {
        case "toAcceptRejectBooking":
            if let nextScene = segue.destination as? AcceptRejectViewController {
                nextScene.bookingData = bookingData?.requestData[sender as! Int]
                nextScene.delegate = self
            }
            break
            
        case "toBookingVC":
            if let nextScene = segue.destination as? OnBookingViewController {
                nextScene.bidBookingDict = bookingData?.activeBidData[sender as! Int]
                nextScene.activeBid = true
            }
            break
            
        case "toTimerView":
            if let nextScene = segue.destination as? TimerViewController {
                nextScene.bookingDict = bookingData?.acceptedData[sender as! Int]
            }
            break
            
        case "homeToInvoice":
            if let nextScene = segue.destination as? InvoiceViewController {
                nextScene.bookingDict = bookingData?.acceptedData[sender as! Int]
            }
            break
            
        default:
            break
            
        }
    }
}

extension MyEventsViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == mainScrollView {
            
            indicatorView.frame = CGRect(x: scrollView.contentOffset.x / 2, y: indicatorView.frame.origin.y, width: self.view.frame.size.width/2, height: 2)
            buttonEnable()
        }
    }
}

extension MyEventsViewController : MoveToUpcomingDelegate{
    func moveToUpcoming() {
        var frame: CGRect = self.mainScrollView.bounds;
        frame.origin.x = self.view.frame.width;
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
}

//Mark: - Home model delegate method
extension MyEventsViewController : EventsModelDelegate{
    
    //last updated r updated driver status
    func checkTheDriverStatus(data:Int , statusRev:Int){
        
        
        if statusRev == 0{
            profileReviewLabel.isHidden = false
        }else{
            profileReviewLabel.isHidden = true
        }
        
        if data == 3
        {
            self.offlineOnlineButton.isSelected = false
            self.offlineOnlineButton.setTitle("Go Offline".localized, for: .normal)
            self.offlineOnlineButton.setBackgroundImage(#imageLiteral(resourceName: "home_online_btn_off"), for: .normal)
        }else if data == 0{
            self.offlineOnlineButton.isSelected = false
            self.offlineOnlineButton.setTitle("Deactivated".localized, for: .normal)
            self.offlineOnlineButton.setBackgroundImage(#imageLiteral(resourceName: "Deactive"), for: .normal)
        }
        else{
            self.offlineOnlineButton.isSelected = true
            self.offlineOnlineButton.setTitle("Go Online".localized, for: .normal)
             self.offlineOnlineButton.setBackgroundImage(#imageLiteral(resourceName: "home_offline_btn_off"), for: .normal)
        }
    }
                
    //** Updated driver status reponse
    
    //bug id- card 70, desc: in provider app even after tapping multiple time it is showing go online , fix- fixed
    func updateTheDriverStatus(status: String ,success:Bool) {
        if status == "3" && success {
            self.offlineOnlineButton.setBackgroundImage(#imageLiteral(resourceName: "home_online_btn_off"), for: .normal)
             self.offlineOnlineButton.setTitle("Go Offline".localized, for: .normal)
            self.offlineOnlineButton.isSelected = false
        }else if status == "4" && success{
             self.offlineOnlineButton.setBackgroundImage(#imageLiteral(resourceName: "home_offline_btn_off"), for: .normal)
            self.offlineOnlineButton.setTitle("Go Online".localized, for: .normal)
            self.offlineOnlineButton.isSelected = true
        }else{
            self.offlineOnlineButton.isSelected = true
        }
    }
}


