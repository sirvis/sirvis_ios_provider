//
//  MyEventsButtonActions.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 05/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

extension MyEventsViewController {
    
    @IBAction func requestsAction(_ sender: UIButton) {
        
        var frame: CGRect = self.mainScrollView.bounds;
        frame.origin.x = self.view.frame.origin.x;
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
    
    @IBAction func upcomingAction(_ sender: UIButton) {
        
        var frame: CGRect = self.mainScrollView.bounds;
        frame.origin.x = self.view.frame.width ;
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
    
    func buttonEnable() {
        
        switch indicatorView.frame.origin.x {
        case 0:
            requestsButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            upcomingButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        case self.view.frame.size.width / 2:
            upcomingButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            requestsButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        default:
            requestsButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            upcomingButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        }
    }
}
