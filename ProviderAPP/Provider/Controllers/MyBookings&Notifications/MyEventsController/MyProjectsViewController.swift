//
//  MyProjectsViewController.swift
//  LSP
//
//  Created by Rahul Sharma on 5/29/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import JTCalendar

class MyProjectsViewController: UIViewController{

    @IBOutlet weak var myProjectsTable: UITableView!
    @IBOutlet weak var noAcceptedView: UIView!
    @IBOutlet weak var calendarMenuView: JTCalendarMenuView!
    @IBOutlet weak var calendarView: JTHorizontalCalendarView!
    
    var calendarManager = JTCalendarManager()
    var dateSelected =  Date.dateWithDifferentTimeInterval()
    var todayDate: Date?
    var minDate: Date?
    var maxDate: Date?
    var distanceTime = Timer()
    var model   = Events() //Events model
    var bookingData:MyeventModel?
    var locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCalendar()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
          setupUI()
          registerForPush()
       
        
        //invoicePaid
    }
    
    func registerForPush() {
        NotificationCenter.default.addObserver(self, selector: #selector(refreshScreen), name: Notification.Name("gotNewBooking"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshScreen), name: Notification.Name("cancelledBooking"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshScreen), name: Notification.Name("invoicePaid"), object: nil)
    }

    func deregisterPush() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "gotNewBooking"), object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "cancelledBooking"), object: nil);
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "invoicePaid"), object: nil);
    }
    
    //Setup UI
    func setupUI() {
        getBooking(byDate: calendarManager, date: todayDate!)
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(false)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        /// updating model if the booking cancelled,ACCEPTED,DECLINED OR COMPLETED
        if UserDefaults.standard.value(forKey: "selectedIndex") != nil && self.bookingData != nil{
            let val = UserDefaults.standard.value(forKey: "selectedIndex") as! Int
            if (self.bookingData?.acceptedData.count)!  > 0 {
                if (self.bookingData?.acceptedData.count)! > 0 {
                    if (self.bookingData?.acceptedData.indices.contains(val))!{
                        if self.bookingData!.acceptedData[val].statusCode == 10{
                            self.bookingData?.acceptedData.remove(at: val)
                        }
                    }
                }
                self.myProjectsTable.reloadData()
            }
        }
    }
//call booking APi, refresh calendar
    @objc func refreshScreen() {
         getBooking(byDate: calendarManager, date: todayDate!)
    }
    
    /// - starts the distance calculation
    func handleDistanceTime() {
        self.distanceTime.invalidate()
        self.distanceTime = Timer.scheduledTimer(timeInterval:5,
                                                 target: self,
                                                 selector: #selector(updatetheDistanceTravelled),
                                                 userInfo: nil,
                                                 repeats: true)
        
    }
    
    //setup weekly calendar
     func setupCalendar() {
           calendarManager = JTCalendarManager(locale: .current, andTimeZone: .current)!
           todayDate = Date.dateWithDifferentTimeInterval()
           calendarManager.delegate = self
           createMinAndMaxDate()
           calendarManager.menuView = calendarMenuView
           calendarManager.contentView = calendarView
           calendarManager.setDate(todayDate)
           calendarManager.settings?.weekModeEnabled = true
           calendarManager.reload()
       }
       
      //set min and maximum week
       func createMinAndMaxDate() {
           todayDate = Date.dateWithDifferentTimeInterval()
           // Min date will be 8 weeks before today
           minDate = calendarManager.dateHelper?.add(to: todayDate, weeks: -8)
           maxDate = calendarManager.dateHelper?.add(to: todayDate, weeks: 8)
       }

       func getBooking(byDate cal:JTCalendarManager, date: Date) {
          
           var currentCalendar =  cal.dateHelper?.calendar()
           currentCalendar?.timeZone = .current
           currentCalendar?.locale = .current
           
           //let startOfDay = currentCalendar?.startOfDay(for: cal.date())
           let startOfDay =  currentCalendar?.startOfDay(for: todayDate!)
           var components = DateComponents()
           components.day = 1
           components.second = -1
           let endOfDay = currentCalendar?.date(byAdding: components, to: startOfDay!)
           let startTimeStamp = startOfDay?.timeIntervalSince1970
           let endTimeStamp = endOfDay?.timeIntervalSince1970
           model.getAcceptedBookingByDate(from: startTimeStamp!, to: endTimeStamp!) { (responseData) in
               self.bookingData = responseData
               self.calendarManager.reload()
               self.myProjectsTable.reloadData()
           }
       }

    @IBAction func preWeekBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.calendarView.loadPreviousPageWithAnimation()
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func nextWeekBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.calendarView.loadNextPageWithAnimation()
            self.view.layoutIfNeeded()
        }
        
    }
    
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        UserDefaults.standard.set(sender, forKey: "selectedIndex")
        UserDefaults.standard.synchronize()

        
        switch segue.identifier! {
        case "toAcceptRejectBooking":
            if let nextScene = segue.destination as? AcceptRejectViewController {
                nextScene.bookingData = bookingData?.requestData[sender as! Int]
               // nextScene.delegate = self
            }
            break
        case "ActiveBidBookingVC":
            if let nextScene = segue.destination as? OnBookingViewController {
                nextScene.bidBookingDict = bookingData?.activeBidData[sender as! Int]
                nextScene.activeBid = true
            }
            break
        case "toBookingVC":
            if let nextScene = segue.destination as? OnBookingViewController {
                nextScene.bookingDict = bookingData?.acceptedData[sender as! Int]
                nextScene.activeBid = false
            }
            break
        case "toTimerView":
            if let nextScene = segue.destination as? TimerViewController {
                nextScene.bookingDict = bookingData?.acceptedData[sender as? Int ?? 0]
                
            }
            break
        case "homeToInvoice":
            if let nextScene = segue.destination as? InvoiceViewController {
                nextScene.bookingDict = bookingData?.acceptedData[sender as! Int]
                //nextScene.bid = String(describing: bookingData?.acceptedData[sender as! Int].bookingId)
            }
            break
        default:
            break
        }
    }
}


extension MyProjectsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (bookingData?.acceptedData.count != nil && bookingData?.acceptedData.count != 0){
            noAcceptedView.isHidden  = true
            return 1
        }else{
            noAcceptedView.isHidden  = false
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if (bookingData?.acceptedData.count) != nil && (bookingData?.acceptedData.count) != 0{
                return (bookingData?.acceptedData.count)!
            }else{
                return 0
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "upcomingCell") as! AcceptedTableCell
        cell.updateTheAcceptData(data: (self.bookingData?.acceptedData[indexPath.row])!)
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 128;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if bookingData?.acceptedData[indexPath.row].callType == .Incall || bookingData?.acceptedData[indexPath.row].callType == .TeleCall{
           
                performSegue(withIdentifier: "toTimerView", sender: indexPath.row)
            
        }else{
            
            if (bookingData?.acceptedData.count) != nil  && (bookingData?.acceptedData.count) != 0{
                switch self.bookingData?.acceptedData[indexPath.row].statusCode{
                case 17?:
                    performSegue(withIdentifier: "ActiveBidBookingVC", sender: indexPath.row)
                    break
                case 3?:
                    //callType
                    performSegue(withIdentifier: "toBookingVC", sender: indexPath.row)
                    
                    break
                case 6?:
                    performSegue(withIdentifier: "toBookingVC", sender: indexPath.row)
                    break
                case 7?:
                    performSegue(withIdentifier: "toTimerView", sender: indexPath.row)
                    break
                case 8?:
                    performSegue(withIdentifier: "toTimerView", sender: indexPath.row)
                    break
                case 9?:
                  
                    performSegue(withIdentifier: "homeToInvoice", sender: indexPath.row)
                    break
                default:
                    break
                }
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension MyProjectsViewController:JTCalendarDelegate {
    
    
      func calendar(_ calendar: JTCalendarManager?, prepareDayView dayView: (UIView & JTCalendarDay)?) {
          
                  if   let dayview1 = dayView as? JTCalendarDayView{
          
                      DispatchQueue.global().async{
                          sleep(1);
                          DispatchQueue.main.async{
                              if self.calendarManager.dateHelper!.date(Date(), isTheSameDayThan: dayview1.date) {
                                  dayview1.circleView.isHidden = false
                                  dayview1.circleView.backgroundColor = COLOR.APP_COLOR
                                  dayview1.dotView.backgroundColor = UIColor.white
                                  dayview1.textLabel?.textColor = UIColor.white
                              }
                              else if  self.calendarManager.dateHelper!.date(self.todayDate, isTheSameDayThan: dayview1.date) { //!dateSelected.isEmpty &&
                                  dayview1.circleView.isHidden = false
                                  dayview1.circleView.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0x808080)
                                  dayview1.dotView.backgroundColor = COLOR.APP_COLOR
                                  dayview1.textLabel?.textColor = UIColor.white
                              }
                              else if !(self.calendarManager.dateHelper!.date(self.calendarView.date, isTheSameMonthThan: dayview1.date)) {
                                  dayview1.circleView.isHidden = true
                                  dayview1.dotView.backgroundColor = COLOR.APP_COLOR
                                  dayview1.textLabel?.textColor = UIColor.lightGray
                              }
                              else {
                                  dayview1.circleView.isHidden = true
                                  dayview1.dotView.backgroundColor = COLOR.APP_COLOR
                                  dayview1.textLabel?.textColor = UIColor.black
                              }
                          }
                      }
                  }
              }
    
          
          func calendar(_ calendar: JTCalendarManager?, didTouchDayView dayView: (UIView & JTCalendarDay)?) {
          
                  if let dayview1 = dayView as? JTCalendarDayView{
                      todayDate = dayview1.date
                      dayview1.circleView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
                      UIView.transition(with: dayview1 as UIView , duration: 0.3, options: [], animations: {() -> Void in
                          dayview1.circleView.transform = CGAffineTransform.identity
                          self.calendarManager.reload()
                          self.getBooking(byDate: calendar!, date: self.todayDate!)
                          //self.scheduleTableview.reloadData()
                      }) { _ in }
                      if !(calendarManager.dateHelper!.date(calendarView.date, isTheSameMonthThan: dayview1.date)) {
                          if calendarView.date.compare(dayview1.date) == .orderedAscending {
                              self.getBooking(byDate: calendar!, date: todayDate!)
                          //    calendarView.loadNextPageWithAnimation()
          
                          }
                          else {
                              self.getBooking(byDate: calendar!, date: todayDate!)
                           //   calendarView.loadPreviousPageWithAnimation()
                          }
                      }
                  }
              }
     
          
          // Used to limit the date for the calendar, optional
          
          
          func calendar(_ calendar: JTCalendarManager?, canDisplayPageWith date: Date?) -> Bool {
              return calendarManager.dateHelper!.date(date, isEqualOrAfter: minDate, andEqualOrBefore: maxDate)
          }
          
      //    func calendar(_ calendar: JTCalendarManager?, canDisplayPageWith date: Date?) -> Bool {
      //        return calendarManager.dateHelper!.date(date, isEqualOrAfter: minDate, andEqualOrBefore: maxDate)
      //    }
          
          
          func calendarDidLoadNextPage(_ calendar: JTCalendarManager?) {
              DispatchQueue.global().async{
                  sleep(1);
                  DispatchQueue.main.async{
                  
                      self.getBooking(byDate: calendar!, date: calendar!.date())
                  }
              }
              
          }
          
     
          
          func calendarDidLoadPreviousPage(_ calendar: JTCalendarManager?) {
              DispatchQueue.global().async{
                  sleep(1);
                  DispatchQueue.main.async{
                     
                      self.getBooking(byDate: calendar!, date: calendar!.date())
                    
                  }
              }
          }
          
     
      }
