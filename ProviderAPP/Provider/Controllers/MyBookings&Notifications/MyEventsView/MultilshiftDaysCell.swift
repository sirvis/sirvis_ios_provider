//
//  MultilshiftDaysCell.swift
//  LSP
//
//  Created by Rahul Sharma on 9/5/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class MultilshiftDaysCell: UICollectionViewCell {
    
    @IBOutlet weak var contentView1: UIView!
    
    @IBOutlet weak var daysLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView1.layer.borderWidth = 1
        contentView1.layer.borderColor = UIColor(hexString: "DBDBDB").cgColor
        contentView1.layer.cornerRadius = contentView1.frame.height / 2
        contentView1.layer.masksToBounds = true
    }
}
