//
//  NotificationCell.swift
//  LSP
//
//  Created by Rahul Sharma on 6/11/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var conetentText: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateTheView(data:Notifications) {
        self.conetentText.text = data.content
        self.timestampLabel.adjustsFontSizeToFitWidth = true
        
        let notiTimeInterval = TimeInterval((data.NotificationTime))
        let notidate = NSDate(timeIntervalSince1970: notiTimeInterval)
        self.timestampLabel.text = Helper.timeAgoSinceDate(date: notidate, numericDates: true)
    }
    
}
