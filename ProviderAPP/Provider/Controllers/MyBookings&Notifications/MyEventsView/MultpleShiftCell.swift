//
//  MultpleShiftCell.swift
//  LSP
//
//  Created by Rahul Sharma on 9/4/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class MultpleShiftCell: UITableViewCell {

    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var bookingTypeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var toBidLabel: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var shiftView: UIView!
    @IBOutlet weak var numberOfShiftLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var shiftLabel: UILabel!
    var expiryTime:Float = 0.0
    var fullTime:Float = 0.0
    var remainingTime:Float = 0.0
    var requestData: Request?
    var activeBidData : ActiveBid?
    var isBidding = false
    var acceptTimer  = Timer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
        setupUI()
    }

    func setupUI() {
        shiftView.layer.borderColor = UIColor(hexString: "DBDBDB").cgColor
        
        shiftView.layer.borderWidth = 1
        shiftView.layer.cornerRadius = 3
        shiftView.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupShiftDataRequest(data: Request) {
        requestData = data
        
        if data.bookingType == BookingType.Schedule {
            shiftLabel.text = "Hour"
            collectionView.isHidden = true
            numberOfShiftLabel.text = Helper.minsToHour(min: data.duration)
        } else {
            shiftLabel.text = "Shifts"
            collectionView.isHidden = false
            collectionView.reloadData()
            numberOfShiftLabel.text = "\(data.shifts)"
        }
        
        self.nameLabel.text = data.firstName
        
        if data.distanceMatrix == 1{
            self.distanceLabel.text =  String(format: "%.02f", data.distance/1609.34) + DistanceUnits.Miles + " away"
        }else{
            self.distanceLabel.text = String(format: "%.02f", data.distance/1000) + DistanceUnits.Kms + " away"
        }
        self.categoryLabel.text = data.categoryName
        self.amountLabel.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.totalAmt))
        
        let startdateArray = Helper.getTheDateFromTimeStamp(timeStamp:data.actuallGigTimeStart).components(separatedBy: "|")
        let endDateArray = Helper.getTheDateFromTimeStamp(timeStamp:data.bookingRequestEnds).components(separatedBy: "|")
        let startDate1 = String(startdateArray[0].dropLast(5))
        let endDate1 = String(endDateArray[0].dropLast(5))
        startDate.text = startDate1
        startTime.text = startdateArray[1]
        endDate.text = endDate1
        endTime.text = endDateArray[1]
        
        
  
            if data.bookingModel == .Bidding {
                self.bookingTypeLabel.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.bidCharge))
                toBidLabel.isHidden = false
                
                
            }
            else{
                if data.bookingType == .Schedule  {
                    self.bookingTypeLabel.text = "SCHEDULE"
                } else if data.bookingType == .Repeat {
                    self.bookingTypeLabel.text = "MULTIPLE SHIFT"
                } else {
                    self.bookingTypeLabel.text = "ASAP"
                }

                toBidLabel.isHidden = true
                
            }
        
  
        
       // self.bookingTime.text = Helper.getTheDateFromTimeStamp(timeStamp:data.bookingRequestedFor)
        
        
        fullTime = Float(data.bookingExpireTime - data.bookingRequestedat)
        expiryTime = Float(self.getCreationTimeInt(expiryTimeStamp: data.bookingExpireTime))
        
        if !acceptTimer.isValid {
            acceptTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
        }
    }

    func setupShiftDataActiveBid(data: ActiveBid) {
        activeBidData = data
        collectionView.reloadData()
        self.nameLabel.text = data.firstName
        
        if data.distanceMatrix == 1{
            self.distanceLabel.text =  String(format: "%.02f", data.distance/1609.34) + DistanceUnits.Miles + " away"
        }else{
            self.distanceLabel.text = String(format: "%.02f", data.distance/1000) + DistanceUnits.Kms + " away"
        }
        self.categoryLabel.text = data.categoryName
        self.amountLabel.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.totalAmt))
        
        let startdateArray = Helper.getTheDateFromTimeStamp(timeStamp:data.actuallGigTimeStart).components(separatedBy: "|")
        let endDateArray = Helper.getTheDateFromTimeStamp(timeStamp:data.bookingRequestEnds).components(separatedBy: "|")
        let startDate1 = String(startdateArray[0].dropLast(5))
        let endDate1 = String(endDateArray[0].dropLast(5))
        startDate.text = startDate1
        startTime.text = startdateArray[1]
        endDate.text = endDate1
        endTime.text = endDateArray[1]
        numberOfShiftLabel.text = "\(data.shifts)"
        
        
        if data.bookingModel == .Bidding { //Bidding
            self.bookingTypeLabel.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.bidCharge))
            toBidLabel.isHidden = false
            
            
        }
        else{
            if data.bookingType == .Schedule  {
                self.bookingTypeLabel.text = "SCHEDULE"
            } else if data.bookingType == .Repeat {
                self.bookingTypeLabel.text = "MULTIPLE SHIFT"
            } else {
                self.bookingTypeLabel.text = "ASAP"
            }
            toBidLabel.isHidden = true
            
        }
        
        
        
        // self.bookingTime.text = Helper.getTheDateFromTimeStamp(timeStamp:data.bookingRequestedFor)
        
        
        fullTime = Float(data.bookingExpireTime - data.bookingRequestedat)
        expiryTime = Float(self.getCreationTimeInt(expiryTimeStamp: data.bookingExpireTime))
        
        if !acceptTimer.isValid {
            acceptTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
        }
    }
    
    /**
     *  Calculate time for Remaining
     */
    @objc func timerTick() {
        let remainingTime: Float = expiryTime - 1
        
        timerLabel.text = timeFormate(Int(remainingTime))
        expiryTime = remainingTime
        
        if expiryTime == 0.0 {
            let notificationName = Notification.Name("gotNewBooking")
            NotificationCenter.default.post(name: notificationName, object: nil)
            acceptTimer.invalidate()
        }
    }
    
    /**
     *  Check for Time formate
     *
     *  @param remainingTime Remaining time
     *
     *  @return Return must 00:00/00
     */
    func timeFormate(_ remainingTime: Int) -> String {
        let hour: Int = Int(remainingTime) / 3600
        let min: Int = Int(remainingTime) / 60 % 60
        let secs: Int = Int(remainingTime) % 60
        
        var minsString = ""
        var secsString = ""
        
        if min < 10{
            minsString = "0" + String(describing:min)
        }else{
            minsString = String(describing:min)
        }
        
        if secs < 10{
            secsString = "0" + String(describing:secs)
        }else{
            secsString = String(describing:secs)
        }
        
        return String(format:"%02i:%02i:%02i", hour, min, secs)
        
        //return minsString + ":" + secsString
    }
    /**
     *  Get time interval between two times
     *
     *  @param dateFrom Datefrom
     *  @param dateTo   DateTo
     *
     *  @return Returns [01h:30m]
     */
    func getCreationTimeInt(expiryTimeStamp : Int64) -> Int64{
        let distanceTime = Date().timeIntervalSince1970
        return expiryTimeStamp - Int64(distanceTime)
    }
}

extension MultpleShiftCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if isBidding {
            
            if activeBidData !=  nil{
                return 1
            }
            else {
                return 0
            }
        }
        else {
            if requestData !=  nil{
                return 1
            }
            else {
                return 0
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isBidding {
            return (activeBidData?.days.count)!
        }
        else {
            return (requestData?.days.count)!
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MultilshiftDaysCell", for: indexPath) as? MultilshiftDaysCell
    
        if isBidding {
            if activeBidData !=  nil {
                cell?.daysLabel.text = activeBidData?.days[indexPath.row]
            }
        }
        else {
            if requestData !=  nil {
                cell?.daysLabel.text = requestData?.days[indexPath.row]
            }
        }
        return cell!
    }
}
