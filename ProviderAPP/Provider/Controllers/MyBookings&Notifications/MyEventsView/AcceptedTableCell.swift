//
//  AcceptedTableCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 30/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class AcceptedTableCell: UITableViewCell {
    @IBOutlet weak var priceAmount: UILabel!
    @IBOutlet weak var toBidLabel: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var progressNStatus: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bookingType: UILabel!
    @IBOutlet weak var bookingTime: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var distanceAway: UILabel!
    var expiryTime:Float = 0.0
    var fullTime:Float = 0.0
    var remainingTime:Float = 0.0
    var acceptTimer  = Timer()
    
    override func awakeFromNib() {
        Helper.shadowView(sender: self.topView, width: UIScreen.main.bounds.size.width - 20, height: 118)

        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateTheAcceptData(data: Accepted) {
        self.customerName.text = data.firstName
        
        if  data.callType == .TeleCall {
            distanceAway.isHidden = true
        } else {
            distanceAway.isHidden = false
            if data.distanceMatrix == 1{
                self.distanceAway.text =  String(format: "%.02f", data.distance/1609.34) +  DistanceUnits.Miles + " away"
            }else{
                self.distanceAway.text = String(format: "%.02f", data.distance/1000) + DistanceUnits.Kms + " away"
            }
        }
        
        
        

        toBidLabel.isHidden = true
        bookingType.text =  Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.totalAmt))
        
        self.progressNStatus.text = data.statusMsg
        
        if data.callType == .Incall{
           self.categoryName.text = data.categoryName + "(In-call)"
        }else if  data.callType == .TeleCall{
        self.categoryName.text = data.categoryName + "(Tele-call)"
        } else {
             self.categoryName.text = data.categoryName
        }
        
         self.priceAmount.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.totalAmt))
        priceAmount.isHidden = true
        self.bookingTime.text = Helper.getTheDateFromTimeStamp(timeStamp:data.bookingRequestedFor)
    }
    
    func updateTheBidData(data: ActiveBid) {
        self.customerName.text = data.firstName
        
        if data.distanceMatrix == 1{
            self.distanceAway.text =  String(format: "%.02f", data.distance/1609.34) +  DistanceUnits.Miles + " away"
        }else{
            self.distanceAway.text = String(format: "%.02f", data.distance/1000) + DistanceUnits.Kms + " away"
        }
        
        
        if data.bookingType == .Now{
            if data.bookingModel == .Bidding {
                self.bookingType.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.totalAmt))
                toBidLabel.isHidden = true
                self.priceAmount.text = "My Quote : " + Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.quotedAmount))
               
                
            }
            else { // non Bidding
                self.bookingType.text = "ASAP"
                toBidLabel.isHidden = true
                self.priceAmount.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.totalAmt))
                
                
            }
        }else if data.bookingType == .Schedule{
            self.bookingType.text = "SCHEDULE"
            self.priceAmount.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.totalAmt))
            toBidLabel.isHidden = true
            
        } else {
            self.bookingType.text = "MULTIPLE SHIFT"
            self.priceAmount.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.totalAmt))
            toBidLabel.isHidden = true
           
        }
        
        fullTime = Float(data.bookingExpireTime - data.bookingRequestedat)
        expiryTime = Float(self.getCreationTimeInt(expiryTimeStamp: data.bookingExpireTime))
        
        
        if !acceptTimer.isValid {
            acceptTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
        }
        
        self.categoryName.text = data.categoryName
        
       // self.progressNStatus.text = data.statusMsg
        self.bookingTime.text = Helper.getTheDateFromTimeStamp(timeStamp:data.bookingRequestedFor)
        
        
    }
    
    
    
    /**
     *  Calculate time for Remaining
     */
    @objc func timerTick() {
        let remainingTime: Float = expiryTime - 1
        
        progressNStatus.text = timeFormate(Int(remainingTime))
        expiryTime = remainingTime
        
        if expiryTime == 0.0 {
            let notificationName = Notification.Name("gotNewBooking")
            NotificationCenter.default.post(name: notificationName, object: nil)
            acceptTimer.invalidate()
        }
    }
    
    /**
     *  Check for Time formate
     *
     *  @param remainingTime Remaining time
     *
     *  @return Return must 00:00/00
     */
    func timeFormate(_ remainingTime: Int) -> String {
        let hour: Int = Int(remainingTime) / 3600
        let min: Int = Int(remainingTime) / 60 % 60
        let secs: Int = Int(remainingTime) % 60
        
        var minsString = ""
        var secsString = ""
        
        if min < 10{
            minsString = "0" + String(describing:min)
        }else{
            minsString = String(describing:min)
        }
        
        if secs < 10{
            secsString = "0" + String(describing:secs)
        }else{
            secsString = String(describing:secs)
        }
        
        return String(format:"%02i:%02i:%02i", hour, min, secs)
        
        //return minsString + ":" + secsString
    }
    /**
     *  Get time interval between two times
     *
     *  @param dateFrom Datefrom
     *  @param dateTo   DateTo
     *
     *  @return Returns [01h:30m]
     */
    func getCreationTimeInt(expiryTimeStamp : Int64) -> Int64{
        let distanceTime = Date().timeIntervalSince1970
        return expiryTimeStamp - Int64(distanceTime)
    }
}
