//
//  MyeventModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 22/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

class PaymentModel {
    var key = ""
    var value = 0.0
    init(keyType:String, valueType:Double) {
        self.key = keyType
        self.value = valueType
    }
}

public class BookingTime {
    var status = 0
    var second = 0
    var startTimeStamp = 0
    init(timer:[String:Any]) {
        if let status = timer["status"] as? NSNumber{
            self.status = Int(status)
        }
        
        if let second = timer["second"] as? NSNumber{
            self.second = Int(second)
        }
        
        if let startTimeStamp = timer["startTimeStamp"] as? NSNumber{
            self.startTimeStamp = Int(startTimeStamp)
        }
    }
}


public class GigTime {
    var name = ""
    var unit = ""
    var price = 0
    var second = 0
    init(dict:[String:Any]) {
        if let name = dict["name"] as? String{
            self.name = name
           
        }
        if let unit = dict["unit"] as? String{
            self.unit = unit
        }
        if let price = dict["price"] as? NSNumber{
            self.price = Int(price)
        }
        if let second = dict["second"] as? NSNumber{
            self.second = Int(second)
        }
    }
}

public class BookingServices {
    
    var serviceName = ""
    var servicePrice = 0.0
    var quantity = 0
    init(service:[String:Any]) {
        if let quant = service["quntity"] as? NSNumber{
            self.quantity =  Int(quant)
        }
        
        if let serviceAmt = service["amount"] as? NSNumber{
            self.servicePrice = Double(serviceAmt)
        }
        
        if let serviceName = service["serviceName"] as? String{
            self.serviceName = serviceName
        }
    }
}

public class QuestionAnswer {
    var question = ""
    var answer = ""
    var type = 0
    
    init(eachQA:[String:Any]) {
        if let question = eachQA["name"] as? String{
            self.question = question
        }
        if let answer = eachQA["answer"] as? String{
            self.answer = answer
        }
        if let answer = eachQA["questionType"] as? Int{
            self.type = answer
        }
    }
}

class MyeventModel: NSObject {
    var acceptedData = [Accepted]()
    var requestData  = [Request]()
    var activeBidData = [ActiveBid]()
    
    var notificationCount = 0
    var pendingBookings = [Int]()
    var isBidCategory = false
    
    func parsingTheServiceResponse(responseData:[String: Any]) -> MyeventModel{
        acceptedData = [Accepted]()
        requestData    = [Request]()
        activeBidData = [ActiveBid]()
        Helper.hidePI()
        let bookingsData = MyeventModel()
        
        if let notiCount = responseData["notificationUnreadCount"] as? Int {
            bookingsData.notificationCount = notiCount
        }
        
        if let pendingArray = responseData["pendingBooking"] as? [Int] {
            for pendingBooking in pendingArray {
                
                bookingsData.pendingBookings.append(pendingBooking)
            }
            
        }
        
       
        bookingsData.isBidCategory = responseData["bid"] as? Bool ?? false
        
        if let acceptedDict = responseData["accepted"] as? [[String: Any]] {
            bookingsData.acceptedData = acceptedDict.map {
                Accepted.init(acceptData: $0)
            }
        }

        if let bookingData = responseData["request"] as? [[String: Any]] {
            bookingsData.requestData = bookingData.map {
                Request.init(bookingData: $0)
            }
        }

        if let bookingData = responseData["activeBid"] as? [[String: Any]] {
            bookingsData.activeBidData = bookingData.map {
                ActiveBid.init(bookingData: $0)
            }
        }
        
        self.handlingThePublishData(bookings:bookingsData)
        return bookingsData
    }
    
    func parseAcceptedDataResponse(responseData:[[String: Any]]) -> MyeventModel {
        acceptedData = [Accepted]()
        let bookingsData = MyeventModel()
        bookingsData.acceptedData = responseData.map {
            Accepted.init(acceptData: $0)
        }
        self.handlingThePublishData(bookings:bookingsData)
        return bookingsData
    }
    
    
    
    ///******on going bookings bid's and status *************//
    func handlingThePublishData(bookings:MyeventModel){
        var bookingSumStr = String()
        
        if bookings.acceptedData.count == 0  && bookings.requestData.count == 0 {
            let ud = UserDefaults.standard
            ud.removeObject(forKey: USER_INFO.SELBID)
            UserDefaults(suiteName: API.groupIdentifier)!.removeObject(forKey: "bookingStr")
            UserDefaults.standard.synchronize()
            ud.synchronize()
            return
        }else{
            
            for accData  in bookings.acceptedData {
                if bookingSumStr.isEmpty {
                    bookingSumStr = String(describing:accData.bookingId) + "|" + String(describing:accData.statusCode)
                }
                else{
                    bookingSumStr = bookingSumStr + "," + String(describing:accData.bookingId) + "|"  + String(describing:accData.statusCode)
                }
            }
            
            for reqData  in bookings.requestData {
                if bookingSumStr.isEmpty {
                    bookingSumStr = String(describing:reqData.bookingId) + "|" + String(describing:reqData.statusCode)
                }
                else{
                    bookingSumStr = bookingSumStr + "," + String(describing:reqData.bookingId) + "|"  + String(describing:reqData.statusCode)
                }
            }
            
            let ud = UserDefaults.standard
            ud.set(bookingSumStr, forKey: USER_INFO.SELBID)
            UserDefaults(suiteName: API.groupIdentifier)!.set(bookingSumStr, forKey: "bookingStr")
            UserDefaults.standard.synchronize()
            ud.synchronize()
        }
    }
}

