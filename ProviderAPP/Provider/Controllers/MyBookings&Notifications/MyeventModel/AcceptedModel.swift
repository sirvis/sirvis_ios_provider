//
//  AcceptedModel.swift
//  LSP
//
//  Created by Vengababu Maparthi on 10/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation

enum ServiceType:Int {
    case Fixed = 1
    case Hourly = 2
    case Bidding = 3
}

enum BookingType:Int {
    case Now = 1
    case Schedule = 2
    case Repeat = 3
}

enum BookingModelType:Int {
    case OnDemand = 1
    case MarketPlace = 2
    case Bidding = 3
}

enum CallType:Int {
    case Incall = 1
    case OutCall = 2
    case TeleCall = 3
}

class Accepted {
    
    var bookingType:BookingType = .Now
    var bookingModel:BookingModelType = .OnDemand
    var serviceType:ServiceType = .Fixed
    var callType:CallType = .OutCall
    
    var addLine1 = ""
    var addLine2 = ""
    var averageRating = 0.0
    var bookingExpireTime:Int64  = 0
    var actuallGigTimeStart:Int64 = 0
    var bookingRequestedFor:Int64 = 0
    var bookingRequestEnds:Int64 = 0
    var bookingRequestedat:Int64  = 0
    var bookingId:Int64  = 0
    var bookingId1:String  = "0"
    var city = ""
    var country = ""
    var firstName = ""
    var gigTime = ""
    var lastName = ""
    var latitude = 0.0
    var longitude = 0.0
    var paymentMethod = ""
    var pincode = ""
    var placeId = ""
    var price = 0.0

    
    var profilePic = ""
    var typeofEvent = ""
    var statusCode = 0
    var mobileNum = ""
    var distance = 0.0
    var customerId = ""
    var bookingTime:BookingTime?
    var GEGTime:GigTime?
    var signature = ""
    
    var currencyType = ""
    var currencySymbol = ""
    var discount = 0.0
    var totalAmt = 0.0
    var distanceMatrix = 0
    var categoryName = ""
    var statusMsg = ""
    var services = [BookingServices]()
    var paymentServices = [PaymentModel]()
    var jobDesc = ""
    var totalHourlyFee = 0.0
    var totalJobTime = 0.0
    var paidByWallet = false
    var travelFees = 0.0
    var bidCharge = 0.0
    var bidAmount = 0.0
    var quotedAmount = 0.0
    var questionAnswers = [QuestionAnswer]()
    var dayARRAY = ""
    var callDuration = 0
    init(acceptData:[String:Any]) {
        
        if let callType = acceptData["callType"] as? Int {
            self.callType =  CallType(rawValue: callType)!
        }
        
        if let quoted = acceptData["quotedPrice"] as? Double {
            self.quotedAmount = quoted
        }
        if let bookingType = acceptData["bookingType"] as? Int {
            
            self.bookingType = BookingType(rawValue: bookingType)!
        }
        
        if let duration = acceptData["callButtonEnableForProvider"] as? Int {
            
            self.callDuration = duration
        }
        
        if let bookingModel = acceptData["bookingModel"] as? Int {
            
            self.bookingModel = BookingModelType(rawValue: bookingModel)!
        }
        if let daysArray = acceptData["daysArr"] as? [String] {
            for day in daysArray{
                
                if self.dayARRAY.isEmpty{
                    self.dayARRAY = day
                }else{
                    self.dayARRAY = self.dayARRAY + " , " + day
                }
            }
        }
        
        if let signatureUrl = acceptData["signatureUrl"] as? String{
            signature = signatureUrl
        }
        
        if  let category      =  acceptData["category"] as? String {
            self.categoryName = category
        }
        
        if  let statusMsg      =  acceptData["statusMsg"] as? String {
            self.statusMsg = statusMsg
        }
        
        if  let address1      =  acceptData["addLine1"] as? String {
            self.addLine1 = address1
        }
        
        if  let currType  = acceptData["currency"] as? String {
            self.currencyType = currType
        }
        
        
        if  let currSymbol  = acceptData["currencySymbol"] as? String {
            self.currencySymbol = currSymbol
        }
        
        if  let distanceAway  = acceptData["distance"] as? NSNumber {
            self.distance = Double(Float(distanceAway))
        }
        
        if  let distanceMatrix  = acceptData["distanceMatrix"] as? NSNumber {
            self.distanceMatrix = Int(distanceMatrix)
        }
        
        if  let address2       = acceptData["addLine2"] as? String {
            self.addLine2 = address2
        }
        
        if  let averageRat  = acceptData["averageRating"] as? NSNumber {
            self.averageRating = Double(Float(averageRat))
        }
        
        if   let expiryTime     = acceptData["bookingExpireForProvider"] as? NSNumber {
            self.bookingExpireTime = Int64(expiryTime)
        }
        
        if   let eventStartTime     = acceptData["eventStartTime"] as? NSNumber {
            self.actuallGigTimeStart = Int64(eventStartTime)
        }
        
        
        if let bookId     = acceptData["bookingId"] as? NSNumber {
            self.bookingId = Int64(bookId)
            
        }
        if let idd = acceptData["bookingId"] as? String{
            self.bookingId1 = idd
        }
        
        if  let city = acceptData["city"] as? String {
            self.city = city
        }
        if   let country     = acceptData["country"] as? String {
            self.country = country
        }
        
        if let firstName     = acceptData["firstName"] as? String {
            self.firstName = firstName
        }
        
        if  let lastname = acceptData["lastName"] as? String {
            self.lastName = lastname
        }
        
        if  let serviceType = acceptData["serviceType"] as? Int {
            self.serviceType = ServiceType(rawValue: serviceType)!
        }
        
        if   let lat     = acceptData["latitude"] as? NSNumber {
            self.latitude = Double(lat)
        }
        
        if let longt     = acceptData["longitude"] as? NSNumber {
            self.longitude = Double(longt)
        }
        
        if  let paymentType = acceptData["paymentMethod"] as? String {
            self.paymentMethod = paymentType
        }
        
        if let pincode     = acceptData["pincode"] as? String {
            self.pincode = pincode
        }
        
        if  let profileImg = acceptData["profilePic"] as? String {
            self.profilePic = profileImg
        }
        
        if  let eventType = acceptData["typeofEvent"] as? String {
            self.typeofEvent = eventType
        }
        
        if let status = acceptData["status"] as? NSNumber{
            self.statusCode = Int(status)
        }
        
        if  let price     = acceptData["price"] as? NSNumber {
            self.price =  Double(price)
        }
        
        if  let bookingRequestedFor  = acceptData["bookingRequestedFor"] as? NSNumber {
            self.bookingRequestedFor = Int64(bookingRequestedFor)
        }
        
        if  let bookingRequestedAt  = acceptData["bookingRequestedAt"] as? NSNumber {
            self.bookingRequestedat = Int64(bookingRequestedAt)
        }
        
        if let endTime = acceptData["bookingEndtime"] as? NSNumber {
            self.bookingRequestEnds = Int64(endTime)
        }
        
        if  let custID  = acceptData["customerId"] as? String  {
            self.customerId = custID
        }
        
        if let mobile  = acceptData["phone"] as? String{
            self.mobileNum = mobile
        }
        
        if let desc = acceptData["jobDescription"] as? String {
            self.jobDesc = desc
        }
        
        if let dict = acceptData["gigTime"] as? [String:Any]{
            let gigDuration = GigTime.init(dict:dict)
            if let name = dict["name"] as? String{
                if let unit = dict["unit"] as? String{
                    self.gigTime = name + " " + unit
                }
            }
            if let price = dict["price"] as? NSNumber{
                self.price = Double(price)
            }
            self.GEGTime = gigDuration
        }
        
        if let timer = acceptData["bookingTimer"] as? [String:Any]{
            self.bookingTime =  BookingTime.init(timer: timer)
        }
        
        if let cartDataDict = acceptData["cartData"] as? [[String:Any]] {
            self.services = cartDataDict.map{
                BookingServices.init(service:$0)
            }
            
        }
        
        
        if let QAArray = acceptData["questionAndAnswer"] as? [[String:Any]] {
            self.questionAnswers = QAArray.map {
                QuestionAnswer.init(eachQA:$0)
            }
        }
        
        if let account = acceptData["accounting"] as? [String:Any]{
            
            if let totalHourlyFee = account["totalActualHourFee"] as? NSNumber{
                self.totalHourlyFee =  Double(totalHourlyFee)
            }

            if let totalTime = account["totalActualJobTimeMinutes"] as? NSNumber{
                self.totalJobTime =  Double(totalTime)
            }
            
            if let totalFee = account["total"] as? NSNumber{
                self.totalAmt =  Double(totalFee)
                if let dues = account["lastDues"] as? Double {
                    self.totalAmt = self.totalAmt - dues
                }
            }
            
            if let discount = account["discount"] as? NSNumber{
                self.discount =  Double(discount)
            }
            
            if let paidByWal = account["paidByWallet"] as? Int {
                if paidByWal == 1 {
                    self.paidByWallet = true
                }
            }
            
            if let bidCredit = account["bidCredit"] as? Double{
                self.bidCharge = bidCredit
            }
            
            if let bidPrice = account["bidPrice"] as? Double{
                self.bidAmount = bidPrice
            }
            
            if let discount = account["discount"] as? Double{
                if discount > 0 {
                    self.paymentServices.append(PaymentModel.init(keyType:"Discount",valueType:discount))
                    
                }
            }
            
            if let visitFees = account["visitFee"] as? Double{
                if visitFees > 0{
                    self.paymentServices.append(PaymentModel.init(keyType:"Visit Fee",valueType:visitFees))
                    
                }
            }
//
//            if let hourlyFees = account["totalActualHourFee"] as? Double{
//                if hourlyFees > 0{
//                    let hourlyMinutes = account["totalExpectJobTimeMinutes"] as? Double ?? 0.0
//                    let tuple = GenericUtilityMethods.minutesToHoursMinutes(minutes: Int(hourlyMinutes) ?? 0)
//                    let hourlyHeader = "\(tuple.hours) hrs : \(tuple.leftMinutes) mins"
//                    self.paymentServices.append(PaymentModel.init(keyType: hourlyHeader,valueType:hourlyFees))
//
//                }
//            }
            
            
            if let travelFee = account["travelFee"] as? Double {
                if travelFee > 0{
                    self.paymentServices.append(PaymentModel.init(keyType:"Travel Fee",valueType:travelFee))
                    
                }
                self.travelFees = travelFee
            }
            
            if let dues = account["lastDues"] as? Double {
                if dues > 0 {
                  //  self.paymentServices.append(PaymentModel.init(keyType:"Last Due",valueType:dues))
                }
            }
        }
    }
}


