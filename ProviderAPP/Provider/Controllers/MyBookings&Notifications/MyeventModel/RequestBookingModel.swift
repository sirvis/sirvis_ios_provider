//
//  RequestBookingModel.swift
//  LSP
//
//  Created by Vengababu Maparthi on 10/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation

class Request {
    var bookingType:BookingType = .Now
    var bookingModel:BookingModelType = .OnDemand
    var serviceType:ServiceType = .Fixed
    var callType:CallType = .OutCall
    var addLine1 = ""
    var addLine2 = ""
    var averageRating = 0.0
    var bookingExpireTime:Int64  = 0
    var bookingRequestedFor:Int64 = 0
    var actuallGigTimeStart:Int64 = 0
    var bookingRequestEnds:Int64 = 0
    var bookingRequestedat:Int64  = 0
    var bookingId:Int64  = 0
    var city = ""
    var country = ""
    var firstName = ""
    var gigTime = ""
    var lastName = ""
    var latitude = 0.0
    var longitude = 0.0
    var paymentMethod = ""
    var pincode = ""
    var placeId = ""
    var price = 0.0
    var profilePic = ""
    var typeofEvent = ""
    var statusCode = 0
    var mobileNum = ""
    var distance = 0.0
    var customerId = ""
    var GEGTime:GigTime?
    var currencyType = ""
    var currencySymbol = ""
    var discount = 0.0
    var totalAmt = 0.0
    var distanceMatrix = 0
    var categoryName = ""
    var statusMsg = ""
    var jobDesc = ""
    var totalHourlyFee = 0.0
    var totalJobTime = 0.0
    var paidByWallet = false
    var bidCharge = 0.0
    var bidAmount = 0.0
    var dayARRAY = ""
    var days = [String]()
    var shifts = 0
    var services = [BookingServices]()
    var questionAnswers = [QuestionAnswer]()
    var paymentServices = [PaymentModel]()
    var duration = 0.0
    
    init(bookingData:[String:Any]) {

        if let bookingType = bookingData["bookingType"] as? Int {
            
            self.bookingType = BookingType(rawValue: bookingType)!
        }
        
        if let callType = bookingData["callType"] as? Int {
            self.callType = CallType(rawValue: callType)!
        }
        
        if let bookingModel = bookingData["bookingModel"] as? Int {
            
            self.bookingModel = BookingModelType(rawValue: bookingModel)!
        }
        
        if let daysArray = bookingData["daysArr"] as? [String] {
            for day in daysArray{
                self.days.append(String(describing: day.first!))
                if self.dayARRAY.isEmpty{
                    self.dayARRAY = day
                }else{
                    self.dayARRAY = self.dayARRAY + " , " + day
                }
            }
        }
        if  let statusMsg      =  bookingData["statusMsg"] as? String {
            self.statusMsg = statusMsg
        }
        
        if  let address1      =  bookingData["addLine1"] as? String {
            self.addLine1   = address1
        }
        
        if  let category      =  bookingData["category"] as? String {
            self.categoryName = category
        }
        
        if  let currType  = bookingData["currency"] as? String {
            self.currencyType = currType
        }
        
        if  let currSymbol  = bookingData["currencySymbol"] as? String {
            self.currencySymbol = currSymbol
        }
        
        if  let address2       = bookingData["addLine2"] as? String {
            self.addLine2 = address2
        }
        
        if  let distanceAway       = bookingData["distance"] as? NSNumber {
            self.distance       =  Double(Float(distanceAway))
        }
        
        if  let distanceMatrix  = bookingData["distanceMatrix"] as? NSNumber {
            self.distanceMatrix = Int(distanceMatrix)
        }
        
        if  let bookingRequestedFor  = bookingData["bookingRequestedFor"] as? NSNumber {
            self.bookingRequestedFor = Int64(bookingRequestedFor)
        }
        
        if  let bookingRequestedAt  = bookingData["bookingRequestedForProvider"] as? NSNumber {
            self.bookingRequestedat = Int64(bookingRequestedAt) //bookingRequestedAt
        }
        
        if let endTime = bookingData["bookingEndtime"] as? NSNumber {
            self.bookingRequestEnds = Int64(endTime)
        }
        
        if   let eventStartTime     = bookingData["eventStartTime"] as? NSNumber {
            self.actuallGigTimeStart = Int64(eventStartTime)
        }
        
        if  let custID  = bookingData["customerId"] as? String  {
            self.customerId = custID
        }
        
        
        if  let averageRat  = bookingData["averageRating"] as? NSNumber {
            self.averageRating = Double(Float(averageRat))
        }
        
        if   let expiryTime     = bookingData["bookingExpireForProvider"] as? NSNumber {
            self.bookingExpireTime = Int64(expiryTime) //bookingExpireTime
        }
        
        if let bookId     = bookingData["bookingId"] as? NSNumber {
            self.bookingId = Int64(bookId)
        }
        
        if  let city = bookingData["city"] as? String {
            self.city = city
        }
        if   let country     = bookingData["country"] as? String {
            self.country = country
        }
        
        if let firstName     = bookingData["firstName"] as? String {
            self.firstName = firstName
        }
        
        if  let lastname = bookingData["lastName"] as? String {
            self.lastName = lastname
        }
        
        if  let serviceType = bookingData["serviceType"] as? Int {
            self.serviceType = ServiceType(rawValue: serviceType)!
        }
        
        if   let lat     = bookingData["latitude"] as? NSNumber {
            self.latitude = Double(lat)
        }
        
        if let longt     = bookingData["longitude"] as? NSNumber {
            self.longitude = Double(longt)
        }
        
        if  let paymentType = bookingData["paymentMethod"] as? String {
            self.paymentMethod = paymentType
        }
        if let pincode     = bookingData["pincode"] as? String {
            self.pincode = pincode
        }
        
        if  let profileImg = bookingData["profilePic"] as? String {
            self.profilePic = profileImg
        }
        
        if  let eventType = bookingData["typeofEvent"] as? String {
            self.typeofEvent = eventType
        }
        
        if let status = bookingData["status"] as? NSNumber{
            self.statusCode = Int(status)
        }
        
        if let mobile  = bookingData["phone"] as? String{
            self.mobileNum = mobile
        }
        
        duration = bookingData["scheduleTime"] as? Double ?? 0.0
        
        if let desc = bookingData["jobDescription"] as? String {
            self.jobDesc = desc
        }
        
        if let QAArray = bookingData["questionAndAnswer"] as? [[String:Any]] {
            self.questionAnswers = QAArray.map {
                QuestionAnswer.init(eachQA:$0)
            }
        }
        
        if let dict = bookingData["gigTime"] as? [String:Any]{
            if let name = dict["name"] as? String{
                if let unit = dict["unit"] as? String{
                    self.gigTime = name + " " + unit
                }
            }
            if let price = dict["price"] as? NSNumber{
                self.price = Double(price)
            }
            self.GEGTime = GigTime.init(dict:dict)
        }
        
        if let cartDataDict = bookingData["cartData"] as? [[String:Any]] {
            
            self.services = cartDataDict.map{
                BookingServices.init(service:$0)
            }

            
        }
        
        if let account = bookingData["accounting"] as? [String:Any]{
            
            if let totalFee = account["total"] as? NSNumber{
                self.totalAmt =  Double(totalFee)
                if let dues = account["lastDues"] as? Double {
                    self.totalAmt = self.totalAmt - dues
                }
            }
            
            if let shift = account["totalShiftBooking"] as? Int {
                self.shifts = shift
            }
            
            if let totalHourlyFee = account["totalActualHourFee"] as? NSNumber{
                self.totalHourlyFee =  Double(totalHourlyFee)
            }
            
            if let totalTime = account["totalActualJobTimeMinutes"] as? NSNumber{
                self.totalJobTime =  Double(totalTime)
            }
            
            if let discount = account["discount"] as? Double{
                if discount > 0{
                    self.paymentServices.append(PaymentModel.init(keyType:"Discount",valueType:discount))
                }
            }
            
            if let visitFees = account["visitFee"] as? Double{
                if visitFees > 0{
                    self.paymentServices.append(PaymentModel.init(keyType:"Visit Fee",valueType:visitFees))
                }
            }
            
            if let travelFree = account["travelFee"] as? Double {
                if travelFree > 0 {
                    self.paymentServices.append(PaymentModel.init(keyType:"Travel Fee",valueType:travelFree))
                }
            }
            
            if let dues = account["lastDues"] as? Double {
                if dues > 0 {
                   // self.paymentServices.append(PaymentModel.init(keyType:"Last Due",valueType:dues))
                }
            }
            
            if let paidByWal = account["paidByWallet"] as? Int {
                if paidByWal == 1 {
                    self.paidByWallet = true
                }
            }
            
            if let bidCredit = account["bidCredit"] as? Double{
                self.bidCharge = bidCredit
            }
            if let bidPrice = account["bidPrice"] as? Double{
                self.bidAmount = bidPrice
            }
        }
        
    }
}
