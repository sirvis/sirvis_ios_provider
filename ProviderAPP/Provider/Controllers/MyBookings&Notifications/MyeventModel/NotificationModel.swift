//
//  NotificationModel.swift
//  LSP
//
//  Created by Rahul Sharma on 6/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import RxAlamofire
import Alamofire
import RxSwift
import RxCocoa

class Notifications {
    var content = ""
    var NotificationTime:Int64 = 0
    var title = ""
    
}

class NotificationModel {
    
    var notifications = [Notifications]()
    let disposebag = DisposeBag()
    
    func fetchNotifications(skip:Int, limit: Int, completionHandler:@escaping (Bool,NotificationModel) -> ()){
        let notificationAPI = NotificationAPI()
        notificationAPI.getNotifications(skip: skip, limit: limit)
        
        notificationAPI.Notification_response
            .subscribe(onNext: {response in
                switch response.httpStatusCode{
                case 200:
                    if let dict = response.data["data"] as? [[String:Any]]  {
                        completionHandler(true,self.parsingTheServiceResponse(responseData: dict))
                    }
                    break
                default:
                    break
                }
            }, onError: { error in
                Helper.alertVC(errMSG: error.localizedDescription)
                print(error)
            }).disposed(by: disposebag)
    }
    
    func parsingTheServiceResponse(responseData:[[String: Any]]) -> NotificationModel{
        let notificationModel = NotificationModel()
        for responsedata in responseData {
            let notification = Notifications()
            if let title  = responsedata["title"] as? String {
                notification.title = title
            }
            if let message = responsedata["msg"] as? String {
                notification.content = message
            }
            if let timestamp = responsedata["timeStamp"] as? NSNumber {
                notification.NotificationTime = Int64(timestamp)
            }
             notificationModel.notifications.append(notification)
        }
       
        
        return notificationModel
    }
}
