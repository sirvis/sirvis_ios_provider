//
//  CompleteBookingModel.swift
//  LSP
//
//  Created by Vengababu Maparthi on 10/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation

var totVat = 0.0

class CompletedBookings:NSObject {
    var bookingType:BookingType = .Now
    var bookingModel:BookingModelType = .OnDemand
    var serviceType:ServiceType = .Fixed
    var callType:CallType = .OutCall
    
    var addLine1 = ""
    var addLine2 = ""
    var averageRating = 0.0
    var bookingExpireTime:Int64 = 0
    var bookingRequestedFor:Int64 = 0
    var bookingRequestedAt:Int64 = 0
    var bookingRequestedEndTime:Int64 = 0
    var bookingId:Int64 = 0
    var distance:Int64 = 0
    var city = ""
    var country = ""
    var firstName = ""
    var gigTime = ""
    var lastName = ""
    var latitude = 0.0
    var longitude = 0.0
    var paymentMethod = 0
    var pincode = ""
    var placeId = ""
    var price = 0.0
    var cancellationFees = 0.0
    var discount = 0.0
    var myEarnings = 0.0
    var appEarnings = 0.0
    var profilePic = ""
    var typeofEvent = ""
    var statusCode = 0
    var mobileNum = ""
    var statusMsg = ""
    var signature = ""
    var GEGTime:GigTime?
    var customerID = ""
    var currencyType = ""
    var currencySymbol = ""
    var cancellationReason = ""
    var distanceMatrix = 0
    var totalHourlyFee = 0.0
    var totalJobTime = 0.0
    var isPaidByWallet = false
    var walletAmtPaid = 0.0
    var cardOrCashPaid = 0.0
    var actuallGigTimeStart:Int64 = 0
    var paidByWallet = false
    var paymentMethod2 = ""
    var categoryName = ""
    var vat = 0.0
    var services = [BookingServices]()
    var additionalSerice = [AdditionalService]()
    var questionAnswers = [QuestionAnswer]()
    var paymentServices = [PaymentModel]()
    
    var pickupNote = ""
    var pickupImagesURL = [String]()
    var dropNote = ""
    var dropImagesURL = [String]()
    
    init(acceptData:[String:Any]) {
        if  let address1   =  acceptData["addLine1"] as? String {
            self.addLine1 = address1
        }
        
        if let callType = acceptData["callType"] as? Int {
            self.callType =  CallType(rawValue: callType)!
        }
        if let url = acceptData["pickupImages"] as? [String] {
            pickupImagesURL = url
        }
        if let url = acceptData["dropImages"] as? [String] {
           dropImagesURL = url
        }
        if let note = acceptData["pickupNotes"] as? String {
          pickupNote = note
        }
        if let note = acceptData["dropNotes"] as? String {
         dropNote = note
        }
        
        if  let bookingType   =  acceptData["bookingType"] as? Int {
            self.bookingType = BookingType(rawValue: bookingType)!
        }
        
        if  let customerID   =  acceptData["customerId"] as? String {
            self.customerID = customerID
        }
        if  let cancelReason  =  acceptData["cancellationReason"] as? String {
            self.cancellationReason = cancelReason
        }
        
        if  let category      =  acceptData["category"] as? String {
            self.categoryName = category
        }
        if  let category      =  acceptData["categoryName"] as? String {
            self.categoryName = category
        }
        
        if   let eventStartTime     = acceptData["eventStartTime"] as? NSNumber {
            self.actuallGigTimeStart = Int64(eventStartTime)
        }
        
        if  let paymentType = acceptData["paymentMethodText"] as? String {
            self.paymentMethod2 = paymentType
        }
        
        if  let currType  = acceptData["currency"] as? String {
            self.currencyType = currType
        }
        
        if  let currSymbol  = acceptData["currencySymbol"] as? String {
            self.currencySymbol = currSymbol
        }
        
        if  let address2    = acceptData["addLine2"] as? String {
            self.addLine2 = address2
        }
        
        
        if   let expiryTime     = acceptData["bookingExpireTime"] as? NSNumber {
            self.bookingExpireTime = Int64(expiryTime)
        }
        
        if let bookId     = acceptData["bookingId"] as? NSNumber {
            self.bookingId = Int64(bookId)
        }
        
        if let distance     = acceptData["distance"] as? NSNumber {
            self.distance = Int64(distance)
        }
        
        if let distanceMatrix     = acceptData["distanceMatrix"] as? NSNumber {
            self.distanceMatrix = Int(distanceMatrix)
        }
        
        if  let city = acceptData["city"] as? String {
            self.city = city
        }
        
        if   let country     = acceptData["country"] as? String {
            self.country = country
        }
        
        if let customerData = acceptData["customerData"] as? [String:Any]{
            if let firstName     = customerData["firstName"] as? String {
                self.firstName = firstName
            }
            if  let lastname = customerData["lastName"] as? String {
                self.lastName = lastname
            }
            
            if  let profileImg = customerData["profilePic"] as? String {
                self.profilePic = profileImg
            }
            
            if let mobile  = customerData["phone"] as? String{
                self.mobileNum = mobile
            }
            if let status = acceptData["status"] as? NSNumber{
                self.statusCode = Int(status)
                if status != 10{
                    if  let averageRat  = customerData["averageRating"] as? NSNumber {
                        self.averageRating = Double(Float(averageRat))
                    }
                }else{
                    if let reviewData = acceptData["reviewByProvider"] as? [String:Any]{
                        if  let averageRat  = reviewData["rating"] as? NSNumber {
                            self.averageRating = Double(Float(averageRat))
                        }
                    }
                }
            }
        }
        
        
        if let firstName     = acceptData["firstName"] as? String {
            self.firstName = firstName
        }
        
        if  let lastname = acceptData["lastName"] as? String {
            self.lastName = lastname
        }
        
        if  let serviceType = acceptData["serviceType"] as? Int {
            self.serviceType = ServiceType(rawValue: serviceType)!
        }
        
        if   let lat     = acceptData["latitude"] as? NSNumber {
            self.latitude = Double(lat)
        }
        
        if let longt     = acceptData["longitude"] as? NSNumber {
            self.longitude = Double(longt)
        }
        
        if  let paymentType = acceptData["paymentMethod"] as? NSNumber {
            self.paymentMethod = Int(paymentType)
        }
        
        
        if let pincode     = acceptData["pincode"] as? String {
            self.pincode = pincode
        }
        
        if  let profileImg = acceptData["profilePic"] as? String {
            self.profilePic = profileImg
        }
        
        if let dict = acceptData["event"] as? [String:Any]{
            if  let eventType = dict["name"] as? String {
                self.typeofEvent = eventType
            }
        }
        
        if let status = acceptData["status"] as? NSNumber{
            self.statusCode = Int(status)
        }
        
        if  let bookingRequestedFor  = acceptData["bookingRequestedFor"] as? NSNumber {
            self.bookingRequestedFor = Int64(bookingRequestedFor)
        }
        
        if  let bookingRequestedAt  = acceptData["bookingRequestedAt"] as? NSNumber {
            self.bookingRequestedAt = Int64(bookingRequestedAt)
        }
        
        if  let bookingEndtime  = acceptData["bookingEndtime"] as? NSNumber {
            self.bookingRequestedEndTime = Int64(bookingEndtime)
        }
        
        if let mobile  = acceptData["phone"] as? String{
            self.mobileNum = mobile
        }
        
        if let statMsg = acceptData["statusMsg"] as? String{
            self.statusMsg = statMsg
        }
        
        if let signatureUrl = acceptData["signatureUrl"] as? String{
            self.signature = signatureUrl
        }
        
        if let reviewData = acceptData["reviewByProvider"] as? [String:Any]{
            if  let averageRat  = reviewData["rating"] as? NSNumber {
                self.averageRating = Double(Float(averageRat))
            }
        }
        
        
        if let QAArray = acceptData["questionAndAnswer"] as? [[String:Any]] {
            self.questionAnswers = QAArray.map {
                QuestionAnswer.init(eachQA: $0)
            }
        }
        
        if let account = acceptData["accounting"] as? [String:Any]{
            
            if let cancelAmount = account["cancellationFee"] as? NSNumber{
                self.cancellationFees = Double(cancelAmount)
            }
            if let vat = account["totalVat"] as? NSNumber{
                self.vat = Double(vat)
                totVat = Double(vat)
            }
            if let myEarning = account["providerEarning"] as? NSNumber{
                self.myEarnings = Double(myEarning)
            }
            
            if let appEarning = account["appEarningPgComm"] as? NSNumber{
                self.appEarnings = Double(appEarning)
            }
            
            if let totalFee = account["total"] as? NSNumber{
                self.price =  Double(totalFee)
                if let dues = account["lastDues"] as? Double {
                    self.price = self.price - dues
                }
            }
            
            if let paidByWal = account["paidByWallet"] as? Int {
                if paidByWal == 1 {
                    self.paidByWallet = true
                }
            }
            if let discount = account["discount"] as? Double{
                
                if discount > 0 {
                    self.paymentServices.append(PaymentModel.init(keyType: "Discount", valueType: discount))
                }
            }
            if let visitFees = account["visitFee"] as? Double{
                if visitFees > 0{
                    self.paymentServices.append(PaymentModel.init(keyType: "Visit Fee", valueType: visitFees))
                }
            }
            
            if let travelFree = account["travelFee"] as? Double {
                if travelFree > 0 {
                    self.paymentServices.append(PaymentModel.init(keyType: "Travel Fee", valueType: travelFree))
                }
            }
            
            if let dues = account["lastDues"] as? Double {
                if dues != 0 {
                   // self.paymentServices.append(PaymentModel.init(keyType: "Last Due", valueType: dues))
                }
            }
            if let totalHourlyFee = account["totalActualHourFee"] as? NSNumber{
                self.totalHourlyFee =  Double(totalHourlyFee)
            }
            
            if let totalTime = account["totalActualJobTimeMinutes"] as? NSNumber{
                self.totalJobTime =  Double(totalTime)
            }
            
            if let paidByWal = account["paidByWallet"] as? Int {
                if paidByWal == 1 {
                    self.isPaidByWallet = true
                }
            }
            if let walletAmt = account["captureAmount"] as? NSNumber{
                self.walletAmtPaid =  Double(walletAmt)
            }
            if let cashCardAmt = account["remainingAmount"] as? NSNumber{
                self.cardOrCashPaid =  Double(cashCardAmt)
            }
            
            if let discount = account["discount"] as? NSNumber{
                self.discount = Double(discount)
            }
        }
        
        if let addionalServices = acceptData["additionalService"] as? [[String:Any]]{
            self.additionalSerice = addionalServices.map {
                AdditionalService.init(addtionalService: $0)
            }
        }
        
        if let dict = acceptData["service"] as? [String:Any]{
            if let name = dict["name"] as? String{
                if let unit = dict["unit"] as? String{
                    self.gigTime = name + unit
                }
            }
            self.GEGTime = GigTime.init(dict: dict)
        }
        
        if let itemDict = acceptData["item"] as? [[String:Any]] {
            self.services = itemDict.map{
                BookingServices.init(service:$0)
            }
        }
    }
}

