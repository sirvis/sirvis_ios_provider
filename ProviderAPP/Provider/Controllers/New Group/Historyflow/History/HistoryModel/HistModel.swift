//
//  HistModel.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 23/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import Foundation

class AdditionalService {
    var price = ""
    var serviceName = ""
    init(addtionalService:[String:Any]) {
        if let price = addtionalService["price"] as? String{
            self.price = price
        }
        
        if let serviceName = addtionalService["serviceName"] as? String{
            self.serviceName = serviceName
        }
    }
}


class HistModel: NSObject {
    
    var bookingsData = [CompletedBookings]()
    var weeksData =  [Double]()
    var startDate:Int64 = 0
    var endDate:Int64 = 0
    var startDate1 = ""
    var endDate1 = ""
    var weekBookingData = [HistModel]()
    
    func parsingTheServiceResponse(responseData:[[String: Any]]) -> HistModel{
        Helper.hidePI()
        let bookingData = HistModel()
        bookingData.bookingsData = responseData.map {
            CompletedBookings.init(acceptData:$0)
        }
        return bookingData
    }
    
    
    func parsingTheBidDicteResponse(bidData:[String: Any]) -> ActiveBid{
        Helper.hidePI()
        return ActiveBid.init(bookingData: bidData)
    }
    
    
    func parsingTheDicteResponse(acceptData:[String: Any]) -> Accepted{
        Helper.hidePI()
        return Accepted.init(acceptData: acceptData)
    }
    
    
    
    func parseTheWeeksAndBookingsCound(responseData:[[String: Any]]) -> [HistModel] {
        weekBookingData = [HistModel]()
        for week in responseData {
            let weekData = HistModel()
            if let startDate = week["startDate"] as? NSNumber{
                weekData.startDate = Int64(startDate)
            }
            
            if let endDate = week["endDate"] as? NSNumber{
                weekData.endDate = Int64(endDate)
            }
            
            if let sDate = week["sDate"] as? String{
                weekData.startDate1 = sDate
            }
            
            if let eDate = week["eDate"] as? String{
                weekData.endDate1 = eDate
            }
            
            if let weeksCount = week["count"] as? [Any]{
                var weeksBookings =  [Double]()
                for week in weeksCount {
                    if let count = week as? NSNumber{
                        if count == 0{
                            weeksBookings.append(0.0001)
                        }else{
                            weeksBookings.append(Double(count))
                        }
                    }
                }
                weekData.weeksData = weeksBookings
            }
            weekBookingData.append(weekData)
        }
        return weekBookingData
    }
}
