//
//  PastBookingDetailsExtns.swift
//  LSP
//
//  Created by Vengababu Maparthi on 11/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import Kingfisher


extension PastBookingController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if bookingId == ""{
            if bookingArray!.statusCode == 5 || bookingArray!.statusCode == 4{
                return 1
            }
            else  if bookingArray!.statusCode == 11 || bookingArray!.statusCode == 12{
                return 7 //5
            }else{
                return 8 //6
            }
        }else{
            if self.serviceAPI {
                return 8
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType : PBSectionType = PBSectionType(rawValue: section)!
        
        switch sectionType {
            
        case .PBDriverDetails:
            return 1
            
        case .PBBillDetails :
            if bookingArray!.statusCode == 11 || bookingArray!.statusCode == 12{
                return 3
            }else{
                return 2 + bookingArray!.services.count
            }
            
        case .PBGrandTot:
            
            return 1 + bookingArray!.additionalSerice.count
            
        case .PBServies:
            return 3
            
            
        case .PBQA:
            return (bookingArray?.questionAnswers.count)!
            
        case .PBPaymentType :
            if (bookingArray?.isPaidByWallet)! {
                return 3
            }
            else {
                return 2
            }
            
            
        case .PBReceiversDetails :
            
            return 2
         
        case .PBDrop :
            return 0 //bookingArray?.pickupImagesURL.count > 0 ? 0 : 0
            
        case .PBPickup:
            return  0 //bookingArray?.pickupImagesURL.count > 0 ? 0 : 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType : PBSectionType = PBSectionType(rawValue: indexPath.section)!
        
        
        let header: PBHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "header") as! PBHeaderTableViewCell
        
        let rowType : PBRowType = PBRowType(rawValue: indexPath.row)!
        
        switch sectionType {
        case .PBDriverDetails:
            
            let cell: PBDriverDetailsCell = tableView.dequeueReusableCell(withIdentifier: PBDriverDetailsCell.identifier) as! PBDriverDetailsCell
            
            switch bookingArray!.statusCode{
            case 5:
                cell.statusView.backgroundColor = COLOR.bookingExpiredColor
                break
            case 4:
                cell.statusView.backgroundColor = COLOR.cancelDeclinedRed
                break
            case 12:
                cell.statusView.backgroundColor = COLOR.cancelDeclinedRed
                break
            case 11:
                cell.statusView.backgroundColor = COLOR.cancelDeclinedRed
                break
            case 10:
                cell.statusView.backgroundColor = COLOR.completedBookingColor
                break
            default:
                cell.statusView.backgroundColor = COLOR.onGoingBlue
                break
            }
            
            if bookingArray!.statusCode == 5 || bookingArray!.statusCode == 4 || bookingArray!.statusCode == 11 || bookingArray!.statusCode == 12{
                cell.alignmentForSenderNAme.constant = 0
                cell.rating.isHidden = true
                cell.starImage.isHidden = true
                cell.howMuchRatedLabel.isHidden = true
                
            }else{
                cell.rating.isHidden = false
                cell.starImage.isHidden = false
                cell.howMuchRatedLabel.isHidden = false
                cell.alignmentForSenderNAme.constant = -6
                cell.rating.text  = String(describing:bookingArray!.averageRating)
            }
            
            cell.setupCell(pastData: bookingArray!)
            
           
            
            return cell
            
        case .PBBillDetails:
            let cell :PBBillDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PBBillDetailsCell") as! PBBillDetailsCell
            switch indexPath.row {
            case 0:
                header.sectionHeader.text = pastBooking.details
                return header
                
            default:
                if bookingArray!.statusCode == 11 || bookingArray!.statusCode == 12{
                    if indexPath.row == 1{
                        cell.key.text = "VAT"
                        if bookingArray!.vat > 0 {
                        cell.value.text  = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.vat))
                        }else{
                             cell.value.text = "0"
                        }
                    }
                    else{
                    cell.key.text = pastBooking.cancellationFee
                    cell.value.text  = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.cancellationFees))
                }
                }else{
                    if indexPath.row == (1 + bookingArray!.services.count) {
                        cell.key.text = "VAT"
                        if bookingArray!.vat > 0 {
                            cell.value.text  = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.vat))
                        }else{
                            cell.value.text = "0"
                        }
                        }
                  else{
                    if bookingArray?.callType == .OutCall {
                        if bookingArray?.serviceType == .Fixed{
                            cell.key.text = (bookingArray?.services[indexPath.row - 1].serviceName)! + "*" + String(format: "%d", (bookingArray?.services[indexPath.row - 1].quantity)!)
                            cell.value.text  = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", (bookingArray?.services[indexPath.row - 1].servicePrice)!))
                        }else{
                            
                           
                            cell.key.text = Helper.timeInHourMin((bookingArray?.totalJobTime)!)
                            cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingArray!.totalHourlyFee))
                        }
                    } else {
                        cell.key.text = "Consultation Fee".localized
                         cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingArray!.totalHourlyFee))
                    }
                }
                }
                return cell
            }
            
            
            
        case .PBGrandTot:
            switch indexPath.row{
            case 0:
                
                if bookingArray?.additionalSerice.count != 0 {
                    let cell :PBBillDetailsCell = tableView.dequeueReusableCell(withIdentifier: PBBillDetailsCell.identifier) as! PBBillDetailsCell
                    cell.key.text = bookingArray?.additionalSerice[indexPath.row].serviceName
                    let extraAmt  = String(format: "%.2f", Double(bookingArray!.additionalSerice[indexPath.row].price)!)
                    cell.value.text = Helper.getTheAmtTextWithSymbol(amt:extraAmt)
                    
                    return cell
                }else{
                    let cell : GrandTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "grandtotal") as! GrandTotalTableViewCell
                    cell.grandTotal.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.price))
                    return cell
                }
                
            default:
                
                let cell : GrandTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "grandtotal") as! GrandTotalTableViewCell
                cell.grandTotal.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.price))
                return cell
                
            }
            
            
            
        case .PBServies:
            let cell : ServicesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "service") as! ServicesTableViewCell
            switch indexPath.row {
            case 0:
                header.sectionHeader.text = pastBooking.yourEarnings
                return header
            case 1:
                cell.key.text =  pastBooking.earnedAmt
                cell.value.text =  Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.myEarnings))
                return cell
            default:
                cell.key.text =  pastBooking.appcomm
                
                cell.value.text =  Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.appEarnings))
                return cell
            }
            
            
        case .PBPaymentType:
            
            switch rowType {
            case .PBHeaderSec:
                
                header.sectionHeader.text = pastBooking.paymentMethod
                
                return header
                
            case .PBRFirstRow:
                
                let cell :PBPaymentTypeCell = tableView.dequeueReusableCell(withIdentifier: PBPaymentTypeCell.identifier) as! PBPaymentTypeCell
                if bookingArray!.paymentMethod == 2{
                    cell.cashOrCardImage.image = #imageLiteral(resourceName: "history_card_icon")
                    cell.cashOrCardOption.text = pastBooking.card
                }else{
                    cell.cashOrCardImage.image = #imageLiteral(resourceName: "cash")
                    cell.cashOrCardOption.text = pastBooking.cash
                }
                return cell
                
            default:
                let cell :PBPaymentTypeCell = tableView.dequeueReusableCell(withIdentifier: PBPaymentTypeCell.identifier) as! PBPaymentTypeCell
                cell.cashOrCardImage.image = #imageLiteral(resourceName: "wallet")
                cell.cashOrCardOption.text = "WALLET".localized
                
                return cell
            }
            
        case .PBQA:
            
            
            
            if bookingArray?.questionAnswers[indexPath.row].type == 10  && bookingArray?.questionAnswers[indexPath.row].answer != "" {
                
                
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "jobPhotos") as! JobPhotosTableCell
                let urlStringArr = bookingArray?.questionAnswers[indexPath.row].answer.components(separatedBy: ",")
                
                cell.photoUrls = urlStringArr!
                cell.setupCell()
                return cell
                
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionInnerCell") as! QuestionInnerCell
                cell.questionLabel.text = "Q:".localized + (bookingArray?.questionAnswers[indexPath.row].question)!
                cell.answerLabel.text = bookingArray?.questionAnswers[indexPath.row].answer
                return cell
            }
            
        case .PBReceiversDetails:
            
            switch rowType {
            case .PBHeaderSec:
                header.sectionHeader.text = pastBooking.signatureConfirm
                return header
                
            default:
                
                let cell :PBReceiversDetailsCell = tableView.dequeueReusableCell(withIdentifier: PBReceiversDetailsCell.identifier) as! PBReceiversDetailsCell
                cell.activityIndicator.startAnimating()
                cell.signatureImage.kf.setImage(with: URL(string: (bookingArray?.signature)!),
                                                placeholder:UIImage.init(named: "profile"),
                                                options: [.transition(ImageTransition.fade(1))],
                                                progressBlock: { receivedSize, totalSize in
                },
                                                completionHandler: { image, error, cacheType, imageURL in
                                                    cell.activityIndicator.stopAnimating()
                })
                
                return cell
            }
            
        case .PBDrop:
            
            switch rowType {
            case .PBHeaderSec :
                header.sectionHeader.text = "Documents at drop".localized
                return header
                
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PickupDocumentCell") as! PickupDocumentCell
                
                cell.pickupNote.text = bookingArray?.dropNote
                
                if bookingArray?.dropImagesURL.count != 0 {
                    cell.imageUrls = bookingArray!.dropImagesURL
                }
                cell.pickupImageCollection.reloadData()
                return cell
                
            }
            
        case .PBPickup:
            switch rowType {
            case .PBHeaderSec :
               
                header.sectionHeader.text = "Documents at pickup".localized
                return header
                
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PickupDocumentCell") as! PickupDocumentCell
                
                cell.pickupNote.text = bookingArray?.pickupNote
                
                if bookingArray?.pickupImagesURL.count != 0 {
                    cell.imageUrls = bookingArray!.pickupImagesURL
                }
                cell.pickupImageCollection.reloadData()
                return cell
                
            }
            
        }
    }
}

extension PastBookingController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

