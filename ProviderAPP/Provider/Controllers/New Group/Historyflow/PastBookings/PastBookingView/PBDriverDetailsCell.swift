//
//  PBDriverDetailsCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 11/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

class PBDriverDetailsCell: UITableViewCell {
    @IBOutlet weak var howMuchRatedLabel: UILabel!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet var rating: UILabel!
    @IBOutlet weak var alignmentForSenderNAme: NSLayoutConstraint!
    
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet var pickAddress: UILabel!
    
    @IBOutlet var totalAmount: UILabel!
    @IBOutlet var senderName: UILabel!
    @IBOutlet var senderImage: UIImageView!
    @IBOutlet weak var eventType: UILabel!
    @IBOutlet weak var bookingStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //setup cell data
    func setupCell(pastData:CompletedBookings) {
        bookingStatus.text = pastData.statusMsg
        pickAddress.text  = pastData.addLine1
        
        totalAmount.text  = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",pastData.price))
        senderName.text   = pastData.firstName
        senderImage.kf.setImage(with: URL(string: (pastData.profilePic)),
                                     placeholder:UIImage.init(named: "signup_profile_default_image"),
                                     options: [.transition(ImageTransition.fade(1))],
                                     progressBlock: { receivedSize, totalSize in
        },
                                     completionHandler: { image, error, cacheType, imageURL in
        })

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var identifier: String {
        return String(describing: self)
    }

    
}
