//
//  PBPaymentTypeCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 11/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class PBPaymentTypeCell: UITableViewCell {

    @IBOutlet var cashOrCardImage: UIImageView!
    @IBOutlet var cashOrCardOption: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var identifier: String {
        return String(describing: self)
    }

}
