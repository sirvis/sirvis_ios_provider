//
//  HelpCenterButtonActions.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 15/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

extension HelpCenterController {
    
    @IBAction func backAction(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func sendAction(_ sender: UIButton) {
        if (name.text?.isEmpty)! {
             Helper.alertVC(errMSG: "please enter your name")
        }else if (email.text?.isEmpty)! {
           Helper.alertVC(errMSG: "please enter your mail id")
        }else if (subject.text?.isEmpty)!{
             Helper.alertVC(errMSG: "please fill the subject")
        
        }else if (messageTextView.text?.isEmpty)! {
            Helper.alertVC(errMSG: "please fill the message")
        }
        else{
            dismiss(animated: true, completion: nil)
        }
    }
}
