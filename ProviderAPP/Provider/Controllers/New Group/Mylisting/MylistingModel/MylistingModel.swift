//
//  Configuration.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

struct ListingConstants {
    
    static var contents = [myListingPlaceHolders.aboutMe,
                           myListingPlaceHolders.musicGenre,
                           myListingPlaceHolders.events,
                           myListingPlaceHolders.rules,
                           myListingPlaceHolders.instruments,
                           myListingPlaceHolders.location,
                           myListingPlaceHolders.radius]
    
   static let Status = myListingPlaceHolders.status
    
    static let Radius = myListingPlaceHolders.radius
    
}
