//
//  ListingCell.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 09/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ListingCell: UITableViewCell {
    
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var readMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
