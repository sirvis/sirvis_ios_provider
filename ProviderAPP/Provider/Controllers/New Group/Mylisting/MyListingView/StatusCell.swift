//
//  StatusCell.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 09/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class StatusCell: UITableViewCell {

    @IBOutlet weak var content: UILabel!
    
    @IBOutlet weak var statusSwitch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
