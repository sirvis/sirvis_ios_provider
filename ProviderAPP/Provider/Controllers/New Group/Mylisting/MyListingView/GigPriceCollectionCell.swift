//
//  GigPriceCollectionCell.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 18/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class GigPriceCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var timing: UILabel!
    @IBOutlet weak var price: UILabel!
    
}
