//
//  GigTime&PriceCell.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 09/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class GigTime_PriceCell: UITableViewCell {
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var serviceCollectionView: UICollectionView!
    var servicesDict = [ServicesData]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateThecollectionView(serviceData: [ServicesData]){
        servicesDict = serviceData
        self.serviceCollectionView.reloadData()
    }
}
extension GigTime_PriceCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        
        return servicesDict.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! GigPriceCollectionCell
        cell.timing.text = servicesDict[indexPath.row].serviceName + " " + servicesDict[indexPath.row].serviceUnits
        cell.price.text = Helper.getTheAmtTextWithSymbol(amt:String(describing:servicesDict[indexPath.row].servicePrice))
    
        return cell
    }
}


