//
//  FieldsTableCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 08/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class FieldsTableCell: UITableViewCell {

    @IBOutlet weak var fieldsTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        fieldsTextField.leftView = paddingView
        fieldsTextField.leftViewMode = .always
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
