//
//  HelpViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit


class HelpViewController: UIViewController {
    
    //These vars are used to play background video in the view

    @IBOutlet weak var handlingImage: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        let locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
        //        locationtracker.startLocationTracking()
        
        if(iPHONE.IS_iPHONE_5)
        {
            handlingImage.image = UIImage(named:"splash_640x1136.png")
        }else if(iPHONE.IS_iPHONE_4s)
        {
            handlingImage.image = UIImage(named:"splash_640x960.png")
        }else if(iPHONE.IS_iPHONE_6)
        {
            handlingImage.image = UIImage(named:"splash_640x1334.png")
        }else if(iPHONE.IS_iPHONE_6_Plus)
        {
            handlingImage.image = UIImage(named:"splash_1242x2208.png")
        }else{
            handlingImage.image = UIImage(named:"splash_1125x2436.png")
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- navigate to register screen
    @IBAction func signupButton(_ sender: Any) {
        performSegue(withIdentifier: "toSignUp", sender: nil)
    }
    
    //MARK:- navigate to login screen
    @IBAction func loginButton(_ sender: Any) {
        performSegue(withIdentifier: "toSignIn", sender: nil)
    }
}
