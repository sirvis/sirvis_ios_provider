//
//  LiveMPageContainerController.swift
//  RunnerLive
//
//  Created by Rahul Sharma on 24/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class LiveMPageContainerController: UIViewController {

    
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var footerLine1: UILabel!
    
    var pageContentDelegate:LiveMHelpContentDelegate? = nil
    var model = LiveMHelpModelLibrary()
    var pageIndex: Int = 0
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        header.text = model.pages[pageIndex][0]
        footerLine1.text = model.pages[pageIndex][1]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        pageContentDelegate?.didSelect1(index: pageIndex)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
