//
//  LiveMHelpProtocol.swift
//  RunnerLive
//
//  Created by Rahul Sharma on 24/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


protocol LiveMHelpContentDelegate {
    func didSelect1(index: Int)
}

protocol LiveMHelpPageVCDelegate {
    func didSelect(index: Int)
}
