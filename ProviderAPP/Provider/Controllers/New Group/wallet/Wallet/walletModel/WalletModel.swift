//
//  WalletModel.swift
//  LSP
//
//  Created by Vengababu Maparthi on 24/03/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation

import RxAlamofire
import RxCocoa
import RxSwift


class WalletModel:NSObject {
    
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    

    func updateTheRechargeAmount(dict:[String:Any],completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message:loading.load)
        let rxApiCall = WalletAPI()
        rxApiCall.rechargeTheWalletAmt(paramDict:dict)
        rxApiCall.Wallet_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    completionHandler(true)
                    break
                default:
                    completionHandler(false)
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    func getTheWalletData(completionHandler:@escaping (Bool) -> ()) {
        Helper.showPI(message:loading.load)
        let rxApiCall = WalletAPI()
        rxApiCall.getTheWalletData()
        rxApiCall.Wallet_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    
                    let ud = UserDefaults.standard
                    if let dict = responseModel.data["data"] as? [String:Any]{
                        
                        if let hardLimit  = dict["hardLimit"]   as? NSNumber  {
                            ud.set(hardLimit , forKey: USER_INFO.hardLimit)
                        }
                        
                        if let softLimit  = dict["softLimit"]   as? NSNumber  {
                            ud.set(softLimit , forKey: USER_INFO.softLimit)
                        }
                        
                        if let walletAmount  = dict["walletAmount"]   as? String  {
                            ud.set(Float(walletAmount) , forKey: USER_INFO.walletAmount)
                        }
                    }
                    completionHandler(true)
                    break
                default:
                    completionHandler(false)
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}
