//
//  StripeViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 23/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Stripe

protocol StripeViewControllerDelegate {
    func didCardAdded()
}

class StripeViewController: UIViewController, STPPaymentCardTextFieldDelegate {
    
    @IBOutlet weak var paymentBackgroundView: UIView!
    
    let screenSize = UIScreen.main.bounds
    var paymentTextField: STPPaymentCardTextField?
    var delegate: StripeViewControllerDelegate?
    fileprivate var didClicked: Bool = false
    
    var cardModel = CardModel()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        Stripe.setDefaultPublishableKey(Utility.stripeKey)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.title = "Add Card".localized
        
        setUp()
        paymentBackgroundView.addSubview(paymentTextField!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        paymentTextField?.becomeFirstResponder()
    }
    
    func setUp() {
        
        paymentTextField = STPPaymentCardTextField()
        
        paymentTextField?.delegate = self
        paymentTextField?.cursorColor = UIColor.blue
        paymentTextField?.borderColor = UIColor.clear
        paymentTextField?.borderWidth = 0
        paymentTextField?.font = UIFont.boldSystemFont(ofSize: 12)
        paymentTextField?.textColor = UIColor.black;
        paymentTextField?.cornerRadius = 2.0;
        
        paymentTextField?.frame = CGRect(x: 0,
                                         y: 0,
                                         width: CGFloat(screenSize.size.width - 90),
                                         height: 50)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        
        Helper.showPI(message: "Saving..".localized)
        if !(paymentTextField?.isValid)! {
            Helper.alertVC(errMSG: "Enter Valid card".localized)
            Helper.hidePI()
        }
//        else if (Stripe.defaultPublishableKey() == nil) || Stripe.defaultPublishableKey()?.count == 0 {
//            Helper.alertVC(errMSG: "Enter Valid Details".localized)
//            Helper.hidePI()
//        }
        else {
            
            guard didClicked == false else {
                Helper.hidePI()
                return
            }
            didClicked = true
//            STPAPIClient.shared().createToken(withCard: (paymentTextField?.cardParams)!,
//                                              completion: { (token:STPToken?, error:Error?) in
//
//                                                if (error != nil) {
//                                                    Helper.alertVC(errMSG: "Error")
//
//                                                    Helper.hidePI()
//                                                }
//                                                else {
//                                                    self.send(token: (token?.tokenId)!)
//                                                    print("Card Token: \((token?.tokenId)!)")
//                                                }
//            })
            let expMonth = paymentTextField!.cardParams.expMonth
            var expMonth1 = "\(paymentTextField!.cardParams.expMonth)"
            if (Int(expMonth!) < 10) {
                expMonth1 = "0\(paymentTextField!.cardParams.expMonth)"
            }
            var soapMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://www.paygate.co.za/PayHOST\"><SOAP-ENV:Body><ns1:SingleVaultRequest>" +
                "<ns1:CardVaultRequest>" +
                "<ns1:Account>" +
                "<ns1:PayGateId>10011064270</ns1:PayGateId>"
               soapMessage = soapMessage + "<ns1:Password>test</ns1:Password></ns1:Account>" +
                "<ns1:CardNumber>\((paymentTextField?.cardNumber)!)</ns1:CardNumber>" +
            "<ns1:CardExpiryDate>\(expMonth1)20\((paymentTextField!.cardParams.expYear))</ns1:CardExpiryDate>"
               soapMessage = soapMessage + "<ns1:UserDefinedFields>" +
                "<ns1:key>CVV</ns1:key>" +
            "<ns1:value>\((paymentTextField!.cardParams.cvc)!)</ns1:value>"
               soapMessage = soapMessage + "</ns1:UserDefinedFields>" +
                "</ns1:CardVaultRequest>" +
                "</ns1:SingleVaultRequest>" +
            "</SOAP-ENV:Body></SOAP-ENV:Envelope>"
            
            let cvcNumber = (paymentTextField?.cvc)!
            var urlRequest = URLRequest(url: URL(string: "https://secure.paygate.co.za/PayHost/process.trans/SingleVault")!, cachePolicy:.useProtocolCachePolicy, timeoutInterval: 60)
            
            urlRequest.httpMethod = "POST"
            urlRequest.setValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
            urlRequest.addValue("\(soapMessage.length)", forHTTPHeaderField: "Content-Length")
            urlRequest.addValue("https://secure.paygate.co.za/PayHost/process.trans/SingleVault", forHTTPHeaderField: "SOAPAction")
            
            let requestBodyData = soapMessage.data(using: .utf8)
            urlRequest.httpBody = requestBodyData
            
            let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                if error != nil {
                    Helper.alertVC(errMSG: (error!.localizedDescription))
                    Helper.hidePI()
                } else {
                    
                    let xmlResult = String(data:data!, encoding: .utf8)
                  
                    //Parse XML Result To Data
                    var parseError:Error? = nil
                    var vaultId = ""
                    
                    let xmlParseDictionary = XMLReader.dictionary(forXMLData: data, error: parseError)
                    
                  
                    do {
                        //create json object from xml dictionary
                        let jsonData = try JSONSerialization.data(withJSONObject: xmlParseDictionary!, options: .prettyPrinted)
                       
                        
                        // handle json...
                        parseError = nil
                        
                        do {
                            //create json object from data
                            if let jsonResponse = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String: Any] {
                                
                                
                                
                                // handle json...
                                
                                if let envolopeDict = jsonResponse["SOAP-ENV:Envelope"] as? [String:Any] {
                                    
                                    if let envolopeBody = envolopeDict["SOAP-ENV:Body"] as? [String:Any] {
                                        
                                        if let singleVaultResponseDict = envolopeBody["ns2:SingleVaultResponse"] as? [String:Any] {
                                            
                                            if let cardVaultResponseDict = singleVaultResponseDict["ns2:CardVaultResponse"] as? [String:Any] {
                                                
                                                if let cardDetailsDict = cardVaultResponseDict["ns2:Status"] as? [String:Any] {
                                                    
                                                    if (cardDetailsDict["ns2:StatusName"] != nil), let vaultIdDict = cardDetailsDict["ns2:VaultId"] as? [String:Any] {
                                                        
                                                        if let vaultIdText = vaultIdDict["text"] as? String {
                                                            
                                                            vaultId = vaultIdText
                                                            if vaultId.length > 0 {
                                                                
                                                                self.send(token: vaultId,cvc: cvcNumber)
                                                                
                                                                
                                                            } else {
                                                                Helper.hidePI()
                                                               Helper.alertVC(errMSG: "Error")
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } catch let error {
                            print(error.localizedDescription)
                        }
                        
                    } catch let error {
                        print(error.localizedDescription)
                    }
                }
            }
            task.resume()
        }
    }
    
    @IBAction func scanCardAction(_ sender: Any) {
        
        if let scanCardViewController = CardIOPaymentViewController(paymentDelegate: self) {
            
            scanCardViewController.hideCardIOLogo = true
            scanCardViewController.disableManualEntryButtons = true
            scanCardViewController.modalPresentationStyle = UIModalPresentationStyle.formSheet
            scanCardViewController.title = "Scan Card".localized
            scanCardViewController.navigationController?.isNavigationBarHidden = false
            present(scanCardViewController, animated: true, completion: nil)
        }
    }
    
    func send(token: String,cvc: String){
        let params : [String : Any] = ["cardToken"   : String(describing: token),
                                         "cvv": cvc]
        
        cardModel.addNewCard(dict: params) { (succeeded) in
            if succeeded{
                DispatchQueue.main.async(execute: {
                    _ = self.navigationController?.popViewController(animated: false)
                    self.delegate?.didCardAdded()
                })
            }
        }
    }
}


//MARK: - Card IO Deleagte -
extension StripeViewController: CardIOPaymentViewControllerDelegate {
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        
        self.dismiss(animated: true, completion: nil)
        
        let cardParameters = STPPaymentMethodCardParams()
        
        cardParameters.number =  cardInfo.cardNumber
        cardParameters.expMonth = cardInfo.expiryMonth as NSNumber
        cardParameters.expYear = cardInfo.expiryYear as NSNumber
        cardParameters.cvc = cardInfo.cvv
        
        // if STPCardValidator.validationState(forCard: cardParameters) == STPCardValidationState.valid {
        paymentTextField?.cardParams = cardParameters;
        doneButtonAction((Any).self)
        //        }
        //        else{
        //            Helper.alertVC(errMSG: "Enter Valid card".localized)
        //        }
    }
    
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}

