//
//  AddCardTableViewCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 27/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Stripe

class AddCardTableViewCell: UITableViewCell {

    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var cardName: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var tickMark: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
