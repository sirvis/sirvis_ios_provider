//
//  LiveMCell.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class LiveMCell: UITableViewCell {
    
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var logo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
