//
//  RateCardModel.swift
//  LSP
//
//  Created by Vengababu Maparthi on 03/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
//All the services

class Services: NSObject {
    var serviceName = ""
    var serviceID = ""
    var serviceDescription = ""
    var maxFees = 0.0
    var minFees = 0.0
    var currencySymbol = ""
    var serviceFee = 0.0
    var status = 0
    var serviceCompletionTime = 0
}

class Subcat: NSObject {
    var subCatDesc = ""
    var subCatID = ""
    var subCatName = ""
    var services =  [Services]()
}

class ServiceCategory: NSObject {
    var categoryName = ""
    var currencySymbol = ""
    var serivceType = 0
    var billingModel = 0
    var hourMax = 0.0
    var hourMin = 0.0
    var hourPrice = 0.0
    var cat_ID = ""
    var categoryServices = [Services]()
    var subCategoryData = [Subcat]()
    
    
    func getTheParsedRateCardDetails(categoriesData:[[String: Any]]) -> [ServiceCategory] {
        var categories = [ServiceCategory]()
        for category in categoriesData {
            let singleCat = ServiceCategory()
            var services = [Services]()
            var subCategoryDetails = [Subcat]()
            if let catName = category["cat_name"] as? String{
                singleCat.categoryName = catName
            }
            
            if let curSymbol = category["currencySymbol"] as? String{
                singleCat.currencySymbol = curSymbol
            }
            
            if let hourFee = category["price_per_fees"] as? NSNumber{
                singleCat.hourPrice = Double(hourFee)
            }
            if let hourMax = category["miximum_fees"] as? NSNumber{
                singleCat.hourMax = Double(hourMax)
            }
            if let hourMin = category["minimum_fees"] as? NSNumber{
                singleCat.hourMin = Double(hourMin)
            }
            
            if let cat_ID = category["_id"] as? String{
                singleCat.cat_ID = cat_ID
            }
            
            if let serivceType = category["service_type"] as? NSNumber{
                singleCat.serivceType = Int(serivceType)
            }
            
            if let billModel = category["billing_model"] as? NSNumber{
                singleCat.billingModel = Int(billModel)
            }
            
            if let servicesData = category["service"] as? [[String: Any]] {
            for serviceData in servicesData {
                let service = Services()
                if let serName = serviceData["ser_name"] as? String{
                    service.serviceName = serName
                }

                if let serviceID = serviceData["_id"] as? String{
                    service.serviceID = serviceID
                }

                if let serviceDescription = serviceData["ser_desc"] as? String{
                    service.serviceDescription = serviceDescription
                }

                if let minimumFee = serviceData["minFees"] as? NSNumber{
                    service.minFees = Double(minimumFee)
                }

                if let maximum = serviceData["maxFees"] as? NSNumber{
                    service.maxFees =  Double(maximum)
                }

                if let curSymbol = serviceData["currencySymbol"] as? String{
                    service.currencySymbol = curSymbol
                }

                if let serviceFees = serviceData["is_unit"] as? NSNumber{
                    service.serviceFee =  Double(serviceFees)
                }

                if let status = serviceData["status"] as? NSNumber{
                    service.status =  Int(status)
                }

                if let serviceTime = serviceData["serviceCompletionTime"] as? NSNumber{
                    service.serviceCompletionTime =  Int(serviceTime)
                }
                services.append(service)
              }
            }
            
            if let subCatData = category["subCatArr"] as? [[String:Any]]{
                for subCat in subCatData{
                    let subcatModel = Subcat()
                    
                    if let subCatID = subCat["subCategoryId"] as? String{
                        subcatModel.subCatID = subCatID
                    }
                    
                    if let subCatName = subCat["sub_cat_name"] as? String{
                        subcatModel.subCatName = subCatName
                    }
                    
                    if let subCatDesc = subCat["sub_cat_desc"] as? String{
                        subcatModel.subCatDesc = subCatDesc
                    }
                    
                    if let subcatServices = subCat["service"] as? [[String: Any]] {
                    
                    for serviceData in subcatServices {
                        let service = Services()
                        if let serName = serviceData["ser_name"] as? String{
                            service.serviceName = serName
                        }
                        
                        if let serviceID = serviceData["_id"] as? String{
                            service.serviceID = serviceID
                        }
                        
                        if let serviceDescription = serviceData["ser_desc"] as? String{
                            service.serviceDescription = serviceDescription
                        }
                        
                        if let minimumFee = serviceData["minFees"] as? NSNumber{
                            service.minFees = Double(minimumFee)
                        }
                        
                        if let maximum = serviceData["maxFees"] as? NSNumber{
                            service.maxFees =  Double(maximum)
                        }
                        
                        if let curSymbol = serviceData["currencySymbol"] as? String{
                            service.currencySymbol = curSymbol
                        }
                        
                        if let serviceFees = serviceData["is_unit"] as? NSNumber{
                            service.serviceFee =  Double(serviceFees)
                        }
                        
                        if let status = serviceData["status"] as? NSNumber{
                            service.status =  Int(status)
                        }
                        
                        if let serviceTime = serviceData["serviceCompletionTime"] as? NSNumber{
                            service.serviceCompletionTime =  Int(serviceTime)
                        }
                        
                        subcatModel.services.append(service)
                      //  services.append(service)
                        
                     }
                    }
                    subCategoryDetails.append(subcatModel)
                }
            }
            
            singleCat.subCategoryData = subCategoryDetails
            singleCat.categoryServices = services
            categories.append(singleCat)
        }
        return categories
    }
}

