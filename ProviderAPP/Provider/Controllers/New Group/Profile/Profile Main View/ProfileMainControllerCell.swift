//
//  ProfileMainControllerCell.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 01/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ProfileMainControllerCell: UITableViewCell {
    
    
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateTheNameImage(name:String , image:UIImage) {
        category.text = name
        categoryImage.image = image
    }

}
