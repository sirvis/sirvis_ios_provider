//
//  APICalls.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 05/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import UIKit
import RxAlamofire
import RxCocoa
import RxSwift

class ProfileListModel {
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    
    
    /// updates the profile data like about, radius, etc
    ///
    /// - Parameters:
    ///   - params: about or music genre or instruments or events ..
    ///   - completionHandler: returns success if the 
    func profileDataUpdateService(params:[String:Any] ,
                                  completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message: loading.saving)
        let rxApiCall = ProfileAPI()
        rxApiCall.profileDataUpdateService(method:API.METHOD.UPDATEPROFILE , parameters: params)
        rxApiCall.Profile_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                    
                case .SuccessResponse:
                    completionHandler(true)
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    completionHandler(false)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    /// updates the profile data like about, radius, etc
    ///
    /// - Parameters:
    ///   - params: about or music genre or instruments or events ..
    ///   - completionHandler: returns success if the
    func profileDataUpdateDocuments(params:[String:Any] ,
                                  completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message: loading.saving)
        let rxApiCall = ProfileAPI()
        rxApiCall.profileDataUpdateService(method:API.METHOD.UPDATEDOCUMENTS , parameters: params)
        rxApiCall.Profile_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                    
                case .SuccessResponse:
                    completionHandler(true)
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    completionHandler(false)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    /// update the new mail id
    ///
    /// - Parameters:
    ///   - params: new mail id params
    ///   - completionHandler: return  if the mail id works
    func updateTheNewEmailID(params:[String:Any] ,
                                  completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message: loading.saving)
        let rxApiCall = ProfileAPI()
        rxApiCall.profileDataUpdateService(method:API.METHOD.UPDATENEWEMAIL , parameters: params)
        rxApiCall.Profile_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                    
                case .SuccessResponse:
                    completionHandler(true)
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    completionHandler(false)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}

