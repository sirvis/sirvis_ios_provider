//
//  FAQCell.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 09/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class FAQCell: UITableViewCell {
    
    
    @IBOutlet weak var expandButton: UIButton!
    @IBOutlet weak var answerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
