//
//  ZendeskViewModel.swift
//  Zendesk
//
//  Created by Vengababu Maparthi on 26/12/17.
//  Copyright © 2017 Vengababu Maparthi. All rights reserved.
//


import UIKit
import RxAlamofire
import RxCocoa
import RxSwift


/// new ticket params
class NewTicketRequest {
    let subject :String!
    let body:String!
    let status:String!
    let type:String!
    let priority:String!
    let requester_id:Int64!
    init(sub:String,comments:String,status:String,ticketType:String,priority:String,req_id:Int64) {
        self.subject = sub
        self.body = comments
        self.status = status
        self.type = ticketType
        self.priority = priority
        self.requester_id = req_id
    }
}

class ZendeskModel:NSObject {
    
    let disposebag = DisposeBag()
    var modelData = ModelClass()
    var ticketsHist = historyModel()
    

    /// Creates the new ticket
    ///
    /// - Parameters:
    ///   - newTicketData: new ticket contains ticket data like subject, body, status, priority..
    ///   - completionHandler: completion return true if the ticket created successfully, else return false as error msg
    func postTheNewTicket(newTicketData:NewTicketRequest,completionHandler:@escaping (Bool) -> ()) {

        let ticketDict:[String:Any] = ["subject"  :newTicketData.subject,
                                       "body"        :newTicketData.body,
                                       "status"      :"open",
                                       "priority"    :newTicketData.priority,
                                       "type"         :"problem",
                                       "requester_id": Utility.RequestID]
        
        let rxApiCall = ZendeskAPI()
        rxApiCall.postTheNewTicket(paramDict:ticketDict)
        rxApiCall.Zendesk_Response
            .subscribe(onNext: {response in
                if response.httpStatusCode == 200{
                    Helper.alertVC(errMSG: "you request for new ticket is successfull and ticket will be created soon")
                    completionHandler(true)
                }else{
                    completionHandler(false)
                }
                
            }, onError: {error in
            }).disposed(by: disposebag)
    }
    
    /// get the all tickets data
    ///
    /// - Parameters:
    ///   - userID: get the tickets data using artist user id
    ///   - completionHandler: return the tickets data(ModelClass) on completion
    func getTheTicketData(userID:String,completionHandler:@escaping (Bool,ModelClass) -> ()) {
        Helper.showPI(message: "getting tickets..")
        let rxApiCall = ZendeskAPI()
        rxApiCall.getTheTicketData(id:userID)
        rxApiCall.Zendesk_Response
            .subscribe(onNext: {response in
                if response.httpStatusCode == 200{
                    completionHandler(true,self.modelData.getTheParsedData(dict: response.data["data"] as! [String : Any]))
                }
                
            }, onError: {error in
            }).disposed(by: disposebag)
    }
    
    /// get the particular ticket history data
    ///
    /// - Parameters:
    ///   - userID: ticket history data using the ticket id(userID)
    ///   - completionHandler: return only the particular ticket data as historyModel(ticket history)
    func getTheTicketsHistory(userID:String,completionHandler:@escaping (Bool,historyModel) -> ()) {
        let rxApiCall = ZendeskAPI()
        rxApiCall.getTheTicketHistory(id:userID)
        rxApiCall.Zendesk_Response
            .subscribe(onNext: {response in
                if response.httpStatusCode == 200{
                    completionHandler(true,self.ticketsHist.getTheParsedTicketsData(dict: response.data["data"] as! [String : Any]))
                }
                
            }, onError: {error in
            }).disposed(by: disposebag)
    }
    
   
    /// post a comment to the
    ///
    /// - Parameters:
    ///   - params: params contains the comment and ticket id
    ///   - completionHandler: return true when the comment posted successfully
    func postTheNewTicketComment(params:[String:Any],completionHandler:@escaping (Bool) -> ()) {
        let rxApiCall = ZendeskAPI()
        rxApiCall.postTheNewTicketComment(paramDict:params)
        rxApiCall.Zendesk_Response
            .subscribe(onNext: {response in
                if response.httpStatusCode == 200{
                    completionHandler(true)
                }else{
                    completionHandler(false)
                }
                
            }, onError: {error in
            }).disposed(by: disposebag)
    }
}


