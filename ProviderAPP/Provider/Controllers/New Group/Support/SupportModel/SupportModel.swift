//
//  SupportModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 10/06/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxAlamofire
import RxCocoa
import RxSwift

class Support:NSObject {
    
    
    var ArrayData   = [Support]()
    var SubData     = [Support]()
    let disposebag = DisposeBag()
    var supportName = ""
    var supportUrl = ""
    var supportDesc = ""
    
    
    /// get the supports data
    ///
    /// - Parameter completionHandler: return the support model data
    func getTheSupportData(completionHandler: @escaping(Bool,[Support]) ->()){
        Helper.showPI(message: loading.load)
        let rxApiCall = SupportAPI()
        rxApiCall.getSupportData(method:API.METHOD.SUPPORT)
        rxApiCall.Support_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .dataNotFound:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                case .SuccessResponse:
                    completionHandler(true,self.supportGetData(dict: responseModel.data["data"] as! [AnyObject]))
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
        
    }
    
    
    /// parse the support api response
    ///
    /// - Parameter dict: contains support response
    /// - Returns: returns model object on support data contains subsupport and support data
    func supportGetData(dict: [AnyObject])->[Support]{
        print(dict)
        if dict.count != 0{
            for data in dict {
                if let data = data as? [String:Any]{
                    let model = Support()
                    if let titleTemp = data["Name"] as? String {
                        model.supportName = titleTemp
                    }
                    if let titleTemp = data["link"] as? String {
                        model.supportUrl = titleTemp
                    }
                    if let titleTemp = data["desc"] as? String {
                        model.supportDesc = titleTemp
                    }
                    if (data["subcat"] != nil) {
                        let titleTemp: [AnyObject] = data["subcat"] as! [AnyObject]
                        for dataSub in titleTemp {
                            if let dataSub = dataSub as? [String:Any] {
                                let subModel = Support()
                                if let titleTemp = dataSub["Name"] as? String {
                                    subModel.supportName = titleTemp
                                }
                                if let titleTemp = dataSub["link"] as? String {
                                    subModel.supportUrl = titleTemp
                                }
                                if let titleTemp = dataSub["desc"] as? String {
                                    subModel.supportDesc = titleTemp
                                }
                                model.SubData.append(subModel)
                            }
                        }
                    }
                    ArrayData.append(model)
                }
            }
        }
        return ArrayData
    }
}
