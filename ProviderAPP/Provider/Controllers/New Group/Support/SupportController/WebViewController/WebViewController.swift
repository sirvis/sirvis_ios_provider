//
//  WebViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 20/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet var titleLabel: UILabel!
    
    var urlFrom = ""
    var htmlFrom = ""
    var titleOfController = ""
    
    var webView: WKWebView!

    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleOfController
        if urlFrom.length != 0 {
//            Helper.showPI(message: "Loading..".localized)
//            webView.loadRequest(NSURLRequest(url: URL(string: urlFrom)!) as URLRequest)
            webView.load(URLRequest(url: URL(string: urlFrom)!))
            webView.allowsBackForwardNavigationGestures = true
        }else if htmlFrom.length != 0{
//            Helper.showPI(message: "Loading..".localized)
            let htmlString:String! = htmlFrom
            webView.loadHTMLString(htmlString, baseURL: nil)
        }else{
            self.present(Helper.alertVC(title: "Message".localized, message:"no proper link provided"), animated: true, completion: nil)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }

}

//extension WebViewController : UIWebViewDelegate {
//
//    func webViewDidStartLoad(_ webView: UIWebView) {
//
//    }
//
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        Helper.hidePI()
//    }
//
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        Helper.hidePI()
//    }
//}
