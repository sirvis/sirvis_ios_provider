//
//  RadiusController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 20/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import TGPControls

protocol MyRadiusDelegate {
    func updateTheNewRadius(radius:Int)
}


    


class RadiusController: UIViewController {
    var delegate: MyRadiusDelegate?
    
    @IBOutlet weak var content: UILabel!
    
    @IBOutlet weak var radiusSlider: TGPDiscreteSlider!
    @IBOutlet weak var dis1: UILabel!
    @IBOutlet weak var dis2: UILabel!
    @IBOutlet weak var dis3: UILabel!
    @IBOutlet weak var dis4: UILabel!
    @IBOutlet weak var dis5: UILabel!
    @IBOutlet weak var dis6: UILabel!
    @IBOutlet weak var dis7: UILabel!
    var updateEventsModel = ProfileListModel()
    @IBOutlet weak var distanceUnits: UILabel!
    
    var distanceUnit = ""
    var maxVal = 0
    var minVal = 0
    var exactRadius = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        content.text = ListingConstants.Radius
        let incrementValue = (maxVal - minVal) / 5
        radiusSlider.incrementValue = incrementValue
        radiusSlider.minimumValue = CGFloat(minVal)
        dis1.text =  String(describing:minVal)
        dis2.text =  String(describing:(minVal + incrementValue))
        dis3.text =  String(describing:(minVal + incrementValue * 2))
        dis4.text =  String(describing:(minVal + incrementValue * 3))
        dis5.text =  String(describing:(minVal + incrementValue * 4))
     //   dis6.text =  String(describing:(maxVal/6)*6)
        dis7.text =  String(describing:maxVal)
        distanceUnits.text = distanceUnit
    }
    
    override func viewWillAppear(_ animated: Bool) {
        radiusSlider.value = CGFloat(exactRadius)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        let params: [String : Any] =  [
                "radius" :radiusSlider.value]
            
        
        updateEventsModel.profileDataUpdateService( params: params,completionHandler: { (succeeded) in
             if succeeded{
                self.delegate?.updateTheNewRadius(radius:Int(self.radiusSlider.value))
                self.dismiss(animated: true, completion: nil)
             }else{
            }
        })
    }
}
