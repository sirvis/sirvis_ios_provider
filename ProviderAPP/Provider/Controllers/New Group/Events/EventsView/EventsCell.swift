//
//  EventsCell.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 21/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class EventsCell: UITableViewCell {

    @IBOutlet weak var event: UILabel!
    @IBOutlet weak var activeSwitch: UISwitch!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
