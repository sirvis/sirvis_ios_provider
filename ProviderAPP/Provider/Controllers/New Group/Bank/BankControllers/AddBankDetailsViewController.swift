//
//  AddBankDetailsViewController.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class AddBankDetailsViewController: UIViewController {
    
    @IBOutlet var stateTF: FloatLabelTextField!
    @IBOutlet var addressTF: FloatLabelTextField!
    @IBOutlet var holderName: FloatLabelTextField!
    @IBOutlet var DateOFB: FloatLabelTextField!
    @IBOutlet var personalIDTF: FloatLabelTextField!
    @IBOutlet var cityTF: FloatLabelTextField!
    @IBOutlet var posyalCodeTF: FloatLabelTextField!
    @IBOutlet var addPhotoID: UIImageView!
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerViewButtonConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lastName: FloatLabelTextField!
    
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var contentScrollView: UIView!
    
    @IBOutlet weak var addOrVerifyStripe: UIButton!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var view7: UIView!
    @IBOutlet weak var view8: UIView!
    
    @IBOutlet weak var titleView: UILabel!
    var toVerify:Bool = false
    
    var stripeSavedDict:BankDetails?
    var addBankStripeModel = AddbankStripeModel()
    
    var activeTextField = UITextField()
    var pickedImage = UIImageView()
    var temp = [UploadImage]()
    var bankPhotoUrl = ""
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    var bankDetail = [BankDetail.BankName]()
    var selcetedRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickedImage.image = nil
        pickerView.delegate = self
        updateTheTextFileds()
        BankDetail().getBankDetail(completionHanlder: {sucess,data in
            if sucess {
                self.bankDetail = data
                self.pickerView.reloadAllComponents()
            }
        })
        holderName.text = Utility.firstName
        lastName.text = Utility.lastName
        
        
        if toVerify == true {
            titleView.text = bankDetailsMsg.verifyStripeTitle
            addOrVerifyStripe.setTitle(bankDetailsMsg.verifyStripe, for: .normal)
            DateOFB.text = stripeSavedDict?.dateOFBirth
            holderName.text = stripeSavedDict?.holderName
            lastName.text = stripeSavedDict?.lastName
            cityTF.text = stripeSavedDict?.city
            stateTF.text = stripeSavedDict?.state
            addressTF.text = stripeSavedDict?.address
            posyalCodeTF.text = stripeSavedDict?.postalCode
            
        }else{
            titleView.text = bankDetailsMsg.addStripeTitle
            addOrVerifyStripe.setTitle(bankDetailsMsg.addStripe, for: .normal)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func updateTheTextFileds(){
        holderName.autocapitalizationType = .words
        lastName.autocapitalizationType = .words
        stateTF.autocapitalizationType = .words
        addressTF.autocapitalizationType = .words
    }
    
    func isEmptyLists(dict: [String: Any]) -> Bool {
        if dict.isEmpty {
            return false
        }else{
            return true
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        setupGestureRecognizer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     override func viewDidAppear(_ animated: Bool) {
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
       }
       
       override func viewWillDisappear(_ animated: Bool) {
           
           NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
       }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            mainScrollView.contentInset = contentInset
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let keyboardSize = CGSize(width: 320, height: 240)
        let height: CGFloat = UIDevice.current.orientation.isPortrait ? keyboardSize.height : keyboardSize.width
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            var edgeInsets: UIEdgeInsets = self.mainScrollView.contentInset
            edgeInsets.bottom = height
            self.mainScrollView.contentInset = edgeInsets
            edgeInsets = self.mainScrollView.scrollIndicatorInsets
            edgeInsets.bottom = height
            self.mainScrollView.scrollIndicatorInsets = edgeInsets
        })
    }
    
    @IBAction func bankButtonAction(_ sender: Any) {
        self.view.endEditing(true)
         self.pickerViewButtonConstraint.constant = 0
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        self.addressTF.text = self.bankDetail[selcetedRow].code
        self.personalIDTF.text = self.bankDetail[selcetedRow].name
        self.pickerViewButtonConstraint.constant = 266
    }
    @IBAction func tapGestureToDismissKeyBoard(_ sender: Any) {
        self.dismisskeyBord()
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.pickerViewButtonConstraint.constant = 266
    }
    @IBAction func addPhotoID(_ sender: Any) {
        self.selectImage()
    }
    
    @IBAction func backButton(_ sender: Any) {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func saveTheBankDetails(_ sender: Any) {
        self.makeTheServiceCallToSaveTheBank()  //@locate AddbankDetailsExtensionVC
    }
    
    //AddStripe Account API
    func sendRequestForSignup() {
        let name = DateOFB.text!.components(separatedBy: "/")
        Helper.showPI(message: loading.addStripe)
        var bankDetails = [String: Any]()
        var ipAddress = Helper.getIPAddresses()
        
        bankDetails = [
            "bankName"     :addressTF.text!,
            "accountNumber"  :personalIDTF.text!,
            "branchCode":DateOFB.text!,
            "ip"            :ipAddress[1]]
        // "branch"    :posyalCodeTF.text!,
        addBankStripeModel.addStripeAccount(params: bankDetails, completionHanlder: { success in
            if success{
                _ = self.navigationController?.popToRootViewController(animated: true)
            }else{
                
            }
        })
    }
    
    func uploadProfileimgToAmazon(){
        
        var url = String()
        
        temp = [UploadImage]()
        let timeStamp = Helper.currentTimeStamp
        url = AMAZONUPLOAD.DOCUMENTS + timeStamp + "_" + "bankPhotoID" + ".png"
        
        temp.append(UploadImage.init(image: addPhotoID.image!, path: url))
        
        let upMoadel = UploadImageModel.shared
        upMoadel.uploadImages = temp
        upMoadel.start()
        
        bankPhotoUrl = Utility.amazonUrl + AMAZONUPLOAD.DOCUMENTS + timeStamp +  "_" + "bankPhotoID" + ".png"
    }
    
    
    func dismisskeyBord(){
        self.view.endEditing(true)
    }
    
    /// MARK: - actionSheet
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: signup.selectImage as String?,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: signup.cancel as String?,
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: signup.gallery as String?,
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: signup.camera as String?,
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    // MARK: - photo gallery
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = true
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    // MARK: - Camera selection
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            imagePickerObj.allowsEditing = true
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
        }
    }
}


// MARK: - ImagePicker delegate method
extension AddBankDetailsViewController : UIImagePickerControllerDelegate {
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    let image = info[.originalImage] as? UIImage
        addPhotoID.image = Helper.resizeImage(image: image!, newWidth: 200)
        pickedImage.image = Helper.resizeImage(image: image!, newWidth: 200)
        bankPhotoUrl = ""
        self.uploadProfileimgToAmazon()
        self.dismiss(animated: true, completion: nil)
    }
}


extension AddBankDetailsViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
      //  navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}
extension AddBankDetailsViewController:UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bankDetail.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return bankDetail[row].name
    }
   
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selcetedRow = row
    }
    
}





