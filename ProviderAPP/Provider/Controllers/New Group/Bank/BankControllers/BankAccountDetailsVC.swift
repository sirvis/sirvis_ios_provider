//
//  BankAccountDetailsVC.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class BankAccountDetailsVC: UIViewController {
    
    @IBOutlet var bankDetailsTableView: UITableView!

    @IBOutlet weak var linkAccount: UIButton!
    var bankDate = BankDetails()
    var toVerify = false
    
    var bankServiceCallDone = false
    
    var verifiedorNot = String()
    var StripeHolderName = String()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.linkAccount.isHidden = true
        bankDetailsTableView.estimatedRowHeight = 10
        bankDetailsTableView.rowHeight = UITableView.automaticDimension
        //        if (self.bankDate.address.length > 0)  {  //
        //            self.linkAccount.isHidden = false  //
        //        }else{  //
        //             self.linkAccount.isHidden = true  //
        //        }  //
        // Do any additional setup after loading the view.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        self.getBankDate()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTheBankDetails(_ sender: Any) {
        if (self.bankDate.address.length > 0)  {
            performSegue(withIdentifier: "toAddBank", sender: nil)
        }
    }
    
    @IBAction func addStripeDetailsAction(_ sender: Any) {
        toVerify = false
        performSegue(withIdentifier: "toAddStripe", sender: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "toAddStripe":
            if let addStripe = segue.destination as? AddBankDetailsViewController {
                addStripe.toVerify = toVerify
                addStripe.stripeSavedDict = bankDate
            }
            break
        default:
            break
        }
    }
    
    func getBankDate(){
        bankDate.getBankDate(completionHandler: { bankResults in
            switch bankResults{
            case .success(let bankDetails):
                self.bankDate = bankDetails
                self.verifiedorNot = self.bankDate.verifiedRNot
                self.bankServiceCallDone = true
                //                if self.bankDate.bankAccountData.count != 0 { //
                //                    self.linkAccount.isHidden = false  //
                //                }else{   //
                //                    self.linkAccount.isHidden = true  //
                //                } //
                self.bankDetailsTableView.reloadData()
                
            case .failure(let error):
                print(error)
                break
            }
        })
    }
}


extension BankAccountDetailsVC : UpdateBankDataDelegate{
    func getCurrentBankDetails() {
        self.getBankDate()
    }
}
