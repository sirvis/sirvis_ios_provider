//
//  BankTableViewCell.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class BankTableViewCell: UITableViewCell {
    @IBOutlet weak var bankName: UILabel!
    
    @IBOutlet var defaultButton: UIButton!
    @IBOutlet var acountNumber: UILabel!
    @IBOutlet var holderName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
   
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateBankAccount(bankData:BankAccountDataModel) {
        acountNumber.text = "Account number: " + bankData.accountNum
       // holderName.text = "Account Holder Name:" + bankData.holderName
        bankName.text = "Bank name: " + bankData.bankName
    }
}
