//
//  ArtistAddressTableCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 30/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ArtistAddressTableCell: UITableViewCell {
    @IBOutlet weak var addressTypeImage: UIImageView!
    @IBOutlet weak var addressTitle: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var deleteAddress: UIButton!
    
    @IBOutlet weak var defaultButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
