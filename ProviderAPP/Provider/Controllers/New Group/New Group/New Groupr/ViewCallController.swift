//
//  ViewCallController.swift
//  LSP
//
//  Created by Vengababu Maparthi on 06/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class ViewCallController: UIViewController {
    @IBOutlet weak var categoryTableView: UITableView!
    var callModel = CallModel() //view model
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    @IBAction func backToVc(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
          getTheCategorydata()
    }
    
    func getTheCategorydata()  {
        callModel.getCallData { (success, data) in
            if success{
                
                self.callModel.catData = data.sorted{ $0.catName < $1.catName}
                self.categoryTableView.reloadData()
                
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editCall"{
            if let nextScene = segue.destination as? EditCallController{
                nextScene.catData = self.callModel.catData[sender as! Int]
            }
        }
    }
}

extension ViewCallController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.callModel.catData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewCallCell", for: indexPath) as! ViewCallCell
        cell.keyLabel.text = self.callModel.catData[indexPath.row].catName
        return cell
    }
}

extension ViewCallController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      //  if self.callModel.catData[indexPath.row].callData.count > 0{
            self.performSegue(withIdentifier: "editCall", sender: indexPath.row)
      //  }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
