//
//  EditCallController.swift
//  LSP
//
//  Created by Vengababu Maparthi on 06/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class EditCallController: UIViewController {
    
    @IBOutlet weak var catName: UILabel!
    var callModel = CallModel()
    var catData:CategoryCall!
    var callParam:[String:Any] = ["inCall": "0",
                                  "outCall" : "0",
                                  "teleCall": "0"]
    
    @IBOutlet weak var editCatTableView: UITableView!
    override func viewDidLoad() {
        catName.text = catData.catName
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func changeTheStatus(_ sender: UISwitch) {
        
        var param = [String:Any]()
        callParam["categoryId"] = catData.catId
        if self.catData.callData[sender.tag].callVal == "1"{
            param = ["categoryId":catData.catId,
                     self.catData.callData[sender.tag].callKey:"2"]
            callParam.updateValue("2", forKey: self.catData.callData[sender.tag].callKey)
            
            callModel.updatetheCallType(dict: callParam) { (success) in
                if success{
                    self.catData.callData[sender.tag].callVal = "2"
                    self.editCatTableView.reloadData()
                }
            }
            
        }else if self.catData.callData[sender.tag].callVal == "2" {
            param = ["categoryId":catData.catId,
                     self.catData.callData[sender.tag].callKey:"1"]
            callParam.updateValue("1", forKey: self.catData.callData[sender.tag].callKey)
            callModel.updatetheCallType(dict: callParam) { (success) in
                if success{
                    self.catData.callData[sender.tag].callVal = "1"
                    self.editCatTableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func backToVc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension EditCallController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.catData.callData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditCallCell", for: indexPath) as! EditCallCell
        if self.catData.callData[indexPath.row].callKey == "inCall"{
           cell.keyLabel.text = "Walk In"
        }
        if self.catData.callData[indexPath.row].callKey == "outCall"{
            cell.keyLabel.text = "Call Out"
        }
        callParam[self.catData.callData[indexPath.row].callKey] = self.catData.callData[indexPath.row].callVal
        if self.catData.callData[indexPath.row].callVal == "2"{
            cell.switchButton.isOn = false
        }else if self.catData.callData[indexPath.row].callVal == "1" {
            cell.switchButton.isOn = true
        }
        cell.switchButton.tag = indexPath.row
        return cell
    }
}
