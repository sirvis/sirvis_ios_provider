//
//  HistoryController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 18/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import WebKit

class PayrollWebViewVC: UIViewController, WKNavigationDelegate {
    
    
    var activityIndicator = UIActivityIndicatorView()
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var navTitle: UILabel!
    
    var payrollWebView: WKWebView!

    override func loadView() {
        payrollWebView = WKWebView()
        payrollWebView.navigationDelegate = self
        view = payrollWebView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        navTitle.text = "Earnings".localized
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
//        animatedTabBar.animationTabBarHidden(false)
//        self.tabBarController?.tabBar.isHidden = false
        activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)

        self.payrollWebView.addSubview(activityIndicator)
//        activityIndicator.startAnimating()
        requestUrls()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
//        animatedTabBar.animationTabBarHidden(false)
//        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backToVc(_ sender: Any) {
        if(payrollWebView.canGoBack) {
            //Go back in webview history
            payrollWebView.goBack()
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func requestUrls(){
        payrollWebView.load(URLRequest(url: URL(string: API.payRollWebView )!))
        payrollWebView.allowsBackForwardNavigationGestures = true
//        self.payrollWebView.loadRequest(URLRequest(url: URL(string: API.payRollWebView )!))
    }
}

//MARK: - Delegate of webview

//extension PayrollWebViewVC : UIWebViewDelegate {
//
//    func webViewDidStartLoad(_ webView: UIWebView) {
//
//    }
//
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//
//        activityIndicator.stopAnimating()
//    }
//
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        activityIndicator.stopAnimating()
//
//    }
//}
