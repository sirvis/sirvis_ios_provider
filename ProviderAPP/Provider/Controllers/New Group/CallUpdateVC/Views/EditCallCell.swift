//
//  EditCallCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 06/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class EditCallCell: UITableViewCell {

    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var keyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
