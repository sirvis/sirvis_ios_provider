//
//  CallEditModel.swift
//  LSP
//
//  Created by Vengababu Maparthi on 06/12/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation


import Foundation

import RxAlamofire
import RxCocoa
import RxSwift


class CallModel:NSObject {
    
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    var catData = [CategoryCall]()
  
    
    /// get the rate card details
    ///
    /// - Parameters:
    ///   - completionHandler: returns the ratecardModel model data on completion
    func getCallData(completionHandler:@escaping (Bool, [CategoryCall] ) -> ()) {
        
        Helper.showPI(message:loading.load)
        
        let rxApiCall = CallDetailsAPI()
        rxApiCall.getTheCallDetails()
        rxApiCall.Notification_response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    Helper.hidePI()
                    if  let data =  responseModel.data["data"] as? [[String:Any]]{
                        self.catData = data.map {
                            CategoryCall.init(dict: $0)
                        }
                        completionHandler(true, self.catData )
                    }
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    /// get the rate card details
    ///
    /// - Parameters:
    ///   - completionHandler: returns the ratecardModel model data on completion
    func updatetheCallType(dict:[String:Any],completionHandler:@escaping (Bool) -> ()) {
        
        
        Helper.showPI(message:loading.load)
        
        let rxApiCall = CallDetailsAPI()
        rxApiCall.updatetheCallData(params:dict)
        rxApiCall.Notification_response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    Helper.hidePI()
                    completionHandler(true)
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}

class CategoryCall {
    var catId = ""
    var catName = ""
    var callData = [CallMod]()
    init(dict:[String:Any]) {
        if let catID = dict["categoryId"] as? String{
            self.catId = catID
        }
        if let catName = dict["categoryName"] as? String{
            self.catName = catName
        }
        if let incall = dict["inCall"] as? NSNumber{
            if incall != 0 {
                callData.append(CallMod.init(key: "inCall", val: "\(incall)"))
            }
        }
        if let outCall = dict["outCall"] as? NSNumber{
            if outCall != 0 {
                callData.append(CallMod.init(key: "outCall", val: "\(outCall)"))
            }
        }
        if let teleCall = dict["teleCall"] as? NSNumber{
            if teleCall != 0 {
                callData.append(CallMod.init(key: "teleCall", val: "\(teleCall)"))
            }
        }
    }
}

class CallMod {
    var callKey = ""
    var callVal = ""
    init(key:String,val:String) {
        self.callKey = key
        self.callVal = val
    }
}
