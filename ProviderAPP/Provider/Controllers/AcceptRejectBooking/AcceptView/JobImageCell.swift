//
//  JobImageCell.swift
//  LSP
//
//  Created by Rahul Sharma on 8/14/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class JobImageCell: UICollectionViewCell {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var jobImageview: UIImageView!
}
