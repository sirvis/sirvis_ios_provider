//
//  ShiftDetailCell.swift
//  LSP
//
//  Created by Rahul Sharma on 9/6/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class ShiftDetailCell: UITableViewCell {

    @IBOutlet weak var totalAmt: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var numberOfShift: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var shiftView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var hourPerShift: UILabel!
    @IBOutlet weak var costPerShift: UILabel!
    @IBOutlet weak var totalCost: UILabel!
    
     var requestData: Request?
    var activeBidData: ActiveBid?
    var isBidding = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        shiftView.layer.borderColor = UIColor(hexString: "DBDBDB").cgColor
        shiftView.layer.borderWidth = 1
        shiftView.layer.cornerRadius = 3
        shiftView.layer.masksToBounds = true
    }

    
    func setupCellData(data: Request) {
        requestData = data
        collectionView.reloadData()
        if data.distanceMatrix == 1{
            self.distanceLabel.text =  String(format: "%.02f", data.distance/1609.34) + DistanceUnits.Miles + " away".localized
        }else{
            self.distanceLabel.text = String(format: "%.02f", data.distance/1000) + DistanceUnits.Kms + " away".localized
        }
        //totalAmt.text = Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",data.totalAmt))
        let startdateArray = Helper.getTheDateFromTimeStamp(timeStamp:data.actuallGigTimeStart).components(separatedBy: "|")
        let endDateArray = Helper.getTheDateFromTimeStamp(timeStamp:data.bookingRequestEnds).components(separatedBy: "|")
        let startDate1 = String(startdateArray[0].dropLast(5))
        let endDate1 = String(endDateArray[0].dropLast(5))
        startDate.text = startDate1
        startTime.text = startdateArray[1]
        endDate.text = endDate1
        endTime.text = endDateArray[1]
        numberOfShift.text = "\(data.shifts)"
        
        costPerShift.text = Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",data.totalAmt))
        hourPerShift.text =  Helper.minsToHour(min: data.totalJobTime) 
        let totalPrice = Double(data.shifts) * data.totalAmt
        totalCost.text = Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",totalPrice))
    }
    
    func setupActiveBid(data: ActiveBid) {
        isBidding = true
        activeBidData = data
        collectionView.reloadData()
        if data.distanceMatrix == 1{
            self.distanceLabel.text =  String(format: "%.02f", data.distance/1609.34) + DistanceUnits.Miles + " away".localized
        }else{
            self.distanceLabel.text = String(format: "%.02f", data.distance/1000) + DistanceUnits.Kms + " away".localized
        }
        //totalAmt.text = Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",data.totalAmt))
        let startdateArray = Helper.getTheDateFromTimeStamp(timeStamp:data.actuallGigTimeStart).components(separatedBy: "|")
        let endDateArray = Helper.getTheDateFromTimeStamp(timeStamp:data.bookingRequestEnds).components(separatedBy: "|")
        let startDate1 = String(startdateArray[0].dropLast(5))
        let endDate1 = String(endDateArray[0].dropLast(5))
        startDate.text = startDate1
        startTime.text = startdateArray[1]
        endDate.text = endDate1
        endTime.text = endDateArray[1]
        numberOfShift.text = "\(data.shifts)"
        
        costPerShift.text = Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",data.totalAmt))
        hourPerShift.text =  Helper.minsToHour(min: data.totalJobTime)
        let totalPrice = Double(data.shifts) * data.totalAmt
        totalCost.text = Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",totalPrice))
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ShiftDetailCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if isBidding {
            
            if activeBidData !=  nil{
                return 1
            }
            else {
                return 0
            }
        }
        else {
            if requestData !=  nil{
                return 1
            }
            else {
                return 0
            }
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isBidding {
            return (activeBidData?.days.count)!
        }
        else {
            return (requestData?.days.count)!
        }
        //return isBidding ? (activeBidData?.days.count)! : (requestData?.days.count)!
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MultilshiftDaysCell", for: indexPath) as? MultilshiftDaysCell
        if isBidding {
            if activeBidData !=  nil {
                cell?.daysLabel.text = activeBidData?.days[indexPath.row]
            }
        }
        else {
            if requestData !=  nil {
                cell?.daysLabel.text = requestData?.days[indexPath.row]
            }
        }
        
        return cell!
    }
}
