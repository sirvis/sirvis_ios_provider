//
//  ProfileDataTableCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 29/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ProfileDataTableCell: UITableViewCell {
    @IBOutlet weak var key: UILabel!
    @IBOutlet weak var value: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
