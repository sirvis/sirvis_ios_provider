//
//  JobPhotoCell.swift
//  LSP
//
//  Created by Rahul Sharma on 8/16/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class JobPhotoCell: UITableViewCell {

    @IBOutlet weak var jobImageview: UIImageView!
    @IBOutlet weak var qsLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
