//
//  AcceptRejectViewController.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 29/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher

protocol MoveToUpcomingDelegate {
    func moveToUpcoming()
}

class AcceptRejectViewController: UIViewController,UIScrollViewDelegate {
    var delegate: MoveToUpcomingDelegate?
    
    @IBOutlet weak var acceptTableView: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var acceptBidButton: ZFRippleButton!
    @IBOutlet weak var acceptRejectView: UIView!
    
    var bookingData:Request?
    var acceptRejectVC = AcceptRejectModel()
    var bookingMarker = GMSMarker()
    var tableHeaderViewHeight = 200 as CGFloat
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    
    @IBOutlet weak var bookingID: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        acceptTableView.estimatedRowHeight = 10
        acceptTableView.rowHeight = UITableView.automaticDimension
        inititateTheview()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(true)
        setupGestureRecognizer()
       // self.navigationController?.presentTransparentNavigationBar()
        NotificationCenter.default.addObserver(self, selector: #selector(bookingCancelHandlingAcceptedRejected(_:)), name: Notification.Name("cancelledBooking"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleExpiredBooking), name: Notification.Name("BookingExpired"), object: nil)
        bookingID.text = "BID:".localized + "\(self.bookingData!.bookingId)"
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func inititateTheview()  {
        self.view.layoutIfNeeded()
        mapView = (self.acceptTableView.tableHeaderView as! GMSMapView)
        self.acceptTableView.tableHeaderView = nil
        self.acceptTableView.addSubview(mapView)
        self.acceptTableView.contentInset = UIEdgeInsets(top: tableHeaderViewHeight,left: 0, bottom: 0, right: 0)
        self.acceptTableView.contentOffset = CGPoint(x:0,y:-tableHeaderViewHeight)
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees((bookingData?.latitude)!) ,longitude: CLLocationDegrees((bookingData?.longitude)!) , zoom: 16)
        var destinationLocation = CLLocation()
        destinationLocation = CLLocation(latitude: (bookingData?.latitude)!,  longitude: (bookingData?.longitude)!)
        bookingMarker.position = destinationLocation.coordinate
        bookingMarker.icon = #imageLiteral(resourceName: "home_pickup_icon")
        self.mapView.animate(to: camera)
        bookingMarker.map = self.mapView
        
        if bookingData?.bookingModel == .Bidding {
            acceptBidButton.isHidden = false
            acceptRejectView.isHidden = true
            let bid = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", (bookingData?.bidCharge)!))
            acceptBidButton.setTitle("\(bid) TO BID FOR THIS JOB".localized, for: .normal)
        }
        else {
            acceptBidButton.isHidden = true
            acceptRejectView.isHidden = false
        }
    
       self.mapView.frame = CGRect(x:0, y: self.acceptTableView.contentOffset.y, width:self.acceptTableView.bounds.size.width, height: -self.acceptTableView.contentOffset.y)
    }
    
    
    @IBAction func bidBtnTapped(_ sender: Any) {
        //self.updateTheAcceptRejectResponse(status:3)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let bidVC = storyboard.instantiateViewController(withIdentifier: "BidAmountViewController") as? BidAmountViewController
        bidVC?.bookingData = bookingData
       
        self.navigationController?.pushViewController(bidVC!, animated: true)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.acceptTableView.contentOffset.y < -tableHeaderViewHeight {
            mapView.frame = CGRect(x:0, y: self.acceptTableView.contentOffset.y, width:self.view.bounds.size.width, height: -self.acceptTableView.contentOffset.y)
        }
    }
    
    ///********notifies when the booking has been cancelled*******//
    @objc func bookingCancelHandlingAcceptedRejected(_ notification: NSNotification) {
        if String(describing:notification.userInfo!["bookingId"]!)  == String(describing:bookingData!.bookingId) {
            _ = navigationController?.popViewController(animated:true)
        }
    }
    
    @objc func handleExpiredBooking() {
         _ = navigationController?.popViewController(animated:true)
    }
    
   
    @IBAction func backToVc(_ sender: Any) {
        _ = navigationController?.popViewController(animated:true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
      //  self.navigationController?.hideTransparentNavigationBar()

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "cancelledBooking"), object: nil);
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "BookingExpired"), object: nil);
        
    }
    
    @IBAction func declineAction(_ sender: Any) {
        self.updateTheAcceptRejectResponse(status:4)
        
    }
    
    func updateTheAcceptRejectResponse(status:Int){
        let ud = UserDefaults.standard
        Helper.showPI(message: loading.load)
        var dict = [String : Any]()
        if bookingData?.bookingType == .Schedule{
            let reminderData = ReminderModel()
            reminderData.startDate = (bookingData?.bookingRequestedFor)!
            reminderData.enddate = (bookingData?.bookingRequestEnds)!
            reminderData.latit = (bookingData?.latitude)!
            reminderData.logit = (bookingData?.longitude)!
            reminderData.bookingID = (bookingData?.bookingId)!
            reminderData.address = (bookingData?.addLine1)!
            
            ReminderModel().eventReminder(data: reminderData, completion: { (reminderID) in
                let lati = Helper.unwrappOptional(str : ud.object(forKey: "currentLat") as? String)
                let longi = Helper.unwrappOptional(str : ud.object(forKey: "currentLog") as? String)
                dict =  ["bookingId" :self.bookingData!.bookingId as Any,
                         "latitude" : lati,
                         "longitude" : longi,
                         "status":status as Any,
                         "reminderId":reminderID]
                self.acceptRejectVC.acceptRejectModelBooking(params: dict ,completionHandler: { (succeeded) in
                    if succeeded{
                        Helper.hidePI()
                        self.bookingData?.statusCode = status
                        _ = self.navigationController?.popViewController(animated: true)
                       self.delegate?.moveToUpcoming()
                    }else{
                        Helper.hidePI()
                    }
                })
            })
           
        }else if bookingData?.bookingType == .Now {
            let lati = Helper.unwrappOptional(str : ud.object(forKey: "currentLat") as? String)
            let longi = Helper.unwrappOptional(str : ud.object(forKey: "currentLog") as? String)
            dict =  ["bookingId" :bookingData!.bookingId as Any,
                     "latitude" : lati,
                     "longitude" : longi,
                     "status":status as Any]
            self.acceptRejectVC.acceptRejectModelBooking(params: dict ,completionHandler: { (succeeded) in
                if succeeded{
                    Helper.hidePI()
                    self.bookingData?.statusCode = status
                    _ = self.navigationController?.popViewController(animated: true)
                    if  self.bookingData?.bookingModel == .Bidding {
                        self.delegate?.moveToUpcoming()
                    }
                    
                    
                }else{
                    Helper.hidePI()
                }
            })
        }
        else {
            let lati = Helper.unwrappOptional(str : ud.object(forKey: "currentLat") as? String)
            let longi = Helper.unwrappOptional(str : ud.object(forKey: "currentLog") as? String)
            dict =  ["bookingId" :bookingData!.bookingId as Any,
                     "latitude" : lati,
                     "longitude" : longi,
                     "status":status as Any]
            self.acceptRejectVC.acceptRejectModelBooking(params: dict ,completionHandler: { (succeeded) in
                if succeeded{
                    Helper.hidePI()
                    self.bookingData?.statusCode = status
                    _ = self.navigationController?.popViewController(animated: true)
                   if  self.bookingData?.bookingModel == .Bidding {
                     self.delegate?.moveToUpcoming()
                    }
                }else{
                    Helper.hidePI()
                }
            })
        }
        
    }
    
   
    
    @IBAction func acceptAction(_ sender: Any) {
        self.updateTheAcceptRejectResponse(status:3)
        
    }
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        switch segue.identifier! as String {
        case "customerReviews":
            let nav = segue.destination as! UINavigationController
            if let review: CustReviewsVC = nav.viewControllers.first as! CustReviewsVC?
            {
                
                
                review.customerID = (bookingData?.customerId)!
                review.custPic = (bookingData?.profilePic)!
                review.cusName = (bookingData?.firstName)!
            }
            break
        default:
            break
        }
    }
}

extension AcceptRejectViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // customerReviews
        if indexPath.row == 0 && indexPath.section == 1{
            self.performSegue(withIdentifier: "customerReviews", sender: nil)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension AcceptRejectViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 1 //addressline
            
        case 1:
            return 2  //medium, customerDetails
            
        case 2:
            if bookingData?.bookingType == .Repeat {
                return 2 //medium, shiftcell
            }
            return 3  //medium, title, total amt and data
            
        case 3:
            if bookingData?.bookingModel == .Bidding{
                return 0
            }
            return 2 + (bookingData?.services.count)!  //service count
        case 4:
            if bookingData?.bookingModel == .Bidding{
                return 0
            }
            return bookingData!.paymentServices.count
        case 5:
//            if bookingData?.bookingModel == .Bidding{
//                return 0
//            }
            return 1
        case 6:
            return 3
        case 7:
            if bookingData?.jobDesc == "" {
                return 0
            }
            return 3
        case 8:
            if 1 + (bookingData?.questionAnswers.count)! > 0 {
                return 1 + (bookingData?.questionAnswers.count)!
            }
            return 0
        case 9:
            return 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderTableCell
        let mediumView = tableView.dequeueReusableCell(withIdentifier: "mediumView") as! MediumViewTableCell
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileAddress") as! AcceptAddressTableCell
            cell.address.text = bookingData?.addLine1
            return cell
            
        case 1:
            switch indexPath.row{
            case 0:
                return mediumView
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "profileProgress") as! ProfileProgressTableCell
                cell.updateTheRequestData(data:(bookingData)!)
                return cell
            }
        case 2:
            
            if bookingData?.bookingType == .Repeat {
                
                switch indexPath.row{
                case 0:
                    return mediumView
                    
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ShiftDetailCell") as! ShiftDetailCell
                    cell.setupCellData(data: bookingData!)
                    return cell
                }
            }
            else {
                switch indexPath.row{
                case 0:
                    return mediumView
                case 1 :
                    header.headerOfCell.text = "Amount".localized
                    return header
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "totalAmt") as! TotalAmtTableCell
                    cell.updateTheFieldsFromAcceptReject(req:bookingData)
                    return cell
                }
            }
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileData") as! ProfileDataTableCell
            switch indexPath.row{
            case 0:
                return mediumView
            case 1 :
                header.headerOfCell.text = "Selected Services".localized
                return header
                
            default:
                
                if bookingData?.serviceType == .Fixed{
                    cell.key.text = (bookingData?.services[indexPath.row - 2].serviceName)! + " * " + String(describing:bookingData!.services[indexPath.row - 2].quantity)
                    cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.services[indexPath.row - 2].servicePrice))
                    
                }else{
                    cell.key.text = Helper.timeInHourMin((bookingData?.totalJobTime)!)
                    cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.totalHourlyFee))
                }
                return cell
            }
            
        case 4:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileData") as! ProfileDataTableCell
            cell.key.text = (bookingData?.paymentServices[indexPath.row].key)!
            cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.paymentServices[indexPath.row].value))
            return cell
        case 5:
            let totalCell = tableView.dequeueReusableCell(withIdentifier: "finalAmt") as! FinalAmtTableCell
            
            let total = bookingData?.totalAmt
            
            totalCell.totalAmt.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))
            
            return totalCell
            
        case 6:
            switch indexPath.row{
            case 0:
                return mediumView
            case 1 :
                header.headerOfCell.text = "Payment Method".localized
                return header
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "paymentType") as! PaymentMethodTableCell
                
                if (bookingData?.paidByWallet)! {
                    cell.paymentType.text = "Wallet".localized + " + " + (bookingData?.paymentMethod)!
                }else{
                    cell.paymentType.text = bookingData?.paymentMethod
                }
                  if bookingData?.paymentMethod == "Cash" {
                                  cell.paymentTypeImage.image = #imageLiteral(resourceName: "cash")
                              } else {
                                  cell.paymentTypeImage.image = #imageLiteral(resourceName: "card")
                              }
                              
                return cell
            }
            
        case 7:
            switch indexPath.row{
            case 0:
                return mediumView
            case 1:
                header.headerOfCell.text =  "Job Description".localized
                return header
            case 3:
                return mediumView
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "jobDesc") as! JobDescTableCell
                if bookingData?.jobDesc == "" {
                    cell.jobDescription.text = "This booking does not have a description".localized
                }
                else {
                    cell.jobDescription.text = bookingData!.jobDesc
                }
                
                return cell
            }
            
        
        case 8:
            switch indexPath.row{
            case 0:
                return mediumView
            default:
                
                if bookingData?.questionAnswers[indexPath.row - 1].type == 10  && bookingData?.questionAnswers[indexPath.row - 1].answer != "" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "jobPhotos") as! JobPhotosTableCell
                    let urlStringArr = bookingData?.questionAnswers[indexPath.row - 1].answer.components(separatedBy: ",")
                    
                    cell.photoUrls = urlStringArr!
                    cell.setupCell()
                    return cell
               
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionInnerCell") as! QuestionInnerCell
                    cell.questionLabel.text = "Q:".localized + (bookingData?.questionAnswers[indexPath.row - 1].question)!
                    cell.answerLabel.text = bookingData?.questionAnswers[indexPath.row - 1].answer
                    return cell
                }
            
            }
            
        default:
            switch indexPath.row{
            case 0:
                return mediumView
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "jobPhotos") as! JobPhotosTableCell
                return cell
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}









extension AcceptRejectViewController: UINavigationControllerDelegate {
    
    internal func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}
