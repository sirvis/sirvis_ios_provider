//
//  BidAmountViewController.swift
//  LSP
//
//  Created by Rahul Sharma on 6/15/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import RxKeyboard
import RxSwift

class BidAmountViewController: UIViewController {

    @IBOutlet weak var bidAmountTF: UITextField!
    @IBOutlet weak var descTF: UITextField!
    @IBOutlet weak var submitBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var cutomerAmtLabel: UILabel!
    @IBOutlet weak var currencySymbol: UITextField!
    
    let disposeBag = DisposeBag()
    var bookingID = 0
     var bookingData:Request?
    var acceptRejectVC = AcceptRejectModel()
    var cutomerAmt = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descTF.delegate = self
        bidAmountTF.delegate = self
       // bidAmountTF.becomeFirstResponder()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BidAmountViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
      
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
       }
       
       override func viewWillDisappear(_ animated: Bool) {
           
           NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
       }
    
    
    @IBAction func submitBtnTapped(_ sender: Any) {
        postBid()
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupUI(){
      bidAmountTF.placeholder = "0".localized
      currencySymbol.text = Utility.currencySymbol
    }
    
    @objc func  dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    func postBid() {
         let ud = UserDefaults.standard
        var dict = [String : Any]()
        var bidPrice = 0.0
       
      //  if bookingData?.bookingType == 1 {
            var bidDesc = ""
            if descTF.text == "" {
                bidDesc = ""
            }
            else {
                bidDesc = descTF.text!
            }
            
            if let bidText = bidAmountTF.text as? String {
                if bidText != "" {
                     
                    bidPrice = Double(bidText) ?? 0.0
                    
                    let lati = Helper.unwrappOptional(str : ud.object(forKey: "currentLat") as? String)
                    let longi = Helper.unwrappOptional(str : ud.object(forKey: "currentLog") as? String)
                    
                dict =  ["bookingId" :bookingData?.bookingId,
                         "latitude" : lati,
                         "longitude" : longi,
                         "status":3,
                         "quotedPrice": bidPrice,
                         "bidDescription": bidDesc]
                self.acceptRejectVC.acceptRejectModelBooking(params: dict ,completionHandler: { (succeeded) in
                    if succeeded{
                        Helper.hidePI()
                        self.bookingData?.statusCode = 3
                        _ = self.navigationController?.popToRootViewController(animated: true)
                        //self.goToMyProjects()
                        // self.delegate?.moveToUpcoming()
                    }else{
                        Helper.hidePI()
                    }
                })
                } else {
                    Helper.alertVC(errMSG: "Please enter your bid amount".localized)
                }
        }
      //  }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            
            submitBottomConstraint.constant = 25
              self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                
                self.submitBottomConstraint.constant = keyboardVisibleHeight
                self.view.layoutIfNeeded()
            })
            .disposed(by: disposeBag)
    }
    
}

extension BidAmountViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                
                self.submitBottomConstraint.constant = keyboardVisibleHeight
                self.view.layoutIfNeeded()
            })
            .disposed(by: disposeBag)
    
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
