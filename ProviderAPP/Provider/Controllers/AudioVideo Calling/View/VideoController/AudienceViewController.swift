
//
//  AudienceViewController.swift
//  Live
//  Created by Vengababu Maparthi on 24/11/18.
//  Copyright © 2018 Vengababu Maparthi. All rights reserved.
//


import UIKit
import Kingfisher

enum CallingType:String {
    case call = "call"
    case request = "request"
    case video = "video"
    case audio = "audio"
}

class AudienceViewController: UIViewController {
    
    @IBOutlet weak var localHeight: NSLayoutConstraint!
    @IBOutlet weak var localWidth: NSLayoutConstraint!
    @IBOutlet weak var localViewtopView: NSLayoutConstraint!
    @IBOutlet weak var localViewTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var bottomContraintForCallView: NSLayoutConstraint!
    
    
    @IBOutlet weak var remoteView: RTCEAGLVideoView!
    @IBOutlet weak var localView: RTCCameraPreviewView!
    
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var inputOverlay: UIVisualEffectView!
    
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var connectionLabel: UILabel!
    
    var usersName = ""
    var usersImage = ""
    var userID = ""
    var callID = ""
    var bookingID = ""
    
    var webrtc:webRTC?
    var streamModel = CallingModule()
    
    var joiningCall = false
    var callDisplayTimer = Timer()
    var timer : Timer?
    var secound = 0
    var isSwitch : Bool?
    
    
    @IBOutlet weak var callTimeLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.videoCallFromTo()
        self.updateTheLocalView(hasRemoteView: false)
    }
    
    func videoCallFromTo() {
        if !joiningCall{
            self.startingTheCall()
            timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timeoutRemoveScreen), userInfo: nil, repeats: false)
            updateTheview()
        }else{
            self.webrtc = webRTC(localView: self.localView, remoteView: self.remoteView, callID: Helper.CalledID())
            self.webrtc?.delegate = self
            UIView.animate(withDuration: 0.2, animations: {
                self.inputOverlay.alpha = 0
            }) { (finished) in
                self.inputOverlay.isHidden = true
            }
            
            MQTT.sharedInstance.subscribeChannel(withChannelName: MQTTTopics.CallTopic + Helper.CalledID())
        }
        MQTT.sharedInstance.delegate = self
    }
    
    
    func updateTheview()  {
        userName.text = usersName
        userImage.kf.setImage(with: URL(string: usersImage),
                              placeholder:UIImage.init(named: "signup_profile_default_image"),
                              options: [.transition(ImageTransition.fade(1))],
                              progressBlock: { receivedSize, totalSize in
        },
                              completionHandler: { image, error, cacheType, imageURL in
        })
    }
    
    //timeout remove audioScreen
    @objc func timeoutRemoveScreen() {
        timer?.invalidate()
        self.endingCall(callType:CallingType.call.rawValue)
        
    }
    
    
    //profilePic
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disconnecTheTopics()
    }
    
    func disconnecTheTopics()  {
        if Helper.CalledID() != ""{
            webrtc?.disconnect()
            MQTT.sharedInstance.unsubscribeTopic(topic: MQTTTopics.CallTopic + Helper.CalledID())
        }
    }
    
    func endingCall(callType:String) {
        if Helper.CalledID() != ""{
            streamModel.rejectTheCall(param: RejectCall.init(roomID: Helper.CalledID(), typeReject: callType)) { (success) in
                if success{
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func startingTheCall() {
        
        streamModel.createCall(param: CreateCallParam.init(typeCall: CallingType.video.rawValue, roomID: callID, userID: userID, boookingID: bookingID)) { (succcess) in
            if succcess{
               
                self.webrtc = webRTC(localView: self.localView, remoteView: self.remoteView, callID: Helper.CalledID())
                self.webrtc?.delegate = self
                
            }else{
               // Helper.alertVC(errMSG: "already in call")
               
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    @objc func updateTimer(){
        secound += 1
        callTimeLabel.text = timeString(time: TimeInterval(secound))
    }
    
    @objc func dismissCallView() {
         webrtc?.disconnect()
        self.dismiss(animated: true, completion: nil)
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    
    @IBAction func cancelCall(_ sender: Any) {
        webrtc?.disconnect()
        endingCall(callType:CallingType.call.rawValue)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func switchCamera(_ sender: Any) {
        
        if isSwitch == false{
            isSwitch = true
           webrtc?.switchCamera()
        }else{
            isSwitch = false
             webrtc?.switchCamera()
        }
    }
    
    
    @IBAction func muteMikeAction(_ sender: UIButton) {
        if sender.isSelected == true{
            webrtc?.client?.unmuteAudioIn()
            sender.isSelected = false
        }else{
            sender.isSelected = true
            webrtc?.client?.muteAudioIn()}
    }
    
    @IBAction func tapGestureAction(_ sender: Any) {
        if bottomContraintForCallView.constant == 0 {
            bottomContraintForCallView.constant = -180
        }else{
            bottomContraintForCallView.constant = 0
        }
    }
    
    
    func updateTheLocalView(hasRemoteView:Bool) {
        DispatchQueue.main.async {
            if hasRemoteView{
                self.localHeight.constant = 180
                self.localWidth.constant = 130
                self.localViewtopView.constant = 20
                self.localViewTrailing.constant = 20
            }else{
                self.localHeight.constant = UIScreen.main.bounds.width
                self.localWidth.constant = UIScreen.main.bounds.height
                self.localViewtopView.constant = 0
                self.localViewTrailing.constant = 0
            }
        }
    }
}



extension AudienceViewController:webRTCdelegate{
    func remoteViewReceived(_ remoteViewReceived: Bool) {
        timer?.invalidate()
        self.remoteView.bringSubviewToFront(self.localView)
        UIView.animate(withDuration: 0.2, animations: {
            self.inputOverlay.alpha = 0
        }) { (finished) in
            self.inputOverlay.isHidden = true
        }
        self.updateTheLocalView(hasRemoteView: true)
    }
    
    func appClientStatus(_ client: ARDAppClient, status: ARDAppClientState) {
        switch (status){
        case ARDAppClientState.connected :
            self.connectionLabel.text = ""
            if callDisplayTimer.isValid != true {
                callDisplayTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer) , userInfo: nil, repeats: true)
            }
            break
            
        case ARDAppClientState.connecting :
            DispatchQueue.main.async {
                 self.connectionLabel.text = "connecting.."
            }
           
            
            break
            
        case ARDAppClientState.disconnected:
            DispatchQueue.main.async {
                self.connectionLabel.text = "disconnecting.."
                Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.dismissCallView), userInfo: nil, repeats: true)
            }
            
           // webrtc?.disconnect()
            //self.dismiss(animated: true, completion: nil)
            break
        }
    }
}



extension AudienceViewController: MQTTManagerDelegate {
    func receivedMessage(_ message: [String : Any]!, andChannel channel: String!) {
        if channel.contains("Calls") ||  channel.contains("call"){
            if let data = message["data"] as? [String:Any]{
                if let type = data["type"] as? Int {
                    if type == 7 || type == 2{
                        webrtc?.disconnect()
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                
                if let action = data["action"] as? Int {
                    if action == 4 {
                        webrtc?.disconnect()
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
}


