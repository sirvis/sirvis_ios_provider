 //
//  IncomingCallView.swift
//  webRtc_module
//
//  Created by Imma Web Pvt Ltd on 05/09/17.
//  Copyright © 2017 3embed. All rights reserved.
//

import UIKit
 import AVFoundation
 import Kingfisher
 import CocoaLumberjack

class IncomingAudiocallView: UIView {

    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var callTypelbl: UILabel!
    
    @IBOutlet weak var acceptCall: UIButton!

    @IBOutlet weak var rejectCall: UIButton!
    
    
    @IBOutlet weak var userName: UILabel!

    var callID:String?
    var callerID : String?
    var messageDict : [String:Any]?
    var timer : Timer?
    var player: AVAudioPlayer?
    
    var streamModel = CallingModule()
    //init
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    private func commonInit(){
        
        Bundle.main.loadNibNamed("IncomingAudiocallView", owner: self, options: nil)
        userImageView.layer.cornerRadius = userImageView.frame.width/2
        userImageView.clipsToBounds = true
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
        //start timer 60 sec for incoming calling screen
         timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timeoutRemoveScreen), userInfo: nil, repeats: false)
        
    }
    
    
    //time out remove incomingscreen
    @objc func timeoutRemoveScreen() {
        player?.stop()
        timer?.invalidate()
        self.endingCall(callType:CallingType.request.rawValue)
        self.removeFromSuperview()
    }
    
    func endingCall(callType:String) {
        if Helper.CalledID() != ""{
            streamModel.rejectTheCall(param: RejectCall.init(roomID: Helper.CalledID(), typeReject: callType)){ (success) in
                if success{
                    
                }
            }
        }
    }
    
    //accept button cliked..
    @IBAction func acceptCallAction(_ sender: Any) {
        DDLogDebug("\n\n\n********acceptCallAction  cliked:*****\n\n")
        timer?.invalidate()
        player?.stop()
        
        streamModel.acceptTheCall(callID: Helper.CalledID()){ (success) in
            if success{
                if let callType = self.messageDict!["type"] as? String, callType == CallingType.audio.rawValue{
                    self.makeAudioCall()
                }else{
                    self.makeTheVideoCall()
                }
                self.removeFromSuperview()
            }
        }
    }

    func makeTheVideoCall() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let audioView = VideoCallView(frame: CGRect(x:0, y:0, width: window.frame.width, height: window.frame.height))
        audioView.usersName =  messageDict?["userName"] as? String ?? ""
        audioView.userNameLabel.text = messageDict?["userName"] as? String ?? ""
     //   audioView.userID = (bookingDict?.customerId)!
      //  audioView.bookingID = String(describing: bookingDict?.bookingId)
        let imageURL = messageDict?["userImage"] as? String ?? ""

        audioView.joiningCall = true
        audioView.callID = messageDict?["room"] as? String ?? ""
        audioView.userImage.kf.setImage(with: URL(string: imageURL),
                                        placeholder:UIImage.init(named: "signup_profile_default_image"),
                                        options: [.transition(ImageTransition.fade(1))],
                                        progressBlock: { receivedSize, totalSize in
        },
                                        completionHandler: { image, error, cacheType, imageURL in
        })
        audioView.videoCallFromTo()
        window.addSubview(audioView)
        
       /* let storyboard = UIStoryboard(name: "CallController", bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let controller = storyboard.instantiateViewController(withIdentifier: "audience") as! AudienceViewController
        controller.joiningCall = true
        controller.usersName = messageDict!["userName"] as! String
        controller.callID = messageDict!["room"] as! String
        controller.usersImage = messageDict!["userImage"] as! String
        appDelegate.window?.rootViewController!.present(controller, animated: true, completion: nil) */
        
        
    }
    
    
    func makeAudioCall() {
        let window = UIApplication.shared.keyWindow!
        let audioView = AudioCallView(frame: CGRect(x:0, y:0, width: window.frame.width, height: window.frame.height))
        audioView.tag = 15
        if let imageUrl = messageDict!["userImage"] as? String {
            audioView.userImageView.kf.setImage(with: URL(string: imageUrl ), placeholder: #imageLiteral(resourceName: "signup_profile_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
            })
        }
        audioView.userNameLbl.text = userName.text
        audioView.initWebrtc(messageData: messageDict!)
        audioView.messageDict = messageDict!
        window.addSubview(audioView);
        audioView.stopTimer()
    }

    
    //reject button cliked..
    @IBAction func rejectCallAction(_ sender: Any) {
        
        DDLogDebug("\n\n\n*******acceptCallAction  cliked:*******\n\n")
        player?.stop()
        timer?.invalidate()
        self.endingCall(callType:CallingType.request.rawValue)
        ///play endcall sound here
        self.playSound("end_of_call", loop: 1)
        let when = DispatchTime.now() + 0.30
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.removeFromSuperview()
        }
    }
    
    
    //set data here
    func setCallId(messageData: [String:Any]) {
        callID = messageData["callId"] as? String
        callerID  = messageData["callerId"] as? String
        messageDict = messageData
    }
    
    
    func playSound(_ soundName: String ,loop: Int){
        
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "wav")else{ return}
        do {
            
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [])
            try AVAudioSession.sharedInstance().setActive(true)
            player  = try AVAudioPlayer(contentsOf: url)
            player?.delegate = self
            player?.numberOfLoops = loop
            guard let player = player else { return}
            player.play()
            
        }catch let error{
            DDLogDebug("error \(error.localizedDescription)")
        }
    }

}
 
 
 extension IncomingAudiocallView: AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool){
        DDLogDebug("sound finished here ...")
    }
 }
