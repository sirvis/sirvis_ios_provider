//
//  AudioCallView.swift
//  webRtc_module
//
//  Created by Imma Web Pvt Ltd on 05/09/17.
//  Copyright © 2017 3embed. All rights reserved.
//

import UIKit
import AVFoundation
import CocoaLumberjack


class AudioCallView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var callTypeLbl: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var callTimerLbl: UILabel!
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var endCallButton: UIButton!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var webRtc: webRTC?
    
    var messageDict : [String:Any]?
    var timer : Timer?
    
    var player: AVAudioPlayer?
    var callDisplayTimer = Timer()
    var secound = 0
    var resumeTapped = false
    var callerID: String? =  ""
    var callId : String?
    var bookingID = ""
    var streamModel = CallingModule()
    var mqtt = MQTT.sharedInstance
    
    
    //init
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    
    //set data here
    func setMessageData(messageData:[String : Any]) {
        messageDict = messageData
        callId = messageData["callId"] as? String
    }
    
    
    private func commonInit(){
        
        Bundle.main.loadNibNamed("AudioCallView", owner: self, options: nil)
        userImageView.layer.cornerRadius = userImageView.frame.width/2
        userImageView.clipsToBounds = true
        callTimerLbl.text = "Connecting...".localized
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
        //start timer 60 sec for incoming calling screen
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timeoutRemoveScreen), userInfo: nil, repeats: false)
    }
    
    /// Stop timer here
    func stopTimer() {
        timer?.invalidate()
    }
 
    func startingCallFromCustomer() {
        callId  = Helper.randomString(length: 100)
    }
    
    
    func startingTheCall() {
        streamModel.createCall(param: CreateCallParam.init(typeCall: CallingType.audio.rawValue, roomID: callId!, userID: callerID!, boookingID: bookingID )) { (succcess) in
            if succcess{
                self.playSound("end_of_call", loop: 0)
                self.webRtc = webRTC.init(localView: nil, remoteView: nil, callID: Helper.CalledID())
                self.webRtc?.delegate = self
                MQTT.sharedInstance.delegate = self
            }else{
                //Helper.alertVC(errMSG: "already in call")
                self.removeFromSuperview()
            }
        }
    }
    
    @objc func updateTimer(){
        secound += 1
        callTimerLbl.text = timeString(time: TimeInterval(secound))
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    //timeout remove audioScreen
    @objc func timeoutRemoveScreen() {
    
        timer?.invalidate()
        DDLogDebug("***\n\n**********time out called ***************\n*****")
        
        self.endingCall(callType:"call") //end the call
        self.removeFromSuperview()
    }
    
    func endingCall(callType:String) {
        
        if Helper.CalledID() != ""{
            if self.webRtc == nil{
                self.removeFromSuperview()
                return
            }
            streamModel.rejectTheCall(param: RejectCall.init(roomID: Helper.CalledID(), typeReject: callType))
            { (success) in
                if success{
                    
                }
            }
        }
    }
    
    //init webRtc
    func initWebrtc(messageData: [String:Any])  {
        MQTT.sharedInstance.delegate = self
        webRtc = webRTC.init(localView: nil, remoteView: nil, callID: messageData["callId"] as! String)
        webRtc?.delegate = self
        callId = (messageData["callId"] as! String)
        messageDict = messageData
    }
    
    
    //mute button cliked..
    @IBAction func muteAction(_ sender: Any) {
        
        if muteButton.isSelected == true{
            webRtc?.client?.unmuteAudioIn()
            muteButton.isSelected = false
        }else{
            muteButton.isSelected = true
            webRtc?.client?.muteAudioIn()
        }
    }
    
    
    //speaker button cliked..
    @IBAction func speakerAction(_ sender: Any) {
        
        if speakerButton.isSelected == true{
            speakerButton.isSelected = false
            webRtc?.enableSpeaker()
            //webRtc?.client?.enableSpeaker()
            
        }else{
            speakerButton.isSelected = true
            webRtc?.disableSpeaker()
           // webRtc?.client?.disableSpeaker()
        }
    }
    
    
    @IBAction func hideAndViewBottomView(_ sender: Any) {
        if bottomConstraint.constant == 0{
            bottomConstraint.constant = -270
        }else{
            bottomConstraint.constant = 0
        }
    }
    
    //end button cliked..
    @IBAction func endAction(_ sender: Any) {
        timer?.invalidate()
        webRtc?.disconnect()
        self.endingCall(callType:CallingType.call.rawValue)
        self.playSound("end_of_call", loop: 1)
        let when = DispatchTime.now() + 0.30
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            self.removeFromSuperview()
        }
    }
    
    
    
    func playSound(_ soundName: String,loop: Int){
        let audioSession:AVAudioSession = AVAudioSession.sharedInstance()
        
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "wav")else{ return}
        do {
            //                   try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, mode: AVAudioSessionModeDefault, options: [])
            //            try AVAudioSession.sharedInstance().setActive(true)
            try audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.none)
            try audioSession.setActive(true)
            
            player  = try AVAudioPlayer(contentsOf: url)
            player?.delegate = self
            guard let player = player else { return}
            player.play()
        }catch let error{
            DDLogDebug("error \(error.localizedDescription)")
        }
    }
}


extension AudioCallView: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool){
        DDLogDebug("sound finished here ...")
    }
}


extension AudioCallView : webRTCdelegate{
    func remoteViewReceived(_ remoteViewReceived: Bool) {
       stopTimer()
    }
    
    func appClientStatus(_ client: ARDAppClient, status: ARDAppClientState) {
        DDLogDebug("webRTC status changed =\(status)")
        switch (status){
        case ARDAppClientState.connected :
            
            if callDisplayTimer.isValid != true {
                callDisplayTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer) , userInfo: nil, repeats: true)
            }
            break
            
        case ARDAppClientState.connecting :
            DispatchQueue.main.async {
                self.callTimerLbl.text = "Connecting..".localized
            }
            
            
            break
            
        case ARDAppClientState.disconnected:
             DispatchQueue.main.async {
              self.callTimerLbl.text = "Disconnected.".localized
             }
            webRtc?.disconnect()
            self.removeFromSuperview()
            break
        }
    }
}

extension AudioCallView:MQTTManagerDelegate{
    func receivedMessage(_ message: [String : Any]!, andChannel channel: String!) {
        if channel.contains("Calls")  ||  channel.contains("call") {
             if let data = message["data"] as? [String:Any]{
                if let type = data["type"] as? Int {
                    if type == 7 || type == 2{
                        webRtc?.disconnect()
                        self.removeFromSuperview()
                    }
                }
                if let action = data["action"] as? Int {
                    if action == 4 {
                        webRtc?.disconnect()
                        self.removeFromSuperview()
                        
                        
                    }
                }
            }
  
        }
    }
}

