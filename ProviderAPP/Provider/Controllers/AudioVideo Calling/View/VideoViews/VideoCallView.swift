//
//  VideoCallView.swift
//  LSP
//
//  Created by 3Embed on 22/06/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit
import AVFoundation
import CocoaLumberjack
import Kingfisher

/*enum CallingType:String {
    case call = "call"
    case request = "request"
    case video = "video"
    case audio = "audio"
}*/

class VideoCallView: UIView {
    
     @IBOutlet var contentView: UIView!
    @IBOutlet weak var localHeight: NSLayoutConstraint!
    @IBOutlet weak var localWidth: NSLayoutConstraint!
    @IBOutlet weak var localViewtopView: NSLayoutConstraint!
    @IBOutlet weak var localViewTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var bottomContraintForCallView: NSLayoutConstraint!
    
    @IBOutlet weak var remoteView: RTCEAGLVideoView!
    @IBOutlet weak var localView: RTCCameraPreviewView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var inputOverlay: UIVisualEffectView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var connectionLabel: UILabel!
    
    var usersName = ""
    var usersImage = ""
    var userID = ""
    var callID = ""
    var bookingID = ""
    
    var webrtc:webRTC?
    var streamModel = CallingModule()
    
    var joiningCall = false
    var callDisplayTimer = Timer()
    var timer : Timer?
    var secound = 0
    var isSwitch : Bool?
    var isConnected: Bool?
    var bookingEndTime: Double?
    
    
    @IBOutlet weak var callTimeLabel: UILabel!

    //init
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    func commonInit() {
       // self.videoCallFromTo()
        Bundle.main.loadNibNamed("VideoCallView", owner: self, options: nil)
        userImage.layer.cornerRadius = userImage.frame.width/2
        userImage.clipsToBounds = true
        connectionLabel.text = "Connecting...".localized
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
       // self.updateTheLocalView(hasRemoteView: false)
    }
    
    
    func videoCallFromTo() {
        if !joiningCall{
            self.startingTheCall()
            timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timeoutRemoveScreen), userInfo: nil, repeats: false)
            //updateTheview()
        }else{
            self.webrtc = webRTC(localView: self.localView, remoteView: self.remoteView, callID: Helper.CalledID())
            self.webrtc?.delegate = self
            UIView.animate(withDuration: 0.2, animations: {
                self.inputOverlay.alpha = 0
            }) { (finished) in
                self.inputOverlay.isHidden = true
            }
            
            MQTT.sharedInstance.subscribeChannel(withChannelName: MQTTTopics.CallTopic + Helper.CalledID())
        }
        MQTT.sharedInstance.delegate = self
    }
    
    
    func updateTheview()  {
        userNameLabel.text = usersName
        userImage.kf.setImage(with: URL(string: usersImage),
                              placeholder:UIImage.init(named: "signup_profile_default_image"),
                              options: [.transition(ImageTransition.fade(1))],
                              progressBlock: { receivedSize, totalSize in
        },
                              completionHandler: { image, error, cacheType, imageURL in
        })
    }
    
    //timeout remove audioScreen
    @objc func timeoutRemoveScreen() {
        timer?.invalidate()
        self.endingCall(callType:CallingType.call.rawValue)
        
    }
    
    
   
    
    func disconnecTheTopics()  {
        if Helper.CalledID() != ""{
            webrtc?.disconnect()
            MQTT.sharedInstance.unsubscribeTopic(topic: MQTTTopics.CallTopic + Helper.CalledID())
        }
    }
    
    func endingCall(callType:String) {
        if Helper.CalledID() != ""{
            streamModel.rejectTheCall(param: RejectCall.init(roomID: Helper.CalledID(), typeReject: callType)) { (success) in
                if success{
                     self.removeFromSuperview()
                }else{
                     self.removeFromSuperview()
                }
            }
        }
    }
    
    func startingTheCall() {
        
        streamModel.createCall(param: CreateCallParam.init(typeCall: CallingType.video.rawValue, roomID: callID, userID: userID, boookingID: bookingID ?? "")) { (succcess) in
            if succcess{
                
                self.webrtc = webRTC(localView: self.localView, remoteView: self.remoteView, callID: Helper.CalledID())
                self.webrtc?.delegate = self
                
            }else{
                // Helper.alertVC(errMSG: "already in call")
                
                 self.removeFromSuperview()
            }
        }
    }
    
    /*override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }*/
    
    
    @objc func updateTimer(){
        secound += 1
        callTimeLabel.text = timeString(time: TimeInterval(secound))
        
        let currentTime = Date().timeIntervalSince1970
        
        
        if Double(currentTime) == bookingEndTime {
            webrtc?.disconnect()
            self.removeFromSuperview()
        }
        
        let  endTimeinDate = Date(timeIntervalSince1970: bookingEndTime ?? 0)
        let reminderTime =  endTimeinDate.addingTimeInterval(-2 * 60)
    let reminderTimeInterval = reminderTime.timeIntervalSince1970
        if Double(currentTime) == Double(reminderTimeInterval) {
            Helper.alertVC(errMSG: "Your slot for the call shall end in the next 2 minutes.Please book another slot ".localized)
        }
            
            
    }
    
    @objc func dismissCallView() {
        if !isConnected!  {
            webrtc?.disconnect()
            self.removeFromSuperview()
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    
    @IBAction func cancelCall(_ sender: Any) {
        webrtc?.disconnect()
        endingCall(callType:CallingType.call.rawValue)
         self.removeFromSuperview()
    }
    
    @IBAction func switchCamera(_ sender: Any) {
        
        if isSwitch == false{
            isSwitch = true
            webrtc?.switchCamera()
        }else{
            isSwitch = false
            webrtc?.switchCamera()
        }
    }
    
    
    @IBAction func muteMikeAction(_ sender: UIButton) {
        if sender.isSelected == true{
            webrtc?.client?.unmuteAudioIn()
            sender.isSelected = false
        }else{
            sender.isSelected = true
            webrtc?.client?.muteAudioIn()}
    }
    
    @IBAction func tapGestureAction(_ sender: Any) {
        if bottomContraintForCallView.constant == 0 {
            bottomContraintForCallView.constant = -180
        }else{
            bottomContraintForCallView.constant = 0
        }
    }
    
    
    func updateTheLocalView(hasRemoteView:Bool) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        self.addGestureRecognizer(tap)
        UIView.animate(withDuration: 0.3) {
            DispatchQueue.main.async {
                if hasRemoteView{
                    self.localHeight.constant = 180
                    self.localWidth.constant = 130
                    self.localViewtopView.constant = 20
                    self.localViewTrailing.constant = 20
                }else{
                    self.localHeight.constant = UIScreen.main.bounds.width
                    self.localWidth.constant = UIScreen.main.bounds.height
                    self.localViewtopView.constant = 0
                    self.localViewTrailing.constant = 0
                }
                
                self.layoutIfNeeded()
            }
        }
       
    }
    
    @objc func viewTapped() {
        if bottomContraintForCallView.constant == 0 {
            bottomContraintForCallView.constant = -180
        }else{
            bottomContraintForCallView.constant = 0
        }
    }
    
}



extension VideoCallView:webRTCdelegate{
    func remoteViewReceived(_ remoteViewReceived: Bool) {
        timer?.invalidate()
        self.remoteView.bringSubviewToFront(self.localView)
        UIView.animate(withDuration: 0.2, animations: {
            self.inputOverlay.alpha = 0
        }) { (finished) in
            self.inputOverlay.isHidden = true
        }
        self.updateTheLocalView(hasRemoteView: true)
    }
    
    func appClientStatus(_ client: ARDAppClient, status: ARDAppClientState) {
        switch (status){
        case ARDAppClientState.connected :
            isConnected = true
            
            DispatchQueue.main.async {
                self.localView.isHidden = false
                self.connectionLabel.text = ""
                
            }
            
            if callDisplayTimer.isValid != true {
                callDisplayTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer) , userInfo: nil, repeats: true)
            }
            break
            
        case ARDAppClientState.connecting :
            DispatchQueue.main.async {
               
                self.connectionLabel.text = "Connecting.."
                
            }
            
            
            break
            
        case ARDAppClientState.disconnected:
            isConnected = false
            DispatchQueue.main.async {
                self.connectionLabel.text = "Reconnecting.."
                Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.dismissCallView), userInfo: nil, repeats: true)
            }
            
            // webrtc?.disconnect()
            //self.dismiss(animated: true, completion: nil)
            break
        }
    }
}


extension VideoCallView: MQTTManagerDelegate {
    func receivedMessage(_ message: [String : Any]!, andChannel channel: String!) {
        if channel.contains("Calls") ||  channel.contains("call"){
            if let data = message["data"] as? [String:Any]{
                if let type = data["type"] as? Int {
                    if type == 7 || type == 2{
                        webrtc?.disconnect()
                        self.removeFromSuperview()
                    }
                }
                
                if let action = data["action"] as? Int {
                    if action == 4 {
                        webrtc?.disconnect()
                       self.removeFromSuperview()
                    }
                }
                
                if let endTime = data["bookingEndtime"] as? Double {
                  self.bookingEndTime = endTime
                }
            }
        }
    }
}
