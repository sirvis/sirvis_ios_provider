//
//  IncomingVideocallView.swift
//  webRtc_module
//
//  Created by Imma Web Pvt Ltd on 08/09/17.
//  Copyright © 2017 3embed. All rights reserved.
//

import UIKit
import AVFoundation
import CocoaLumberjack


class IncomingVideocallView: UIView {

    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var remoteView: RTCEAGLVideoView!
    @IBOutlet weak var localView: RTCCameraPreviewView!
    @IBOutlet weak var callEndBtn: UIButton!
    @IBOutlet weak var callAcceptBtn: UIButton!


    @IBOutlet weak var videoCallLbl: UILabel!
    @IBOutlet weak var videoCallIcon: UIImageView!
    @IBOutlet weak var incomingVidLbl: UILabel!
    @IBOutlet weak var declineLbl: UILabel!
    @IBOutlet weak var acceptlbl: UILabel!
    
    
    @IBOutlet weak var switch_btn: UIButton!
    @IBOutlet weak var endVcall_btn: UIButton!
    @IBOutlet weak var muteBtn: UIButton!
    
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var calling_userName: UILabel!
    @IBOutlet weak var calling_status: UILabel!
    
    @IBOutlet weak var localViewConstantY: NSLayoutConstraint!
    @IBOutlet weak var endBtnConstantY: NSLayoutConstraint!
    
    
    

    var webRtc: webRTC?
    var streamModel = CallingModule()
    var callId : String?
    var messageDict : [String:Any]?
    var timer : Timer?
    var callerId : String?
    var isSwitch : Bool?
   // var chatViewObj : ChatViewController? = nil
    var player: AVAudioPlayer?
    
    var tapGester : UITapGestureRecognizer?
    var swipeGester : UISwipeGestureRecognizer?
    var panGester : UIPanGestureRecognizer?
    @objc var remoteViewPanGesture: UIPanGestureRecognizer?
    var isvideoSwiped = false
    var otherCallerId : String?  = ""
    
    
    
    var captureSession = AVCaptureSession()
    var sessionOutput = AVCaptureStillImageOutput()
    var previewLayer = AVCaptureVideoPreviewLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    //set data here
    func setMessageData(messageData:[String : Any]) {
        messageDict = messageData
        callId = messageData["callId"] as? String
        callerId = messageData["callerId"] as? String
    }
    
    private func commonInit(){
        
        
        Bundle.main.loadNibNamed("IncomingVideocallView", owner: self, options: nil)
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
        
        localView.isHidden = true
        switch_btn.isHidden = true
        endVcall_btn.isHidden = true
        muteBtn.isHidden = true
        isSwitch = false
        userImageView.isHidden = true
        calling_userName.isHidden = true
        calling_status.isHidden = true
        
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        userImageView.clipsToBounds = true
        
        //start timer 60 sec for incoming calling screen
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timeoutRemoveScreen), userInfo: nil, repeats: false)
        
        
        tapGester = UITapGestureRecognizer.init(target: self, action: #selector(tapGesterCliked))
        tapGester?.numberOfTapsRequired = 1
       
        
        swipeGester = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeGesterCliked))
        swipeGester?.numberOfTouchesRequired = 1
        swipeGester?.direction = .down
        
        panGester = UIPanGestureRecognizer.init(target: self, action: #selector(panGesterCliked))
        
        remoteViewPanGesture = UIPanGestureRecognizer(target: self.remoteView, action: #selector(setter: remoteViewPanGesture))
        self.remoteView.addGestureRecognizer(remoteViewPanGesture!)
    }
    
    
    
    @objc func panGesterCliked(_ panGester: UIPanGestureRecognizer){
        
        if panGester.state == .began || panGester.state == .changed{
            
            let traslation = panGester.translation(in: UIApplication.shared.keyWindow)
            
            panGester.view?.center =  CGPoint(x: panGester.view!.center.x + traslation.x, y: panGester.view!.center.y + traslation.y)
            
            panGester.setTranslation(CGPoint.zero, in: UIApplication.shared.keyWindow)
        }
    }
    
    func remoteViewPanGesture(_ panGesture: UIPanGestureRecognizer){
        if panGesture.state == .began || panGesture.state == .changed{
            let traslation = panGesture.translation(in: UIApplication.shared.keyWindow)
            panGesture.view?.center =  CGPoint(x: panGesture.view!.center.x + traslation.x, y: panGesture.view!.center.y + traslation.y)

            panGesture.setTranslation(CGPoint.zero, in: UIApplication.shared.keyWindow)
        }
    }
    
    @objc func swipeGesterCliked(_ swipeGester: UISwipeGestureRecognizer){
        
        
        if isvideoSwiped == false{
        
        self.frame = CGRect.init(x: 50, y: 300, width:150, height: 150)
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
        self.localView.isHidden = true
        self.switch_btn.isHidden = true
        self.endVcall_btn.isHidden = true
        self.muteBtn.isHidden = true
        isvideoSwiped = true
        self.addGestureRecognizer(panGester!)
            
        }else{
            
        }
        
    }
    
    @objc func tapGesterCliked() {
        if isvideoSwiped == false{
            UIView.animate(withDuration: 3 ,delay:5 , options: .showHideTransitionViews , animations: {
                self.localViewConstantY.constant = -(self.localViewConstantY.constant + self.localView.frame.size.height)
                self.endBtnConstantY.constant =  -(self.endBtnConstantY.constant + self.endVcall_btn.frame.size.height)
            }) { (isCompl) in}
            
        }else{
            isvideoSwiped = false
            self.frame = CGRect.init(x: 0, y: 0, width:UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.layer.cornerRadius = 0
            self.clipsToBounds = false
            self.localView.isHidden = false
            self.switch_btn.isHidden = false
            self.endVcall_btn.isHidden = false
            self.muteBtn.isHidden = false
           
            self.removeGestureRecognizer(panGester!)
        }
    }
    
    
    
    //timeout remove audioScreen
    @objc func timeoutRemoveScreen() {
        timer?.invalidate()
    self.endingCall(callType:CallingType.call.rawValue)
    }
    func endingCall(callType:String) {
        if Helper.CalledID() != ""{
            streamModel.rejectTheCall(param: RejectCall.init(roomID: Helper.CalledID(), typeReject: callType)) { (success) in
                if success{
                    self.removeFromSuperview()
                }else{
                     self.removeFromSuperview()
                }
            }
        }
    }
    
    //init webRtc
    func initWebrtc(messageData: [String:Any])  {
        UserDefaults.standard.set(true, forKey: "iscallgoingOn")
         UserDefaults.standard.synchronize()
        
        
        let callID = UserDefaults.standard.object(forKey: "callerID") as? String
        
        webRtc = webRTC.init(localView: self.localView, remoteView: self.remoteView, callID: callID ?? "")
        callId = messageData["callId"] as? String
        messageDict = messageData
       
        contentView.addGestureRecognizer(tapGester!)
        contentView.addGestureRecognizer(swipeGester!)
        
    }

    
    @IBAction func endBtncliked(_ sender: Any) {
        webRtc?.disconnect()
    endingCall(callType:CallingType.call.rawValue)
     self.removeFromSuperview()
    }
   
    
    @IBAction func acceptBtnCliked(_ sender: Any) {
        
        hideSubviews()
        player?.stop()
        timer?.invalidate()
        
       // MQTTCallManager.sendAcceptCallStatus(messageData: messageDict!)
        self.removeCameraLayer()
        self.initWebrtc(messageData:messageDict! )
       
        // webRtc?.switchLocalRemoteView(_localView: self.localView, _remoteView: self.remoteView)
    
    }
    
    
    func switchViews() {
        
      //  webRtc?.switchLocalRemoteView(_localView: self.localView, _remoteView: self.remoteView)
        localView.isHidden = false
        userImageView.isHidden = true
        calling_userName.isHidden = true
        calling_status.isHidden = true

    }
    
    
    
    func hideSubviews() {
        localView.isHidden = false
        userName.isHidden = true
        videoCallLbl.isHidden = true
        videoCallIcon.isHidden = true
        incomingVidLbl.isHidden = true
        callEndBtn.isHidden = true
        callAcceptBtn.isHidden = true
        declineLbl.isHidden = true
        acceptlbl.isHidden = true
        
        switch_btn.isHidden = false
        endVcall_btn.isHidden = false
        muteBtn.isHidden = false
        
        userImageView.isHidden = true
        calling_userName.isHidden = true
        calling_status.isHidden = true
    }
    
    
    /*methodes after pick call*/
    
    
    //switch camera button action//
    @IBAction func switchBtnCliked(_ sender: Any) {
        
        if isSwitch == false{
            isSwitch = true
            //webRtc?.client?.swapCameraToBack()
        }else{
            isSwitch = false
            //webRtc?.client?.swapCameraToFront()
        }
        
    }
    
    
    
    
    //end video button action//
    @IBAction func endVideocallCliked(_ sender: Any) {
        
        
        webRtc?.disconnect()
        endingCall(callType:CallingType.call.rawValue)
       self.removeFromSuperview()
    }
    
    
    
    
    
    func playSound(_ soundName: String,loop: Int){
        
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "wav")else{ return}
        do {
            
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            player  = try AVAudioPlayer(contentsOf: url)
            player?.delegate = self
            player?.numberOfLoops = loop
            guard let player = player else { return}
            player.play()
            
        }catch let error{
            DDLogDebug("error \(error.localizedDescription)")
        }
        
    }
    
    
    
    //mute button action//
    @IBAction func muteBtnCliked(_ sender: Any) {
        if muteBtn.isSelected == true{
            webRtc?.client?.muteAudioIn()
            muteBtn.isSelected = false
        }else{
            muteBtn.isSelected = true
            webRtc?.client?.unmuteAudioIn()}
    }
    
    
    func setCallId() {
      //  callId = randomString(length: 100)
        hideSubviews()
        localView.isHidden = true
        userImageView.isHidden = false
        calling_userName.isHidden = false
        calling_status.isHidden = false
    }
    
    
    
    
    //Add camera layer
    func addCameraView(){
        
//        let device  = AVCaptureDevice.defaultDevice(withDeviceType: AVCaptureDevice.DeviceType.builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: AVCaptureDevice.Position.front)
//
//        do {
//
//            if device != nil{
//                let input = try AVCaptureDeviceInput.init(device: device)
//                if captureSession.canAddInput(input){
//                    captureSession.addInput(input)
//                    sessionOutput.outputSettings = [AVVideoCodecKey  : AVVideoCodecJPEG]
//
//                    if captureSession.canAddOutput(sessionOutput){
//                        captureSession.addOutput(sessionOutput)
//                        captureSession.startRunning()
//                        previewLayer = AVCaptureVideoPreviewLayer.init(session: captureSession)
//                        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
//                        previewLayer.connection!.videoOrientation = AVCaptureVideoOrientation.portrait
//
//                        self.contentView.layer.addSublayer(previewLayer)
//                        previewLayer.position = CGPoint.init(x: self.frame.width/2, y: self.frame.height/2)
//                        previewLayer.bounds = self.frame
//
//
//                        self.contentView.bringSubview(toFront: callEndBtn)
//                        self.contentView.bringSubview(toFront: acceptlbl)
//                        self.contentView.bringSubview(toFront: declineLbl)
//                        self.contentView.bringSubview(toFront: callAcceptBtn)
//                        self.contentView.bringSubview(toFront: videoCallLbl)
//                        self.contentView.bringSubview(toFront: videoCallIcon)
//                        self.contentView.bringSubview(toFront: userName)
//                        self.contentView.bringSubview(toFront: incomingVidLbl)
//                        self.contentView.bringSubview(toFront: switch_btn)
//                        self.contentView.bringSubview(toFront: endVcall_btn)
//                        self.contentView.bringSubview(toFront: muteBtn)
//                        self.contentView.bringSubview(toFront: userImageView)
//                        self.contentView.bringSubview(toFront: calling_status)
//                        self.contentView.bringSubview(toFront: calling_userName)
//
//                    }
//                }
//
//            }
//        }
//        catch{
//
//            DDLogDebug("print error")
//        }
    }

    
    
    
    //remove camera layer
    func removeCameraLayer(){
        self.contentView.layer.addSublayer(previewLayer)
        captureSession.stopRunning()
        previewLayer.removeFromSuperlayer()
    }
        
    
    
}







extension IncomingVideocallView: AVAudioPlayerDelegate{
    
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool){
    
      DDLogDebug("sound finished here ...")
        
    }
    
    
    
}




