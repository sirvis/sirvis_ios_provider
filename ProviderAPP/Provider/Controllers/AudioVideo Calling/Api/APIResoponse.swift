//
//  APIResoponse.swift
//  LSP
//
//  Created by Rahul Sharma on 3/23/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import Foundation
import Alamofire

class APIErrors {
    enum ErrorCode: Int {
        case success                = 200
        case badRequest             = 400
        case Unauthorized           = 401
        case Required               = 402
        case Forbidden              = 403
        case NotFound               = 404
        case MethodNotAllowed       = 405
        case NotAcceptable          = 406
        case Other                  = 409
        case PreconditionFailed     = 412
        case RequestEntityTooLarge  = 413
        case TooManyAttemt          = 429
        case ExpiredToken           = 477
        case InvalidToken           = 499
        case internalServerError    = 500
        
        init(rawValue: Int)
        {
            switch (rawValue) {
            case 200:
                self = .success
                break
                
            case 400:
                self = .badRequest
                break
                
            case 401:
//                Session.expired()
                self = .Unauthorized
                break
                
            default:
                self = .Other
                break
            }
        }
    }
     class func getAOTHCallHeader() -> HTTPHeaders {
          
          let dict: HTTPHeaders = [
              "lan" : "en",
              "authorization" : Helper.callAuthToken()
          ]
          
        //  DDLogVerbose("Session Token: \(Utility.sessionToken)")
          
          return dict
      }
    
}


class ApiResponse {
    
    
    var status = Bool()
    var data = [String:Any]()
    var statusCode: APIErrors.ErrorCode?
    
    init(status:Bool,response:[String:Any]) {
        self.status = status
        self.data = response
    }
    
    init(status: Bool, response:[String:Any], statusCode: APIErrors.ErrorCode) {
        self.status = status
        self.data = response
        self.statusCode = statusCode
    }
}
