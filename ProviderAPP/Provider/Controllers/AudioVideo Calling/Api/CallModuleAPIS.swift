//
//  CallModuleAPIS.swift
//  FlagitDriver
//
//  Created by Vengababu Maparthi on 12/03/19.
//  Copyright © 2019 3Embed. All rights reserved.3
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire


class CallModuleAPI {
    let disposebag = DisposeBag()
    let subject_response = PublishSubject<[String:Any]>()
    let apiResponse = PublishSubject<ApiResponse>()
    
    
    func createTheCall( params: [String:Any])
    {
        print(params)
        let apiFullName = CallAPIS.createCalling
        RxAlamofire.requestJSON(.post, apiFullName, parameters: params, headers: APIErrors.getAOTHCallHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as! [String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode.init(rawValue: head.statusCode)
            if errNum == .success {
                self.apiResponse.onNext(ApiResponse.init(status: true, response: bodyIn))
            }else{
                self.apiResponse.onNext(ApiResponse.init(status: false, response: bodyIn))
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    
    func rejectTheCall(param:[String:Any])
    {
        let apiFullName = CallAPIS.rejectCall
        RxAlamofire.requestJSON(.delete, apiFullName, parameters: param, encoding:JSONEncoding.default, headers: APIErrors.getAOTHCallHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as! [String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode.init(rawValue: head.statusCode)
            if errNum == .success {
                self.apiResponse.onNext(ApiResponse.init(status: true, response: bodyIn))
            }else{
                self.apiResponse.onNext(ApiResponse.init(status: false, response: bodyIn))
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    
    func acceptTheCall(callID:String)
    {
        let apiFullName = CallAPIS.answerCall + callID
        RxAlamofire.requestJSON(.put, apiFullName, parameters: nil, headers: APIErrors.getAOTHCallHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as! [String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode.init(rawValue: head.statusCode)
            if errNum == .success {
                self.apiResponse.onNext(ApiResponse.init(status: true, response: bodyIn))
            }else{
                self.apiResponse.onNext(ApiResponse.init(status: false, response: bodyIn))
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func validateTheCallID(callID:String,completionBlock:@escaping(Bool, Bool)->())
    {
        let apiFullName = CallAPIS.answerCall + callID
        var hasVideo = false
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil,  headers: APIErrors.getAOTHCallHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as! [String:Any]
            Helper.hidePI()
            if let data = bodyIn["data"] as? [String:Any] {
                if let callType = data["type"] as? String {
                    if callType == "video" {
                        hasVideo = true
                    } else {
                        hasVideo = false
                    }
                }
            }
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode.init(rawValue: head.statusCode)
            if errNum == .success {
                completionBlock(true, hasVideo)
                self.apiResponse.onNext(ApiResponse.init(status: true, response: bodyIn))
            }else{
                completionBlock(false, hasVideo)
                self.apiResponse.onNext(ApiResponse.init(status: false, response: bodyIn))
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
}
