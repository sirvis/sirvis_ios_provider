//
//  CallConstants.swift
//  FlagitDriver
//
//  Created by Vengababu Maparthi on 12/03/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import Foundation


struct CallAPIS {

    //Live
     static let CallBaseURL = "https://call.service-genie.xyz/"
    //dev
//     static let CallBaseURL = "https://dev-call.service-genie.xyz/"
    
    
    static let createCalling               = CallBaseURL + "call"
    
    static let answerCall                  = CallBaseURL + "call/" //callid
    
    static let rejectCall                  = CallBaseURL + "call"
    
    static let validateCall                = CallBaseURL + "call"
    
}

