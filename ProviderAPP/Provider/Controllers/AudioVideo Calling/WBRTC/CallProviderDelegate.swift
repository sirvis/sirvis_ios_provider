//
//  CallProviderDelegate.swift
//  MQTT Chat Module
//
//  Created by Imma Web Pvt Ltd on 12/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import CallKit
import AVFoundation
import Kingfisher
import CocoaLumberjack

class CallProviderDelegate: NSObject,CXProviderDelegate {
    /// Called when the provider has been reset. Delegates must respond to this callback by cleaning up all internal call state (disconnecting communication channels, releasing network resources, etc.). This callback can be treated as a request to end all calls without the need to respond to any actions
    @available(iOS 10.0, *)
    
    func providerDidReset(_ provider: CXProvider) {
        
        
    }
    
    
    let provider : CXProvider
    var callID : String = ""
    var messageData:[String:Any] = [:]
    var calleeName :String = ""
    var uuId : UUID?
    
    var streamModel = CallingModule()

    
    static var providerConfiguration: CXProviderConfiguration {
        
        let providerConfiguration: CXProviderConfiguration?
        providerConfiguration = CXProviderConfiguration.init(localizedName: " ")//type of call
        providerConfiguration!.supportsVideo = false
        providerConfiguration?.maximumCallGroups = 1
        
        if let iconmaskImg = UIImage.init(named: "video_call"){
            providerConfiguration?.iconTemplateImageData = iconmaskImg.pngData()
        }
        
        providerConfiguration?.ringtoneSound = "ring.caf"
       
        return providerConfiguration!
    }
    
    
    override init() {
        
        provider = CXProvider.init(configuration: CallProviderDelegate.providerConfiguration)
    
        super.init()
        provider.setDelegate(self, queue: nil)
        
    }
    
    
    
    func displayIncomingcall(uuid: UUID, handel : String, hasVideo:Bool,complition:((NSError?)-> Void)? = nil ){
        MQTT.sharedInstance.delegate = self
        calleeName = handel
        uuId = uuid
        let update = CXCallUpdate()
        update.remoteHandle  = CXHandle.init(type: .generic, value: calleeName)
        update.hasVideo = hasVideo
        self.providerDidBegin(provider)
        provider.reportNewIncomingCall(with: uuid, update: update) { error in
            complition?(error as NSError?)
        }
    }
    
    
    internal func providerDidBegin(_ provider: CXProvider){
        
        _ = Timer.scheduledTimer(timeInterval: 58, target: self, selector: #selector(CallProviderDelegate.expireCall), userInfo: nil, repeats: false)
    }
    
    
    @objc func expireCall() {
        provider.reportCall(with:uuId! , endedAt: nil, reason: .remoteEnded)
    }
    
    
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        UserDefaults.standard.set(callID, forKey: "callerID")
        streamModel.acceptTheCall(callID: callID){ (success) in
            if success{
                if let callType = self.messageData["type"] as? String, callType == CallingType.audio.rawValue{
                    self.loadTheAudioCallView()
                }else{
                    self.makeTheVideoCall()
                }
            }else{
                self.expireCall()
            }
        }
        
        action.fulfill()
    }
    
    
    func makeTheVideoCall() {
       /* let storyboard = UIStoryboard(name: "CallController", bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let controller = storyboard.instantiateViewController(withIdentifier: "audience") as! AudienceViewController
        controller.joiningCall = true
        controller.usersName = messageData["userName"] as! String
        controller.callID = messageData["room"] as! String
        controller.usersImage = messageData["userImage"] as! String
        appDelegate.window?.rootViewController!.present(controller, animated: true, completion: nil) */
        
        
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let audioView = VideoCallView(frame: CGRect(x:0, y:0, width: window.frame.width, height: window.frame.height))
        audioView.usersName =  messageData["userName"] as? String ?? ""
        audioView.userNameLabel.text =  messageData["userName"] as? String ?? ""
        //audioView.userID = (bookingDict?.customerId)!
        //audioView.bookingID = String(describing: bookingDict?.bookingId)
        let imageURL = messageData["userImage"] as? String ?? ""
        audioView.callID = messageData["room"] as? String ?? ""
        audioView.joiningCall = true
        audioView.userImage.kf.setImage(with: URL(string: imageURL),
                                        placeholder:UIImage.init(named: "signup_profile_default_image"),
                                        options: [.transition(ImageTransition.fade(1))],
                                        progressBlock: { receivedSize, totalSize in
        },
                                        completionHandler: { image, error, cacheType, imageURL in
        })
        
        
        audioView.videoCallFromTo()
        
        
        window.addSubview(audioView)
    }
    
    
    func loadTheAudioCallView()  {
        
        let window = UIApplication.shared.keyWindow!
        let audioView = AudioCallView(frame: CGRect(x:0, y:0, width: window.frame.width, height: window.frame.height))
        audioView.userImageView.kf.setImage(with: URL(string: messageData["userImage"] as! String ), placeholder: #imageLiteral(resourceName: "signup_profile_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
        })

        audioView.userNameLbl.text = calleeName
        audioView.initWebrtc(messageData: messageData)
        
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.none)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [])
            try AVAudioSession.sharedInstance().setActive(true)
        } catch _ {
        }
        audioView.messageDict = messageData
        window.addSubview(audioView);
    }
    
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction){
        
        streamModel.rejectTheCall(param: RejectCall.init(roomID: callID, typeReject: "request")){ (success) in
            provider.reportCall(with:self.uuId! , endedAt: nil, reason: .remoteEnded)
            
        }
    }

    
    
    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction){
        
        DDLogDebug("mute called here")        
    }
    
    
    
    /// Called when the provider's audio session activation state changes.
    
    internal func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession){
        
        DDLogDebug("Audio session didActive")
        
        
    }
    
    
    internal func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession){
        
        DDLogDebug("Audio session didDeactivate")
        
    }
    
}


extension CallProviderDelegate: MQTTManagerDelegate {
    func receivedMessage(_ message: [String : Any]!, andChannel channel: String!) {
        if channel == MQTTTopics.CallTopic + Helper.CalledID(){
            if let data = message["data"] as? [String:Any]{
                if let action = data["action"] as? Int {
                    if action == 4{
                        expireCall()
                    }
                }
            }
        }
    }
}


