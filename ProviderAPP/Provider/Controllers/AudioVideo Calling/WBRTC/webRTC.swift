//
//  webRTC.swift
//  webRtc_module
//
//  Created by Imma Web Pvt Ltd on 01/09/17.
//  Copyright © 2017 3embed. All rights reserved.
//

import UIKit
import AVFoundation
import CocoaLumberjack


@objc protocol webRTCdelegate {
   @objc optional func remoteViewReceived(_ remoteViewReceived:Bool)
   @objc optional func appClientStatus(_ client:ARDAppClient , status: ARDAppClientState)
}

class webRTC: NSObject,ARDAppClientDelegate,RTCVideoViewDelegate {
   
    
    
    
    var client: ARDAppClient?
    var captureController: ARDCaptureController?
    var localCaptureView: RTCCameraPreviewView?
   // var localVideoTrack: RTCVideoTrack?
    var remoteVideoTrack: RTCVideoTrack?
    
    let settingModel = ARDSettingsModel()
    //var localView: RTCEAGLVideoView?
    var remoteView : RTCEAGLVideoView?
    
    var delegate : webRTCdelegate?
    
    //init
    init(localView: RTCCameraPreviewView! ,remoteView : RTCEAGLVideoView! ,callID: String) {
        
        super.init()
        // Do any additional setup after loading the view.
        self.initialize()
        //        self.localView = localView
        self.localCaptureView = localView
        self.remoteView = remoteView
        connectToChatRoom(callID: callID)
    }
    
    
    func initialize(){
        disconnect()
        //Initializes the ARDAppClient with the delegate assignment
        client = ARDAppClient.init(delegate: self)
        
        //RTCEAGLVideoViewDelegate provides notifications on video frame dimensions
        self.remoteView?.delegate = self
       // self.localView?.delegate = self
        settingModel.storeVideoResolutionSetting("1024x768")
    }
    
    
    //connect to Room
    func connectToChatRoom(callID: String){
        
        let callId  = callID.replacingOccurrences(of: " ", with: "")
        //client?.serverHostUrl = "https://apprtc.appspot.com"
      //  client?.serverHostUrl = "https://apprtc-222616.appspot.com"//"https://datum-apprtc.appspot.com"
        client?.connectToRoom(withId: callId, settings: settingModel, isLoopback: false)  //channelName!
    }
    
    func switchLocalRemoteView(_localView : RTCEAGLVideoView!, _remoteView: RTCEAGLVideoView!)  {
        
        
        if(remoteVideoTrack != nil){
            remoteVideoTrack?.remove(remoteView!)
        }
        
        // self.remoteVideoTrack?.remove(self.remoteView)
        
        //        self.localVideoTrack?.add(_localView)
        self.remoteVideoTrack?.add(_remoteView)
        
        //        self.localView = _localView
        self.remoteView = _remoteView
    }
    
    
    
    //disconnect from webrtc
    func disconnect(){
        if(client != nil){
            //            if(localVideoTrack != nil && localView != nil){
            //                localVideoTrack?.remove(localView!)
            //            }
            
            if(remoteVideoTrack != nil && remoteView != nil){
                remoteVideoTrack?.remove(remoteView!)
            }
            
            //            localVideoTrack = nil
            localCaptureView?.captureSession = nil
            remoteVideoTrack = nil
            captureController?.stopCapture()
            captureController = nil
            client?.disconnect()
            let session = AVAudioSession.sharedInstance()
            do {
                try session.setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [])
                try session.setActive(false)
            } catch let error as NSError {
                print("Unable to activate audio session:  \(error.localizedDescription)")
            }
        }
    }
    
 
    
    func remoteDisconnected(){
        if(remoteVideoTrack != nil){
            remoteVideoTrack?.remove(remoteView!)
        }
        remoteVideoTrack = nil
    }
    
    
    
    func appClient(_ client: ARDAppClient!, didCreateLocalCapturer localCapturer: RTCCameraVideoCapturer!) {
        print("*********\n\n ***start capture video *******\n\n")
        self.localCaptureView?.captureSession = localCapturer.captureSession
        captureController = ARDCaptureController(capturer: localCapturer, settings: settingModel)
               captureController?.startCapture()
    }
    
    func appClient(_ client: ARDAppClient!, didGetStats stats: [Any]!) {
    
    }
    
    func appClient(_ client: ARDAppClient!, didChange state: RTCIceConnectionState) {
        switch state {
        case .connected:
            print("\n\n**************ICE connected************************\n\n")
            
            if (delegate != nil){
                delegate?.appClientStatus!(client, status: .connected)
            }
            captureController?.startCapture()
            do{
                try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
            }catch let error{
                print(error.localizedDescription)
            }
            
            break
        case .disconnected:
            if (delegate != nil){
                delegate?.appClientStatus!(client, status: ARDAppClientState.disconnected)
            }
            print("disconnected ICE")
            break
            
            
        case .failed:
            print("failed ICE connection")
        default:
            break
        }
    }
    
    func videoView(_ videoView: RTCVideoRenderer, didChangeVideoSize size: CGSize) {
        
    }
    
    
    func appClient(_ client: ARDAppClient!, didChange state: ARDAppClientState) {
        switch state{
        case ARDAppClientState.connected:
            
//            if (delegate != nil){
//                delegate?.appClientStatus!(client, status: .connected)
//            }
            DDLogDebug("Client Connected")
            
            DDLogDebug("AppRTC CallConnected********************************")
            
            break
        case ARDAppClientState.connecting:
            print("\n\n**************************channel connectiong status**************************\n\n")
            if (delegate != nil){
                delegate?.appClientStatus!(client, status: ARDAppClientState.connecting)
            }
            
            DDLogDebug("AppRTC CallConnecting*****************************")
            DDLogDebug("Client Connecting")
            
            break
        case ARDAppClientState.disconnected:
            DDLogDebug("Client Disconnected")
            DDLogDebug("AppRTC CallDisconnected********************************")
                  //     remoteDisconnected()
            break
        }
    }
    
    
    
    
    func appClient(_ client: ARDAppClient!, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack!) {
        print("*********\n\n ***Local video render *******\n\n")
        DDLogDebug("*********\n\n ***local video render *******\n\n")
        //        self.localVideoTrack = localVideoTrack
        //        if let view = localView{
        //            self.localVideoTrack?.add(view)
        //        }
        if (delegate != nil){
            delegate?.appClientStatus!(client, status: ARDAppClientState.connecting)
        }
    }
    
    func appClient(_ client: ARDAppClient!, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack!) {
        print("*********\n\n ***Remote video render *******\n\n")
        DDLogDebug("*********\n\n ***Remote video render *******\n\n")
        self.remoteVideoTrack = remoteVideoTrack
        
        if let view = remoteView{
            self.remoteVideoTrack?.add(view)
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(8)) {
            self.delegate?.remoteViewReceived!(true)
        }
    }
    
    func appClient(_ client: ARDAppClient!, didError error: Error!) {
        // Handle the error
        showAlertWithMessage(error.localizedDescription)
        disconnect()
    }
    
    
    //show alertView
    func showAlertWithMessage(_ message: String){
        let alertView: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertController.Style.alert)
        let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        alertView.addAction(alertAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
    
    
    /// To enable speaker in audio and video call
    func enableSpeaker(){
        do{
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        }catch let error{
            print(error.localizedDescription)
        }
    }
    
    
    /// To switch camera in video call if front camera is active then switch to back and if back camera is active then switch to front camera
    func switchCamera(){
        captureController?.switchCamera()
    }
    
    /// To disable speaker in audio and video call
    func disableSpeaker(){
        do{
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.none)
        }catch let error{
            print(error.localizedDescription)
        }
    }
}
