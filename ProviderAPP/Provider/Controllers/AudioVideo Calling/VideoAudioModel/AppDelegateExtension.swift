//
//  AppDelegateExtension.swift
//  FlagitDriver
//
//  Created by Vengababu Maparthi on 12/03/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import CallKit
import PushKit


extension AppDelegate:PKPushRegistryDelegate {
    

    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        let pushtoken = pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined()
        UserDefaults.standard.set(pushtoken, forKey: "pushToken")
        UserDefaults.standard.synchronize()
        print(pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined())
    }

    @available(iOS 11.0, *)
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        let state = UIApplication.shared.applicationState
        if state == .active && state != .background{
            return
        }
        let callModule = CallingModule()
        let config = CXProviderConfiguration(localizedName: "")
        
        //     config.iconTemplateImageData = UIImage(named: "iPhone")!.UIImagePNGRepresentation()
        guard type == .voIP else { return }
        let dict:[String:Any] = payload.dictionaryPayload as! [String : Any]
        
        config.ringtoneSound = "ring.caf"
        config.includesCallsInRecents = false;
        config.supportsVideo = true;
        guard let callID = dict["callId"] as? String else { return}
        guard let calleeName = dict["userName"] as? String else { return}
        
        callModule.validateTheCallID(callID: callID) { (success, hasVideo) in
            if success{
                UserDefaults.standard.set(callID, forKey: "callerID")
                UserDefaults.standard.synchronize()
                MQTT.sharedInstance.subscribeChannel(withChannelName: MQTTTopics.CallTopic + callID)
                MQTT.sharedInstance.subscribeChannel(withChannelName: "Calls/" + callID)
                self.callProviderDelegate = CallProviderDelegate.init()
                self.callProviderDelegate?.callID = callID
                self.callProviderDelegate?.messageData = dict
                
                self.callProviderDelegate?.displayIncomingcall(uuid : UUID() , handel: calleeName , hasVideo: hasVideo, complition: { (error) in
                    print("error calllleeeeeeeeeeeeeeee\(String(describing: error))")
                })
            }
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
         print("didFail ......push")
    }
    
    
}

