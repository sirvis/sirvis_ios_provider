//
//  CallModuleAPI.swift
//  Live
//
//  Created by Vengababu Maparthi on 07/03/19.
//  Copyright © 2019 io.ltebean. All rights reserved.
//

import Foundation
import RxSwift

class CallingModule {
    let disposebag = DisposeBag()
    
    func createCall(param:CreateCallParam!,completionBlock:@escaping(Bool)->()) {
        Helper.showPI(message: "Creating Call..")
        let dict:[String:Any] = ["type":param.type,
                                 "room":param.roomID,
                                 "to":param.calledID,
                                 "bookingId": param.bookingID]
        
        let callingAPI = CallModuleAPI()
        callingAPI.createTheCall(params:dict)
        
        callingAPI.apiResponse.subscribe(onNext: { (response) in
            Helper.hidePI()
            if response.status == true {
                if let data = response.data["data"] as? [String:Any]{
                    if let callid = data["callId"] as? String{
                        UserDefaults.standard.set(callid, forKey: "callerID")
                        UserDefaults.standard.synchronize()
                        MQTT.sharedInstance.subscribeChannel(withChannelName: MQTTTopics.CallTopic + callid)
                        MQTT.sharedInstance.subscribeChannel(withChannelName: "Calls/" + callid)
                    }
                }
                completionBlock(true)
                
            }else{
                completionBlock(false)
            }
            
        }, onError: { (error) in
            completionBlock(false)
            Helper.hidePI()
        }, onCompleted: {
            
        }) .disposed(by: disposebag)
    }
    
    
    func rejectTheCall(param:RejectCall,completionBlock:@escaping(Bool)->()) {
        Helper.showPI(message: "Rejecting Call..")
        let dict:[String:Any] = [ "callId": param.callId,
                                  "callFrom":param.callFrom]
        
        let callingAPI = CallModuleAPI()
        callingAPI.rejectTheCall(param: dict)
        callingAPI.apiResponse.subscribe(onNext: { (response) in
            Helper.hidePI()
            if response.status == true {
                
                completionBlock(true)
                
            }else{
                completionBlock(false)
            }
            
        }, onError: { (error) in
            Helper.hidePI()
            completionBlock(false)
        }, onCompleted: {
            
        }) .disposed(by: disposebag)
    }
    
    func acceptTheCall(callID:String,completionBlock:@escaping(Bool)->()) {
        Helper.showPI(message: "Accepting Call..")
        let callingAPI = CallModuleAPI()
        callingAPI.acceptTheCall(callID: callID)
        callingAPI.apiResponse.subscribe(onNext: { (response) in
            Helper.hidePI()
            if response.status == true {
                
                completionBlock(true)
                
            }else{
                completionBlock(false)
            }
            
        }, onError: { (error) in
            Helper.hidePI()
            completionBlock(false)
        }, onCompleted: {
            
        }) .disposed(by: disposebag)
    }
    
    func validateTheCallID(callID:String,completionBlock:@escaping(Bool, Bool)->()) {
        let callingAPI = CallModuleAPI()
        callingAPI.apiResponse.subscribe(onNext: { (response) in
            
            if response.status == true {
                
              //  completionBlock(true)
                
            }else{
                //completionBlock(false)
            }
            
        }, onError: { (error) in
            Helper.hidePI()
           // completionBlock(false)
        }, onCompleted: {
            
        }) .disposed(by: disposebag)
        callingAPI.validateTheCallID(callID: callID) { (success,hasVideo) in
            if success {
                completionBlock(true,hasVideo)
            } else {
                completionBlock(false,hasVideo)
            }
            
        }
    }
}


struct CreateCallParam {
    var type = ""
    var roomID = ""
    var calledID = ""
    var bookingID = ""
    init(typeCall:String,roomID:String,userID:String, boookingID: String) {
        self.type = typeCall
        self.roomID = roomID
        self.calledID = userID
        self.bookingID = boookingID
    }
}

struct RejectCall {
    var callId = ""
    var callFrom = ""
    init(roomID:String,typeReject:String) {
        self.callId = roomID
        self.callFrom = typeReject
    }
}
