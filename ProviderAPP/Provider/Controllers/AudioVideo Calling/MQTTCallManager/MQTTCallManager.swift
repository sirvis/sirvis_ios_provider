//
//  MQTTCallManager.swift
//  Live
//
//  Created by Vengababu Maparthi on 07/03/19.
//  Copyright © 2019 io.ltebean. All rights reserved.
//

import Foundation
import Kingfisher
import AVFoundation
import RxSwift

class MQTTCallManager:NSObject {
    let disposebag = DisposeBag()
    let callingMod = CallingModule()
    
     func loadTheAudioCallView(data:[String:Any])  {
        let state = UIApplication.shared.applicationState
        if state == .active && state != .background{
            if let audiDict = data["data"] as? [String:Any]{
                if let callId = audiDict["callId"] as? String {
                    
                    if Helper.CalledID() == callId
                    {
                        return
                    }
                    self.validateCall(callid:callId,dict:audiDict)
                }
            }
        }
    }
    
    
  func validateCall(callid:String,dict:[String:Any]) {
        callingMod.validateTheCallID(callID: callid) { (success, hasVideo) in
            if success{
                self.makeTheCall(audiDict: dict)
            }
        }
    }
    
    func makeTheCall(audiDict:[String:Any]) {
        
        let window = UIApplication.shared.keyWindow!
        let audioView = IncomingAudiocallView(frame: CGRect(x:0, y:0, width: window.frame.width, height: window.frame.height))
        
        audioView.messageDict = audiDict
        audioView.playSound("ringtone", loop: 2)
        
        if let name = audiDict["userName"] as? String {
            audioView.userName.text  = name
        }
        if let callType = audiDict["type"] as? String, callType == CallingType.audio.rawValue{
            audioView.callTypelbl.text = CallingType.audio.rawValue
        }else{
            audioView.callTypelbl.text = CallingType.video.rawValue
        }
        if let imageUrl = audiDict["userImage"] as? String {
            audioView.userImageView.kf.setImage(with: URL(string: imageUrl ), placeholder: #imageLiteral(resourceName: "signup_profile_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
            })
        }
        
        if let callId = audiDict["callId"] as? String {
            MQTT.sharedInstance.subscribeChannel(withChannelName: "call/" + callId)
            MQTT.sharedInstance.subscribeChannel(withChannelName: "Calls/" + callId)
            if Helper.CalledID() == callId
            {
                return
            }
            UserDefaults.standard.set(callId, forKey: "callerID")
            UserDefaults.standard.synchronize()
        }
        audioView.playSound("end_of_call", loop: 0)
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [])
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.none)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch _ {
        }
        
        window.addSubview(audioView);
    }
}
