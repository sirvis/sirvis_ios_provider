//
//  WebViewVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 26/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: UIViewController, WKNavigationDelegate  {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var urlFrom = ""
    var titleOfController = ""
    
    var webView: WKWebView!

          override func loadView() {
              webView = WKWebView()
              webView.navigationDelegate = self
              view = webView
          }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleOfController
        if urlFrom.length != 0 {
            let trimmed = urlFrom.trimmingCharacters(in: .whitespacesAndNewlines)
            print(trimmed)
            webView.load(URLRequest(url: URL(string: trimmed)!))
            webView.allowsBackForwardNavigationGestures = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    
}
//extension WebViewVC : UIWebViewDelegate {
//
//    func webViewDidStartLoad(_ webView: UIWebView) {
//
//    }
//
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        Helper.hidePI()
//    }
//
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        Helper.hidePI()
//    }
//}
