//
//  ReceiptPageView.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 09/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class ReceiptPageView: UIView {
    @IBOutlet weak var serviceName: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var serviceAmount: UILabel!
    @IBOutlet weak var signatureImage: UIImageView!
    @IBOutlet weak var eventID: UILabel!
    @IBOutlet weak var contentView: UIView!
    var isShown:Bool = false
    var signUrl = ""
    
    private static var share: ReceiptPageView? = nil
    
    static var instance: ReceiptPageView {
        if (share == nil) {
            share = Bundle(for: self).loadNibNamed("ReceiptPage",
                                                   owner: nil,
                                                   options: nil)?.first as? ReceiptPageView
        }
        return share!
    }
    
    /// Show Receipt Page
    func show(bookingData:Accepted?) {
        serviceName.text = "Consultation Fee"
        total.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.totalAmt))
        discount.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.discount))
        
        
        
        serviceAmount.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.totalHourlyFee))
        
        
        
        eventID.text = "Job ID: " + String(describing:bookingData!.bookingId)
        self.activityIndicator.startAnimating()
        self.signatureImage.kf.setImage(with: URL(string: signUrl),
                                        placeholder:UIImage.init(named: "profile"),
                                        options: [.transition(ImageTransition.fade(1))],
                                        progressBlock: { receivedSize, totalSize in
        },
                                        completionHandler: { image, error, cacheType, imageURL in
                                            self.activityIndicator.stopAnimating()
        })
        
        
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
    }
    
    /// Hide Receipt Page
    func hide() {
        
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            ReceiptPageView.share = nil
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
    }

    @IBAction func closeTheReceipt(_ sender: Any) {
        self.hide()
    }
}
