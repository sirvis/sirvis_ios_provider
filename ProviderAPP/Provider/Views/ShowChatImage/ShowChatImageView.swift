//
//  ShowChatImageView.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 15/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class ShowChatImageView: UIView {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    private static var share: ShowChatImageView? = nil
    var isShown:Bool = false
    
    static var instance: ShowChatImageView {
        if (share == nil) {
            share = Bundle(for: self).loadNibNamed("ShowChatImage",
                                                   owner: nil,
                                                   options: nil)?.first as? ShowChatImageView
        }
        return share!
    }
    
    func show(image:String) {
        self.scrollView.delegate = self
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 3.0
        
        
        self.imageView.kf.setImage(with: URL(string: image),
                                          placeholder:UIImage.init(named: "signup_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
        },
                                          completionHandler: { image, error, cacheType, imageURL in
        })
        
        self.scrollView.contentSize = self.imageView.frame.size

        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
        
    }
    @IBAction func hidetheImageView(_ sender: Any) {
        hide()
    }
    
    /// Hide Receipt Page
    func hide() {
        
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            ShowChatImageView.share = nil
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
    }
    
    //Preview view scrollview's zoom calculation
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = self.imageView.frame.size.height / scale
        zoomRect.size.width  = self.imageView.frame.size.width  / scale
        let newCenter = self.imageView.convert(center, from: self.scrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    @IBAction func doubleTapOnImage(_ sender: Any) {
        if self.scrollView.zoomScale == 1 {
            self.scrollView.zoom(to: zoomRectForScale(scale: self.scrollView.maximumZoomScale, center: (sender as AnyObject).location(in: (sender as AnyObject).view)), animated: true)
        } else {
            self.scrollView.setZoomScale(1, animated: true)
        }
    }
}

extension ShowChatImageView:UIScrollViewDelegate{
    
    //Preview view scrollview zooming
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
}
