//
//  HistoryDBManager.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 20/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation



class HistoryDBManager:NSObject {
    var historyDB = Couchbase.sharedInstance
    
    /// retrivng the saved month wise data
    func gethistoryData(forUserID: String, completion: @escaping ([[String:Any]]) -> ()) {
        if let doc = historyDB.database.existingDocument(withID:forUserID){
            let properties = doc.properties
            if let dict = properties!["historyDict"] as? [[String:Any]]{
                completion(dict)
            }
        }
    }
    
    //// storing the month wise history data
    func saveThehistory(forUserID: String, dict:[String:Any]) {
        if let doc = historyDB.database.existingDocument(withID:forUserID){
            var properties = doc.properties
            properties!["historyDict"]! = dict["historyDict"] as Any
            try! doc.putProperties(properties!)
        }else{
            historyDB.createDocumentWithID(sendDocumentID: forUserID, data: dict)
        }
    }
    
    
    func getWeeksData(forUserID: String, completion: @escaping ([[String:Any]]) -> ()) {
        if let doc = historyDB.database.existingDocument(withID:forUserID){
            let properties = doc.properties
            if let dict = properties!["weeksDict"] as? [[String:Any]]{
                completion(dict)
            }
        }
    }
    
    func saveTheWeeksData(forUserID: String, dict:[String:Any]) {
        if let doc = historyDB.database.existingDocument(withID:forUserID){
            var properties = doc.properties
            properties!["weeksDict"]! = dict["weeksDict"] as Any
            try! doc.putProperties(properties!)
        }else{
            historyDB.createDocumentWithID(sendDocumentID: forUserID, data: dict)
        }
    }
}

