//
//  ChatDBManager.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 09/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


class ChatManager:NSObject {
    var chatDB = Couchbase.sharedInstance
    
    func downloadAllMessages(forUserID: String, completion: @escaping ([[String:Any]]) -> ()) {
        if let doc = chatDB.database.existingDocument(withID:forUserID){
            let properties = doc.properties
            if let dict = properties!["chatDict"] as? [[String:Any]]{
                completion(dict)
            }
        }
    }
    
    func saveTheMessage(forUserID: String, dict:[String:Any]) {
        if let doc = chatDB.database.existingDocument(withID:forUserID){
            var properties = doc.properties
            properties!["chatDict"]! = dict["chatDict"] as Any
            try! doc.putProperties(properties!)
        }else{
            chatDB.createDocumentWithID(sendDocumentID: forUserID, data: dict)
        }
    }
    
    func saveTheSingleMessage(forUserID: String, dict:[String:Any]) {
        var chatDict = [[String:Any]]()
        if let doc = chatDB.database.existingDocument(withID:forUserID){
            var properties = doc.properties
            chatDict = properties!["chatDict"] as! [[String:Any]]
            chatDict.append(dict)
            properties!["chatDict"] = chatDict
            try! doc.putProperties(properties!)
        }else{
            chatDict.append(dict)
            let finalDict:[String:Any] = ["chatDict":chatDict]
            chatDB.createDocumentWithID(sendDocumentID: forUserID, data: finalDict)
        }
    }
}
