//
//  ScheduleDBManager.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 20/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


class ScheduleDBManager:NSObject {
    var scheduleDB = Couchbase.sharedInstance
    
    /// retrivng the saved month wise data
    func getMonthWiseScheduleData(forUserID: String, completion: @escaping ([[String:Any]]) -> ()) {
        if let doc = scheduleDB.database.existingDocument(withID:forUserID){
            let properties = doc.properties
            if let dict = properties!["scheduleDict"] as? [[String:Any]]{
                completion(dict)
            }
        }
    }
    
    //// storing the month wise schedule data
    func saveTheScheduleDataMonthWise(forUserID: String, dict:[String:Any]) {
        if let doc = scheduleDB.database.existingDocument(withID:forUserID){
            var properties = doc.properties
            properties!["scheduleDict"]! = dict["scheduleDict"] as Any
            try! doc.putProperties(properties!)
        }else{
            scheduleDB.createDocumentWithID(sendDocumentID: forUserID, data: dict)
        }
    }
    

    func getScheduleData(forUserID: String, completion: @escaping ([String:Any]) -> ()) {
        if let doc = scheduleDB.database.existingDocument(withID:forUserID){
            let properties = doc.properties
            if let dict = properties!["scheduleDict"] as? [String:Any]{
                completion(dict)
            }
        }
    }
    
    func saveTheScheduleData(forUserID: String, dict:[String:Any]) {
        if let doc = scheduleDB.database.existingDocument(withID:forUserID){
            var properties = doc.properties
            properties!["scheduleDict"]! = dict["scheduleDict"] as Any
            try! doc.putProperties(properties!)
        }else{
            scheduleDB.createDocumentWithID(sendDocumentID: forUserID, data: dict)
        }
    }
}

