//
//  CancelDBManager.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 20/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
class CancelDBManager:NSObject {
    var cancelDB = Couchbase.sharedInstance
    
    /// retrivng the cancel reasons
    func getcancelData(forUserID: String, completion: @escaping ([[String:Any]]) -> ()) {
        if let doc = cancelDB.database.existingDocument(withID:forUserID){
            let properties = doc.properties
            if let dict = properties!["cancelDict"] as? [[String:Any]]{
                completion(dict)
            }
        }
    }
    
    //// storing the cancel data
    func saveTheCancelReasons(forUserID: String, dict:[String:Any]) {
        if let doc = cancelDB.database.existingDocument(withID:forUserID){
            var properties = doc.properties
            properties!["cancelDict"]! = dict["cancelDict"] as Any
            try! doc.putProperties(properties!)
        }else{
            cancelDB.createDocumentWithID(sendDocumentID: forUserID, data: dict)
        }
    }
}
