//
//  DateFormatter.swift
//  Notary
//
//  Created by 3Embed on 28/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    class func initTimeZoneDateFormat() -> DateFormatter  {
        
        let dateFormat = DateFormatter()
        dateFormat.timeZone = Helper.getTimeZoneFromCurrentLoaction()//Adding current timezone 
        
        return dateFormat
    }
}
