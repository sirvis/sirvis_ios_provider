
//
//  File.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 06/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import EventKit


class ReminderModel:NSObject{
    
    var startDate:Int64 = 0
    var enddate:Int64   = 0
    var latit     = 0.00
    var logit     = 0.00
    var bookingID:Int64 = 0
    var address   = ""
    
    func eventReminder(data:ReminderModel?,completion:@escaping(String) ->()){
        let ud = UserDefaults.standard
        
        if (ud.value(forKey: "schedBook") != nil ){
            let lastBid:Int64 = ud.value(forKey: "schedBook") as! Int64
            if data!.bookingID   == lastBid{
                return
            }
        }
        
        ud.set(data!.bookingID, forKey: "schedBook")
        ud.synchronize()
        
        
        let eventStore = EKEventStore()
        eventStore.requestAccess(to: .event, completion: {
            granted, error in
            if (granted) && (error == nil) {
                print("granted \(granted)")
                print("error  \(String(describing: error))")
                let event:EKEvent = EKEvent(eventStore: eventStore)
                event.title = "Scheduled Booking"
                event.startDate =  NSDate(timeIntervalSince1970:TimeInterval((data?.startDate)!)) as Date
                var endDate = data?.enddate
                if endDate == 0 {
                    endDate = data?.startDate
                }
               event.endDate =  NSDate(timeIntervalSince1970:TimeInterval((data?.startDate)!)) as Date
                event.endDate =  NSDate(timeIntervalSince1970:TimeInterval((data?.enddate)!)) as Date
                let location = CLLocation(latitude: (data?.latit)!, longitude: (data?.logit)!)
                let structuredLocation = EKStructuredLocation(title: data!.address)
                structuredLocation.geoLocation = location
                event.structuredLocation = structuredLocation
                event.notes = "Your have booking id " + String(describing:data!.bookingID)  + " to complete"
                let alarms = [EKAlarm(relativeOffset: -60.0 * 60.0 * 1)]
                event.alarms = alarms
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                    print("events added with dates:")
                    completion(eventStore.eventStoreIdentifier)
                } catch let e as NSError {
                    print(e.description)
                    return
                }
            }
        })
    }
    
    
    func removeReminder(_ startDate: Int64 , endDate:Int64) {
        let eventStore = EKEventStore()
        
        let predicate = eventStore.predicateForReminders(in: [])
        eventStore.fetchReminders(matching: predicate) { reminders in
            for _ in reminders! {
            }}
        
        
        // What about Calendar entries?
        let startDate = NSDate(timeIntervalSince1970:TimeInterval(startDate))
        let endDate = NSDate(timeIntervalSince1970:TimeInterval(endDate))
        
        let predicate2 = eventStore.predicateForEvents(withStart: startDate as Date as Date, end: endDate as Date, calendars: nil)
        
        let eV = eventStore.events(matching: predicate2) as [EKEvent]?
        
        if eV != nil {
            for i in eV! {
                // Uncomment if you want to delete
                try? eventStore.remove(i , span: .thisEvent, commit: true)                }
        }
    }
        
}
