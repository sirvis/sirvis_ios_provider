//
//  OneSkyHelper.swift
//  ServiceGenie
//
//  Created by Rahul Sharma on 26/04/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation
class OneSkyHelper : NSObject {
    
class func getLanguageCode() -> String {
    
    //        return "en"//"1"
    
    let currentLanguage = (Locale.preferredLanguages.first)!
    let languageDict:[String:String] = Locale.components(fromIdentifier: currentLanguage)
    let languageCode = languageDict["kCFLocaleLanguageCodeKey"]
    
    return languageCode!
}
}
