//
//  TimeFormats.swift
//  DayRunner
//
//  Created by Rahul Sharma on 05/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class TimeFormats: NSObject {
    
    /********************************************************/
    //MARK: - TimeFormats
    /********************************************************/
    
    /// Current Date and Time
    static var currentDateTime: String {
        let now = Date.dateWithDifferentTimeInterval()
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        let indianLocale = NSLocale(localeIdentifier: "en_US")
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: now)
    }
    
    /// Current Date and Time
    static var currentDateWithoutTime: String {
        
        let now = Date.dateWithDifferentTimeInterval()
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        let indianLocale = NSLocale(localeIdentifier: "en_US")
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "dd MMM yyyy"
        return dateFormatter.string(from: now)
    }
    
    class func timeInterval(withStartedTime jobStartedAt: String) -> String {
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let fromDate: Date? = dateFormatter.date(from: jobStartedAt)
        let toDate = Date()
        let gregorianCalendar = Calendar(identifier: .gregorian)
        let components : DateComponents = gregorianCalendar.dateComponents([.hour,.minute,.second], from: fromDate!
            , to: toDate)
        let hour = Int((components.hour)!)
        let min = Int((components.minute)!)
        let sec = Int((components.second)!)
        return String(format: "%02d:%02d:%02d", hour, min, sec)
    }
    
    //currentdateandtime
    
    class func changeDateFormate(_ originalDate: String) -> String {
        
        guard originalDate.isEmpty == false else {
            return ""
        }
        
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date: Date? = dateFormatter.date(from: originalDate)
        dateFormatter.dateFormat = "MMM dd, HH:mm a" //22-Nov-2012
        let formattedDateString: String = dateFormatter.string(from: date!)
        
        return formattedDateString
    }
    
    class func changeDateFormate(timeStamp: String) -> String {
        
        guard timeStamp.isEmpty == false else {
            return ""
        }
        
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp)!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "dd/MM/yyyy , hh:mm a" //22-Nov-2012
        let formattedDateString: String = dateFormatter.string(from: date)
        
        return formattedDateString
    }
    
    class func changeDateFormateSelectSl(timeStamp: String) -> String {
        
        guard timeStamp.isEmpty == false else {
            return ""
        }
        
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp)!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "yyyy-MM-dd , hh:mm:ss" //22-Nov-2012
        let formattedDateString: String = dateFormatter.string(from: date)
        
        return formattedDateString
    }
    
    class func changeDateFormateSelectSl24(timeStamp: String) -> String {
        
        guard timeStamp.isEmpty == false else {
            return ""
        }
        // yyyy-MM-dd
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp)!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "dd/MM/yyyy , hh:mm a" //22-Nov-2012
        let formattedDateString: String = dateFormatter.string(from: date)
        
        return formattedDateString
    }
    
    
    class func changeDateFormateFor24Hours(timeStampN: String) -> String {
        
        guard timeStampN.isEmpty == false else {
            return ""
        }
        
        let date = Date(timeIntervalSince1970: TimeInterval(timeStampN)!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "dd/MM/yyyy , HH:MM:SS" //22-Nov-2012
        let formattedDateString: String = dateFormatter.string(from: date)
        
        return formattedDateString
    }
    
    class func changeDateFormateFor24HoursSelectSlot(timeStampN: String) -> String {
        
        guard timeStampN.isEmpty == false else {
            return ""
        }
        
        let date = Date(timeIntervalSince1970: TimeInterval(timeStampN)!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "yyyy-MM-dd , HH:MM:SS" //22-Nov-2012
        let formattedDateString: String = dateFormatter.string(from: date)
        
        return formattedDateString
    }
    
    class func changeDateFormateFor24HoursSelectInCa(timeStampN: String) -> String {
        
        guard timeStampN.isEmpty == false else {
            return ""
        }
        
        let date = Date(timeIntervalSince1970: TimeInterval(timeStampN)!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "yyyy-MM-dd , hh:mm a" //22-Nov-2012
        let formattedDateString: String = dateFormatter.string(from: date)
        
        return formattedDateString
    }
    
    
    class func changeDateFormateForCalender(timeStamp: String) -> String {
        
        guard timeStamp.isEmpty == false else {
            return ""
        }
        
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp)!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "dd MMM yyyy" //22-Nov-2012
        let formattedDateString: String = dateFormatter.string(from: date)
        
        return formattedDateString
    }
    
    class func getDateObjForCalender(timeStamp: String) -> Date {
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp)!)
        return date
    }
    
    
    /// Current time stamp
    static var currentTimeStamp: String {
        return String(format: "%0.0f", Date.dateWithDifferentTimeInterval().timeIntervalSince1970 * 1000)
    }
    
    /// Current time Stamp in Int64 format
    static var currentTimeStampInt64: Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    class func roundDate(_ mydate: Date) -> Date {
        
        var newDate = mydate
        
        // Get the nearest 5 minute block
        let time: DateComponents? = Calendar.current.dateComponents([.hour, .minute], from: mydate)
        let minutes: Int? = time?.minute
        let remain: Int = minutes! % 15
        
        // Add the remainder of time to the date to round it up evenly
        newDate = newDate.addingTimeInterval(TimeInterval(60 * (15 - remain)))
        return newDate
    }
    
    class func getCreationTimeInt(expiryTimeStamp : Int64) -> Int64{
        let distanceTime = Date.dateWithDifferentTimeInterval().timeIntervalSince1970
        return expiryTimeStamp - Int64(distanceTime )
        
    }
    
    class func getstartTimeInt(expiryTimeStamp : Int64) -> Int64{
        let distanceTime = Date.dateWithDifferentTimeInterval().timeIntervalSince1970
        return  Int64(distanceTime) - expiryTimeStamp 
        
    }
    
    

    
}

extension Date {
    
    /// Returns a Date with the specified days added to the one it is called with
    func add(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        var targetDay: Date
        targetDay = Calendar.current.date(byAdding: .year, value: years, to: self)!
        targetDay = Calendar.current.date(byAdding: .month, value: months, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .day, value: days, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .hour, value: hours, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .minute, value: minutes, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .second, value: seconds, to: targetDay)!
        return targetDay
    }
    
    /// Returns a Date with the specified days subtracted from the one it is called with
    func subtract(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        let inverseYears = -1 * years
        let inverseMonths = -1 * months
        let inverseDays = -1 * days
        let inverseHours = -1 * hours
        let inverseMinutes = -1 * minutes
        let inverseSeconds = -1 * seconds
        return add(years: inverseYears, months: inverseMonths, days: inverseDays, hours: inverseHours, minutes: inverseMinutes, seconds: inverseSeconds)
    }
    
}

