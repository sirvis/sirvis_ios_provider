//
//  SeguesList.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 21/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit


struct segues {
    static let vehicleDetails   = "toEnterVehicleDetails" as String?
    static let countryPicker    = "toCountryCode" as String?
}
