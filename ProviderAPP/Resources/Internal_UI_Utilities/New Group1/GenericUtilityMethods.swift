//
//  GenericUtilityMethods.swift
//  LSP
//
//  Created by Rajan Singh on 04/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Stripe
import GoogleMaps
import GooglePlaces
import EventKit

class GenericUtilityMethods: NSObject {

    static var newAlertWindow: UIWindow? = nil
   
    
    /// eddit navigation bar
    ///
    /// - Parameter navigationController: navigation Controller
    class func editNavigationBar(_ navigationController: UINavigationController)
    {
        navigationController.navigationBar.backIndicatorImage = UIImage(named: "Navigation_back_image")
        navigationController.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "Navigation_back_image")
        navigationController.navigationBar.backgroundColor = UIColor.white
        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)]
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        navigationController.navigationBar.shadowImage = UIImage()
//        navigationController.navigationBar.layer.shadowColor = UIColor.black.cgColor
//        navigationController.navigationBar.layer.shadowOffset = CGSize(width: 3, height: 3)
//        navigationController.navigationBar.layer.shadowRadius = 2.0
//        navigationController.navigationBar.layer.shadowOpacity = 0.08
    }
    
    class func minutesToHoursMinutes (minutes : Int) -> (hours : Int , leftMinutes : Int) {
        return (minutes / 60, (minutes % 60))
    }
    
    class func setStatusBarBackgroundColor(_ color: UIColor) {
        if #available(iOS 13, *)
        {
            let statusBar = UIView(frame: (UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame)!)
            statusBar.backgroundColor = UIColor.systemBackground
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        }else{
            
            guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
            //   UIApplication.shared.va
            statusBar.backgroundColor = color
        }
    }
  
    

    
    
    /// Method to measure height of the label depending on content
    ///
    /// - Parameters:
    ///   - label: input Label
    ///   - width: width of the input label
    /// - Returns: estimated height of the label
    class func measureHeightLabel(_ label:UILabel, width:CGFloat) -> CGFloat {
        
        let constrainedSize = CGSize(width: width, height: 9999)
        let attributesDictionary: [AnyHashable: Any] = [
            NSAttributedString.Key.font : UIFont(name: label.font.fontName, size: label.font.pointSize)!
        ]
        
        let string = NSMutableAttributedString(string: label.text!, attributes: attributesDictionary as! [NSAttributedString.Key : Any])
      
        let requiredHeight: CGRect = string.boundingRect(with: constrainedSize, options: .usesLineFragmentOrigin, context: nil)
        var newFrame: CGRect = label.frame
        newFrame.size.height = requiredHeight.size.height
        return newFrame.size.height
    }
    

    
    /// Get Random String of given Length
    ///
    /// - Parameter length: length of the string
    /// - Returns: random string
    class func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    
    /// Show alert
    ///
    /// - Parameters:
    ///   - alert: alert Title
    ///   - message: alert Message
    class func showAlert(alert: String, message:String) {
        
            hidePI()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            
            let alertController = UIAlertController(title: alert, message: message, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Ok", style: .default) { (true) in
    
                newAlertWindow?.resignKey()
                newAlertWindow?.removeFromSuperview()
                newAlertWindow = nil
            }
            
            alertController.addAction(okAction)
            
            if newAlertWindow == nil {
                newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
                newAlertWindow?.rootViewController = UIViewController()
                //newAlertWindow?.windowLevel = UIWindow.Level.alert + 1
                newAlertWindow?.makeKeyAndVisible()
                newAlertWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
            }else {
                newAlertWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
            }
        }
    }
    

  
    
    // Show with Your Message
    class func showPI(_message:  String) {
        let activityData = ActivityData(size: CGSize(width: 30,height: 30),
                                        message: _message,
                                        type: NVActivityIndicatorType(rawValue: 22),
                                        color: UIColor.white,
                                        padding: nil,
                                        displayTimeThreshold: nil,
                                        minimumDisplayTime: nil)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        let when = DispatchTime.now() + 180
        DispatchQueue.main.asyncAfter(deadline: when){
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
    

    
    // Hide
    class func hidePI() {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        
    }
    
    /// Resize image acording to give width and height
    ///
    /// - Parameters:
    ///   - image: image
    ///   - targetSize: Resized
    ///   - scale: scale
    /// - Returns: returns the resized image
    class func resizeImage(_ image: UIImage,_ targetSize: CGSize,_ scale: Float) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, CGFloat(scale))
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
 
    
    /// get imag eof the card acording to its type
    ///
    /// - Parameter type: pass the type of the card
    /// - Returns: the image of the card
    class func getCardImage(type: String) -> UIImage {
        switch type {
        case "Visa":
            return STPImageLibrary.brandImage(for: STPCardBrand.visa)
            
        case "MasterCard":
            return STPImageLibrary.brandImage(for: STPCardBrand.masterCard)
            
        case "Discover":
            return STPImageLibrary.brandImage(for: STPCardBrand.discover)
            
        case "AmericanExpress":
            return STPImageLibrary.brandImage(for: STPCardBrand.amex)
            
        case "JCB":
            return STPImageLibrary.brandImage(for: STPCardBrand.JCB)
            
        case "DinnersClub":
            return STPImageLibrary.brandImage(for: STPCardBrand.dinersClub)
            
        default:
            return STPImageLibrary.brandImage(for: STPCardBrand.unknown)
        }
    }
    
    /// calcullate the height of the label
    ///
    /// - Parameters:
    ///   - text: string to be added in label
    ///   - font: font of the label
    ///   - width: width of the label
    /// - Returns: CgFloat Height of the label
    class func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    /// calcullate the width Of the label
    ///
    /// - Parameters:
    ///   - textToEvaluate: text whoes width we have to calcullate
    ///   - font: font of the label
    /// - Returns: CGfloat width of the label
    class func evaluateStringWidth (textToEvaluate: String , font: UIFont) -> CGFloat{
        let attributes = NSDictionary(object: font, forKey:NSAttributedString.Key.font as NSCopying)
        let sizeOfText = textToEvaluate.size(withAttributes: (attributes as! [NSAttributedString.Key : AnyObject]))
        
        return sizeOfText.width
    }
    
    /// get radians from degree
    ///
    /// - Parameter degrees: degree as Doble
    /// - Returns: return CGFloat
    class func getRadian(degrees: Double) -> CGFloat {
        return (CGFloat(degrees * .pi / degrees))
    }
    
 
  
    
    /// Method to convert meter to miles
    ///
    /// - Parameter meter: input meter value
    /// - Returns: output miles value
    class func convertMeter(toMiles meter: Double) -> Double {
        
        return meter / 1600.0
    }
    
    
    /// Method to convert meter to kiloMeter
    ///
    /// - Parameter meter: input meter value
    /// - Returns: output kiloMeter value
    class func convertMeter(toKiloMeter meter: Double) -> Double {
        
        return meter / 1000.0
    }
    
    class func getTheScheduleViewTimeFormat(timeStamp:Int64) -> String{
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp))
        let df = DateFormatter()
        df.dateFormat = "hh:mm a"
        let dateString = df.string(from: date as Date)
        return dateString
    }
    
    class func getTheDateFromTimeStamp(timeStamp:Int64) -> String{
        let date = Date(timeIntervalSince1970:TimeInterval(timeStamp))
        
        let df = DateFormatter.initTimeZoneDateFormat()
        
        df.dateFormat = "dd/MM/yyyy | hh:mm a"
        let dateString = df.string(from: date)
        return dateString
    }
    
    class func getTheOnlyDateFromTimeStamp(timeStamp:Int64) -> String{
        let date = NSDate(timeIntervalSince1970:TimeInterval((timeStamp)))
        let df = DateFormatter()
        df.dateFormat = "dd MMMM yyyy"
        let dateString = df.string(from: date as Date)
        return dateString
    }
    

    
    class func getIPAddressNew() {
//        var address: String?
//        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
//        if getifaddrs(&ifaddr) == 0 {
//            var ptr = ifaddr
//            while ptr != nil {
//                defer { ptr = ptr?.pointee.ifa_next }
//
//                let interface = ptr?.pointee
//                let addrFamily = interface?.ifa_addr.pointee.sa_family
//                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
//
//                    if let name: String = String(cString: (interface?.ifa_name)!), name == "en0" {
//                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
//                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
//                        address = String(cString: hostname)
//                    }
//                }
//            }
//            freeifaddrs(ifaddr)
//        }
//
    }
    
    
    class func getTimeZoneFromLoaction(latitude:Double,
                                       longitude:Double) -> TimeZone? {
        
        
        let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
        
        return TimezoneMapper.latLngToTimezone(location)
    }
    
    class func convertDateStringToDate(longDate: String) -> Date{
        
        /* INPUT: longDate = "2017-01-27T05:00:00.000Z" "2018-11-04 20:0:00"
         * OUTPUT: "1/26/17"
         * date_format_you_want_in_string from
         * http://userguide.icu-project.org/formatparse/datetime
         */
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: longDate)
        
        if date != nil {
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            _ = formatter.string(from: date!)
            return date!
        } else {
            return date ?? Date()
        }
    }
    //Minimum Date Fix
    class func converFormettedDateString1(longDate:String) -> String {
        var fullNameArr = longDate.split{$0 == ":"}.map(String.init)
        var date: String = fullNameArr[0]
        var time: String? = fullNameArr.count > 1 ? fullNameArr[1] : nil
        if time != nil {
            let st = Int(time!)
            if st! < 40 && st! > 0 {
                time = "30"
            }
            else{
                time = "00"
                date = "\((Int(date)! + 1))"
            }
            return "\(date):\(time!):00"
        }
        
        return longDate
    }
    //For date Fromate in Receipt should be in dd mmm yyyy
    class func converFormettedDateString(longDate:String) -> String {
        var fullNameArr = longDate.split{$0 == ","}.map(String.init)
        var date: String = fullNameArr[0]
        let time: String? = fullNameArr.count > 1 ? fullNameArr[1] : nil
        var dateArr = date.split{$0 == "/"}.map(String.init)
        if dateArr.count > 0 {
            let day = dateArr[0]
            let month = dateArr[1]
            let year = dateArr[2]
            let d1 = GenericUtilityMethods.monthtoMontName(st:month)
            return ("\(day)-\(d1)-\(year),\(time ?? "")")
        }
        return ""
    }
     class func monthtoMontName(st:String)->String{
        let no = Int(st)
        if no == 1{
            return "JAN"
        }
        else if no == 2 {
            return "FEB"
        }
        else if no == 3 {
            return "March"
        }
        else if no == 4 {
            return "April"
        }
        else if no == 5 {
            return "May"
        }
        else if no == 6 {
            return "June"
        }
        else if no == 7 {
            return "July"
        }
        else if no == 8 {
            return "AUG"
        }
        else if no == 9 {
            return "SEPT"
        }
        else if no == 10 {
            return "OCT"
        }
        else if no == 11 {
            return "NOV"
        }
        else if no == 12 {
            return "DEC"
        }
        return ""
    }
    class func convertDateStringToDateTimeSlot(longDate: String) -> Date{
        
        /* INPUT: longDate = "2017-01-27T05:00:00.000Z" "2018-11-04 20:0:00"
         * OUTPUT: "1/26/17"
         * date_format_you_want_in_string from
         * http://userguide.icu-project.org/formatparse/datetime
         */
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let date = dateFormatter.date(from: longDate)
        
        if date != nil {
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            let dateShort = formatter.string(from: date!)
            return date!
        } else {
            return date!
        }
    }
    
    
    class func  checkCallGoingOn() -> Bool{
        if  let isCallgoingOn = UserDefaults.standard.object(forKey:"iscallgoingOn") as? Bool {
            return isCallgoingOn
        }
        return false
    }
    
    
    class func setCallOnGoingStatus(value: Bool) {
        UserDefaults.standard.set(value, forKey: "iscallgoingOn")
        UserDefaults.standard.synchronize()
    }
    
    // for number masking
    class func hideMidChars(_ value: String) -> String {
        return String(value.enumerated().map { index, char in
            return [0, 1, value.count - 1, value.count - 2].contains(index) ? char : "*"
        })
    }
    
    
    /// Method to get current view controller showing on screen
    ///
    /// - Returns: current view controller that showing on screen
    class func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
        
        var rootVC = rootViewController
        if rootVC == nil {
            rootVC = UIApplication.shared.keyWindow?.rootViewController
        }
        
        if rootVC?.presentedViewController == nil {
            return rootVC
        }
        
        if let presented = rootVC?.presentedViewController {
            if presented.isKind(of: UINavigationController.self) {
                let navigationController = presented as! UINavigationController
                return navigationController.viewControllers.last!
            }
            
            if presented.isKind(of: UITabBarController.self) {
                let tabBarController = presented as! UITabBarController
                return tabBarController.selectedViewController!
            }
            
            return getVisibleViewController(presented)
        }
        return nil
    }
    
    
    class func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
    
    
    class func getWeek(today:String)-> Int {
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale
        formatter.dateFormat = "yyyy-MM-dd"
        let todayDate = formatter.date(from: today)
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let myComponents = myCalendar.components(.weekday, from: todayDate!)
        let weekDay = myComponents.weekday
        return weekDay!
    }
    
    
}
