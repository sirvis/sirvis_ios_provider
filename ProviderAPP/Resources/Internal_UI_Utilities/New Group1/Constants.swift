//
//  Constants.swift
//  Dayrunner Driver
//
//  Created by Rahul Sharma on 24/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit


struct AppDetails{
    static let appName             = "Sirvis Provider"
    static let websiteName         =  "https://sirv.is"
    static let facebookurl         = "https://www.facebook.com/sirvissa"
    static let appstoreLink        = "https://itunes.apple.com/us/app/"
    static let termsNCondtions     = "https://admin.sirv.is/supportText/provider/en_termsAndConditions.php"
    static let privacyPolicy       = " https://admin.sirv.is/supportText/provider/en_privacyPolicy.php"
    static let legal               = "http://45.77.190.140/iServe2.0/iserve-livem-php-central-admin/legal.php"
    static let youtubeChannelLiveM = "https://www.youtube.com/channel/UCE6Njn5YGt5D2YI"
}

struct OneSkyKeys {
    static let APIKey    = "ep6JQNT06AwJ5ttfS7KeLjCWw0FPDKoH"
    static let SecretKey = "pBkyFAQjktRGME6UkzNFwNIxlmyTtMeI"
    static let ProjectID = "137978"
}

struct API
{
    static let payRollWebView               = "https://admin.sirv.is/payrollform/?" + Utility.userId
    
    static let groupIdentifier              = "group.com.sirvis.provider.push"
    
    private static let BASE_IP             =  "https://api.sirv.is/" //
    
    //static let BASE_URL                     =  "https://api.service-genie.xyz/" //
    
    static let BASE_URL                     = "https://api.sirv.is/"
    struct CouchDBName {
        static let dataBaseName = "servicegenieprovider"
    }
    
    struct LiveChat {
        static let Licence_url =    "https://cdn.livechatinc.com/app/mobile/urls.json"
        static let  Licence    =    "4711811"
    }
    
    
    struct METHOD {
        
        static let CARDDETAILS              = "provider/card"
  
        static let PROVIDER                 = "provider/"
        
        static let BOOKINGCHAT              = PROVIDER + "booking/chat"
        
        static let TRANSACTION              = PROVIDER + "wallet/transction"
        
        static let PAYMENTSETT              = PROVIDER + "paymentsettings"
        
        static let RECHARGE_WALLET          = PROVIDER + "rechargeWallet"
        
        static let CUSTREVIEWS              = PROVIDER + "customer/reviewAndRating/"
        
        static let LOGIN                    = PROVIDER + "signIn"
        
        static let SIGNUP                   = PROVIDER + "signUp"
        
        static let SCHEDULEBOOKINGDATA      = PROVIDER  + "booking"
        
        static let VEHICLEDEFAULT           = PROVIDER  + "vehicleDefault"
        
        static let EMAILVALIDATE            = PROVIDER  + "emailValidation"
        
        static let UPDATENEWEMAIL           = PROVIDER  + "email"
        
        static let PHONEVALIDATE            = PROVIDER  + "phoneValidation"
        
        static let UPDATEDRIVERSTATUS       = PROVIDER  + "status"
        
        static let RESPONDAPPT              = PROVIDER + "respondToRequest"
        
        static let ASSIGNEDTRIPS            = PROVIDER + "booking"
        static let BOOKINGS            = PROVIDER + "bookings"
        
        static let PROFILEDATA              = PROVIDER + "profile/me"
        
        static let ACKNOWLEDGEBK            = PROVIDER + "bookingAck"
        
        static let UPDATETIMERSTATUS        = PROVIDER + "bookingTimer"
        
        static let UPDATEBOOKINGSTATUS      = PROVIDER + "bookingStatus"
        
        static let ACCEPTREJECTRESPONSE     = PROVIDER + "bookingResponse"
        
        static let HISTORYSERVICE           = PROVIDER  + "trips"
        
        static let getCities                = PROVIDER + "city"
        
        static let generes                  = PROVIDER + "serviceCategories"
        
        static let UPDATEPROFILE            = PROVIDER + "profile/me"
        
       static let UPDATEDOCUMENTS            = PROVIDER + "documents"
        
        static let CANCELBOOKING            = PROVIDER + "cancelBooking"
        
        static let LOCATION                 = PROVIDER + "location"
        
        static let LOCATIONLOGS             = PROVIDER + "locationLogs"
        
        static let ADDBANKSTRIPE            = "connectAccount"  //PROVIDER + "stripe/me"
        
        static let GETBANKSTRIPEDATA        = "connectAccount"  //PROVIDER + "stripe/me"
        
        static let ADDBANKAFTERSTRIPE       = "externalAccount" //PROVIDER + "stripe/bank/me"
        
        static let DELETEBANK               = "externalAccount" //PROVIDER + "stripe/bank/me"
        
        static let DEFAULTBANK              = "externalAccount" //PROVIDER + "stripe/bankdefault/me"
        
        static let OFFLINELATLONGS          = PROVIDER + "locationLogs"
        
        static let VERIFYPHONESIGNUP        = PROVIDER + "verifyPhoneNumber"
        
        static let BOOKINGSHISTORY          = PROVIDER + "bookingHistory"
        
        static let WEEKSHISTORY            = PROVIDER + "bookingHistoryByWeek"
        
        static let CHANGEPASSWORD           = PROVIDER + "password/me"
        
        static let SCHEDULEAPI              = PROVIDER + "schedule"
        
        static let SignupGetOTP             = PROVIDER + "signupOtp"
        
        static let ADDRESS                  = PROVIDER + "address"
        
        static let getOperators             = PROVIDER + "operators"
        
        static let UPDATEPHONE              = PROVIDER + "phoneNumber"
        
        static let vehicleTypes             = PROVIDER + "vehicleType"
        
        static let verifyOTP                = PROVIDER + "verifyVerificationCode"
        
        static let LOGOUT                   = PROVIDER + "logout"
        
        static let FORGOTPASSWORD           = PROVIDER + "forgotPassword"
        
        static let UPDATEPASSWORD           = PROVIDER + "password"
        
        static let CANCELREASONS            = PROVIDER + "cancelReasons"
        
        static let CONFIG                   = PROVIDER + "config"
        
        static let SUPPORT                  = PROVIDER + "support/2"
        
        static let REFERRALCODE             = PROVIDER + "referralCodeValidation"
        
        static let SUBMITREVIEW             = PROVIDER + "reviewAndRating"
        
        static let NEWACCESSTOKEN           = PROVIDER + "accessToken"
        
        static let RESENDOTP                = PROVIDER + "resendOtp"
        
        static let GETCATEGORIES            = PROVIDER + "category"
        
        static let UPDATESERVICES            = PROVIDER + "services"
        
        static let UPDATESERVICE           = PROVIDER + "service"
        
        static let SERVERTIME               = "server/serverTime"
        //***************Zendesk*********************************//
        
        static let ZENDESK                  = "zendesk/"
        
        static let ZENDESKTickets           = ZENDESK + "ticket"
        
        static let GETZENDESKTickets        =  ZENDESK + "user/" + "ticket/"
        
        static let GETZENDESKTicketHistory  =  ZENDESK  + "ticket/" + "history/"
        
        static let ZENDESKTicketComment     =  ZENDESK  + "ticket/" + "comments"
    }
}


struct SERVER_CONSTANTS{
    static let googleMapsApiKey = "AIzaSyDoFjd8OI-AJrUL5c8ZBKfHvpu9EKklflw"
    static let serverKey        = "AIzaSyDnWr2l90uLTBi-zz_76IlqZcyX2hXx2Sc"
}



struct AMAZONUPLOAD {
    static let APPNAME         = "localGenie/"
    
    static let VEHICLE         = "Vehicles/"
    
    static let PROFILEIMAGE    = APPNAME + "ProfilePics/"
    
    static let VEHICLEIMAGE    = APPNAME + "vehicleImage/"
    
    static let DRIVERLICENCE   = APPNAME + "DriverLincence/"
    
    
    static let SIGNATURE       = APPNAME + "signature/"
    
    
    static let DOCUMENTS       = APPNAME + "completionDoc/"
    
    static let INSURANCE       = VEHICLE + "VehicleDocuments/"
    
    static let DOCUMENTIMAGES  = APPNAME + "VehicleDocuments/"
    
    static let WORKIMAGES    = APPNAME + "providerPreviousWork/"
}

//MARK: user defaults
struct USER_INFO {
    
    static let DID_AGREE_TC = "did_agree_terms&Conditions"
    
    static let COUPON = "coupon"
    
    
    static let USER_EMAIL = "user_email"
    static let USER_DIALCODE = "user_dialcode"
    static let USER_PHONE = "user_phone"
    
    static let SOCKET_SERVER_CHANNEL = "server_channel"
    static let SOCKET_MY_CHANNEL = "my_channel"

    
    
    //***********
    static let SESSION_TOKEN   = "session_token"
    static let PRESENCECHANNEL = "presence_chn"
    static let PUBLISHKEY      = "pub_key"
    static let SUBSCRIBEKEY    = "sub_key"
    static let SERVERCHANNEL   = "server_chn"
    static let DRIVERCHANNEL   = "driver_channel"
    static let DRIVERPUBCHANNEL = "driver_Pub_channel"
    static let VEHTYPEID       = "typeId"
    static let USER_ID         = "user_id"
    static let REFERRAL_CODE   = "Referal_Code"
    static let REQUEST_ID      = "Request_id"
    static let OLDPASSWORD     = "Oldpassword"
    static let USER_NAME       = "user_name"
    static let USERIMAGE       = "pPic"
    static let FIRSTNAME       = "first"
    static let LASTNAME        = "last"
    static let CURRENCYSYMBOL  = "$"
    static let DISTANCE        = "kms"
    static let WEIGHT          = "Kgs"
    static let DEVICE_ID       = "device_id"
    static let PUSH_TOKEN      = "default_push_token"
    static let SAVEDID         = "savedID"
    static let SAVEDPASSWORD   = "password"
    static let SELBID          = "undefined"
    static let SELCHN          = "chn"
    static let BOOKSTATUS      = "4"
    static let PRESENCE        = "presence"
    static let APIINTERVAL     = "5"
    static let BOOKAPIINTERVAL = "10"
    static let DISTANCESTORINGDATA  = "15"
    static let VERSIONMANDATORY     = "mandatoryUpdate"
    static let ServerDriverVersion  = "10"
    static let SESSIONCHECK         = "ordinory"
    static let FCMTOPIC             = "topicFcm"
    
    static let hardLimit           = "hardLimit"
    static let softLimit           = "softLimit"
    static let hardLimitReached    = "hardLimitReached"
    static let softLimitReached    = "softLimitReached"
    static let walletEnabled       = "walletEnabled"
    static let walletAmount        = "walletAmount"
    static let STRIPE_KEY          = "stripe_key"
    static let LAST4               = " Add card"
    static let CARDID              = "card id"
    static let CARDBRAND           = "card brand"
    
    static let WHICHPROVIDERTYPE  = "providerType"
    static let CURRENCYPLACE  = "currencyPreSuff"
    static let TIMEZONETIMEINTERVAL = "TimezoneTimeInterval"
}


//***********************************API REQUEST*******************************************//
struct SIGNUP_REQUEST {
    //Signup
    static let NAME             = "firstName"
    static let LAST_NAME        = "lastName"
    static let EMAIL            = "email"
    static let PASSWORD         = "password"
    static let PHONE_NUMBER     = "mobile"
    static let COUNTRY_CODE     = "countryCode"
    static let LATITUDE         = "latitude"
    static let LONGITUDE        = "longitude"
    static let USER_IMAGE       = "profilePic"
    static let DEVICE_ID        = "deviceId"
    static let DATE_OF_BIRTH    = "dob"
    static let ARTIST_CATEGORY  = "catlist"
    static let DEVICE_TYPE      = "deviceType"
    static let YOUTUBE_LINK     = "link"
    static let PUSH_TOKEN       = "pushToken"
    static let APP_VERSION      = "appVersion"
    static let DEVICE_MAKE      = "deviceMake"
    static let DEVICE_MODEL     = "deviceModel"
    static let CITY_ID          = "cityId"
    static let DEVICE_VERSION   = "deviceOsVersion"
    static let ARTIST_ADDRESS   = "addLine1"
    static let DOCUMENTS        = "document"
    static let TAGGED_ID        = "taggedAs"
}

struct VERIFY_OTP_REQUEST {
    
    static let CODE              = "code"
    static let PROVIDERID        = "providerId"
    static let USERID           = "userId"
    static let USERTYPE         = "userType"
    
}

struct SIGNIN_REQUEST {
    
    static let EMAIL_PHONE       = "mobileOrEmail"
    static let PASSWORD        = "password"
    static let DEVICE_VERSION   = "deviceOsVersion"
    static let APP_VERSION      = "appVersion"
    static let DEVICE_MAKE      = "deviceMake"
    static let DEVICE_MODEL     = "deviceModel"
    static let DEVICE_TYPE      = "deviceType"
    static let PUSH_TOKEN       = "pushToken"
    static let BATTERY_PER      = "batteryPercentage"
    static let DEVICETIME       = "deviceTime"
    static let LOCATION_HEADING = "locationHeading"
    static let DEVICE_ID        = "deviceId"
    
}

struct FORGOTPASSWORD_REQUEST{
    static let EMAIL_PHONE      = "emailOrPhone"
    static let COUNTRY_CODE     = "countryCode"
    static let USER_TYPE        = "userType"
    static let TYPE             = "type"
}

struct VALIDATEEMAIL_PHONE_REQUEST{
    static let EMAIL          = "email"
    static let COUNTRY_CODE   = "countryCode"
    static let MOBILE         = "mobile"
}

//******************************** REQUEST PARAMS ENDS************************************//

struct SERVICE_RESPONSE {
    
    static let Error               = "error"
    static let ErrorFlag           = "errFlag"
    static let ErrorMessage        = "message"
    static let ErrorNumber         = "errNum"
    static let fcmtopic            = "fcmTopic"
    static let cardDetail          = "cardDetail"
    static let DataResponse        = "data"
    static let catArr              = "catArr"
    static let last4               = "last4"
    static let authToken           = "authToken"
    
    
    //MyProfile Service Response
    static let About               = "about"
    static let Dob                 = "dateOfBirth"
    static let Genres              = "preferredGenres"
    static let Photo               = "profilePic"
    static let CountryCode         = "countryCode"
    
    
    //Login & signup Service Response
    static let expireOtp           = "expireOtp"
    static let Sid                 = "sid"
    static let Email               = "email"
    static let PhoneNumber         = "phone"
    static let ReferralCode        = "referralCode"
    static let RefferalText        = "description"
    static let CurrencyCode        = "CurrencyCode"
    static let ProfilePic          = "profilePic"
    static let FirstName           = "firstName"
    static let LastName            = "lastName"
    static let StripeAPIKey        = "PublishableKey"
    static let ZenDeskRequesterID  = "requester_id"
    
    //Token
    static let Token               = "token"
    
    //Music Genres
    static let Genres_Name         = "catName"
    static let Genres_Id           = "id"
    static let Card_Brand          = "brand"
    
    // Reviews
    static let reviews             = "reviews"
    static let reviewCount         = "reviewCount"
    static let averageRating       = "averageRating"
    
    struct categoryData {
        static let cityData     = "cityData"
        static let polygons     = "polygons"
        static let coordinates  = "coordinates"
        static let currencySym  = "currencySymbol"
        static let distanceMat  = "distanceMatrix"  // 0 km, 1 miles
        static let currencyAbr  = "currencyAbbr"
        static let paymentMode  = "paymentMode"
        static let card         = "card"
        static let wallet       = "wallet"
        static let cash         = "cash"
        static let customerFreq =  "customerFrequency"
        static let custLocInt   =  "customerHomePageInterval"
        static let stripeTesK   =  "stripeTestKeys"
        static let publishableK =  "PublishableKey"
        static let recommendA   =  "recommendedArr"
        static let recBannerIA  = "recomdedBannerImageApp"
        static let treBannerIA  = "iconApp"
        static let catID        = "_id"
        static let treRecID     = "categoryId"
        static let trendingArr  = "trendingArr"
        static let catName      = "cat_name"
        static let stripeKeys   = "stripeKeys"
    }
    
    // chat History
    
    struct ChatData {
        static let accepted     = "accepted"
        static let past         = "past"
        static let bookingId    = "bookingId"
        static let firstName    = "firstName"
        static let lastName     = "lastName"
        static let profilePic   = "profilePic"
        static let catName      = "catName"
        static let lastCahtMTS  = "lastCahtMsgTimeStamp"
        static let providerId   = "providerId"
        static let currencySym  = "currencySymbol"
        static let currencyAbbr = "currencyAbbr"
        static let amount       = "amount"
        static let bookingModel = "bookingModel"
        static let callType     = "callType"
    }
    
    
    // Boking History
    
    struct ProviderBookings {
        static let ratingPro        = "rating"
        static let id               = "_id"
        static let jobDuration      = "scheduleTime"
        static let name             = "name"
        static let cancelReason     = "reason"
        static let canResId         = "res_id"
        static let serViceAmount    = "unitPrice"
        static let serviceName      = "serviceName"
        static let checkOutItem     = "serviceNameLang"
        static let aitionalService  = "additionalService"
        static let cart             = "cartData"
        static let ratingLog        = "ratingLog"
        static let queAns           = "questionAndAnswer"
        static let customerRating   = "customerRating"
        static let accounting       = "accounting"
        static let amount           = "amount"
        static let cancellationFee  = "cancellationFee"
        static let discount         = "discount"
        static let last4            = "last4"
        static let paymentMethod    = "paymentMethod"
        static let pgName           = "paymentMethodText"
        static let totalActualHo    = "totalActualHo"
        static let total            = "total"
        static let bookingReqDT     = "bookingRequestedFor"
        static let visitFee         = "visitFee"
        static let travelFee        = "travelFee"
        static let quotedPrice      = "quotedPrice"
        static let addLine1         = "addLine1"
        static let timeStamp        = "timeStamp"
        static let bookingType      = "bookingType"
        static let cancellRes       = "cancellationReason"
        static let currency         = "currency"
        static let distance         = "distance"
        static let distanceMatrix   = "distanceMatrix"
        static let firstName        = "firstName"
        static let gigTime          = "gigTime"
        static let Gigname          = "name"
        static let Gigprice         = "price"
        static let Gigunit          = "unit"
        static let GigQuantity      = "quntity"
        static let lastName         = "lastName"
        static let profilePic       = "profilePic"
        static let bidDescription   = "bidDescription"
        static let phone            = "phone"
        static let status           = "status"
        static let callType         = "callType"
        static let favouritePro     = "favouriteProvider"
        static let statusMsg        = "statusMsg"
        static let signUrl          = "signatureUrl"
        static let past             = "past"
        static let pending          = "pending"
        static let upcoming         = "upcoming"
        static let averageRating    = "averageRating"
        static let currencySymbol   = "currencySymbol"
        static let totalAmount      = "totalAmount"
        static let category         = "category"
        static let bookingId        = "bookingId"
        static let jobDescription   = "jobDescription"
        static let bookingReqFor    = "bookingRequestedFor"
        static let callBEFCustomer  = "callButtonEnableForCustomer"
        static let providerDetail   = "providerDetail"
        static let providerId       = "providerId"
        static let content          = "content"
        static let questName        = "name"
        static let quesAnswer       = "answer"
        static let quesType         = "questionType"
        static let lastMsg          = "lastMsg"
        static let proLocation      = "proLocation"
        static let bidLocation      = "location"
        static let chat             = "chat"
        static let bidDispatchLog   = "bidDispatchLog"
        static let bidProvider      = "bidProvider"
        static let latitude         = "latitude"
        static let longitude        = "longitude"
        static let bookingExpTime   = "bookingExpireTime"
        static let bookingEndtime   = "bookingEndtime"
        static let bookingTimer     = "bookingTimer"
        static let bookingModel     = "bookingModel"
        static let startTimeStamp   = "startTimeStamp"
        static let serverTime       = "serverTime"
        static let providerData     = "providerData"
        static let categoryId       = "categoryId"
        static let categoryName     = "categoryName"
        static let captureAmount    = "captureAmount"
        static let paidByWallet     = "paidByWallet"
        static let remainingAmount  = "remainingAmount"
        static let bidAmount        = "bidAmount"
        static let bidPrice         = "bidPrice"
        static let totalJobTime     = "totalJobTime"
        static let currencyAbbr     = "currencyAbbr"
        static let additionalSFee   = "additionalServiceFee"
        static let totalShiftBoo    = "totalShiftBooking"
    }
    
    // Live Track
    
    struct liveTrack {
        static let latitude         =  "latitude"
        static let longitude        =  "longitude"
        static let proId            =  "pid"
        static let distance         =  "locationHeading"
        static let status           =  "status"
        static let bookingId        =  "bookingId"
    }
    
}

struct COLOR {
    static let APP_COLOR         = UIColor(hexString: "50688B")
    
    static let SCHEDULE         = UIColor(hexString: "9C60DA")
    static let ONGOINGSCHED     = UIColor(hexString: "4ACA59")
    static let MYLISTINGTEXTCOLOR     = UIColor(hexString: "666666")
    static let NAVIGATION_BAR    = UIColor (cgColor: 0xB88955 as! CGColor)
    static let NAVIGATION_TITLE  = WHITE
    static let BACK_BUTTON       = WHITE
    
    
    static let WHITE = UIColor(hexString: "FFFFFF")
    static let BLACK = UIColor.black
    static let GRAY = UIColor.gray
    static let DARK_GRAY = UIColor.darkGray
    static let LIGHT_GRAY = UIColor.lightGray
    static let EBEBEB = UIColor (cgColor: 0xEBEBEB as! CGColor)
    static let F8F8F8 =  UIColor(hexString: "F8F8F8")
    static let E1E1E1 =  UIColor(hexString: "E1E1E1")
    static let receiverColor   = UIColor(hexString: "95D600")
    static let senderColor     = UIColor(hexString: "F0F0F0")
    
    static let cancelDeclinedRed = UIColor(hexString: "F96155")
    
    static let onGoingBlue  = UIColor(hexString: "3185EC")
    
    static let bookingExpiredColor = UIColor(hexString: "EFA174")
    
    static let completedBookingColor = UIColor(hexString: "85DD83")
    
    static let categoryDeselectColor = UIColor(hexString: "3B3B3B")
    
    static let textFieldBorderColor = UIColor(hexString: "CCCCCC")
}

struct DistanceUnits {
    static let Miles = " Miles"
    static let Kms   = " Kms"
}
var ServiceDetailsString = [" Service(s), ",
"To pay: ",
"Consultation fee"]
let hourlyStringFile = "Hourly"
var visitTraveString    = ["Visit Fee",
"Travel Fee",
"Cancellation Fee",
"Discount"]

let paymentTypeConfig = [
"Card",
"Wallet",
"Cash",
 ]
let cardPaymentStringFile = "Card"
enum HelveticaNeue: String {
    
    case HelveticaNeue    = "HelveticaNeue"
    case Italic           = "HelveticaNeue-Italic"
    case Bold             = "HelveticaNeue-Bold"
    case UltraLight       = "HelveticaNeue-UltraLight"
    case CondensedBlack   = "HelveticaNeue-CondensedBlack"
    case BoldItalic       = "HelveticaNeue-BoldItalic"
    case CondensedBold    = "HelveticaNeue-CondensedBold"
    case Medium           = "HelveticaNeue-Medium"
    case Light            = "HelveticaNeue-Light"
    case Thin             = "HelveticaNeue-Thin"
    case ThinItalic       = "HelveticaNeue-ThinItalic"
    case LightItalic      = "HelveticaNeue-LightItalic"
    case UltraLightItalic = "HelveticaNeue-UltraLightItalic"
}

struct FONT {
    static let NormalFont = UIFont(name: HelveticaNeue.Medium.rawValue, size: 15)
    static let NAVIGATION_TITLE = UIFont(name: HelveticaNeue.Medium.rawValue, size: 15)
    static let BACK_BUTTON = UIFont(name: HelveticaNeue.Medium.rawValue, size: 12)
}

//************* IPHONE SIZES*****************//
struct iPHONE {
    //  Device IPHONE
    static let IS_iPHONE_4s: Bool =  (UIScreen.main.bounds.size.height == 480)
    static let IS_iPHONE_5: Bool =  (UIScreen.main.bounds.size.height == 568)
    static let IS_iPHONE_6: Bool =  (UIScreen.main.bounds.size.height == 667)
    static let IS_iPHONE_6_Plus: Bool =  (UIScreen.main.bounds.size.height == 736)
}


enum errorCodes: Int {
    case error400 = 400
    case error401 = 401
    case error402 = 402
    case error403 = 403
    case UserNotFound = 404
    case error405 = 405
    case error406 = 406
    case error407 = 411
    case error408 = 410
    case error409 = 409
    
    case error412 = 412
    case error413 = 413
    case error414 = 414
    case error415 = 415
    case error416 = 416
    case error417 = 417
    case error418 = 418
    case error419 = 419
    case error420 = 420
    
    case error421 = 421
    case error422 = 422
    case error423 = 423
    case error424 = 424
    case error425 = 425
    case error426 = 426
    case error427 = 427
    case error428 = 428
    case error429 = 429
    case TokenExpired = 440
    case UserLoggedOut = 498
    case InterServerError = 500
}

//************* IPHONE SIZES*****************//
struct iPHONEWIDTH {
    //  Device IPHONE
    
    static let IS_iPHONE_5: Bool =  (UIScreen.main.bounds.size.height == 320)
    static let IS_iPHONE_6: Bool =  (UIScreen.main.bounds.size.height == 375)
    static let IS_iPHONE_6_Plus: Bool =  (UIScreen.main.bounds.size.height == 414)
}
let paymentDis = [
"Visiting Fees" ,
"mins Gig",
"Discount",
"Total",
"Wallet",
"Add wallet balance",
"Add new card",
"Pay with cash",
"Service Fee"
]
var ReciptHeaderString = ["Requested Services",
"Additional Charges",
"Payment Method"]


struct NOTIFICATION_NAME {
    
    static let ADD_POST_JOB_BUTTON = "add_post_job_button"
    static let REMOVE_POST_JOB_BUTTON = "remove_post_job_button"
    static let POST_JOB_BUTTON_ACTION = "post_job_button_action"
}


enum RequestType : Int {
    
    case language
    
    case signIn
    case signUp
    case signVerifyOTP
    case fpCMverifyOtp
    case resendOTP
    case verifyMobileNumber
    case verifyReferralCode
    case getRefferal
    case verifyEmail
    case forgetVerifyOTP
    case forgetPassword
    case changePassword
    case updatePassword
    case getUserProfile
    case updateUserProfile
    case logOut
    case changeEmail
    case changeMobile
    
    case config
    
    case updateAppVersion
    case sendOTP
    case signUpOTP
    case getAllApptDetails
    case getAllOngoingApptDetails
    case getParticularApptDetails
    case getInvoiceDetails
    case getChatMessages
    case updateReview
    
    case forgotPasswordVerifyPhoneNumber
    case signOut
    case guestLogin
    case liveBooking
    
    case addCard
    case getAllCards
    case deleteCard
    case defaultCard
    
    case addAddress
    case deleteAddress
    case GetAddress
    case updateAddress
    
    case getFavProv
    case deletFavPro
    
    case checkCoupon
    
    case GetReviews
    case GetSupport
    
    case getCategories
    case providerProfile
    case providerList
    
    case GetSupportDetails
    case ChangeBookingStatus
    case musicGenres
    case refressAccessToken
    
    // Provider Rating
    case providerRating
    case pendingRating
    case bookingPaymentPending
    case invoice
    
    // favourite
    case addFavourite
    
    // Cart
    case getServices
    case getCart
    case updateCart
    
    // chat List
    case getChatList
    
    case getAllProviders
    case acceptBidding
    // Booking
    case booking
    case allBookings
    case bookingDetails
    case businessInvoice
    case lastBookingCharge
    case cancelBooking
    case cancelBookingRes
    case timeSlot
    // zendesk
    case createTicket
    
    // wallet
    case GetWalletDetails
    case RechargeWallet
    case GetWalletTransaction
    
    // Server Time
    case GetServerTime
    
    // Live Chat Api
    case liveChat
    
    // promotion code
    case getPromocode
    case applyPromo
    
    // call
    case callNotificationApi
    
    // add Reminder
    case reminderAdded
    
    // search provider
    case searchProvider
}



struct UIMessages {
    
    static let Alert = "Alert"
    static let OK = "OK"
    static let YES = "YES"
    static let NO = "NO"
    static let Message = "Message"
    static let Error = "Error"
    static let Cancel = "Cancel"
    
    struct SPLASH {
        
        static let WELCOME = "Welcome"
        static let HI = "Hi"
        static let Hi_MESSAGE = "Welcome"
    }
    
    struct Warnings {
        // Contact Sync Message
        static let ContactAccessTitle = "Can't access contact"
        static let ContactAccessMessage = "Please go to Settings -> Trustpals to enable contact permission"
        
        static let CameraSupportMessage = "Your device doesn't support Camera. Please choose other option."
    }
}

struct AppConstants {
    
    
    struct API {
        static let pendingCalls = "pendingCalls"
    }
    
    struct MQTT {
        static let callsAvailability = "CallsAvailability/"
        static let calls  = "Calls/"
    }
    
    struct CallTypes {
        
        static let audioCall = "0"
        static let videoCall = "1"
    }
    
    struct UserDefaults {
        static let userID = "1"
        static let callPushToken = "callPush"
        static let User = "userName"
    }
    
}

struct appDelegetConstant {
    static let incomingViewController = "IncomingCallViewController"
    static let window = UIApplication.shared.keyWindow!
}
