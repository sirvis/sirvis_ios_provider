//
//  MessageNotificationDelegate.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 31/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import CocoaLumberjack


class MQTTDelegate: NSObject, MQTTSessionManagerDelegate {
    
    let mqttHandler = MQTTResponseHandler()
    let mqttChatHandler = MQTTChatResponseHandler()
    let mqttCallManager = MQTTCallManager()
    
    func handleMessage(_ data: Data!, onTopic topic: String!, retained: Bool) {
        do {
            guard let dataObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
            DDLogDebug("Received \(dataObj) on:\(topic) r\(retained)")
            // After getting new message this method will respond.
            
             let state = UIApplication.shared.applicationState
            
            if topic ==  MQTTTopics.CallTopic +  Utility.userId ||
             topic == "Calls/" + Utility.userId ||
             topic ==  Utility.userId  ||
             topic == "Calls/" + Helper.CalledID()  ||
            topic.contains("Optional") && topic.contains("Calls")  ||
            topic.contains("call")
                {
                MQTT.sharedInstance.delegate?.receivedMessage(dataObj, andChannel: topic)
                do {
                    guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
                    if state == .active && state != .background{
                        mqttCallManager.loadTheAudioCallView(data:json)
                    }
                }
                catch let jsonError {
                    print("Error !!!\(jsonError)")
                }
                
            }
            else if topic == "message/" + Utility.userId {
                self.mqttChatHandler.gotChatResponeFromMqtt(responseData: data, topicChannel: topic)
            }else if (topic.range(of: AppConstants.MQTT.calls) != nil ) || (topic.range(of: AppConstants.MQTT.calls) != nil){
                do {
                    guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
                    MQTT.sharedInstance.delegate?.receivedMessage(dataObj, andChannel: topic)
                    // MQTTCallManager.didRecieve(withMessage: json, inTopic: topic)
                } catch let jsonError {
                    print("Error !!!\(jsonError)")
                }
            }
            else{
                self.mqttHandler.gotResponeFromMqtt(responseData: data, topicChannel: topic)
            }
            
        } catch let jsonError {
            DDLogDebug("Error !!!\(jsonError)")
        }
    }
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didDeliverMessage msgID: UInt16) {
        print("Message delivered")
    }
    
    func messageDelivered(_ session: MQTTSession!, msgID: UInt16, topic: String!, data: Data!, qos: MQTTQosLevel, retainFlag: Bool) {
        DDLogDebug( "\(msgID)Message delivered")
        guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
        session.persistence.deleteAllFlows(forClientId: userID)
    }
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didChange newState: MQTTSessionManagerState) {
          let mqtt = MQTT.sharedInstance
        switch newState {
        case .connected:
            print("connected")
          
            if Utility.userId == "user_id" {
                mqtt.isConnected = false
                return
            }
            
            mqtt.isConnected = true
            mqtt.subscribeChannel(withChannelName:  Utility.userId)
            mqtt.subscribeChannel(withChannelName: "booking/" + Utility.userId)
            mqtt.subscribeChannel(withChannelName: "jobStatus/" + Utility.userId)
            mqtt.subscribeChannel(withChannelName: "message/" + Utility.userId)
             mqtt.subscribeChannel(withChannelName: "call/")
             mqtt.subscribeChannel(withChannelName: "Calls/")
             mqtt.subscribeChannel(withChannelName: "Calls/" + Helper.CalledID())
             mqtt.subscribeChannel(withChannelName: "call/" + Helper.CalledID())
            mqtt.subscribeChannel(withChannelName: "call/" + Utility.userId)
             mqtt.subscribeChannel(withChannelName: "Calls/" + Utility.userId)
            
            
            
           // guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
            MQTT.sharedInstance.subscribeTopic(withTopicName: AppConstants.MQTT.calls + Utility.userId, withDelivering: .atLeastOnce)
            
        case .closed:
            mqtt.isConnected = false
            DDLogDebug("disconnected")
            
          
            
        case .error: print("error \(sessionManager.lastErrorCode)")
            
        default:
            DDLogDebug("disconnected")
        }
    }
}

//when i get the call

//MQTT.sharedInstance.delegate?.receivedMessage(mqttData, andChannel: topic)
//if topic ==  (MQTTConstants.userID as! String){  //userid
//    if state == .active && state != .background{
//        MQTTCallManager.loadTheAudioCallView(data:mqttData)
//    }
//}
